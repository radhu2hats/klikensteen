/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
		 // Setup form validation on the #register-form element
		   jQuery("#commentform").validate({
			
				// Specify the validation rules
				rules: {
				   rating:{
						required:true,
						number:true,
						max:10,
					 }
				},
				
				// Specify the validation error messages
				messages: {
					rating:{
						required:"Please enter Review rating.",
						number:"Please enter only number value",
						max:'You can give your review point between 1 to 10.'
					}
					
				},
				
				submitHandler: function(form) {
					form.submit();
				}
			});
        
        // Isotope home page blog section
        jQuery('.grid').isotope({
            itemSelector: '.grid-item'
        });
        
        // Equal height
        function equalHeightExpertOrg(){
            if(jQuery('.banner_section .banner_image .banner_image_text').length){
                if(jQuery(window).width() < 768){
                    if(!jQuery('.banner_section .banner_image_text_mobile').length){
                        jQuery('.banner_image_text_mobile').html('');
                        var bannerContent = '';
                        bannerContent = jQuery('.banner_section .banner_image .banner_image_text').html();
                        if(jQuery('.banner_section .banner_image .author_name').length){
                            var authorName = '';
                            authorName = jQuery('.banner_section .banner_image .author_name').html();
                            jQuery('<div class="banner_image_text_mobile">'+bannerContent+'<div class="author_name">'+authorName+'</div></div>').insertAfter('.banner_section .banner_image');
                        }else{
                            jQuery('<div class="banner_image_text_mobile">'+bannerContent+'</div>').insertAfter('.banner_section .banner_image');
                        }
                    }
                }
            }
            if(jQuery('.resulted-grid').length){
                jQuery('.resulted-grid').find('ul.products').addClass('grid');
            }
            if(jQuery('.banner_section .banner_color .banner_equal').length){
                var bannerHeight = jQuery('.banner_section .banner_color .banner_equal:first-child').height();
                if(bannerHeight > 183){
                    jQuery('.banner_section .banner_color .banner_equal').css('height',bannerHeight+'px');
                }else{
                    jQuery('.banner_section .banner_color .banner_equal').css('height','185px');
                }
            }
            jQuery('.products_wrapper .products.grid .training_block .white_bg_block').matchHeight();
            jQuery('.choose_training_area .training_block .white_bg_block').matchHeight();
            jQuery('.stimulansz_experts_organization .white_bg_block').matchHeight();
            jQuery('.stimulansz_experts_organization .white_bg_block h4').matchHeight();
            jQuery('.stimulansz_experts_organization .white_bg_block .white_bg_block_container').matchHeight();
            if(jQuery(window).width() > 767){
                var imageBlockHeight = jQuery('.stimulansz_experts_organization .white_bg_block.padding-bottom .equal-block').height();
                setTimeout(function(){ jQuery('.stimulansz_experts_organization .white_bg_block.padding-bottom .equal-block').css('height',(imageBlockHeight+18)+'px'); }, 1000);
            }
        }
        equalHeightExpertOrg();
        
        // Mobile nav
        function mobileNav(){
            if (jQuery(window).width() < 768) {
                if (!jQuery("#panel").length){
                    jQuery('#masterhead').nextUntil('#masterfooter').next('#masterfooter').andSelf().wrapAll( '<main id="panel" class="panel slideout-panel"></main>');
                }
                if (!jQuery(".mobile-nav").length){
                    var navHtml = '';
                    navHtml = jQuery("#menu-header-menu-bar").html() + jQuery("#top_menu_header_wrapper").html();
                    jQuery(jQuery("#menu")).append("<ul class='mobile-nav'>"+navHtml+"</ul>");
                }
                
                var slideout = new Slideout({
                    'panel': document.getElementById('panel'),
                    'menu': document.getElementById('menu'),
                    'side': 'right'
                  });
                document.querySelector('.js-slideout-toggle').addEventListener('click', function() {
                    jQuery(this).toggleClass('open');
                    slideout.toggle();
                });
                document.querySelector('.menu').addEventListener('click', function() {
                    jQuery('.js-slideout-toggle').toggleClass('open');
                    slideout.close(); 
                });
            }else{
                /* remove "panel" div for desktop devices */
                var cnt = jQuery("#panel").contents();
                jQuery("#panel").replaceWith(cnt);
            }
        }
        mobileNav();
                
        // Page load
        jQuery(window).load(function() {
            equalHeightExpertOrg();
        });

        // Page resize
        jQuery(window).resize(function(){
            equalHeightExpertOrg();
        });
        
        // academy and product page after filter
         jQuery(document).on('wpsolr_on_ajax_success', function() { 
           // remove the old group
//            jQuery('.products_wrapper .products.grid .training_block .white_bg_block').matchHeight({ remove: true });
//            jQuery('.products_wrapper .products.grid .training_block .white_bg_block').matchHeight();
            // apply matchHeight on the new selection, which includes the new element
            jQuery('.choose_training_area .training_block .white_bg_block').matchHeight({ remove: true });
            jQuery('.choose_training_area .training_block .white_bg_block').matchHeight();
	}); 
        
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');
	
		
      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);
 

})(jQuery); // Fully reference jQuery after this point.
