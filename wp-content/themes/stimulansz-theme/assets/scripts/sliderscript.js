/*  Slider script*/
var $slider = $('.slider-innermaincont ul');
var $progressBar = $('.progress');
var $progressBarLabel = $('.slider__label');

$slider.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
    var calc = ((nextSlide) / (slick.slideCount - 1)) * 100;

    $progressBar
        .css('background-size', calc + '% 100%')
        .attr('aria-valuenow', calc);

    $progressBarLabel.text(calc + '% completed');
});

$slider.slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    speed: 400
}); 
/*  Slider script*/
alert('hello dear');


$('.slider-innermaincont').append(' <div class="progress" role="progressbar" aria-valuemin="0" aria-valuemax="100"><span  class="slider__label sr-only"></span> </div>   ');