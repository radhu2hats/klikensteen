jQuery(function($){
	jQuery('.stimulansz_loadmore').click(function(){
        var userid= jQuery('.stimulansz_loadmore').attr('author_id');
		var max_page= jQuery('.stimulansz_loadmore').attr('max_page');
		
		var button = jQuery(this),
		    data = {
			'action': 'loadmore',
			//'query': stimulansz_loadmore_params.posts, // that's how we get params from wp_localize_script() function
			'page' : stimulansz_loadmore_params.current_page,
		    'author_id' : userid,
		};
 
		jQuery.ajax({
			url : stimulansz_loadmore_params.ajaxurl, // AJAX handler
			data : data,
			type : 'POST',
			beforeSend : function ( xhr ) {
				button.text('laden...'); // change the button text, you can also add a preloader image
			},
			success : function( data ){
				if( data ) { 
					button.text( 'Meer blogartikelen' ).prev().before(data); // insert new posts
					stimulansz_loadmore_params.current_page++;
 
					if ( stimulansz_loadmore_params.current_page == $.trim(max_page)) 
						button.remove(); // if last page, remove the button
				} else {
					button.remove(); // if no data, remove the button as well
				}
			}
		});
	});
});