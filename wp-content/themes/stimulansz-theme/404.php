<?php get_template_part('templates/page', 'header'); ?>
 <div class="fixed-container-wrapper">
    <div class="fixed-container container">
        <section class="training_content_section">
            <div class="row">
                <div class="col-xs-12">
                    <div class="white_bg_block">
                    <div class="alert alert-warning">
                        <?php _e('Sorry, but the page you were trying to view does not exist.', 'sage'); ?>
                    </div>
                    <?php get_search_form(); ?>
                </div>
             </div>       
            </div>
        </section>
    </div>
 </div>   

