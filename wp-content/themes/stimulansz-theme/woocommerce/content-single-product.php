<?php

/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
?>

<?php
/**
 * woocommerce_before_single_product hook.
 *
 * @hooked wc_print_notices - 10
 */
do_action('woocommerce_before_single_product');

if (post_password_required()) {
    echo get_the_password_form();
    return;
}
?>
<?php
$banner_image = get_field('banner_image', get_the_id());
$stimulansz_video = '';
if (get_field('video_url', get_the_ID())) :
    $stimulansz_video = 'stimulansz_video';
endif;
?>
<?php
// Filter call.
$feedback_rating = apply_filters('stimulanz_get_average_preview', get_the_ID());
?>
<!-- activity banner section -->
<section class="banner_section content_only activity_banner_section <?php echo $stimulansz_video; ?>">
    <div class="container">
        <div class="banner_color">
            <div class="row">
                <div class="col-md-9 col-sm-7 col-xs-12 banner_equal">
                    <div class="banner_color_text">
                        <h1><?php echo get_the_title(get_the_id()); ?></h1>
                        <div class="desc">
                            <?php echo get_field('banner_text', get_the_id()); ?>
                        </div>

                        <?php
                        if (isset($feedback_rating['average'])) :
                        ?>
                            <div class="rating_section">
                                <span><?php _e('Waardering', 'stimulansz'); ?></span><span class="reviews"><?php
                                                                                                            if (isset($feedback_rating['average'])) {
                                                                                                                echo $feedback_rating['average'];
                                                                                                            }
                                                                                                            ?></span>
                            </div>
                        <?php endif;
                        ?>
                    </div>
                </div>
                <div class="col-md-3 col-sm-5 col-xs-12 banner_equal">
                    <div class="banner_video">
                        <div class="banner_video_container">
                            <a href="#"><?php
                                        if (has_post_thumbnail()) {
                                            the_post_thumbnail('full', ['class' => 'img-responsive responsive-full', 'data-toggle' => 'modal', 'data-target' => '#demo-2', 'title' => get_the_title()]); ?></a>
                        <?php    } else {
                                            $url = get_field('video_url', get_the_ID());
                                            $class = '';
                                            if (strpos($url, 'youtube') !== false) {
                                                $video_url = get_field('video_url', get_the_ID()) . '/?controls=0&showinfo=0&rel=0&autoplay=1&loop=1"';
                                            } else {
                                                $video_url = get_field('video_url', get_the_ID());
                                            }
                        ?>
                            <?php
                                            if (get_field('video_url', get_the_ID())) :
                                                $class = 'available_video';
                            ?>
                                <iframe src="<?php echo $video_url; ?>" frameborder="0" allowfullscreen></iframe>
                        <?php endif;
                                        }
                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- .activity banner section -->

<!-- container -->
<div class="fixed-container-wrapper <?php echo $class; ?>">
    <div class="fixed-container container">
        <section class="training_content_section activity_content_section">
            <div class="row column-top-xs">
                <div class="col-md-3 col-sm-5 col-xs-12 mobile-column visible-xs">
                    <?php $gallery = get_field('custom_product_gallery', $post->ID);
                    if ($gallery) {
                        $data = '#demo-3'; ?>
                        <div class="white_bg_block sharing_block info">


                            <h3>Meer informatie</h3>


                            <a href="#" data-toggle="modal" data-target="<?php echo $data; ?>" class="underline">Klik hier</a>
                            <p>voor meer informatie over </p>
                            <div class="white_bg_block_container">

                                <h4><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>

                            </div>

                        </div>
                    <?php
                    } ?>
                    <div class="white_bg_block orange_bg">
                        <div class="vc_general vc_cta3 blue-cta small-cta vc_cta3-style-classic vc_cta3-shape-rounded vc_cta3-align-left vc_cta3-color-classic vc_cta3-icon-size-md vc_cta3-actions-bottom">
                            <div class="white_bg_block_container">
                                <div class="training_blog_points traning_schedule ">
                                    <?php
                                    $price = get_post_meta(get_the_ID(), '_regular_price', true);
                                    $currency = get_woocommerce_currency_symbol();
                                    if (isset($price) && !empty($price)) :?>
                                      <h3><?php  echo $currency . ' ' . $price;?></h3>
                                      <?php
                                    endif;
                                    ?>
                                </div>
                                <div class="stimulansz_single_shop_cart">
                                    <?php do_action('woocommerce_single_product_summary'); ?>
                                </div>

                            </div>
                        </div>
                    </div>
                    <?php get_template_part('templates/content', 'sidebar-block') ?>
                    <div class="wpb_widgetised_column wpb_content_element social-media-aside">
                        <div class="wpb_wrapper">
                            <section class="widget a2a_share_save_widget-2 widget_a2a_share_save_widget details-social-icons">
                                <h3><?php _e('Deel dit product', 'stimulansz'); ?></h3>
                                <?php
                                //addtoany social share
                                echo do_shortcode('[addtoany]');
                                ?>
                            </section>
                        </div>
                    </div>
                </div>
                <div class="col-md-9 col-sm-7 col-xs-12 left_section">

                    <?php
                    // check if the repeater field has rows of data
                    if (!empty(get_field('usp_section_title')) || !empty(get_field('basistraining_point'))) :
                    ?>
                        <div class="white_bg_block">
                            <div class="white_bg_block_container">
                                <h4 class="green"><?php echo get_field('usp_section_title'); ?></h4>
                                <div class="learning_points">
                                    <?php echo get_field('basistraining_point'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="divide_green_seprator"></div>
                    <?php endif; ?>

                    <div class="white_bg_block stimulansz_vc_blue_header">
                        <div class="white_bg_block_container">
                            <div id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
                                <?php
                                the_content();
                                /**
                                 * woocommerce_before_single_product_summary hook.
                                 *
                                 * @hooked woocommerce_show_product_sale_flash - 10
                                 * @hooked woocommerce_show_product_images - 20
                                 */
                                //do_action( 'woocommerce_before_single_product_summary' );
                                ?>

                                <div class="summary entry-summary">

                                    <?php
                                    /**
                                     * woocommerce_single_product_summary hook.
                                     *
                                     * @hooked woocommerce_template_single_title - 5
                                     * @hooked woocommerce_template_single_rating - 10
                                     * @hooked woocommerce_template_single_price - 10
                                     * @hooked woocommerce_template_single_excerpt - 20
                                     * @hooked woocommerce_template_single_add_to_cart - 30
                                     * @hooked woocommerce_template_single_meta - 40
                                     * @hooked woocommerce_template_single_sharing - 50
                                     * @hooked WC_Structured_Data::generate_product_data() - 60
                                     */
                                    // do_action( 'woocommerce_single_product_summary' );
                                    ?>

                                </div><!-- .summary -->


                            </div>
                        </div>
                    </div>
                    <?php
                    /**
                     * stimulanz instructieurs
                     * @hook stimulanz_instructieurs
                     * 
                     */
                    do_action('stimulanz_instructieurs');
                    ?>

                    <div class="choose_training_area">
                        <div class="product_listing_section grid-item resulted-grid">
                            <?php
                            /**
                             * @Custom Hooks for display upsell product
                             */
                            //do_action('stimulanz_single_product_summary');
                            do_action('stimulanz_custom_upsell_article', 'product_cat');
                            ?>


                        </div>
                    </div>
                </div>
                <div class="sidebar-blue-fixblocksz wpb_column vc_column_container vc_col-sm-3 blue-sidebar desktop-column">
                    <?php get_template_part('templates/content', 'sidebar-block') ?>
                    <?php $gallery = get_field('custom_product_gallery', $post->ID);
                    if ($gallery) {
                        $data = '#demo-3'; ?>
                        <div class="white_bg_block sharing_block info">

                            <h3>Meer informatie</h3>


                            <a href="#" data-toggle="modal" data-target="<?php echo $data; ?>" class="underline">Klik hier</a>
                            <p>voor meer informatie over </p>

                            <div class="white_bg_block_container">

                                <h4><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>

                            </div>


                        </div>
                    <?php
                    } ?>

                    <div class="white_bg_block orange_bg">
                        <div class="vc_general vc_cta3 blue-cta small-cta vc_cta3-style-classic vc_cta3-shape-rounded vc_cta3-align-left vc_cta3-color-classic vc_cta3-icon-size-md vc_cta3-actions-bottom">
                            <div class="white_bg_block_container sidebar-blue-fixblocksz wpb_column vc_column_container blue-sidebar">
                                <div class="training_blog_points traning_schedule ">
                                    <?php
                                    $price = get_post_meta(get_the_ID(), '_regular_price', true);
                                    $currency = get_woocommerce_currency_symbol();
                                    if (isset($price) && !empty($price)) :?>
                                        <h3><?php  echo $currency . ' ' . $price;?></h3>
                                        <?php
                                      endif;
                                      ?></div>
                                <div class="stimulansz_single_shop_cart">

                                    <?php do_action('woocommerce_single_product_summary'); ?>


                                </div>

                            </div>
                        </div>
                    </div>

                    <?php //get_template_part('templates/content','sidebar-block') 
                    ?>

                    <div class="wpb_widgetised_column wpb_content_element social-media-aside">
                        <div class="wpb_wrapper">
                            <section class="widget a2a_share_save_widget-2 widget_a2a_share_save_widget details-social-icons">
                                <h3><?php _e('Deel dit product', 'stimulansz'); ?></h3>
                                <?php
                                //addtoany social share
                                echo do_shortcode('[addtoany]');
                                ?>
                            </section>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
</div>
<?php do_action('woocommerce_after_single_product'); ?>