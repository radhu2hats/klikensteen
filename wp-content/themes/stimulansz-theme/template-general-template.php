<?php
/**
 * Template Name: General Template
 */
?>

<div class="page-template">
  <?php the_content(); ?>
</div>

<script>
  jQuery( ".counter" ).each(function( gi ) {
    jQuery( this ).find('.vc_tta-title-text').each(function( ci ) {
      var count = ci + 1;
      jQuery( this ).prepend('<span>'+count+'. </span>');
    });
  });
</script>