<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <meta name=“msvalidate.01” content=“8E6E76A78E27B8920CD8AD17E2F18268” />
  <!--<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/jquery.mCustomScrollbar.css" />-->
      <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/jquery.scrollbar.css" />
    <link rel="stylesheet" href="https://www.jqueryscript.net/demo/Fully-Responsive-Flexible-jQuery-Carousel-Plugin-slick/css/slick.css" />
	<link rel="preload" href="/wp-content/plugins/easy-social-icons/css/font-awesome/webfonts/fa-brands-400.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="/wp-content/themes/stimulansz-theme/assets/fonts/OpenSans.woff" as="font" type="font/woff" crossorigin>
	<link rel="preload" href="/wp-content/themes/stimulansz-theme/assets/fonts/Open%20Sans.woff2" as="font" type="font/woff2" crossorigin>
	<link rel="preload" href="/wp-content/themes/stimulansz-theme/assets/fonts/Open%20Sans%20Semibold.woff2" as="font" type="font/woff2" crossorigin>
	<link rel="preload" href="/wp-content/themes/stimulansz-theme/assets/fonts/Open%20Sans%20Light.woff2" as="font" type="font/woff2" crossorigin>
	<link rel="preload" href="/wp-content/themes/stimulansz-theme/dist/fonts/fontawesome-webfont.woff2?v=4.7.0" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="/wp-content/themes/stimulansz-theme/assets/fonts/PMN%20Caecilia%2055%20Roman.woff2" as="font" type="font/woff2" crossorigin> 
       



  <?php wp_head(); ?>
  <!-- Hotjar Tracking Code for www.stimulansz.nl -->
    <script>
     (function(h,o,t,j,a,r){
     h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
     h._hjSettings={hjid:820469,hjsv:6};
     a=o.getElementsByTagName('head')[0];
     r=o.createElement('script');r.async=1;
     r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
     a.appendChild(r);
     })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
    <script>
    
    jQuery(document).ready(function() {
    if (jQuery(window).width() <= 767) { 
        jQuery('.wdm_ul ul').hide();
        jQuery('.wpsolr_facet_title').click(function() {
           jQuery(this).toggleClass("minusicon");
               jQuery(this).next('ul').toggle();
            });       
    }
    });
</script>
</head>
