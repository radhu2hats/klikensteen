<?php
$plugin_dir = ABSPATH . 'wp-content/plugins/wpsolr-pro/wpsolr/core/ajax_solr_services.php';
require_once($plugin_dir);

/**
 * Custom object array
 * which is define in wpslor plugin in wp-content/plugins/wpsolr-pro/wpsolr/core/ajax_solr_services.php
 */
$obj = stimulansz_getObj();
if (empty($_GET['wpsolr_fq'])) {
    //Default Load results
    $obj->set_filter_query_fields(array('0' => "type:kennisbanken"));
    $obj->set_wpsolr_sort("sort_by_date_desc");
} else {
    //Get Query string object value
    $cate = $_GET['wpsolr_fq']['0'];
    if (isset($cate)):
        $cat_explode = explode(':', $cate);
    else:
        $cat_explode = '';
    endif;
    if (empty($cat_explode[1])) {
       $obj->set_filter_query_fields(array('0' => "type:kennisbanken"));
   } else {
    $obj->set_filter_query_fields($_GET['wpsolr_fq']);
}
}
?>
<?php
$author = get_the_author(get_the_id());
$banner_image = get_field('page_banner', get_the_id());
$banner_button_on = get_field('banner_button_on', get_the_id());
$banner_button_text = get_field('banner_button_text', get_the_id());
$banner_button_link = get_field('banner_button_link', get_the_id());
?>
<!-- training listing banner section -->
<section class="banner_section training_listing_banner_section">
<?php the_content(); ?>
   </section>
   <!-- .training listing banner section -->

   <!-- container -->
   <div class="fixed-container-wrapper stimulansz-overviewpage">
    <div class="fixed-container container">
        <section class="training_content_section training_listing_content_section">
            <div class="row">
               <?php
               if (isset($obj)) {
                stimulansz_custom_search_indexed_data($obj);
            }
            ?>
        </div>
    </section>
</div>
</div>

<!-- .container -->