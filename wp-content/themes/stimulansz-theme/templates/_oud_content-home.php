<div class="fixed-container-wrapper">
    <div class="fixed-container container">
        <section class="training_content_section">
            <!-- blog and news section -->
            <section class="blogs_news_section">
                <div class="row">
                    <div class="col-md-8 col-sm-12 col-xs-12 no-padding-xs">
                        <div class="blogs_wrapper grid">
                           <?php 
                           $args = array(
                                'post_type' => 'post',
                                'posts_per_page' => 3,
                                'post_status ' =>'publish',
                                'order'   => 'DESC',
                                'orderby' =>'post_date'
                               );
                    $news_query = new \ WP_Query( $args );

                   if ( $news_query->have_posts() ) : while ( $news_query->have_posts() ) : $news_query->the_post();
            ?> 

                           <div class="col-sm-6 col-xs-12 grid-item">
                                <div class="white_bg_block banner_block">
                                        <?php do_action('stimulanz_categoryname',get_the_ID(),'actueel'); ?>
                                     <?php if (has_post_thumbnail()): ?>
                                            <div class="banner_image"> 
                                                <a href="<?php the_permalink(); ?>">
                                                     <?php the_post_thumbnail('stimulansz_block_image', ['class' => 'img-responsive responsive-full', 'title' => get_the_title()]); ?>                                           
                                                </a>
                                            </div> 
                                    <?php endif; ?> 
                                    
                                    <div class="banner_content">
                                       
                                       <a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4></a>
                                        <div class="white_bg_block_container">
                                            <div class="block_content">
                                                <div class="each_block">
                                                    <div class="date">
                                                        <?php $dateStart = date_i18n("j F Y", strtotime(get_post_time('c', true))); ?>
                                                       <span class="icon dddd"><i class="fa fa-calendar"></i></span>
                                 <span class="author_dates"><?php the_time('j F Y'); ?></span>
                                         
                            
                                                        <p class="byline author vcard">
                                                                    <?php
                                                                            $author_post_id =get_field('contactpersoon_for_blog',get_the_id());
                                                                            $post_title = get_the_title($author_post_id);
                                                                                 if ($author_post_id) {
                                                                                     echo '<span class="icon"><i class="fa fa-user"></i></span>';
                                                                                     echo '<span class="blog-author">'.__('Door ', 'stimulansz').'<a href="'.get_permalink($author_post_id).'" rel="author">'.$post_title.'</a></span>';
                                                                                 }
                                                                        ?>
                                                                   </p>
                                                        
                                                    </div>
                                                    <div class="title">
                                                        <?php 
                                                            $intro = get_field('additional_banner_text');
                                                            if(isset($intro) && !empty($intro)):
                                                                echo $intro.' <a class="stimulansz-home-more-link" href="' . get_permalink() . '">' . esc_html__( 'Read more', 'stimulansz' ) . '</a>';
                                                            else:
                                                                the_excerpt();
                                                            endif;
                                                        ?>
                                                   </div>
                                                </div>
                                                <a href="<?php the_permalink(); ?>" class="blue-btn"><?php echo _e('Lees het artikel','stimulansz'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                             <?php 
                             endwhile; 
                                wp_reset_postdata();
                            else : ?>
                                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                        <?php endif; ?>
                            <!-- Training block -->  
                             <?php 
                           $traing_args = array(
                                'post_type' => 'training',
                                'posts_per_page' => 1,
                                'post_status ' =>'publish',
                                'order'   => 'DESC',
                                'orderby' =>'post_date'
                               );
                    $training_query = new \ WP_Query( $traing_args );
                   
                      
                    
                   if ( $training_query->have_posts() ) : while ( $training_query->have_posts() ) : $training_query->the_post();
                    $post_id = get_the_ID();
                     $current_date = date('Y-m-d');
                     $event_query ="SELECT post_id, MIN(meta_value) as meta_dt FROM st_postmeta WHERE post_id IN ($post_id) and (meta_key like 'event_dates_%_event_start_date' 
                       and DATE(meta_value) >='$current_date' ) GROUP BY post_id ORDER BY DATE(meta_value) ASC LIMIT 6";
                      $all_sorted_events = $wpdb->get_results($event_query);
                   ?> 
                            <div class="col-sm-6 col-xs-12 grid-item">
                                <div class="white_bg_block banner_block">
                                          <?php do_action('stimulanz_categoryname',get_the_ID(),'training_cat'); ?>
                                       <!-- <i class="fa fa-address-card" aria-hidden="true"></i> -->
                                    <?php if (has_post_thumbnail()): ?>
                                    <div class="banner_image">
                                        <a href="<?php the_permalink(); ?>">
                                             <?php the_post_thumbnail('stimulansz_block_image', ['class' => 'img-responsive responsive-full', 'title' => get_the_title()]); ?>                                           
                                        </a>
                                        
                                    </div>   
                                    <?php endif; ?> 
                                    
                                <div class="banner_content">
                                   
                                    <a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4></a>
                                    <div class="white_bg_block_container">
                                        <div class="block_content">
                                            <div class="each_block">
                                                <div class="date">
                                                    <?php 
                                                    if(isset($all_sorted_events[0]->meta_dt)):
                                                       // echo date("j F Y",strtotime($all_sorted_events[0]->meta_dt));
                                                        $dateStart = date_i18n("j F Y", strtotime($all_sorted_events[0]->meta_dt));
                                                    echo $dateStart;
                                                    else:
                                                  endif;
                                                ?>
                                                   
                                                </div>
                                                
                                                <div class="title">
                                                    <?php 
                                                            $intro_training = get_field('banner_text');
                                                            if(isset($intro_training) && !empty($intro_training)):
                                                                echo $intro_training.' <a class="stimulansz-home-more-link" href="' . get_permalink() . '">'. esc_html__('Read more', 'stimulansz' ) . '</a>';
                                                            else:
                                                                the_excerpt();
                                                            endif;
                                                        ?>
                                                    
                                                 </div>
                                            </div>
                                            <a href="<?php echo get_permalink(); ?><?php //echo get_field('more_training_page_link','option'); ?>" class="blue-btn"><?php _e('Bekijk de activiteit','stimulansz'); ?></a>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                       
                    
                     <?php 
                             endwhile; 
                                wp_reset_postdata();
                            else : ?>

                        <?php endif; ?>
                    </div>
                </div>
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        
                        <div class="white_bg_block">
                            <div class="white_bg_block_container">
                                <div class="news_block">
                                <script type="text/javascript">(function() {
                                if (!window.mc4wp) {
                                        window.mc4wp = {
                                                listeners: [],
                                                forms    : {
                                                        on: function (event, callback) {
                                                                window.mc4wp.listeners.push({
                                                                        event   : event,
                                                                        callback: callback
                                                                });
                                                        }
                                                }
                                        }
                                    }
                                })();
                                </script>
                                <?php echo do_shortcode('[mc4wp_form id="70"]'); ?>
                                </div>
                            </div>
                        </div>
                        
                        <div class="white_bg_block">
                            <div class="box-top-right-icon">
                                <i class="fa fa-calendar" aria-hidden="true"></i>
                            </div>
                            
                            <?php echo apply_filters('stimulanz_traing_agenda',''); ?>  
                        </div>
                        
                        
                        <div class="white_bg_block">
                            <h4><?php _e('Themapagina\'s','stimulansz'); ?></h4>
                            <div class="white_bg_block_container">
                                <div class="block_content">
                                    <?php
                             if (has_nav_menu('theme_paginas')) :
                                 wp_nav_menu(array(
                                     'theme_location' => 'theme_paginas',
                                     'container_class' => '',
                                     'menu_class' => 'list-inline theme_pages',
                                     'menu_id' => 'themepages_menu',
                                     'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                         )
                                 );
                             endif;
                             ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- .blog and news section -->

            <!-- experts and organization  -->
            <section class="stimulansz_experts_organization">
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <a href="<?php echo get_field('experts_block_link',get_the_ID()); ?>">
                            <div class="white_bg_block padding-bottom">
                                <h4><?php echo get_field('stimulansz_experts_text', get_the_ID()); ?> </h4>
                                <div class="white_bg_block_container equal-block">
                                    <?php $banner_image = get_field('stimulansz_experts_image', get_the_ID());  ?>
                                    <div class="block_image" style="background: url(<?php if (isset($banner_image['url'])) {
                                        echo $banner_image['url'];
                                    } ?>) no-repeat;background-size: cover;"></div>
                                </div>
                            </div>
                    </a>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="white_bg_block org-logos">
                            <h4><?php echo get_field('stimulansz_organisatie_title', get_the_ID()); ?></h4>
                            <div class="white_bg_block_container equal-block">
                                <div class="block_content">
                                    <?php if (have_rows('stimulansz_organisatie')): ?>
                                        <?php
                                        while (have_rows('stimulansz_organisatie')): the_row();
                                            // Company logo image
                                            $company_logo = get_sub_field('company_logo');
                                            $image_url = get_sub_field('company_url');
                                            ?>
                                            <div class="each_org_icon">
                                                <a href="<?php echo $image_url; ?>" target="_blank"><img src="<?php echo $company_logo['url']; ?>" alt="<?php echo $company_logo['alt']; ?>" class="img-responsive"/></a> 
                                            </div>
                  <?php endwhile; ?>
                <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>
            <!-- .experts and organization -->
        </section>
    </div>
</div>

<?php //wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>