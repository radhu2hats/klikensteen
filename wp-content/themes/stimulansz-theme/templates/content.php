<!--<article <?php //post_class('stimulansz-overviewpage'); ?>>-->
    <div class="col-md-4 col-sm-6 col-xs-12 training_block">
        <div class="white_bg_block banner_block">
            <?php do_action('stimulanz_categoryname', get_the_ID(), 'nieuws'); ?>
           
            <?php
                if (has_post_thumbnail()) {
                    echo '<div class="banner_image">';
                       echo '<a href="'.get_permalink().'">';  
                                                the_post_thumbnail('stimulansz_related_post_image', ['class' => 'img-responsive responsive-full', 'title' => get_the_title()]);
                    echo '</a></div>';
                    }
                                             
             ?>
                
            
            <div class="banner_content">
                <header>
                    <a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4></a>
                     <?php 
                    if ( 'begrippenlijst' != get_post_type() ){
                        ?>
                        <?php get_template_part('templates/entry-meta'); ?>
                        <?
                    }
                    ?>
                </header>
                <div class="white_bg_block_container">
                    <div class="training_blog_points">
                        <div class="training_blog_points">
                       <?php 
                            $intro = get_field('additional_banner_text');
                                if(isset($intro) && !empty($intro)):
                                    echo $intro.' <a class="stimulansz-home-more-link" href="' . get_permalink() . '">' . esc_html__( 'Read more', 'stimulansz' ) . '</a>';
                                else:
                                    the_excerpt();
                                endif;
                        ?>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!--</article>-->