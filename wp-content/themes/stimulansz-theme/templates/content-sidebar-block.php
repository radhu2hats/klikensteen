<?php
global $post;
$sticky_block_on_off = get_post_meta($post->ID, 'sticky_on_off', true);
$sidebar_block_on_off = get_post_meta($post->ID, 'sidebar_block_on/off', true);
$sidebar_title_alignment = get_post_meta($post->ID, 'sidebar_title_alignment', true);
$sidebar_subtitle_alignment = get_post_meta($post->ID, 'sidebar_subtitle_alignment', true);
$sidebar_content_alignment = get_post_meta($post->ID, 'sidebar_content_alignment', true);
$sidebar_title = get_post_meta($post->ID, 'sidebar_title', true);
$sidebar_subtitle = get_post_meta($post->ID, 'sidebar_subtitle', true);
$sidebar_content = get_post_meta($post->ID, 'sidebar_content', true);
$sidebar_button_text = get_post_meta($post->ID, 'sidebar_button_text', true);
$sidebar_button_link = get_post_meta($post->ID, 'sidebar_button_link', true);
$title_for_orange_section_button = get_post_meta($post->ID, 'title_for_orange_section_button', true);
$buttonid = get_post_meta($post->ID, 'sidebar_button_id', true);

if ($sidebar_block_on_off == 1) {
?>
	<div class=" <?php if ($title_for_orange_section_button) {
						echo 'test';
					} else {
						echo 'no-orange-section1';
					} ?>">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">
				<section class="vc_cta3-container">
					<div class="vc_general vc_cta3 blue-cta small-cta vc_cta3-style-classic vc_cta3-shape-rounded vc_cta3-align-left vc_cta3-color-classic vc_cta3-icon-size-md vc_cta3-actions-bottom">
						<div class="vc_cta3_content-container">
							<div class="vc_cta3-content">
								<header class="vc_cta3-content-header">
									<h2 style="text-align:<?php echo $sidebar_title_alignment; ?>">
										<?php echo $sidebar_title; ?>
									</h2>
								</header>
								<h3 style="text-align:<?php echo $sidebar_subtitle_alignment; ?>">
									<?php echo $sidebar_subtitle; ?>
								</h3>
								<p style="text-align:<?php echo $sidebar_content_alignment; ?>">
									<?php echo $sidebar_content; ?>
								</p>
							</div>
							<div class="vc_cta3-actions">
							<div id="sticky-button-sidebar" class="vc_btn3-container  btn-white vc_btn3-center 
									<?php if ($sticky_block_on_off == 1) {
										echo 'sidebar-widget-button-sticky';
									} ?>">
									<a style="background-color:#fcfcfc; color:#0087d7;" class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-custom" href="<?php echo $sidebar_button_link; ?>" id="<?php echo $buttonid; ?>">
										<?php echo $sidebar_button_text; ?>
									</a>
								</div>
							</div>
						</div>
					</div>
				</section>

			</div>
		</div>
	</div>

<?php
}
?>