<footer class="site-footer" id="masterfooter">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 footer-title">
                <?php echo get_field('stimulansz_footer_text','option'); ?>
                </div>
        </div>
    </div>
    <div class="footer-head">
        <div class="container">
            <div class="footer-head-container">
                <div class="footer-head-col">
                    <!-- image icon -->
                    <div>
                        <?php $footer_icon_1 = get_field('footer_icon_1', 'option'); ?>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <?php if (isset($footer_icon_1['url'])): ?> <div class="knowledge"><img src="<?php echo $footer_icon_1['url']; ?>" alt="<?php echo $footer_icon_1['alt']; ?>"></div><?php endif; ?>
                    </div>
                    <div><?php the_field('footer_text_line_1', 'option'); ?></div>
                </div>
                <div class="footer-head-col">
                    <div>
                        <?php $footer_icon_2 = get_field('footer_icon_2', 'option'); ?>
                        <i class="fa fa-users" aria-hidden="true"></i></div>
                    <div><?php the_field('footer_text_line_2', 'option'); ?></div>
                </div>
                <div class="footer-head-col">
                    <div>
                        <?php $footer_icon_3 = get_field('footer_icon_3', 'option'); ?>
                        <i class="fa fa-gavel" aria-hidden="true"></i>
                        <?php if (isset($footer_icon_3['url'])): ?><div class="organization"><img src="<?php echo $footer_icon_3['url']; ?>" alt="<?php echo $footer_icon_3['alt']; ?>"></div><?php endif; ?>
                    </div>
                    <div>
                        <?php the_field('footer_text_line_3', 'option'); ?></div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-above">
        <div class="container">
            <div class="footer-col col-md-3">
                <h3> <?php echo get_field('footer_menu_item_title_1', 'option');?></h3>
                     
                <?php
                if (has_nav_menu('footer-werkvelden-menu')) :
                    wp_nav_menu(array(
                        'theme_location' => 'footer-werkvelden-menu',
                        'container_class' => 'footer_menu_one',
                        'menu_class' => 'each-footer-col',
                        'menu_id' => 'footer_menu_one',
                        'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                            )
                    );
                endif;
                ?>
            </div>
            <div class="footer-col col-md-3">
                <h3> <?php echo get_field('footer_menu_item_title_2', 'option');?> </h3>
                <?php
                if (has_nav_menu('footer-acties-menu')) :
                    wp_nav_menu(array(
                        'theme_location' => 'footer-acties-menu',
                        'container_class' => 'footer_menu_acties',
                        'menu_class' => 'each-footer-col',
                        'menu_id' => 'footer_menu_acties',
                        'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                            )
                    );
                endif;
                ?>
            </div>
            <div class="footer-col col-md-3">
                  <h3><?php echo get_field('footer_menu_item_title_3', 'option');?>  </h3>
                <?php
                if (has_nav_menu('footer-producten-menu')) :
                    wp_nav_menu(array(
                        'theme_location' => 'footer-producten-menu',
                        'container_class' => 'footer_menu_producten',
                        'menu_class' => 'each-footer-col producten-col',
                        'menu_id' => 'footer_menu_producten',
                        'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                            )
                    );
                endif;
                ?>
            </div>
            <div class="footer-col col-md-3">
                <?php if (is_active_sidebar('sidebar-footer-4')) : ?>
                    <?php dynamic_sidebar('sidebar-footer-4'); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="footer-below">
        <div class="container">
            <div class="footer-below-container">
                <div class="footer_below_left">
                    <?php dynamic_sidebar('sidebar-footer'); ?>
                    <?php
                    if (has_nav_menu('bottom-footer-menu')) :
                        wp_nav_menu(array(
                            'theme_location' => 'bottom-footer-menu',
                            'container_class' => 'bottom_footer_menu',
                            'menu_class' => 'list-inline',
                            'menu_id' => 'bottom_footer_menu',
                            'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                )
                        );
                    endif;
                    ?> 
                </div>
                <div class="footer_below_right footer_below_social_media">
                    <?php if (function_exists('cn_social_icon')) echo cn_social_icon(); ?>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
     var slideIndex = 1;
        showSlides(slideIndex);

        function plusSlides(n) {
          showSlides(slideIndex += n);
        }

        function currentSlide(n) {
          showSlides(slideIndex = n);
        }

        function showSlides(n) {
          var i;
          var slides = document.getElementsByClassName("mySlides");
          var dots = document.getElementsByClassName("dot");
          if (n > slides.length) {slideIndex = 1}    
          if (n < 1) {slideIndex = slides.length}
          for (i = 0; i < slides.length; i++) {
              slides[i].style.display = "none";  
          }
          for (i = 0; i < dots.length; i++) {
              dots[i].className = dots[i].className.replace(" active", "");
          }
          slides[slideIndex-1].style.display = "block";  

}
</script>
</footer>
<script>
    jQuery( document ).ready(function() {
   
     jQuery('.page-template-template-search .wpsolr_facet_categories,.page-template-template-search .wpsolr_facet_training_cat_str,.page-template-template-search .wpsolr_facet_product_cat_str,.page-template-template-search .wpsolr_facet_training_workfield_str,\n\
            .page-template-template-search .wpsolr_facet_training_level_str,.page-template-template-search .wpsolr_facet_training_region_str,.page-template-template-search .wpsolr_facet_kennisbanken_cat_str,\n\
            .page-template-template-search .wpsolr_facet_kennisbanken_werkveld_str,.page-template-template-search .wpsolr_facet_kennisbanken_niveau_str,.page-template-template-search .wpsolr_facet_kennisbanken_regio_str,.page-template-template-search .wpsolr_facet_advies_cat_str,\n\
            .page-template-template-search .wpsolr_facet_advies_werkveld_str,.page-template-template-search .wpsolr_facet_advies_niveau_str,.page-template-template-search .wpsolr_facet_advies_regio_str,.page-template-template-search .wpsolr_facet_pa_werkveld_str,.page-template-template-search .wpsolr_facet_pa_regio_str,.page-template-template-search .wpsolr_facet_event_start_date_str,.page-template-template-search .wpsolr_facet_training_werkveld_str,.page-template-template-search .wpsolr_facet_training_regio_str,.page-template-template-search .wpsolr_facet_landingspagina_cat_str').hide();
    //Product wpsolr_facet_product_cat_str
   jQuery('.page-template-template-search div#type\\:page,.page-template-template-search div#type\\:shop_order,.page-template-template-search div#type\\:tribe_events,.page-template-template-search div#type\\:tribe_organizer').hide();
});
     
    jQuery(document).on('wpsolr_on_ajax_success', function () {
     
       var str =jQuery('.page-template-template-search div#type\\:post').text();
       var res = str.replace('post', "blog, opinie en nieuws");
       var str =jQuery('.page-template-template-search div#type\\:post').text(res);
        
        setTimeout(function(){ 
        jQuery('.products_wrapper .training_block .white_bg_block').matchHeight({remove: true});
        jQuery('.products_wrapper .training_block .white_bg_block').matchHeight();
        
        }, 2000);
        
    });
</script>

<div class="modal fade" id="demo-2" tabindex="-1" >
  <div class="modal-dialog">
   <div class="modal-content">

   
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>

    </div>
    <div class="modal-body">

     <?php
     if (has_post_thumbnail()) {
      the_post_thumbnail('full', ['class' => 'img-responsive responsive-full', 'data-toggle' => 'modal', 'data-target'=> '#demo-2', 'title' => get_the_title()]); } ?>


    </div>

  </div>
</div>
</div>
<!-- gallerys plugin -->
<div class="modal fade" id="demo-3" tabindex="-1">
  <div class="modal-dialog">
   <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal"><i class="icon-xs-o-md"></i></button>
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>

    </div>
    <div class="modal-body">
      <?php $gallery = get_field('custom_product_gallery',$post->ID);
      if($gallery){
          $i=1;
        foreach ($gallery as $gallerys) {
          //echo '<pre>'; print_r($gallerys);
          if($gallerys['upload_image']['type'] == 'image') {  ?>
 <div class="mySlides <?php if($i == 1){ echo 'active'; } ?>">
  
  <img src="<?php echo $gallerys['upload_image']['url']; ?>" style="width:100%" height="50%">
  <div class="text"><?php echo $gallerys['image_or_video_text']; ?></div>
</div>
<?php  }  else {  ?>
 <div class="mySlides  <?php if($i == 1){ echo 'active'; } ?>">
 
  <iframe  width="100%" height="252" src="<?php echo $gallerys['youtube_or_vimeo_video_link']; ?>" frameborder="0" allowfullscreen></iframe>
  <div class="text"><?php echo $gallerys['image_or_video_text']; ?></div>
</div>
<?php $i++; }}   if($gallery){ ?>
<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
<a class="next" onclick="plusSlides(1)">&#10095;</a>
<?php } } ?>


      

  </div>

</div>
</div>
</div>
