<article <?php post_class(); ?>>
<?php 

$author = get_the_author(get_the_id());

?>
    
<!-- training banner -->
<section class="training_banner">
    <div class="container">
        <div class="banner_image">
            <div class="banner_image_text">
                <h3 class="title"><?php echo get_the_title(get_the_id()); ?></h3>
                <div></div>
            </div>
            <div class="author_name">
                <span class="icon"><i class="fa fa-user"></i></span><span><?php if(isset($author)){ echo $author; } ?></span>
            </div>
        </div>
    </div>
</section>
<!-- .training banner -->


<div class="fixed-container-wrapper">
    <div class="fixed-container container">
        <section class="training_content_section">
            <div class="row">
                <div class="col-md-9 col-sm-12 col-xs-12">
                    <!-- training description -->
                    <div class="white_bg_block">
                        <div class="white_bg_block_container">
                            <?php while (have_posts()) : the_post(); ?>
                             <div class="brief_intro entry-content">
                                    <?php the_content(); ?>
                             </div>
                           </div> 
                                <?php endwhile; ?>
                        </div>
					</div>
                 
                </div>
            </div>
        </section>
    </div>
</article>
<?php //wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>