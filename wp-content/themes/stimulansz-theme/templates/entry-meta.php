 <span class="icon dddd"><i class="fa fa-calendar"></i></span>
                                 <span class="author_date"><?php the_time('j F Y'); ?></span>
<p class="byline author vcard">
     <?php
                           $author_post_id =get_field('contactpersoon_for_blog',get_the_id());
                           $post_title = get_the_title($author_post_id);
                         
                                if ($author_post_id) {
                                    
                                    echo '<span class="icon"><i class="fa fa-user"></i></span>';
                                     echo '<span class="blog-author">'.__('Door ', 'stimulansz').'<a href="'.get_permalink($author_post_id).'" rel="author">'.$post_title.'</a></span>';
                                }
                                ?>
    </p>
