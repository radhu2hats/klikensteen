<article <?php post_class('single-image-training');  ?>>
    <?php
    $author = get_the_author(get_the_id());
    $banner_image = get_field('banner_image', get_the_id());
    $stimulansz_video = '';
    if (get_field('video_url', get_the_ID())):
        $stimulansz_video = 'stimulansz_video';
    endif;
    ?>
    <?php
    // Filter call.
    $feedback_rating = apply_filters('stimulanz_get_average_preview', get_the_ID());
    ?>    
    <!-- activity banner section -->
    <div class="container">
      <?php $feat_image = get_field('background_image', get_the_id());
      //$image = get_field('background_image');
      if( $feat_image ){ ?>
      <section class="banner_section content_only activity_banner_section banner_image" style="background: url('<?php echo  $feat_image['url']; ?>')right center;
       background-size: cover;">
       
       <div class="banner_color">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12 banner_equal">
                <div class="banner_color_text">
                    <h1><?php echo get_the_title(get_the_id()); ?></h1>
                    <div class="desc">
                        <?php echo get_field('banner_text', get_the_id()); ?>
                    </div>
                    <?php
                    if (isset($feedback_rating['average'])):
                        ?>
                    <div class="rating_section">
                        <span>
                            <?php _e('Waardering', 'stimulansz'); ?>
                        </span>
                        <span class="reviews">
                            <?php
                            if (isset($feedback_rating['average'])) {
                                echo $feedback_rating['average'];
                            }
                            ?>
                        </span>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12 ">
         <div class="banner_video">
            <div class="banner_video_container">
             <?php $image = get_field('background_image');
             if( !empty($image) ){ ?>

             <img src="<?php //echo $image['url']; ?>">
             <?php }  else {
                $url = get_field('video_url', get_the_ID());
                $class = '';
                if (strpos($url, 'youtube') !== false) {
                    $video_url = get_field('video_url', get_the_ID()) . '/?controls=0&showinfo=0&rel=0&autoplay=1&loop=1"';
                } else {
                    $video_url = get_field('video_url', get_the_ID());
                }
                ?>
                <?php
                if (get_field('video_url', get_the_ID())):
                    $class = 'available_video';
                ?>
                <iframe src="<?php //echo $video_url; ?>" frameborder="0" allowfullscreen></iframe>
                <?php
                endif;
            }?>
        </div>
    </div>

</div>
</div>
</div>
</div>
</section>
<?php }  else { ?>

<section class="banner_section content_only activity_banner_section <?php echo $stimulansz_video; ?>">
    
        <div class="banner_color">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 banner_equal">
                    <div class="banner_color_text">
                        <h1><?php echo get_the_title(get_the_id()); ?></h1>
                        <div class="desc">


                            <?php echo get_field('banner_text', get_the_id()); ?>

                        </div>

                        <?php
                        if (isset($feedback_rating['average'])):
                            ?>
                        <div class="rating_section">
                            <span><?php _e('Waardering', 'stimulansz'); ?></span><span class="reviews"><?php
                            if (isset($feedback_rating['average'])) {
                                echo $feedback_rating['average'];
                            }
                            ?></span>
                        </div>
                    <?php endif;
                    ?>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 banner_equal">


                <div class="banner_video">
                    <div class="banner_video_container">
                        <?php
                        if (has_post_thumbnail()) {
                                    //the_post_thumbnail('full', ['class' => 'img-responsive responsive-full', 'title' => get_the_title()]);
                        } else {
                            $url = get_field('video_url', get_the_ID());
                            $class = '';
                            if (strpos($url, 'youtube') !== false) {
                                $video_url = get_field('video_url', get_the_ID()) . '/?controls=0&showinfo=0&rel=0&autoplay=1&loop=1"';
                            } else {
                                $video_url = get_field('video_url', get_the_ID());
                            }
                            ?>
                            <?php
                            if (get_field('video_url', get_the_ID())):
                                $class = 'available_video';
                            ?>
                            <iframe src="<?php //echo $video_url; ?>" frameborder="0" allowfullscreen></iframe>
                            <?php
                            endif;
                        }
                        ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<?php } ?>

<!-- container -->
</div>
<div class="fixed-container-wrapper <?php echo $class; ?>">
    <div class="fixed-container container">
        <section class="training_content_section activity_content_section">
            <div class="row column-top-xs">
                <div class="col-md-3 col-sm-5 col-xs-12 mobile-column visible-xs">
                    <div class="white_bg_block orange_bg">
                        <?php do_action('stimulanz_categoryname', get_the_ID(), 'training_cat'); ?>
                        <div class="white_bg_block_container">
                            <div class="training_blog_points traning_schedule hide-anchor">
                                <?php //apply_filters('stimulanz_acf_icon_text', get_the_ID()); ?>
                                <?php if(get_field('duration')) { ?>
                                <div>

                                    <span><i class="fa fa-clock-o"></i></span><span style="vertical-align: inherit;"><?php the_field('duration'); ?></span>
                                </div>
                                <?php } ?>
                                <?php if(get_field('aantal_deelnemers')) { ?>
                                <div>
                                   <span><i class="fa fa-user"></i></span><span style="vertical-align: inherit;"><?php the_field('aantal_deelnemers'); ?></span>
                               </div>
                               <?php } ?>
                               <?php if(get_field('training_type')) { ?>
                               <div>
                                  <span><i class="fa fa-building-o"></i></span><span style="vertical-align: inherit;"><?php the_field('training_type'); ?></span>
                              </div>
                              <?php } ?>
                              <?php if(get_field('price')) { ?>
                              <div>
                                 <span><i class="fa fa-eur"></i></span><span style="vertical-align: inherit;"><?php the_field('price'); ?></span>
                             </div>
                             <?php } ?>
                             <?php $eventdate= get_field('event_data_and_item_link', $post->ID);
                             if($eventdate){
                                foreach($eventdate as $eventdates){ ?>

                                <div class="overview_page">

                                 <a href="<?php echo $eventdates['event_item_link']; ?>"><span><i class="fa fa-calendar"></i></span>

                                     <?php if($eventdates['event_start_date']){ ?>
                                     <span class="event-location">
                                         <span><p><?php echo $eventdates['event_start_date']; ?></p></span>
                                         <?php if($eventdates['event_end_date']){ ?>
                                         <span class="event-end-date">- <p><?php echo $eventdates['event_end_date']; ?></p></span> 
                                         <?php } ?>

                                         <span></span><p><?php echo $eventdates['event_time']; ?></p><p class="event-location"><?php echo $eventdates['event_location']; ?></p></span>


                                         <?php } ?>
                                     </a>

                                 </div>

                                 <?php   } } ?>
                             </div>
                             <!--a href="<?php //echo get_field('info_en_aanmelden', 'option');   ?>?item_id=<?php //echo get_the_ID();   ?>" class="orange-btn"><?php //_e('Info en aanmelden', 'stimulansz');   ?></a-->

                         </div>
                     </div>

                     <div class="orange-button fixed-orange">
                        <?php $title_for_orange_section_button = get_field('title_for_orange_section_button');
                        if ($title_for_orange_section_button):
                            ?>
                        <a href="<?php echo get_field('url_of_orange_section_button'); ?>" class="orange-btn"><?php echo $title_for_orange_section_button; ?></a>
                    <?php endif; ?>                
                </div>
                <div class="col-xs-12 mobile-side-btn">
                        <?php get_template_part('templates/content','sidebar-block') ?>
                    </div>
                <div class="white_bg_block sharing_block logo">
                    <h4><?php _e('Deel dit product', 'stimulansz'); ?></h4>
                    <div class="white_bg_block_container">        
                        <?php
//addtoany social share
                        echo do_shortcode('[addtoany]');
                        ?>

                    </div>
                </div>
                <!-- Show Logo Image -->
                <?php $extralogo = get_field('training_detail_logo', $post->ID);
                if( $extralogo) { 
                    foreach($extralogo as $extralogos) { ?>
                    <div class="white_bg_block sharing_block logo">

                        <div class="white_bg_block_container">        
                            <a href="<?php echo $extralogos['logo_link']; ?>"><img src="<?php echo $extralogos['image']['url']; ?>" alt="logo"></a>

                        </div>
                    </div>
                    <?php } } ?>
                </div>
                <div class="col-md-9 col-sm-7 col-xs-12 left_section single-training-left">
                    <?php
// check if the repeater field has rows of data
                    if (get_field('usp_section')):
                        ?>
                    <div class="white_bg_block">
                        <div class="white_bg_block_container">
                            <h4 class="green"><?php echo get_field('usp_section_title'); ?></h4>
                            <div class="learning_points">
                                <?php echo get_field('usp_section'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="divide_green_seprator"></div>
                <?php endif; ?>
                <div class="white_bg_block stimulansz_vc_blue_header">
                    <div class="white_bg_block_container">
                        <?php while (have_posts()) : the_post(); ?>
                        <div class="brief_intro entry-content">
                            <?php the_content(); ?>
                        </div>
                    </div> 
                <?php endwhile; ?>
            </div>

            <?php do_action('stimulanz_instructieurs', get_the_ID()); ?>

            <div class="section_title">
                <?php if (isset($feedback_rating['average'])): ?>
                <h4><?php _e('Ervaringen', 'stimulansz'); ?></h4>
                <div class="rating_section">
                    <span><?php _e('Gemiddelde score', 'stimulansz'); ?></span><span class="reviews"><?php
                    if (isset($feedback_rating['average'])) {
                        echo $feedback_rating['average'];
                    }
                    ?></span><span class="total"><?php
                    if (isset($feedback_rating['total_comment'])) {
                        echo 'op basis van ' . $feedback_rating['total_comment'] . ' ervaringen';
                    }
                    ?></span>
                </div>
            <?php endif; ?>
        </div>
        <?php if (is_single() && comments_open()) : ?>
        <div class="ask_training_questions">
            <div class="white_bg_block">
                <div class="white_bg_block_container">
                    <?php comments_template('/templates/comments.php'); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php
/**
 * stimulanz traning related post 
 */
do_action('stimulanz_custom_upsell_article','training_cat');
?>
</div>
<div class="col-md-3 col-sm-5 col-xs-12 desktop-column single-training-right" >

    <div id="side_div">
  <?php //get_template_part('templates/content','sidebar-block') ?>
        <div class="white_bg_block orange_bg right_sidebar">
       
           <div class=side-bar-height>
            <?php do_action('stimulanz_categoryname', get_the_ID(), 'training_cat'); ?>
            <div class="white_bg_block_container">
                <div class="training_blog_points traning_schedule hide-anchor">
                   <?php if(get_field('duration')) { ?>
                   <div class="trainig-duration">

                    <span><i class="fa fa-clock-o"></i></span><div class="trainig-duration123"><span style="vertical-align: inherit;"><?php the_field('duration'); ?></span></div>
                </div>
                <?php } ?>
                <?php if(get_field('aantal_deelnemers')) { ?>
                <div class="aantal_deelnemers">
                   <span><i class="fa fa-user"></i></span><div class="trainig-duration123"><span style="vertical-align: inherit;"><?php the_field('aantal_deelnemers'); ?></span></div>
               </div>
               <?php } ?>
               <?php if(get_field('training_type')) { ?>
               <div class="training_type">
                  <span><i class="fa fa-building-o"></i></span><div class="trainig-duration123"><span style="vertical-align: inherit;"><?php the_field('training_type'); ?></span></div>
              </div>
              <?php } ?>
              <?php if(get_field('price')) { ?>
              <div class="trainig-price">
                 <span><i class="fa fa-eur"></i></span><div class="trainig-duration123"><span style="vertical-align: inherit;"><?php the_field('price'); ?></span></div>
             </div>
             <?php } ?>
             <?php //apply_filters('stimulanz_acf_icon_text', get_the_ID()); ?>
             <?php $eventdate= get_field('event_data_and_item_link', $post->ID);
             if($eventdate){
                foreach($eventdate as $eventdates){ ?>
                <div class="overview_page">

                 <a href="<?php echo $eventdates['event_item_link']; ?>"><span><i class="fa fa-calendar"></i></span>

                     <?php if($eventdates['event_start_date']){ ?>
                     <span class="event-location">
                         <span><p><?php echo $eventdates['event_start_date']; ?></p></span>
                         <?php if($eventdates['event_end_date']){ ?>
                         <span class="event-end-date">- <p><?php echo $eventdates['event_end_date']; ?></p></span> 
                         <?php } ?>

                         <span></span><p><?php echo $eventdates['event_time']; ?></p><p class="event-location"><?php echo $eventdates['event_location']; ?></p></span>


                         <?php } ?>
                     </a>

                 </div>
                 <?php   } } ?>
             </div>
             <!--a href="<?php echo get_field('info_en_aanmelden', 'option'); ?>?item_id=<?php echo get_the_ID(); ?>" class="orange-btn"><?php _e('Info en aanmelden', 'stimulansz'); ?></a-->

         </div>
     </div>
 </div>
 <div class="orange-button fixed-orange"> <?php $title_for_orange_section_button = get_field('title_for_orange_section_button');
 if ($title_for_orange_section_button):
    ?>
<a href="<?php echo get_field('url_of_orange_section_button'); ?>" class="orange-btn"><?php echo $title_for_orange_section_button; ?></a>
<?php endif; ?></div>
   <?php get_template_part('templates/content','sidebar-block') ?>
<div class="white_bg_block sharing_block">
    <h4><?php _e('Deel dit product', 'stimulansz'); ?></h4>
    <div class="white_bg_block_container">        
        <?php
//addtoany social share
        echo do_shortcode('[addtoany]');
        ?>

    </div>
</div>
<!-- Show Logo Image -->

<?php $extralogo = get_field('training_detail_logo', $post->ID);
if( $extralogo) { 
    foreach($extralogo as $extralogos) { ?>
    <div class="white_bg_block sharing_block logo">

        <div class="white_bg_block_container">        
            <a href="<?php echo $extralogos['logo_link']; ?>"><img src="<?php echo $extralogos['image']['url']; ?>" alt="logo"></a>
        </div>
    </div>
    <?php } } ?>
</div>
</div>
</div>
</section>
</div>      
</div>

</article>