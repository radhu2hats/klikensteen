<?php
if(isset($_GET['item_id'])):
    $post_id = $_GET['item_id'];
    $item_title = get_the_title($post_id);
    else:
    $post_id = get_the_id();
endif;
		
    $author = get_the_author($post_id);
    $banner_image = get_field('banner_image', $post_id);
    ?>

<!-- activity banner section -->

<section class="banner_section content_only activity_banner_section">
    <div class="container">
        <div class="banner_color">
            <div class="row">
                <div class="col-xs-12">
                    <div class="banner_color_text">
                        <h1><?php echo get_the_title($post_id); ?></h1>
                        <div class="desc">
                            <?php echo get_field('banner_text', $post_id); ?>
                        </div>
                        <div class="author_name">
                            <span class="icon"><i class="fa fa-user"></i></span><span>
                                <?php          
                                if (isset($author)) {
                                          echo $author;
                                          }
                        ?>
                       </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- .activity banner section -->

<!-- container -->
<div class="fixed-container-wrapper">
    <div class="fixed-container container">
        <section class="training_content_section">
            <div class="row">
                <div class="col-md-9 col-sm-12 left_section training-form">
                      <div class="white_bg_block">
                          <div class="white_bg_block_container">
                               <?php the_content(); ?>
                          </div>
                      </div>                   
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="sidebar-on-banner">
                       <?php if(isset($_GET['item_id'])): ?> 
                        <div class="white_bg_block green_bg">
                         <?php 
                         $post_id=$_GET['item_id'];
                        ?>
                        <h4><?php _e('Over de training','stimulansz'); ?></h4>
                        <div class="white_bg_block_container">
                            <div class="training_blog_points traning_schedule">
                                 <?php apply_filters('stimulanz_acf_icon_text',$post_id); ?>
                            </div>
                        </div>
                       
                    </div>
                        <?php endif;
                        
                        if(get_post_type( $post_id )):
                            $posttype = get_post_type( $post_id );
                            if($posttype=='academie'){
                                    $term_workfield = wp_get_post_terms($post_id,'academice_workfield', array('order' => 'DESC'));
                                    $term_level = wp_get_post_terms($post_id,'academice_level', array('order' => 'DESC'));
                                    $term_region = wp_get_post_terms($post_id,'academice_region', array('order' => 'DESC'));
                            }elseif($posttype=='training'){
                                 $term_workfield = wp_get_post_terms($post_id,'training_workfield', array('order' => 'DESC'));
                                 $term_level = wp_get_post_terms($post_id,'training_level', array('order' => 'DESC'));
                                 $term_region = wp_get_post_terms($post_id,'training_region', array('order' => 'DESC'));
                            }
                        
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<script> 
jQuery(document).ready(function(){
    <?php if($item_title): ?>
         jQuery('.training_data_title').empty();
         jQuery('.training_data_title').append('<?php echo $item_title;  ?>');
         jQuery('.ginput_container_text input#input_5_23').val('<?php echo $item_title;  ?>');
         jQuery('.ginput_container_text input#input_5_25').val('<?php echo get_field('aantal_deelnemers',$post_id);  ?>');
         jQuery('.ginput_container_text input#input_5_15').val('<?php echo ucfirst($term_region[0]->name); ?>');
         
         //Hidden item
         //Duration
         jQuery('.ginput_container_text input#input_5_26').val('<?php echo get_field('duration',$post_id);  ?>');
        //Type 
     
    jQuery('.ginput_container_text input#input_5_27').val('<?php echo get_field('training_type',$post_id);  ?>');
         //price
       
    jQuery('.ginput_container_text input#input_5_28').val('<?php echo get_field('price',$post_id);  ?>');
         
    <?php endif; ?>
    
});

</script>
    
<!-- .container -->