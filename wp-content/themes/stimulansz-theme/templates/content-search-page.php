<?php
$banner_image = get_field('page_banner', get_the_id());
 ?>
<section class="banner_section home_banner_section search_page">
    <div class="container">
        <div class="row">
            <div class="banner_image col-xs-12">
                <div class="banner_image_text">
                    <?php if (get_field('banner_title_text')): ?><h1 class="home_banner_text"><?php echo get_field('banner_title_text'); ?></h1> <?php endif; ?>
                    <?php if (get_field('banner_text_area')): ?>
                        <div class="desc">
                            <?php echo get_field('banner_text_area'); ?>
                        </div>
                    <?php endif; ?>
                    <?php if(isset($_GET['wpsolr_q'])): ?><div class="desc"><?php _e('Zoekresulaten voor ','stimulansz'); ?><?php echo $_GET['wpsolr_q']; ?></div><?php endif;?>
                </div>
                <div class="banner_search_wrapper">
                    <?php echo get_search_form(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="fixed-container-wrapper">
    <div class="fixed-container container">
        <section class="training_content_section training_listing_content_section search_container">
            <div class="row">
                <?php
                the_content();
                ?>
            </div>
        </section>
    </div>
</div>