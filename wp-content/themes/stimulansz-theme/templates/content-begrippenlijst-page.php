<article <?php post_class(); ?>>
    <?php
        $author = get_the_author(get_the_id());
        
    ?>
    <section class="banner_section content_only white_bg">
        <div class="container">
            <div class="banner_color">
                <div class="row column-top-xs">
                    <div class="col-sm-6 col-md-7 col-xs-12">
                        <div class="banner_color_text full_width">
                           <h1 class="title"><?php echo get_the_title(get_the_id()); ?></h1>
                            <div class="desc">
                                 <?php echo get_field('banner_text', get_the_id()); ?>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-sm-6 col-md-5 col-xs-12">
                        <?php
                                if (has_post_thumbnail()) {
                                    the_post_thumbnail('stimulansz_single_blog_page_image', ['class' => 'img-responsive responsive-full', 'title' => get_the_title()]);
                                }
                            ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- .training banner -->
    <div class="fixed-container-wrapper">
        <div class="fixed-container container">
            <section class="training_content_section">
                <div class="row column-top-xs">
                    <div class="col-md-3 col-sm-5 col-xs-12 mobile-column visible-xs">
                        <?php 
                        $title_for_orange_section_button = get_field('title_for_orange_section_button');
                        $text_point = get_field('basistraining_point', get_the_id());
                        if((!empty($text_point)) or !empty($title_for_orange_section_button)): ?>
                        <div class="white_bg_block orange_bg">
                            <?php do_action('stimulanz_categoryname', get_the_ID(), 'landingspagina_cat'); ?>
                            <h4><?php echo get_field('academie_box_title', get_the_id()); ?></h4>
                            <div class="white_bg_block_container landingspagina_block">
                                <div class="training_blog_points">
                                    <?php echo get_field('basistraining_point', get_the_id()); ?>
                                     <?php apply_filters('stimulanz_acf_icon_text',get_the_ID()); ?>
                                </div>
                                  
                               <?php $title_for_orange_section_button = get_field('title_for_orange_section_button');
                                 if (isset($title_for_orange_section_button)):
                                    ?>
                                    <a href="<?php echo get_field('url_of_orange_section_button'); ?>" class="orange-btn"><?php echo $title_for_orange_section_button; ?></a>
                                <?php endif; ?> 
                            </div>
                        </div>
                        <?php endif; ?>
                          <?php get_template_part('templates/content','sidebar-block') ?>
                        
                       <?php if ( is_active_sidebar( 'landingspagina_widegt' ) ) : ?>
                               <div class="white_bg_block sharing_block">
                                <?php dynamic_sidebar( 'landingspagina_widegt' ); ?>
                                </div>
                    <?php endif; ?>

                        <div class="white_bg_block sharing_block">
                            <h4><?php _e('Deel deze pagina', 'stimulansz'); ?></h4>
                            <div class="white_bg_block_container">
                                <?php
                                //addtoany social share
                                echo do_shortcode('[addtoany]');
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-7 col-xs-12">
                        <!-- training description -->
                        <div class="white_bg_block stimulansz_vc_blue_header">
                            <div class="white_bg_block_container">
                                    <?php while (have_posts()) : the_post(); ?>
                                    <div class="brief_intro entry-content">
                                  <?php the_content(); ?>
                                    </div>
                                </div> 
                        <?php endwhile; ?>
                        </div>
                       
                         <?php do_action('stimulanz_instructieurs'); ?>
                        <!-- .training description -->

                        <!-- ask training questions -->
                         <?php  if ( is_single() && comments_open() ) : ?>
                        <div class="section_title"><h4><?php _e('Reageer op dit artikel of stel een vraag','stimulansz'); ?></h4></div>
                        <div class="ask_training_questions">
                            <div class="white_bg_block">
                                <div class="white_bg_block_container">
                                  <?php comments_template('/templates/comments.php'); ?>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <!-- ask training questions -->
                        <!-- choose training area -->
                            <?php do_action('stimulanz_custom_upsell_article','landingspagina_cat'); ?>
                        <!-- .choose training area -->
                    </div>
                    <div class="col-md-3 col-sm-5 col-xs-12 desktop-column desktop-landing1">
                                             <?php get_template_part('templates/content','sidebar-block') ?>
              <?php 
                       
                        if((!empty($text_point)) or !empty($title_for_orange_section_button)): ?>
                        <div class="white_bg_block orange_bg ">
                            <?php  do_action('stimulanz_categoryname', get_the_ID(), 'landingspagina_cat'); ?>
                            <h4><?php echo get_field('academie_box_title', get_the_id()); ?></h4>
                            <div class="white_bg_block_container landingspagina_block">
                                <div class="training_blog_points">
                                    <?php echo get_field('basistraining_point', get_the_id()); ?>
                                     <?php apply_filters('stimulanz_acf_icon_text',get_the_ID()); ?>
                                </div>
                                  
                               
                            </div>
                        </div>
                         <div class="landing-orange-sticky">
                        <?php 
                        $title_for_orange_section_button = get_field('title_for_orange_section_button');
                        if (isset($title_for_orange_section_button)):
                            ?>
                            <a href="<?php echo get_field('url_of_orange_section_button'); ?>" class="orange-btn landing-page-sticky" id="<?php echo get_field('popup_id_for_button'); ?>"><?php echo $title_for_orange_section_button; ?></a>
                        <?php endif; ?> 
                                </div>
                        <?php endif; ?>
                         <?php //get_template_part('templates/content','sidebar-block') ?>
                        
                        
                         <?php if ( is_active_sidebar( 'landingspagina_widegt' ) ) : ?>
                               <div class="white_bg_block sharing_block">
                                 <?php dynamic_sidebar( 'landingspagina_widegt' ); ?>
                                </div>
                    <?php endif; ?>
                        <div class="white_bg_block sharing_block">
                            <h4><?php _e('Deel deze pagina', 'stimulansz'); ?></h4>
                            <div class="white_bg_block_container">
                                <?php
                                //addtoany social share
                                echo do_shortcode('[addtoany]');
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</article>
