<?php 
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$academie_args = array(
        'post_type' => 'landingspagina',
        'posts_per_page' =>-1,
        'orderby'   => 'DESC',
        'post_status ' => 'publish',
        'paged' => $paged
    );
 $academie_query = new \ WP_Query($academie_args);
?>
<?php
    $author = get_the_author(get_the_id());
    $banner_image = get_field('page_banner', get_the_id());
     $banner_button_on = get_field('banner_button_on', get_the_id());
$banner_button_text = get_field('banner_button_text', get_the_id());
$banner_button_link = get_field('banner_button_link', get_the_id());
    ?>
<!-- training listing banner section -->
<section class="banner_section training_listing_banner_section">
    <div class="container">
        <div class="banner_image" style="background:url('<?php echo $banner_image['url']; ?>') right center;background-size: cover;">
            <div class="banner_image_text">
                <h1 class="home_banner_text"><?php echo get_field('banner_title_text', get_the_id()) ?></h1>
                <div class="desc"><?php echo get_field('banner_text_area', get_the_id()) ?></div>
                <?php
               if($banner_button_on == 1){
                 ?>
               <a href="<?php echo $banner_button_link; ?>"><?php echo $banner_button_text; ?></a>
               <?php } ?>
            </div>
        </div>
    </div>
</section>
<!-- .training listing banner section -->
<!-- container -->
<div class="fixed-container-wrapper stimulansz-overviewpage landingpage">
    <div class="fixed-container container">
        <section class="training_content_section training_listing_content_section">
            <div class="row">
                <!--<div class="col-md-3 col-sm-4 col-xs-12">
                    <div class="white_bg_block">
                        <div class="white_bg_block_container">
                            
                             <?php
                                    $categories = get_terms( 'landingspagina_cat', 'orderby=count&hide_empty=0' );
                                ?>
                               <?php if(isset($categories)): ?>
                                <h4><?php _e('Type','stimulansz'); ?></h4>
                                <?php foreach($categories as $academie_cat){ ?>
                                <div class="checkboxCustom_content">
                                    <div class="checkboxCustom">
                                        <input type="checkbox" value="<?php echo $academie_cat->slug ?>" id="checkboxCustom1" name="<?php echo $academie_cat->slug ?>"/>
                                        <label class="inner" for="checkboxCustom1"></label>
                                    </div>
                                    <label class="outer" for="checkboxCustom1"><?php echo $academie_cat->name ?><?php if(isset($academie_cat->count)){ ?>(<?php echo $academie_cat->count; ?>) <?php } ?></label>
                                </div>
                                <?php }
                                endif;
                                ?>
                            
                        </div>
                    </div>
                </div> -->
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="section_title">
                        <h2><?php _e('Direct naar:','stimulansz'); ?></h2>
                        <div class="choose_training_area">
                            <?php if ($academie_query->have_posts()) : while ($academie_query->have_posts()) : $academie_query->the_post(); ?> 
                            <div class="col-md-4 col-sm-6 col-xs-12 training_block">
                               <div class="white_bg_block banner_block">
                                   <?php do_action('stimulanz_categoryname',get_the_ID(),'landingspagina_cat'); ?>
                                        <?php
                                            if (has_post_thumbnail()) {
                                                echo ' <div class="banner_image">';
                                                echo '<a href="'.get_permalink().'">';
                                                    the_post_thumbnail('stimulansz_related_post_image', ['class' => 'img-responsive responsive-full', 'title' => get_the_title()]);
                                                echo '</a></div>';
                                                }
                                            ?>
                                    
                            <div class="banner_content">
                                   
                                    <a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4></a>
                                    <div class="white_bg_block_container">
                                        <div class="training_blog_points">
                                          <?php the_excerpt(); ?>
                                        </div>
                                        <div class="training_blog_points traning_schedule">
                                            <?php apply_filters('stimulanz_acf_icon_text',get_the_ID()); ?>
                                            <?php 
                                            $feedback_rating = apply_filters( 'stimulanz_get_average_preview', get_the_ID());
                                           if(isset($feedback_rating['total_comment'])&& !empty($feedback_rating['total_comment'])): 
                                             ?>
                                                <div><span class="reviews"><?php if(isset($feedback_rating['total_comment'])){ echo $feedback_rating['total_comment']; } ?> </span><span><?php _e('beoordelingen','stimulansz');?></span></div> <?php endif; ?>
                                        </div>
                                    </div>
                        </div>
                                </div>
                            </div>
                             <?php endwhile;
                                    endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- .container -->