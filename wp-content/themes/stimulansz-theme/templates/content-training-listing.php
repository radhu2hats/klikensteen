<?php
$plugin_dir = ABSPATH . 'wp-content/plugins/wpsolr-pro/wpsolr/core/ajax_solr_services.php';
require_once($plugin_dir);

$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
$traing_args = array(
    'post_type' => 'training',
    'posts_per_page' => -1,
    'orderby' => 'DESC',
    'post_status ' => 'publish',
    'paged' => $paged
);
$training_query = new \ WP_Query($traing_args);

/**
 * Custom object array
 * which is define in wpslor plugin in wp-content/plugins/wpsolr-pro/wpsolr/core/ajax_solr_services.php
 */
$obj = stimulansz_getObj();
if (empty($_GET['wpsolr_fq'])) {
    //Default Load results
    $obj->set_filter_query_fields(array('0' => "type:training"));
} else {
    //Get Query string object value
    $cate = $_GET['wpsolr_fq']['0'];

    if (isset($cate)):
        $cat_explode = explode(':', $cate);
    else:
        $cat_explode = '';
    endif;
   
    if (empty($cat_explode[1])) {
         $obj->set_filter_query_fields(array('0' => "type:training"));
    } else {
       $obj->set_filter_query_fields($_GET['wpsolr_fq']);
    }
}
?>
<?php
$author = get_the_author(get_the_id());
$banner_image = get_field('page_banner', get_the_id());
$banner_button_on = get_field('banner_button_on', get_the_id());
$banner_button_text = get_field('banner_button_text', get_the_id());
$banner_button_link = get_field('banner_button_link', get_the_id());
?>
<!-- training listing banner section -->
<section class="banner_section training_listing_banner_section">
    <div class="container">
        <div class="banner_image" style="background:url('<?php echo $banner_image['url']; ?>') right center;background-size: cover;">
            <div class="banner_image_text">
                <h1 class="home_banner_text"><?php echo get_field('banner_title_text', get_the_id()) ?></h1>
                <div class="desc"><?php echo get_field('banner_text_area', get_the_id()) ?></div>
                <?php
               if($banner_button_on == 1){
                 ?>
               <a href="<?php echo $banner_button_link; ?>"><?php echo $banner_button_text; ?></a>
               <?php } ?>
            </div>
        </div>
    </div>
</section>
<!-- .training listing banner section -->
<!-- container -->
<div class="fixed-container-wrapper stimulansz-overviewpage">
    <div class="fixed-container container">
        <section class="training_content_section training_listing_content_section">
            <div class="row">
                 <?php
                        if (isset($obj)) { 
                            stimulansz_custom_search_indexed_data($obj);
                        }
                        ?>
            </div>
        </section>
    </div>
</div>

<!-- .container -->