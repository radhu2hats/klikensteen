<article <?php post_class('stimulansz-blog-details'); ?>>
    <?php $author = get_the_author(get_the_id()); ?>
    <!-- training banner -->
    <section class="banner_section content_only">
        <div class="container">
            <div class="banner_color">
                <div class="row column-top-xs">
                    <div class="col-md-6 col-sm-5 col-xs-12">
                        <div class="banner_color_text full_width">
                            <h1><?php echo get_the_title(get_the_id()); ?></h1>
                            <div class="desc"><?php echo get_field('additional_banner_text', get_the_id()) ?></div>
                        </div>
                        <div class="author_name">

                            <?php
                            $author_post_id = get_field('contactpersoon_for_blog', get_the_id());
                            $post_title = get_the_title($author_post_id);
                            //  echo get_permalink($author_post_id);
                            if ($author_post_id) { ?>
                                <span class="icons dddd"><i class="fa fa-calendar"></i></span>
                                <span class="author_date"><?php the_time('j F Y'); ?></span><br>
                                <?php echo '<span class="icons dddd"><i class="fa fa-user"></i></span>';
                                echo '<span> Door<a href="' . get_permalink($author_post_id) . '" rel="author" class="author-name">' . $post_title . '</a></span>';
                                echo '<br>'; ?>

                            <?php }  ?>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-7 col-xs-12">
                        <?php
                        if (has_post_thumbnail()) {
                            the_post_thumbnail('stimulansz_single_blog_page_image', ['class' => 'img-responsive responsive-full', 'title' => get_the_title()]);
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- .training banner -->

    <div class="fixed-container-wrapper">
        <div class="fixed-container container">
            <section class="training_content_section">
                <div class="row">
                    <div class="col-md-9 col-sm-12 col-xs-12">
                        <!-- training description -->
                        <div class="white_bg_block stimulansz_vc_blue_header">
                            <div class="white_bg_block_container">
                                <?php while (have_posts()) : the_post(); ?>
                                    <div class="brief_intro entry-content">
                                        <?php the_content(); ?>
                                    </div>
                            </div>
                        <?php endwhile; ?>
                        </div>
                        <!-- .training description -->

                        <!-- ask training questions -->
                        <?php if (is_single() && comments_open()) : ?>
                            <div class="ask_training_questions">
                                <div class="white_bg_block">
                                    <div class="white_bg_block_container">
                                        <?php comments_template('/templates/comments.php'); ?>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php
                        global $post;
                        $cats = get_the_category($post->ID);
                        if ($cats) {
                        ?>
                            <div class="section_title">
                                <h4><?php _e('Geselecteerd op basis van dit onderwerp', 'stimulansz') ?></h4>
                            </div>

                            <div class="choose_training_area">
                                <?php
                                //for use in the loop, list 5 post titles related to first tag on current post
                                $category_ids = array();
                                foreach ($cats as $individual_category) {
                                    $category_ids[] = $individual_category->term_id;
                                }


                                $args = array(
                                    'category__in' => $category_ids,
                                    'post__not_in' => array($post->ID),
                                    'posts_per_page' => 3,
                                    'caller_get_posts' => 1
                                );

                                $blog_related_query = new \WP_Query($args);

                                if ($blog_related_query->have_posts()) {
                                    while ($blog_related_query->have_posts()) : $blog_related_query->the_post();
                                ?>
                                        <div class="col-md-4 col-sm-6 col-xs-12 training_block">
                                            <div class="white_bg_block banner_block">
                                                <?php do_action('stimulanz_categoryname', get_the_ID(), 'category'); ?>
                                                <?php
                                                if (has_post_thumbnail()) {
                                                    echo '<div class="banner_image_text banner_image">';
                                                    echo '<a href="' . get_permalink() . '">';
                                                    the_post_thumbnail('stimulansz_block_image', ['class' => 'img-responsive responsive-full', 'title' => get_the_title()]);
                                                    echo '</a></div>';
                                                }
                                                ?>

                                                <div class="banner_content">
                                                    <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
                                                    <div class="white_bg_block_container">
                                                        <div class="training_blog_points">
                                                            <?php //the_excerpt(); 
                                                            ?>
                                                            <?php
                                                            $intro = get_field('additional_banner_text');
                                                            if (isset($intro) && !empty($intro)) :
                                                                echo $intro . ' <a class="stimulansz-home-more-link" href="' . get_permalink() . '">' . esc_html__('Read more', 'stimulansz') . '</a>';
                                                            else :
                                                                the_excerpt();
                                                            endif;
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                <?php
                                    endwhile;
                                }
                                wp_reset_query();
                                ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="sidebar-blue-fixblocksz wpb_column vc_column_container col-md-3 col-sm-12 col-xs-12 blue-sidebar">
                        <?php get_template_part('templates/content', 'sidebar-block') ?>
                        <div class="wpb_widgetised_column wpb_content_element social-media-aside">
                            <div class="wpb_wrapper">
                                <section class="widget a2a_share_save_widget-2 widget_a2a_share_save_widget details-social-icons">
                                    <h3><?php _e('Deel deze pagina', 'stimulansz'); ?></h3>
                                    <div class="a2a_kit a2a_kit_size_24 addtoany_list" style="line-height: 24px;">
                                        <?php
                                        //addtoany social share
                                        echo do_shortcode('[addtoany]');
                                        ?>
                                    </div>
                                </section>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
        </div>
    </div>
</article>