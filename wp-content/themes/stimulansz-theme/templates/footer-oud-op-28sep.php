<footer class="site-footer" id="masterfooter">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 footer-title">
        <?php echo get_field('stimulansz_footer_text','option'); ?>
      </div>
    </div>
  </div>
  <div class="footer-head">
    <div class="container">
      <div class="footer-head-container">
        <div class="footer-head-col">
          <!-- image icon -->
          <div>
            <?php $footer_icon_1 = get_field('footer_icon_1', 'option'); ?>
            <i class="fa fa-star" aria-hidden="true"></i>
            <?php if (isset($footer_icon_1['url'])): ?> <div class="knowledge"><img src="<?php echo $footer_icon_1['url']; ?>" alt="<?php echo $footer_icon_1['alt']; ?>"></div><?php endif; ?>
          </div>
          <div><?php the_field('footer_text_line_1', 'option'); ?></div>
        </div>
        <div class="footer-head-col">
          <div>
            <?php $footer_icon_2 = get_field('footer_icon_2', 'option'); ?>
            <i class="fa fa-users" aria-hidden="true"></i></div>
            <div><?php the_field('footer_text_line_2', 'option'); ?></div>
          </div>
          <div class="footer-head-col">
            <div>
              <?php $footer_icon_3 = get_field('footer_icon_3', 'option'); ?>
              <i class="fa fa-gavel" aria-hidden="true"></i>
              <?php if (isset($footer_icon_3['url'])): ?><div class="organization"><img src="<?php echo $footer_icon_3['url']; ?>" alt="<?php echo $footer_icon_3['alt']; ?>"></div><?php endif; ?>
            </div>
            <div>
              <?php the_field('footer_text_line_3', 'option'); ?></div>
            </div>
          </div>
        </div>
      </div>
      <div class="footer-above">
        <div class="container">
          <div class="footer-col col-md-3">
            <h3> <?php echo get_field('footer_menu_item_title_1', 'option');?></h3>
            
            <?php
            if (has_nav_menu('footer-werkvelden-menu')) :
              wp_nav_menu(array(
                'theme_location' => 'footer-werkvelden-menu',
                'container_class' => 'footer_menu_one',
                'menu_class' => 'each-footer-col',
                'menu_id' => 'footer_menu_one',
                'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                )
            );
            endif;
            ?>
          </div>
          <div class="footer-col col-md-3">
            <h3> <?php echo get_field('footer_menu_item_title_2', 'option');?> </h3>
            <?php
            if (has_nav_menu('footer-acties-menu')) :
              wp_nav_menu(array(
                'theme_location' => 'footer-acties-menu',
                'container_class' => 'footer_menu_acties',
                'menu_class' => 'each-footer-col',
                'menu_id' => 'footer_menu_acties',
                'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                )
            );
            endif;
            ?>
          </div>
          <div class="footer-col col-md-3">
            <h3><?php echo get_field('footer_menu_item_title_3', 'option');?>  </h3>
            <?php
            if (has_nav_menu('footer-producten-menu')) :
              wp_nav_menu(array(
                'theme_location' => 'footer-producten-menu',
                'container_class' => 'footer_menu_producten',
                'menu_class' => 'each-footer-col producten-col',
                'menu_id' => 'footer_menu_producten',
                'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                )
            );
            endif;
            ?>
          </div>
          <div class="footer-col col-md-3">
            <?php if (is_active_sidebar('sidebar-footer-4')) : ?>
            <?php dynamic_sidebar('sidebar-footer-4'); ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
    <div class="footer-below">
      <div class="container">
        <div class="footer-below-container">
          <div class="footer_below_left">
            <?php dynamic_sidebar('sidebar-footer'); ?>
            <?php
            if (has_nav_menu('bottom-footer-menu')) :
              wp_nav_menu(array(
                'theme_location' => 'bottom-footer-menu',
                'container_class' => 'bottom_footer_menu',
                'menu_class' => 'list-inline',
                'menu_id' => 'bottom_footer_menu',
                'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                )
            );
            endif;
            ?> 
          </div>
          <div class="footer_below_right footer_below_social_media">
            <?php if (function_exists('cn_social_icon')) echo cn_social_icon(); ?>
          </div>
        </div>
      </div>
    </div>
    <script>
    jQuery(document).ready(function () {
      jQuery('.overview_page a').click(function () {
        var id = jQuery(this).attr('href');
        jQuery('html, body').animate({
                //alert(id);
                scrollTop: jQuery(id).offset().top - 100
              }, 2000);
      });
    });
    
    </script>
    <script type="text/javascript">

    var slideIndex = 1;
    showSlides(slideIndex);

    function plusSlides(n) {
      showSlides(slideIndex += n);
    }

    function currentSlide(n) {
      showSlides(slideIndex = n);
    }

    function showSlides(n) {
      var i;
      var x = document.getElementsByClassName("mySlides");
      if (n > x.length) {slideIndex = 1}    
        if (n < 1) {slideIndex = x.length} ;
      for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
     }
     x[slideIndex-1].style.display = "block"; 
   }


   jQuery(window).scroll(function (event) {
    var scroll = jQuery(window).scrollTop();
    if(scroll > 300){
     jQuery("#side_div").addClass('fixed_rightbar');
   }
   else{
    jQuery("#side_div").removeClass('fixed_rightbar');
  }
});




   </script>
   <?php 
   if(is_checkout() || is_page(14)){ ?>
   
   <?php }else{ ?>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

   <?php } ?>
   <!--<script src="<?php echo get_template_directory_uri(); ?>/assets/scripts/jquery.mCustomScrollbar.concat.min.js"></script>-->
   <script src="<?php echo get_template_directory_uri(); ?>/assets/scripts/jquery.scrollbar.js"></script>
   <script src="<?php echo get_template_directory_uri(); ?>/assets/scripts/jquery.visible.js"></script>
   <script src="https://www.jqueryscript.net/demo/Fully-Responsive-Flexible-jQuery-Carousel-Plugin-slick/js/slick.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
 </footer>
 <script>
 jQuery( document ).ready(function() {

   jQuery('.page-template-template-search .wpsolr_facet_categories,.page-template-template-search .wpsolr_facet_training_cat_str,.page-template-template-search .wpsolr_facet_product_cat_str,.page-template-template-search .wpsolr_facet_training_workfield_str,\n\
    .page-template-template-search .wpsolr_facet_training_level_str,.page-template-template-search .wpsolr_facet_training_region_str,.page-template-template-search .wpsolr_facet_kennisbanken_cat_str,\n\
    .page-template-template-search .wpsolr_facet_kennisbanken_werkveld_str,.page-template-template-search .wpsolr_facet_kennisbanken_niveau_str,.page-template-template-search .wpsolr_facet_kennisbanken_regio_str,.page-template-template-search .wpsolr_facet_advies_cat_str,\n\
    .page-template-template-search .wpsolr_facet_advies_werkveld_str,.page-template-template-search .wpsolr_facet_advies_niveau_str,.page-template-template-search .wpsolr_facet_advies_regio_str,.page-template-template-search .wpsolr_facet_pa_werkveld_str,.page-template-template-search .wpsolr_facet_pa_regio_str,.page-template-template-search .wpsolr_facet_event_start_date_str,.page-template-template-search .wpsolr_facet_training_werkveld_str,.page-template-template-search .wpsolr_facet_training_regio_str,.page-template-template-search .wpsolr_facet_landingspagina_cat_str').hide();
    //Product wpsolr_facet_product_cat_str
    jQuery('.page-template-template-search div#type\\:page,.page-template-template-search div#type\\:shop_order,.page-template-template-search div#type\\:tribe_events,.page-template-template-search div#type\\:tribe_organizer').hide();
  });

jQuery(document).on('wpsolr_on_ajax_success', function () {

 var str =jQuery('.page-template-template-search div#type\\:post').text();
 var res = str.replace('post', "blog, opinie en nieuws");
 var str =jQuery('.page-template-template-search div#type\\:post').text(res);

       // Advisor post type

       var advies_str =jQuery('.page-template-template-search div#type\\:advies').text();
       var addvises_res = advies_str.replace('advies', "Advies & Bedrijfsvoering");

       jQuery('.page-template-template-search div#type\\:advies').text(addvises_res);

        //Traning Trainingen & Evenementen post type type:training

        var traing_str =jQuery('.page-template-template-search div#type\\:training').text();
        var traning_res = traing_str.replace('training', "Trainingen & Evenementen");
        jQuery('.page-template-template-search div#type\\:training').text(traning_res);

        // Landingspagina
        var landingspagina_traing_str = jQuery('.page-template-template-search div#type\\:landingspagina').text();
        var landings_traning_res = landingspagina_traing_str.replace('landingspagina', "Themapagina");
        jQuery('.page-template-template-search div#type\\:landingspagina').text(landings_traning_res);

        // Kennisbanken
        var kennisbanken_str = jQuery('.page-template-template-search div#type\\:kennisbanken').text();
        var kennisbanken_res = kennisbanken_str.replace('kennisbanken', "Juridische Kennisbanken");
        jQuery('.page-template-template-search div#type\\:kennisbanken').text(kennisbanken_res);



        setTimeout(function(){ 
          jQuery('.products_wrapper .training_block .white_bg_block').matchHeight({remove: true});
          jQuery('.products_wrapper .training_block .white_bg_block').matchHeight();
          
        }, 2000);
        
      });
</script>

<script>
    // (function($){
    //     $(window).on("load",function(){
    //         $(".right_sidebar").mCustomScrollbar();
    //     });
    // })(jQuery);

jQuery(document).ready(function(){
 jQuery('.side-bar-height').scrollbar();
});

jQuery( window ).scroll(function() {

  if(jQuery('#training-sticky').visible() ){
    jQuery('#check-visibility').val('Yes');
  }

  var valid = document.getElementById('check-visibility').value;
  console.log(valid);
  if(typeof(valid)!='undefined' && valid==='Yes'){
    jQuery('#sticky-button-sidebar').addClass('hide-this-button');
    if(jQuery('.stimulansz_experts_organization').visible() ){
      jQuery('#check-visibility').val('No');
    }
  }else if(typeof(valid)!='undefined' && valid==='No'){
    jQuery('#sticky-button-sidebar').removeClass('hide-this-button');
  }

});
</script>

<script>
jQuery(document).ready(function() {
  jQuery(window).scroll(function() {
    var popUp = 0;
    if(jQuery(".fixed-orange").length>0){
      var popUp = jQuery('.fixed-orange').position().top;
    }
    var bodyHeight = jQuery(document).height();
    if(jQuery('.sidebar-fixed').length>0)
    {
      if(popUp<bodyHeight*(0.095))
      {
        jQuery('.orange-button').removeClass('sidebar-fixed');
      }

    }
    else
    {
      if (popUp>bodyHeight*(0.095)) {
        jQuery('.orange-button').addClass('sidebar-fixed');
      }

    }
    
    var popUpone = 0;
    if(jQuery(".sidbar-box-wht").length>0){
      var popUpone = jQuery('.sidbar-box-wht').position().top;
    }
    var bodyHeightone = jQuery(document).height();
    if(jQuery('.sidbar-box-wht').length>0)
    {
      if(popUpone>bodyHeightone*(0.348))
      {
       jQuery('.sidbar-box-wht').addClass('side-bottom-btn-fixed');
     }

   } 
    // else
    // {
      if (popUpone<=bodyHeightone*(0.348)) {
        jQuery('.sidbar-box-wht').removeClass('side-bottom-btn-fixed');
      }

    // }
    
  });
});

// jQuery(document).ready(function() {
//     jQuery(window).on('scroll', function(){
//         if (jQuery(".choose_training_area").is(':visible')){
//             $(".btn").addClass("btn-default");
//             alert('Hello, World!!');
//         }
//     });
// });
jQuery(document).ready(function($) {
  jQuery('.responsive').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 6,
    slidesToScroll: 1,
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 3,
        infinite: true,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 479,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
    ]
  });
});
</script>

<div class="modal fade" id="demo-2" tabindex="-1" >
  <div class="modal-dialog">
   <div class="modal-content">


    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>

    </div>
    <div class="modal-body">

     <?php
     if (has_post_thumbnail()) {
      the_post_thumbnail('full', ['class' => 'img-responsive responsive-full', 'data-toggle' => 'modal', 'data-target'=> '#demo-2', 'title' => get_the_title()]); } ?>


    </div>

  </div>
</div>
</div>
<!-- gallerys plugin -->
<div class="modal fade" id="demo-3" tabindex="-1">
  <div class="modal-dialog">
   <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal"><i class="icon-xs-o-md"></i></button>
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>

    </div>
    <div class="modal-body">
      <?php $gallery = get_field('custom_product_gallery',$post->ID);
      if($gallery){
       $i=1;
       foreach ($gallery as $gallerys) {
         if($gallerys['upload_image']['type'] == 'image') {  ?>
         <div class="mySlides <?php if($i == 1){ echo 'active'; } ?>">
           <img src="<?php echo $gallerys['upload_image']['url']; ?>" style="width:100%" height="50%">
           <div class="text"><?php echo $gallerys['image_or_video_text']; ?></div>
         </div>
         <?php 
       }
       $i++;}   if($gallery){ ?>
       <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
       <a class="next" onclick="plusSlides(1)">&#10095;</a>
       <?php } } ?>


       

     </div>

   </div>
 </div>
</div>