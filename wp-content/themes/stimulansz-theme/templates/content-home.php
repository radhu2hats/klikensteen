<div class="fixed-container-wrapper">
    <div class="fixed-container container">
        <section class="training_content_section">
            <!-- blog and news section -->
            <section class="blogs_news_section">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12 no-padding-xs">
	                    <div class="white_bg_block banner_block">
		                    
		                    <div class="box-top-right-icon">
                                <i class="fa fa-calendar" aria-hidden="true"></i>
                            </div>
	                    <h3 class="insp--main-title">Inspiratie</h3>
					                   <?php echo do_shortcode( '[facetwp template="homepage_inspiratie_blogs"]' ); ?>
						</div>
					</div>
					
                    <div class="col-md-6 col-sm-12 col-xs-12">
	                    
	                    <div class="white_bg_block">
                            <div class="box-top-right-icon">
                                <i class="fa fa-calendar" aria-hidden="true"></i>
                            </div>
                            
                            <?php echo apply_filters('stimulanz_traing_agenda',''); ?>   
                        </div>
                        
                        <div class="uitgelichte_posts">
                            <div class="white_bg_block_container">
                                <div class="news_block col-12">
                                <?php  getFeaturePosts1(); ?>
                                </div>
                            </div>
                        </div>
                  

                        
                        <div class="white_bg_block veel_gezocht_block">
                            <h4><?php _e('Themapagina\'s','stimulansz'); ?></h4>
                            <div class="white_bg_block_container">
                                <div class="block_content">
                                    <?php
                             if (has_nav_menu('theme_paginas')) :
                                 wp_nav_menu(array(
                                     'theme_location' => 'theme_paginas',
                                     'container_class' => '',
                                     'menu_class' => 'list-inline theme_pages',
                                     'menu_id' => 'themepages_menu',
                                     'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                         )
                                 );
                             endif;
                             ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- .blog and news section -->

                   </section>
    </div>
</div>

<?php //wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>


