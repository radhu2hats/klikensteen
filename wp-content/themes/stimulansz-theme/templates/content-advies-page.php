<article <?php post_class(); ?>>
    <?php

    $author = get_the_author(get_the_id());
    $banner_image = get_field('banner_image', get_the_id());
    $stimulansz_video = '';
    if (get_field('video_url', get_the_ID())) :
        $stimulansz_video = 'stimulansz_video';
    endif;
    ?>
    <?php
    // Filter call.
    $feedback_rating = apply_filters('stimulanz_get_average_preview', get_the_ID());
    ?>
    <!-- activity banner section -->
    <section class="banner_section content_only activity_banner_section <?php echo $stimulansz_video; ?>">
        <div class="container">
            <div class="banner_color">
                <div class="row">
                    <div class="col-md-9 col-sm-7 col-xs-12 banner_equal">
                        <div class="banner_color_text">
                            <h1><?php echo get_the_title(get_the_id()); ?></h1>
                            <div class="desc">
                                <?php echo get_field('banner_text', get_the_id()); ?>
                            </div>

                            <?php
                            if (isset($feedback_rating['average'])) :
                            ?>
                                <div class="rating_section">
                                    <span><?php _e('Waardering', 'stimulansz'); ?></span><span class="reviews"><?php if (isset($feedback_rating['average'])) {
                                                                                                                    echo $feedback_rating['average'];
                                                                                                                } ?></span>
                                </div>
                            <?php
                            endif; ?>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-5 col-xs-12 banner_equal">


                        <div class="banner_video">
                            <div class="banner_video_container">
                                <?php
                                if (has_post_thumbnail()) {
                                    the_post_thumbnail('full', ['class' => 'img-responsive responsive-full', 'title' => get_the_title()]);
                                } else {
                                    $url = get_field('video_url', get_the_ID());
                                    $class = '';
                                    if (strpos($url, 'youtube') !== false) {
                                        $video_url = get_field('video_url', get_the_ID()) . '/?controls=0&showinfo=0&rel=0&autoplay=1&loop=1"';
                                    } else {
                                        $video_url = get_field('video_url', get_the_ID());
                                    }
                                ?>
                                    <?php if (get_field('video_url', get_the_ID())) :
                                        $class = 'available_video';
                                    ?>
                                        <iframe src="<?php echo $video_url; ?>" frameborder="0" allowfullscreen></iframe>
                                <?php endif;
                                } ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- container -->
    <div class="fixed-container-wrapper <?php echo $class; ?>">
        <div class="fixed-container container">
            <section class="training_content_section activity_content_section">
                <div class="row column-top-xs">
                    <div class="col-md-3 col-sm-5 col-xs-12 mobile-column visible-xs">
                    <?php get_template_part('templates/content','sidebar-block') ?>
                        <div class="vc_column-inner">
                            <div class="wpb_wrapper">
                                <section class="vc_cta3-container">
                                    <div class="vc_general vc_cta3 blue-cta small-cta vc_cta3-style-classic vc_cta3-shape-rounded vc_cta3-align-left vc_cta3-color-classic vc_cta3-icon-size-md vc_cta3-actions-bottom">
                                        <div class="vc_cta3_content-container">
                                            <div class="vc_cta3-content">
                                                <header class="vc_cta3-content-header">
                                                    <h2> <?php do_action('stimulanz_categoryname', get_the_ID(), 'advies_cat'); ?></h2>
                                                </header>
                                                <p> <?php apply_filters('stimulanz_acf_icon_text',get_the_ID()); ?></p>
                                            </div>
                                            <div class="vc_cta3-actions">
                                            <?php $title_for_orange_section_button = get_field('title_for_orange_section_button');
                                if ($title_for_orange_section_button):
                                    ?>
                                                <div class="vc_btn3-container  btn-white vc_btn3-center"><a style="background-color:#fcfcfc; color:#0087d7;" class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-custom" href="<?php echo get_field('url_of_orange_section_button'); ?>" title=""><?php echo $title_for_orange_section_button; ?></a></div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <div class="wpb_widgetised_column wpb_content_element social-media-aside">
                                    <div class="wpb_wrapper">
                                        <section class="widget a2a_share_save_widget-2 widget_a2a_share_save_widget details-social-icons">
                                            <h3><?php _e('Deel dit product', 'stimulansz'); ?></h3>
                                            <div class="a2a_kit a2a_kit_size_24 addtoany_list" style="line-height: 24px;">
                                                <?php
                                                    //addtoany social share
                                                    echo do_shortcode('[addtoany]');
                                                    ?>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-7 col-xs-12 left_section">
                        <?php // check if the repeater field has rows of data
                        if (get_field('usp_section')) : ?>
                            <div class="white_bg_block">
                                <div class="white_bg_block_container">
                                    <h4 class="green"><?php echo get_field('usp_section_title'); ?></h4>
                                    <div class="learning_points">
                                        <?php echo get_field('usp_section'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="divide_green_seprator"></div>
                        <?php endif; ?>
                        <div class="white_bg_block stimulansz_vc_blue_header">
                            <div class="white_bg_block_container">
                                <?php while (have_posts()) : the_post(); ?>
                                    <div class="brief_intro entry-content">
                                        <?php the_content(); ?>
                                    </div>
                            </div>
                        <?php endwhile; ?>
                        </div>

                        <?php do_action('stimulanz_instructieurs'); ?>

                        <div class="section_title">
                            <?php if (isset($feedback_rating['average'])) : ?>
                                <h4><?php _e('Ervaringen', 'stimulansz'); ?></h4>
                                <div class="rating_section">
                                    <span><?php _e('Gemiddelde score', 'stimulansz'); ?></span><span class="reviews"><?php if (isset($feedback_rating['average'])) {
                                                                                                                            echo $feedback_rating['average'];
                                                                                                                        } ?></span><span class="total"><?php if (isset($feedback_rating['total_comment'])) {
                                                                                                                                                        echo 'op basis van ' . $feedback_rating['total_comment'] . ' ervaringen';
                                                                                                                                                    } ?></span>
                                </div>
                            <?php endif; ?>
                        </div>
                        <?php if (is_single() && comments_open()) : ?>
                            <div class="ask_training_questions">
                                <div class="white_bg_block">
                                    <div class="white_bg_block_container">
                                        <?php comments_template('/templates/comments.php'); ?>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php do_action('stimulanz_custom_upsell_article', 'advies_cat'); ?>

                    </div>
                    <div class="sidebar-blue-fixblocksz wpb_column vc_column_container vc_col-sm-3 blue-sidebar">
                    <?php get_template_part('templates/content','sidebar-block') ?>
                        <div class="vc_column-inner">
                            <div class="wpb_wrapper">
                                <section class="vc_cta3-container">
                                    <div class="vc_general vc_cta3 blue-cta small-cta vc_cta3-style-classic vc_cta3-shape-rounded vc_cta3-align-left vc_cta3-color-classic vc_cta3-icon-size-md vc_cta3-actions-bottom">
                                        <div class="vc_cta3_content-container">
                                            <div class="vc_cta3-content">
                                                <header class="vc_cta3-content-header">
                                                    <h2> <?php do_action('stimulanz_categoryname', get_the_ID(), 'advies_cat'); ?></h2>
                                                </header>
                                                <p> <?php apply_filters('stimulanz_acf_icon_text',get_the_ID()); ?></p>
                                            </div>
                                            <div class="vc_cta3-actions">
                                            <?php $title_for_orange_section_button = get_field('title_for_orange_section_button');
                                if ($title_for_orange_section_button):
                                    ?>
                                                <div class="vc_btn3-container  btn-white vc_btn3-center"><a style="background-color:#fcfcfc; color:#0087d7;" class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-custom" href="<?php echo get_field('url_of_orange_section_button'); ?>" title=""><?php echo $title_for_orange_section_button; ?></a></div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <div class="wpb_widgetised_column wpb_content_element social-media-aside">
                                    <div class="wpb_wrapper">
                                        <section class="widget a2a_share_save_widget-2 widget_a2a_share_save_widget details-social-icons">
                                            <h3><?php _e('Deel dit product', 'stimulansz'); ?></h3>
                                            <div class="a2a_kit a2a_kit_size_24 addtoany_list" style="line-height: 24px;">
                                                <?php
                                                    //addtoany social share
                                                    echo do_shortcode('[addtoany]');
                                                    ?>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

</article>