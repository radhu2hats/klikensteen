 <!-- container -->
<?php
$contact_person_id = get_the_id();
$contact_person_title = get_the_title();
$telefoon = get_field('telefoon');
$email_adres = get_field('e_mail_adres');
$contacttext = get_field('contact_detail_text');
$contactperson_author_title = get_field('contactperson_author_title',get_the_ID());
$name_tobe_displayed = substr($contact_person_title, 0, strpos($contact_person_title, ' '));
if($name_tobe_displayed==''):
    $name_tobe_displayed = $contact_person_title;
endif;
?>
<div class="fixed-container-wrapper expert-detail-page">
    <div class="fixed-container container">
        <div class="training_content_section training_listing_content_section expert_section">
            <div class="row">
                <div class="col-md-9 col-sm-8 col-xs-12">
                    <div class="title">
                        <h1><?php echo $contact_person_title; ?></h1>
                        <?php
                        $contactpersoon_rol = get_field('contactpersoon_rol');
                        if ($contactpersoon_rol):
                            ?>
                            <div class="post"><?php echo $contactpersoon_rol; ?><?php _e(' bij Stimulansz', 'stimulansz'); ?></div>
                        <?php endif; ?>
                    </div>
                    <?php if ($telefoon): ?>
                        <div class="telephone"><a href="tel:<?php echo $telefoon; ?>">T: <?php echo $telefoon; ?></a></div>
                    <?php endif; ?>
                    <?php if ($email_adres): ?>
                        <div class="email"><a href="mailto:<?php echo $email_adres; ?>">E: <?php echo $email_adres; ?></a></div>
                    <?php endif; ?>
                    <!-- @TODO: Assuming social media icons coming from plugin same as previous pages -->
                    <div class="social_media">
                        <?php
                        // check if the repeater field has rows of data
                        if (have_rows('social_media_icons_contactperson')):
                         echo '<ul>';     
                            // loop through the rows of data
                         while (have_rows('social_media_icons_contactperson')) : the_row();
                            echo '<li><a target="_blank" href="'.get_sub_field('social_icon_url_contactperson').'">'.get_sub_field('social_icon_contactperson').'</a></li>';
                        endwhile;
                        echo '</ul>';     
                    else :
                        // no rows found
                    endif;
                    ?>
                </div>
                <div class="into_text stimulansz_vc_blue_header">
                    <p><?php echo get_the_content(); ?></p>
                </div>
                <div class="contact_section">
                    <div class="title">
                     <?php if(is_singular( 'contactpersoon' ) ) { ?>
                        <h3>
                            <?php
                            
                            echo $contacttext;
                            ?>
                        </h3>
                    <?php } else { ?>
                       <h3>
                        <?php
                        
                        printf(esc_html__('Neem contact op met %s over', 'stimulansz'), $name_tobe_displayed);
                        ?>
                    </h3>
                <?php } ?>
            </div>
            <div class="choose_training_area">
                <?php
                $producten = get_posts(array(
                    'post_type' => 'product',
                    'meta_query' => array(
                        array(
                                        'key' => 'over_de_instructeurs', // name of custom field
                                        'value' => '"' . $contact_person_id . '"', // matches exaclty "123", not just 123. This prevents a match for "1234"
                                        'compare' => 'LIKE'
                                    )
                    )
                ));
                if ($producten):
                    ?>
                    <div class="col-md-4 col-sm-6 col-xs-12 training_block"> 
                        <div class="white_bg_block banner_block">
                            <div class="banner_content">
                                <div class="box-top-right-icon"><?php _e('Producten', 'stimulansz'); ?></div>
                                <div class="white_bg_block_container">
                                    <div class="training_blog_points">
                                        <ul>
                                            <?php foreach ($producten as $post) : ?> 
                                                <?php setup_postdata($post); ?>    
                                                <li><a href="<?php the_permalink(); ?>" ><?php the_title(); ?></a></li>                                                    
                                            <?php endforeach; ?>    
                                            <?php wp_reset_postdata(); ?>   
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php
                $tribe_events = get_posts(array(
                    'post_type' => 'tribe_events',
                    'meta_query' => array(
                        array(
                                        'key' => 'over_de_instructieurs_event', // name of custom field
                                        'value' => '"' . $contact_person_id . '"', // matches exaclty "123", not just 123. This prevents a match for "1234"
                                        'compare' => 'LIKE'
                                    )
                    )
                ));
                if ($tribe_events):
                    ?>
                    <div class="col-md-4 col-sm-6 col-xs-12 training_block"> 
                        <div class="white_bg_block banner_block">
                            <div class="banner_content">
                                <div class="box-top-right-icon"><?php _e('Events', 'stimulansz'); ?></div>
                                <div class="white_bg_block_container">
                                    <div class="training_blog_points">
                                        <ul>
                                            <?php foreach ($tribe_events as $post) : ?> 
                                                <?php setup_postdata($post); ?>    
                                                <li><a href="<?php the_permalink(); ?>" ><?php the_title(); ?></a></li>                                                    
                                            <?php endforeach; ?>    
                                            <?php wp_reset_postdata(); ?>   
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php
                $advies = get_posts(array(
                    'post_type' => 'advies',
                    'numberposts'=>-1,
                    'meta_query' => array(
                        array(
                                        'key' => 'over_de_instructeurs', // name of custom field
                                        'value' => '"' . $contact_person_id . '"', // matches exaclty "123", not just 123. This prevents a match for "1234"
                                        'compare' => 'LIKE'
                                    )
                    )
                ));
                if ($advies):
                    ?>
                    <div class="col-md-4 col-sm-6 col-xs-12 training_block"> 
                        <div class="white_bg_block banner_block">
                            <div class="banner_content">
                                <div class="box-top-right-icon"><?php _e('Advies', 'stimulansz'); ?></div>
                                <div class="white_bg_block_container">
                                    <div class="training_blog_points">
                                        <ul>
                                            <?php foreach ($advies as $post) : ?> 
                                                <?php setup_postdata($post); ?>    
                                                <li><a href="<?php the_permalink(); ?>" ><?php the_title(); ?></a></li>                                                    
                                            <?php endforeach; ?>    
                                            <?php wp_reset_postdata(); ?>   
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php
                $kennisbanken = get_posts(array(
                    'post_type' => 'kennisbanken',
                    'posts_per_page'=>-1, 
                    'numberposts'=>-1,
                    'meta_query' => array(
                        array(
                                        'key' => 'over_de_instructeurs', // name of custom field
                                        'value' => '"' . $contact_person_id . '"', // matches exaclty "123", not just 123. This prevents a match for "1234"
                                        'compare' => 'LIKE'
                                    )
                    )
                ));
                if ($kennisbanken):
                    ?>
                    <div class="col-md-4 col-sm-6 col-xs-12 training_block"> 
                        <div class="white_bg_block banner_block">
                            <div class="banner_content">
                                <div class="box-top-right-icon"><?php _e('kennisbanken', 'stimulansz'); ?></div>
                                <div class="white_bg_block_container">
                                    <div class="training_blog_points">
                                        <ul>
                                            <?php foreach ($kennisbanken as $post) : ?> 
                                                <?php setup_postdata($post); ?>    
                                                <li><a href="<?php the_permalink(); ?>" ><?php the_title(); ?></a></li>                                                    
                                            <?php endforeach; ?>    
                                            <?php wp_reset_postdata(); ?>   
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php
                $trainingen = get_posts(array(
                    'post_type' => 'training',
                    'meta_query' => array(
                        array(
                                        'key' => 'over_de_instructeurs', // name of custom field
                                        'value' => '"' . $contact_person_id . '"', // matches exaclty "123", not just 123. This prevents a match for "1234"
                                        'compare' => 'LIKE'
                                    )
                    )
                ));
                if ($trainingen):
                    ?>
                    <div class="col-md-4 col-sm-6 col-xs-12 training_block"> 
                        <div class="white_bg_block banner_block">
                            <div class="banner_content">
                                <div class="box-top-right-icon"><?php _e('Trainingen', 'stimulansz'); ?></div>
                                <div class="white_bg_block_container">
                                    <div class="training_blog_points">
                                        <ul>
                                            <?php foreach ($trainingen as $post) : ?> 
                                               <?php setup_postdata($post); ?>    
                                               <li><a href="<?php the_permalink(); ?>" ><?php the_title(); ?></a></li>                                                   
                                           <?php endforeach; ?>    
                                           <?php wp_reset_postdata(); ?>   
                                       </ul>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               <?php endif; ?>
           </div>
       </div>
       <?php if (empty($producten) && empty($tribe_events) && empty($advies) && empty($kennisbanken) && empty($trainingen)): ?>
       <script>jQuery(".contact_section").css("display", "none");</script>
   <?php endif; ?>
   <?php
   global $wp_query;
   $author_of_current_post = $wpdb->get_col("SELECT ID FROM {$wpdb->prefix}users where display_name='$contact_person_title'");
   $args = array(
    'author' => $author_of_current_post[0],
    'post_status' => 'publish',
    'post_type' => 'post',
    'posts_per_page' =>5,
);
   $all_blogs = new \WP_Query($args);
   
   ?>  
   <?php if ($all_blogs->have_posts() && $author_of_current_post): ?>
    <div class="recent_blogs_section">
        <div class="title"><h3>
           <?php if($contactperson_author_title){
            printf(esc_html__('Blogs van  %s', 'stimulansz'), $contactperson_author_title);
        }
        else
        { 
           printf(esc_html__('Blogs van  %s', 'stimulansz'), $name_tobe_displayed); ?>
       <?php } ?>
   </h3></div>
   <div class="choose_training_area">
    <div class="training_block">
        <div class="white_bg_block banner_block">
            <div class="banner_content">
                <div class="box-top-right-icon"><?php _e('Blogs', 'stimulansz'); ?></div>
                <div class="white_bg_block_container">
                    <div class="training_blog_points">
                        <ul>
                            <?php while ( $all_blogs->have_posts() ) : $all_blogs->the_post(); ?>
                                <?php //setup_postdata($post); ?>
                                <li>
                                    <a href="<?php the_permalink(); ?>" class="instructer_blog_title"><?php the_title(); ?></a>
                                    <div class="blog_details">
                                        <div class="intro">
                                            <?php
                                            $post_date = get_the_date();
                                            if ($post_date):
                                                ?>  
                                                <div class="date"><?php echo $post_date; ?></div>
                                            <?php endif; ?>
                                            
                                            <div class="content">
                                                <?php 
                                                $intro = get_field('additional_banner_text');
                                                if(isset($intro) && !empty($intro)):
                                                    echo $intro.' <a class="stimulansz-home-more-link" href="' . get_permalink() . '">' . esc_html__( 'Read more', 'stimulansz' ) . '</a>';
                                            else:
                                                the_excerpt();
                                            endif;
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php endwhile; ?>  
                        <li></li>
                        
                        <?php    
                        if (  $all_blogs->max_num_pages > 1 ){
                          $maxpag = trim($all_blogs->max_num_pages);
                                                                  echo '<div class="stimulansz_loadmore" author_id="'.$author_of_current_post[0].'" max_page="'.$maxpag.'"> Meer blogartikelen </div>'; // you can use <a> as well
                                                              }
                                                              
                                                              ?>
                                                              <?php 
                                                              wp_reset_postdata();  ?>
                                                          </ul>
                                                          <ul>
                                                             
                                                          </ul>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  
                              </div>
                          <?php endif; ?>        
                      </div>
                      
                      <?php $contact_preson_full_image = get_field('full_image', get_the_ID());
                      if(isset($contact_preson_full_image['url'])):
                       ?>
                       <div class="col-md-3 col-sm-4 hidden-xs expert_image">
                        <img src="<?php echo $contact_preson_full_image['url']; ?>">
                    </div>   
                <?php endif; ?>  
                
            </div>
        </div>
    </div>      
</div>