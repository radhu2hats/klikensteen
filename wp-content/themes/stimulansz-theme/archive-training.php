<?php 
$plugin_dir = ABSPATH . 'wp-content/plugins/wpsolr-pro/wpsolr/core/ajax_solr_services.php';
require_once($plugin_dir);

$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$traing_args = array(
        'post_type' => 'training',
        'posts_per_page' =>-1,
        'orderby'   => 'DESC',
        'post_status ' => 'publish',
        'paged' => $paged
    );
 $training_query = new \ WP_Query($traing_args);

/**
 * Custom object array
 * which is define in wpslor plugin in wp-content/plugins/wpsolr-pro/wpsolr/core/ajax_solr_services.php
 */
$obj = stimulansz_getObj();
if (empty($_GET['wpsolr_fq'])) {
    //Default Load results
     $obj->set_filter_query_fields(array('0'=>"type:training"));
}else{
    //Get Query string object value
    $cate=$_GET['wpsolr_fq']['0'];
   if(isset($cate)):
        $cat_explode =  explode(':', $cate);
        else:
         $cat_explode = '';   
    endif;
    if (empty($cat_explode[1])) {
		$obj->set_filter_query_fields(array('0' => "type:training"));
        
    } else {
        $obj->set_filter_query_fields($_GET['wpsolr_fq']);
    }
}
?>

<!-- container -->
<div class="fixed-container-wrapper">
    <div class="fixed-container container">
        <section class="training_content_section training_listing_content_section">
            <div class="row">
                <h2 class="archive_post col-xs-12"><?php _e('Training Archive','stimulansz');?></h2>
                 <?php
                        if (isset($obj)) {
                            stimulansz_custom_search_indexed_data($obj);
                        }
                        ?>
                </div>
            </div>
        </section>
    </div>
</div>
    
<!-- .container -->