var currentPage = 1;
var LeerdoelData = jQuery("#leerdoelData");
var leerdoel = jQuery("#leerdoel");
var uitvoering = jQuery("#uitvoering");
var werkervaring_niveau = jQuery("#werkervaring_niveau");
var selectedOptions = jQuery('#selectedOptions');

function loadLeerdoelData(paged_num) {
  console.log(paged_num);
  var leerdoel = jQuery("#leerdoel").val();
  var uitvoering = jQuery("#uitvoering").val();
  var werkervaring_niveau = jQuery("#werkervaring_niveau").val();
   

  var posts_per_page = 7;
  var data = {
    action: "get_training_items",
    posts_per_page: posts_per_page,
    paged: paged_num,
    leerdoel: leerdoel,
    uitvoering: uitvoering,
    werkervaring_niveau: werkervaring_niveau,
    parentWerkveldSlug: parentWerkveldSlug
  };

  jQuery.ajax({
    type: "POST",
    url: "/wp-admin/admin-ajax.php",
    dataType: "json",
    data: data,
    success: function (data) {
      
      // total = parseInt(data.pagination.total);
      // current = parseInt(data.pagination.current);
      
      jQuery("#leerdoelData").html(data.training_list);
      jQuery('.card-block').matchHeight();
      jQuery('.card-block .title_wrap').matchHeight();
      jQuery('.blockquote').matchHeight();
      jQuery('.post-repeater .item').matchHeight();

    },
  });
}


function loadLeerdoelMobileData(paged_num) {
  var leerdoel = jQuery(".leerdoel-mob-filter:checkbox:checked").map(function(){
    return jQuery(this).val();
  }).get();

  var uitvoering = jQuery(".uitvoering-mob-filter:checkbox:checked").map(function(){
    return jQuery(this).val();
  }).get();

  var werkervaring_niveau = jQuery(".werkervaring_niveau-mob-filter:checkbox:checked").map(function(){
    return jQuery(this).val();
  }).get();


  var posts_per_page = 7;
  var data = {
    action: "get_training_items",
    posts_per_page: posts_per_page,
    paged: paged_num,
    leerdoel: leerdoel,
    uitvoering: uitvoering,
    werkervaring_niveau: werkervaring_niveau,
    parentWerkveldSlug: parentWerkveldSlug
  };

  jQuery.ajax({
    type: "POST",
    url: "/wp-admin/admin-ajax.php",
    dataType: "json",
    data: data,
    success: function (data) {
      
      // total = parseInt(data.pagination.total);
      // current = parseInt(data.pagination.current);
      
      jQuery("#leerdoelData").html(data.training_list);
      jQuery('.card-block').matchHeight();
      jQuery('.card-block .title_wrap').matchHeight();
      jQuery('.blockquote').matchHeight();
      jQuery('.post-repeater .item').matchHeight();

    },
  });
}

function updateSelectedOptions() {
  var leerdoel = jQuery("#leerdoel");
  var uitvoering = jQuery("#uitvoering");
  var werkervaring_niveau = jQuery("#werkervaring_niveau");
  var selectedOptions = jQuery('#selectedOptions');
  selectedOptions.empty();
  if(leerdoel.val()){
    jQuery('#leerdoel option:selected').each(function(){ selectedOptions.append('<div class="option" data-select="#leerdoel" data-value="'+jQuery(this).val()+'">'+jQuery(this).text()+' <i class="fas fa-times"></i></div>'); });
  }
  if(uitvoering.val()){
    jQuery('#uitvoering option:selected').each(function(){ selectedOptions.append('<div class="option" data-select="#uitvoering" data-value="'+jQuery(this).val()+'">'+jQuery(this).text()+' <i class="fas fa-times"></i></div>'); });
  }
  if(werkervaring_niveau.val()){
    jQuery('#werkervaring_niveau option:selected').each(function(){ selectedOptions.append('<div class="option" data-select="#werkervaring_niveau" data-value="'+jQuery(this).val()+'" >'+jQuery(this).text()+' <i class="fas fa-times"></i></div>'); });
  }
  if(werkervaring_niveau.val() || uitvoering.val() || leerdoel.val()){
    jQuery('.status-wrapper').addClass('visible');
  } else {
    jQuery('.status-wrapper').removeClass('visible');
  }
}

function updateMobileSelectedOptions() {
  var leerdoel = jQuery(".leerdoel-mob-filter:checkbox:checked").map(function(){
    return jQuery(this).attr('data-text');
  }).get();

  console.log(leerdoel.length);

  var uitvoering = jQuery(".uitvoering-mob-filter:checkbox:checked").map(function(){
    return jQuery(this).val();
  }).get();

  console.log(uitvoering);

  var werkervaring_niveau = jQuery(".werkervaring_niveau-mob-filter:checkbox:checked").map(function(){
    return jQuery(this).val();
  }).get();

  console.log(werkervaring_niveau);

  var selectedOptions = jQuery('#selectedOptions');
  selectedOptions.empty();
  if(leerdoel.length){
    jQuery.each( leerdoel, function( index, value ){
      console.log(value);
      selectedOptions.append('<div class="option" data-select="#leerdoel">'+value+' <i class="fas fa-times"></i></div>');
    });
  
  }
  
  if(uitvoering.val()){
    jQuery('#uitvoering option:selected').each(function(){ selectedOptions.append('<div class="option" data-select="#uitvoering" data-value="'+jQuery(this).val()+'">'+jQuery(this).text()+' <i class="fas fa-times"></i></div>'); });
  }
  if(werkervaring_niveau.val()){
    jQuery('#werkervaring_niveau option:selected').each(function(){ selectedOptions.append('<div class="option" data-select="#werkervaring_niveau" data-value="'+jQuery(this).val()+'" >'+jQuery(this).text()+' <i class="fas fa-times"></i></div>'); });
  }

  if(werkervaring_niveau.length || uitvoering.length || leerdoel.length){
    jQuery('.status-wrapper').addClass('visible');
  } else {
    jQuery('.status-wrapper').removeClass('visible');
  }
}

jQuery(document).on('click', '.status-wrapper .selected-options .option i' ,function(e){
  var itemToRemove = jQuery(this).parent().data('select');
  var itemValue = jQuery(this).parent().data('value');
  
  
  
  jQuery(itemToRemove).next(".multi-select-container").find('input:checkbox[value="' + itemValue + '"]').prop('checked',false);
  jQuery(itemToRemove+" option[value='"+itemValue+"']").prop("selected", false);
  jQuery(itemToRemove).change();

  jQuery("#leerdoelData").html("Loading...");
  e.preventDefault();
});

jQuery(document).on('click', '.status-wrapper .reset-wrap .reset' ,function(e){
  var leerdoel = jQuery("#leerdoel");
  var uitvoering = jQuery("#uitvoering");
  var werkervaring_niveau = jQuery("#werkervaring_niveau");
  
  leerdoel.val("");
  uitvoering.val("");
  werkervaring_niveau.val("");
  
  jQuery("#leerdoel").next(".multi-select-container").find('input').prop('checked',false);
  jQuery("#uitvoering").next(".multi-select-container").find('input').prop('checked',false);
  jQuery("#werkervaring_niveau").next(".multi-select-container").find('input').prop('checked',false);
  
  leerdoel.change();
  uitvoering.change();
  werkervaring_niveau.change();

  loadLeerdoelData(parseInt(1));
  updateSelectedOptions();
  jQuery("#leerdoelData").html("Loading...");
  e.preventDefault();
});

jQuery(document).ready(function(){
  jQuery(document).on('change', '#leerdoel' ,function(e){
    loadLeerdoelData(parseInt(1));
    updateSelectedOptions();
    jQuery("#leerdoelData").html("Loading...");
    e.preventDefault();
  });

jQuery(document).on('change', '#uitvoering' ,function(e){
  loadLeerdoelData(parseInt(1));
  updateSelectedOptions();
  jQuery("#leerdoelData").html("Loading...");
  e.preventDefault();
});

jQuery(document).on('change', '#werkervaring_niveau' ,function(e){
  loadLeerdoelData(parseInt(1));
  updateSelectedOptions();
  jQuery("#leerdoelData").html("Loading...");
  e.preventDefault();
});

jQuery(document).on('click', '.page-numbers a' ,function(e){
  // var currentPage = jQuery(this).text();
  // console.log(currentPage);
  url = jQuery(this).attr( "href" ),
							page = url.match( /\d+/ ), // Get the page number
							currentPage = page[0],
  loadLeerdoelData(parseInt(currentPage));
  jQuery("#leerdoelData").html("Loading...");
  e.preventDefault();
});

updateSelectedOptions();

});