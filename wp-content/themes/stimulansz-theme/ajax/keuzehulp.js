jQuery('#keuzehulpSlider').slick({
  dots: false,
  arrows: false,
  fade: true,
  infinite: false,
  speed: 300,
  draggable: false,
  swipeToSlide: false,
  slidesToShow: 1,
  adaptiveHeight: false,
  initialSlide: 0,
  centerMode: false
});


function gotoSlide(e, count){
  console.log('s');
  jQuery('#keuzehulpSlider').slick('slickGoTo', parseInt(count));
  e.preventDefault();
}

$("#keuzehulp").find('.btn-orange').attr("disabled", "true");
$("#keuzehulp").find('.btn-orange').attr("style",'color:#4f7291');
$("#keuzehulp").find('.btn-orange').removeClass("btn-orange");

$training_werkveld_button = $("#keuzehulp .werkveld").find('.keuzehulp-next');
$training_leerdoel_button = $("#keuzehulp .leerdoel").find('.keuzehulp-next');
$training_huidige_button  = $("#keuzehulp .functie").find('.keuzehulp-next');
$training_final_button    = $("#keuzehulp .werkervaring").find('.keuzehulp-next');

$training_werkveld = $("#keuzehulp").find('input[name="training_werkveld[]"]').click(function() {
  
  if($training_werkveld.is(":checked")){
    $training_werkveld_button.removeAttr("disabled");
    $training_werkveld_button.removeAttr("style");
    $training_werkveld_button.addClass("btn-orange");
  }else{
    $training_werkveld_button.attr("disabled", "true");
    $training_werkveld_button.attr("style",'color:#4f7291');
    $training_werkveld_button.removeClass("btn-orange");
  } 


});

$training_leerdoel = $("#keuzehulp").find('input[name="training_leerdoel[]"]').click(function() {
  
  if($training_leerdoel.is(":checked")){
    $training_leerdoel_button.removeAttr("disabled");
    $training_leerdoel_button.removeAttr("style");
    $training_leerdoel_button.addClass("btn-orange");
  }else{
    $training_leerdoel_button.attr("disabled", "true");
    $training_leerdoel_button.attr("style",'color:#4f7291');
    $training_leerdoel_button.removeClass("btn-orange");
  } 


});

$training_huidige = $("#keuzehulp").find('input[name="training_huidige[]"]').click(function() {
  
  if($training_huidige.is(":checked")){
    $training_huidige_button.removeAttr("disabled");
    $training_huidige_button.removeAttr("style");
    $training_huidige_button.addClass("btn-orange");
  }else{
    $training_huidige_button.attr("disabled", "true");
    $training_huidige_button.attr("style",'color:#4f7291');
    $training_huidige_button.removeClass("btn-orange");
  } 


});

$training_final = $("#keuzehulp").find('input[name="training_werkervaring[]"]').click(function() {
  
  if($training_final.is(":checked")){
    $training_final_button.removeAttr("disabled");
    $training_final_button.removeAttr("style");
    $training_final_button.addClass("btn-orange");
  }else{
    $training_final_button.attr("disabled", "true");
    $training_final_button.attr("style",'color:#4f7291');
    $training_final_button.removeClass("btn-orange");
  } 


});