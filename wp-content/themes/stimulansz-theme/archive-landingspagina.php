<?php
$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
$landingpagena_args = array(
    'post_type' => 'landingspagina',
    'posts_per_page' => -1,
    'orderby' => 'DESC',
    'post_status ' => 'publish',
    'paged' => $paged
);
$landing_pagina_query = new \ WP_Query($landingpagena_args);
?>
<?php
$author = get_the_author(get_the_id());
$banner_image = get_field('page_banner', get_the_id());
?>


<!-- container -->
<div class="fixed-container-wrapper landingpage">
    <div class="fixed-container container">
        <section class="training_content_section training_listing_content_section">
            <div class="row">
                <h2 class="archive_post col-xs-12"><?php _e('Landingspagina Archive', 'stimulansz'); ?></h2>
                 <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="section_title">
                        <div class="choose_training_area">
                                <?php if ($landing_pagina_query->have_posts()) : while ($landing_pagina_query->have_posts()) : $landing_pagina_query->the_post(); ?> 
                                    <div class="col-md-4 col-sm-6 col-xs-12 training_block">
                                        <div class="white_bg_block banner_block">
                                            <?php do_action('stimulanz_categoryname', get_the_ID(), 'landingspagina_cat'); ?>
                                           
                                                <?php
                                                if (has_post_thumbnail()) {
                                                    echo '<div class="banner_image">';
                                                      echo '<a href="'.get_permalink().'">';
                                                        the_post_thumbnail('stimulansz_related_post_image', ['class' => 'img-responsive responsive-full', 'title' => get_the_title()]);
                                                     echo '</a></div>'; 
                                                }
                                                
                                                ?>

                                            <div class="banner_content">
                                                <a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4></a>
                                                <div class="white_bg_block_container">
                                                    <div class="training_blog_points">
        <?php the_excerpt(); ?>
                                                    </div>
                                                    <div class="training_blog_points traning_schedule">
                                                        <?php apply_filters('stimulanz_acf_icon_text',get_the_ID()); ?>
        <?php
        $feedback_rating = apply_filters('stimulanz_get_average_preview', get_the_ID());
        if (isset($feedback_rating['total_comment']) && !empty($feedback_rating['total_comment'])):
            ?>
                                                            <div><span class="reviews"><?php if (isset($feedback_rating['total_comment'])) {
                echo $feedback_rating['total_comment'];
            } ?> </span><span><?php _e('beoordelingen', 'stimulansz'); ?></span></div> <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
    <?php endwhile;
endif;
?>
                        </div>
<?php
// do_action('stimulanz_pagination_number',3);
?>
                    </div>
                </div>
            </div>
    </div>
</div>
</div>
<!-- .container -->