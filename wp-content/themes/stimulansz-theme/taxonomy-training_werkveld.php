<?php

/**
 * Template Name: Taxonomy Werkveld Template
 */
$parentTerm = get_queried_object();
$site_url = site_url();
$uploadUrl = wp_upload_dir();
$posts_per_page = 7;
?>

<script>
  var parentWerkveldSlug = "<?php echo $parentTerm->slug; ?>";
</script>

<link rel='stylesheet' id='js_composer' href='<?php echo $site_url; ?>/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=6.0.5' type='text/css' media='all' />
<link rel='stylesheet' id='vc_tta_style-css' href='<?php echo $site_url; ?>/wp-content/plugins/js_composer/assets/css/js_composer_tta.min.css?ver=6.0.5' type='text/css' media='all' />
<script type='text/javascript' src='<?php echo $site_url; ?>/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js?ver=6.0.5'></script>
<script type='text/javascript' src='<?php echo $site_url; ?>/wp-content/plugins/js_composer/assets/lib/vc_accordion/vc-accordion.min.js?ver=6.0.5'></script>
<script type='text/javascript' src='<?php echo $site_url; ?>/wp-content/plugins/js_composer/assets/lib/vc-tta-autoplay/vc-tta-autoplay.min.js?ver=6.0.5'></script>
<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/ajax/scripts.js'></script>

<div class="page-template">


  <?php
  $args = array(
    'post_type' => 'training',
    'paged' => 1,
    'posts_per_page' => $posts_per_page,
    'post_status' => 'publish',
    'orderby' => 'menu_order',
    'order' => 'DESC',
    'tax_query' => array(
      array(
        'taxonomy' => 'training_werkveld',
        'field' => 'slug',
        'terms' => $parentTerm->slug,
      ),
    ),
  );
  $queries = new WP_Query($args);
  ?>



  <div class="display-filter-group" id="display_filter_group" style="display: none;">
    <div class="filter-bar">
      <label>Selecteer filters</label>
      <div class="filter-controls">
        <button id="options_selected"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/tick-green.svg" alt="tick-green"></button>
        <button id="close_filter"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/cross-green.svg" alt="cross-green"></button>
      </div>
    </div>
    <div class="filter-content">

      <?php
      if (taxonomy_exists('leerdoel')) :
        $leerdoel = get_terms('leerdoel', array('fields' => 'all', 'parent' => 0, 'hide_empty' => false));
      ?>

        <div class="filter-boxes-mobile">
          <div class="filter-item-mobile" id="leerdoel_div">
            <label class="mobile-filter-label">Leerdoel</label>
            <!-- <select id="leerdoel_mob" name="leerdoel_mob" class="white_bg_block " multiple> -->
            <ul class="filter-lists">
              <?php
              $count = 0;
              foreach ($leerdoel as $cat) :
                $counter = $count++; ?>

                <li>
                  <input type="checkbox" data-text="<?php echo $cat->name; ?>"  class="mobile-filter-class leerdoel-mob-filter" value="<?php echo $cat->slug; ?>" name="mobile_filter_l" id="mobile_filter_l<?php echo $counter; ?>"> <label for="mobile_filter_l<?php echo $counter; ?>"><?php echo $cat->name; ?></label><span><?php echo $cat->count ?></span>
                </li>

              <?php
              endforeach;
              ?>
            </ul>
            <input class="clear-leerdoel-filter-btn clear-filter-btn" type="button" value="Wis selectie">
          </div>
        <?php endif; ?>

        <?php
        if (taxonomy_exists('uitvoering')) :
          $uitvoering = get_terms('uitvoering', array('fields' => 'all', 'parent' => 0, 'hide_empty' => false));
        ?>
          <div class="filter-item-mobile">
            <label class="mobile-filter-label">Uitvoering</label>
            <ul class="filter-lists">
              <?php
              $count = 0;
              foreach ($uitvoering as $cat) :
                $counter = $count++;
              ?>
                <li>
                  <input type="checkbox" class="mobile-filter-class uitvoering-mob-filter" value="<?php echo $cat->slug; ?>" name="mobile_filter_u" id="mobile_filter_u<?php echo $counter; ?>"> <label for="mobile_filter_u<?php echo $counter; ?>"><?php echo $cat->name; ?></label> <span><?php echo $cat->count ?></span>
                </li>
              <?php
              endforeach;
              ?>
            </ul>
            <input class="clear-uitvoering-filter-btn clear-filter-btn" type="button" value="Wis selectie">
          </div>
        <?php endif; ?>

        <?php
        if (taxonomy_exists('werkervaring_niveau')) :
          $werkervaring_niveau = get_terms('werkervaring_niveau', array('fields' => 'all', 'parent' => 0, 'hide_empty' => false));
        ?>
          <div class="filter-item-mobile">
            <label class="mobile-filter-label">Werkervaring</label>
            <ul class="filter-lists">

              <?php
              $count = 0;
              foreach ($werkervaring_niveau as $cat) :
                $counter = $count++;
              ?>
                <li>
                  <input type="checkbox" class="mobile-filter-class werkervaring_niveau-mob-filter" value="<?php echo $cat->slug; ?>" name="mobile_filter" id="mobile_filter<?php echo $counter; ?>"> <label for="mobile_filter<?php echo $counter; ?>"><?php echo $cat->name; ?></label><span><?php echo $cat->count ?></span>
                </li>
              <?php
              endforeach;
              ?>
            </ul>
            <input class="clear-werkervaring-filter-btn clear-filter-btn" type="button" value="Wis selectie">
          </div>
        </div>
      <?php endif; ?>

    </div>
  </div>

  <section class="section workfield-wrapper blue top-section-block">
    <div class="vc_row wpb_row vc_row-fluid container">
      <div class="wpb_column vc_column_container vc_col-sm-9" id="content-mainouterblock">

        <div class="section training-intro training-cat-intro space-bottom-100 margin-sectionbottom">
          <div class="vc_row wpb_row vc_row-fluid container">
            <div class="wpb_column vc_column_container vc_col-sm-8">
              <div class="left-block">
                <h1><span class="workfield-title-dot" style="background-color:<?php the_field('werkveld_dot_color', $parentTerm); ?>;" ></span><?php echo $parentTerm->name; ?></h1>
                <div class="intro-msg block">
                  <h4><?php echo term_description($parentTerm->term_id); ?></h4>
                </div>
              </div>
            </div>

          </div>
        </div>


        <div class="wpb_column vc_column_container vc_col-sm-12 padding-left-60">
          <div class="filter-wrapper">
            <h2>Filter op</h2>
            <hr />

            <div class="filter-option" style="display: none;">
              <div class="filter-boxes">
                <div class="filter-item" id="mob_filter_div">
                  <div class="filter-select-wrapper">
                    <a href="#" id="display_filter_block" class="white_bg_block " onclick="showDiv()">Selecteer filters <span id="filter_count"></span>
                    </a>
                  </div>
                </div>
              </div>
              <div class="status-wrapper">
     
                <div class="reset-wrap reset-wrapper">
                  <button class="reset btn-link">Verwijder alle filters</button>
                </div>
              </div>
            </div>

            <div class="filter-options">

              <?php
              if (taxonomy_exists('leerdoel')) :
                $leerdoel = get_terms('leerdoel', array('fields' => 'all', 'parent' => 0, 'hide_empty' => false));
              ?>

                <div class="filter-boxes">
                  <div class="filter-item" id="leerdoel_div">
                    <label for="leerdoel" class="filter-desk-label">Leerdoel</label>
                    <select id="leerdoel" name="leerdoel" class="white_bg_block " multiple>
                      <?php

                      foreach ($leerdoel as $cat) :
                      ?>
                        <option data-count="<?php echo get_trainings_count($cat->slug, 'leerdoel', $parentTerm); ?>" value="<?php echo $cat->slug; ?>"><?php echo $cat->name; ?><label></label></option>
                      <?php
                      endforeach;
                      ?>
                    </select>
                  </div>
                <?php endif; ?>

                <?php
                if (taxonomy_exists('uitvoering')) :
                  $uitvoering = get_terms('uitvoering', array('fields' => 'all', 'parent' => 0, 'hide_empty' => false));
                ?>
                  <div class="filter-item">
                    <label for="uitvoering" class="filter-desk-label">Uitvoering</label>
                    <select id="uitvoering" name="uitvoering" class="white_bg_block " multiple>
                      <?php

                      foreach ($uitvoering as $cat) :
                      ?>
                        <option data-count="<?php echo get_trainings_count($cat->slug, 'uitvoering', $parentTerm); ?>" value="<?php echo $cat->slug; ?>"><?php echo $cat->name; ?></option>
                      <?php
                      endforeach;
                      ?>
                    </select>
                  </div>
                <?php endif; ?>

                <?php
                if (taxonomy_exists('werkervaring_niveau')) :
                  $werkervaring_niveau = get_terms('werkervaring_niveau', array('fields' => 'all', 'parent' => 0, 'hide_empty' => false));
                ?>
                  <div class="filter-item">
                    <label for="werkervaring_niveau" class="filter-desk-label">Werkervaring</label>
                    <select id="werkervaring_niveau" name="werkervaring_niveau" class="white_bg_block " multiple>

                      <?php

                      foreach ($werkervaring_niveau as $cat) :
                      ?>
                        <option data-count="<?php echo get_trainings_count($cat->slug, 'werkervaring_niveau', $parentTerm); ?>" value="<?php echo $cat->slug; ?>"><?php echo $cat->name; ?></option>
                      <?php
                      endforeach;
                      ?>
                    </select>
                  </div>
                </div>
              <?php endif; ?>
              <div class="status-wrapper">
                <div class="selected-options" id="selectedOptions"></div>
                <div class="reset-wrap">
                  <button class="reset btn-link">Verwijder alle filters</button>
                </div>
              </div>
            </div>
          </div>
          <div class="wpb_column vc_column_container vc_col-sm-12 filter-search-result" id="leerdoelData">
            <h2>Er zijn <strong> <?php echo $queries->found_posts; ?> trainingen </strong> gevonden</h2>
            <div class="vc_row wpb_row vc_row-fluid listing-blocks-outer   post-repeater">
              <?php
              $count = 1;
              foreach ($queries->posts as $query) :
                $image = wp_get_attachment_url(get_post_thumbnail_id($query->ID), 'thumbnail');
                // $image = get_field('background_image', $query->ID);
              ?>

                <?php if ($count == 1) : 
                  $featured_training = get_field('featured_content_left', $parentTerm);             
                $image_feature = wp_get_attachment_url(get_post_thumbnail_id($featured_training->ID), 'thumbnail');
                ?>
                  <div class="wpb_column vc_column_container vc_col-sm-8 item">
                    <div class="card card-block white_bg_block post-list-item wide">
                    <a href="<?php echo get_permalink($featured_training->ID); ?>">
                      <div class="card-inner">
                        <div class="img-wrap large-bllk">
                        <?php
                            $featured_label = get_field('training_post_featured_label',$featured_training->ID);
                            if ($featured_label) : ?>
                              <label class="featured-sticker"> <?php echo $featured_label[0]; ?></label>
                            <?php endif; ?>
                           <img src="<?php  echo $image_feature; ?>" alt="<?php echo $featured_training->post_title; ?>" onerror="src='<?php echo $uploadUrl['baseurl']; ?>/placeholder-large.png'">
                          <!-- <img src="<?php //echo $image['sizes']['stimulansz_card_wide']; ?>" /> -->
                          <?php
                            $new_label = get_field('training_post_new_label',$featured_training->ID);
                            if ($new_label) : ?>
                              <label class="new-sticker"> <?php echo $new_label[0]; ?></label>
                            <?php endif; ?>
                        </div>
                        <div class="cnt-wrap">
                          <div class="title_wrap">
                            <div class="cat">
                              <?php
                               $selectedTerms = [];
                               foreach (wp_get_post_terms($featured_training->ID, 'training_cat') as $term) {
                                 $selectedTerms[] = $term->name;
                               }
                               echo implode(', ', $selectedTerms);
                              ?>
                            </div>
                            <div class="title"><?php echo $featured_training->post_title ?></div>
                            <p><?php echo $featured_training->post_excerpt; ?></p>
                          </div>
                          <div class="sub_title_wrap">
                            <div class="sub_title sub_title_1"><i class="far fa-clock"></i> <?php echo get_field('duration', $featured_training->ID) ?></div>
                           <span class="readmore" tabindex="0"> <i class="fas fa-arrow-right">&nbsp;</i> </span>
                          </div>
                        </div>
                      </div>
                    </a>
                    </div>
                  </div>
                  <div class="wpb_column vc_column_container vc_col-sm-4 item">
                    <div class="vc_general vc_cta3 pink-cta vc_cta3-style-classic vc_cta3-shape-rounded vc_cta3-align-center vc_cta3-color-classic vc_cta3-icon-size-md vc_cta3-icons-top vc_cta3-actions-bottom vc_custom_1617022095910">
                      <div class="vc_cta3-icons">
                        <div class="vc_icon_element vc_icon_element-outer vc_icon_element-align-left">
                          <div class="vc_icon_element-inner vc_icon_element-color-blue vc_icon_element-size-md vc_icon_element-style- vc_icon_element-background-color-grey"><span class="vc_icon_element-icon vc_li vc_li-paperplane"></span></div>
                        </div>
                      </div>
                      <div class="vc_cta3_content-container">
                        <?php
                        $featured_static_content_title = get_field('featured_static_content_title', $parentTerm);
                        $featured_static_content_description = get_field('featured_static_content_description', $parentTerm);
                        $featured_static_content_button = get_field('featured_static_content_button', $parentTerm);
                        ?>
                        <div class="vc_cta3-content">
                          <header class="vc_cta3-content-header">
                            <h3><?php echo $featured_static_content_title; ?></h3>
                            <h4><?php echo $featured_static_content_description; ?></h4>
                          </header>
                        </div>
                        <div class="vc_cta3-actions">
                          <div class="vc_btn3-container  btn-white vc_btn3-center"><a style="background-color:#ffffff; color:#af197d;" class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-custom" href="<?php echo $featured_static_content_button['url']; ?>" title=""><?php echo $featured_static_content_button['title']; ?></a></div>
                        </div>
                      </div>
                    </div>
                  </div>
                <?php else : ?>

                  <div class="wpb_column vc_column_container vc_col-sm-4 item">
                    <div class="card card-block white_bg_block post-list-item">
                      <a href="<?php echo get_permalink($query->ID); ?>">
                      <div class="card-inner">
                        <div class="img-wrap">
                          <img src="<?php  echo $image;  ?>" alt="<?php  echo $query->post_title  ?>"  onerror="src='<?php echo $uploadUrl['baseurl']; ?>/placeholder-large.png'">
                          <!-- <img src="<?php //echo $image['sizes']['stimulansz_card_image']; ?>" /> -->

                          <?php
                            $new_label = get_field('training_post_new_label',$query->ID);
                            if ($new_label) : ?>
                              <label class="new-sticker"> <?php echo $new_label[0]; ?></label>
                            <?php endif; ?>
                        </div>
                        <div class="cnt-wrap">
                          <div class="title_wrap">
                            <div class="cat">
                              <?php
                              $selectedTerms = [];
                              foreach (wp_get_post_terms($query->ID, 'training_cat') as $term) {
                                $selectedTerms[] = $term->name;
                              }
                              echo implode(', ', $selectedTerms);
                              ?>
                            </div>
                            <div class="title"><?php echo $query->post_title ?></div>
                            <p><?php echo $query->post_excerpt; ?></p></p>
                            <?php   $new_label = get_field('training_post_new_label');
                           ?>
                            <p> <?php  print_r($new_label); ?></p>
                          </div>
                          <div class="sub_title_wrap">
                            <div class="sub_title sub_title_1"><i class="far fa-clock"></i> <?php echo get_field('duration', $query->ID) ?></div>
                            <span class="readmore" tabindex="0"> <i class="fas fa-arrow-right">&nbsp;</i> </span>
                          </div>
                        </div>
                      </div>
                      </a>
                    </div>
                  </div>
                <?php endif; ?>

              <?php
                $count++;
              endforeach;
              ?>
            </div>
            <?php wpex_pagination($queries->max_num_pages, 1); ?>
          </div>
        </div>
      </div>

      <div class="wpb_column vc_column_container vc_col-sm-3 sticky-sidebar" style="position: sticky; top:250px;">
        <div class="vc_column-inner">
          <div class="wpb_wrapper">
            <section class="vc_cta3-container" id="sticky-content">
              <div class="vc_general vc_cta3 blue-cta overflow-visible small-cta vc_cta3-style-classic vc_cta3-shape-rounded vc_cta3-align-left vc_cta3-color-classic vc_cta3-icon-size-md vc_cta3-actions-bottom">
                <div class="vc_cta3_content-container">
                  <div class="vc_cta3-content">
                    <?php
                    $person_object = get_field('contact_content', $parentTerm);
                    $person_icon = wp_get_attachment_url(get_post_thumbnail_id($person_object->ID), 'thumbnail');
                    ?>
                    <div class="avatar-wrap img-blockouter">
                      <img src="<?php echo $person_icon ?>" alt="img" style="max-width : 200px">
                    </div>
                    <header class="vc_cta3-content-header">
                      <h3 id="vragen-of-advies-nodig"><span style="vertical-align: inherit;">Vragen of advies nodig?</span></h3>
                    </header>
                    <p><?php print get_field('contactpersoon_rol', $person_object->ID) . '  ' . $person_object->post_title; ?> helpt u graag verder</p>
                    <footer class="vc_cta3-content-footer vc_cta3-content-center">
                      <span>
                        <a href="tel:<?php echo (get_field('telefoon', $person_object)); ?>" class="phone"><?php echo (get_field('telefoon', $person_object)); ?></a>
                        <a href="mailto:<?php echo (get_field('e_mail_adres', $person_object)); ?>" class="email"><?php echo (get_field('e_mail_adres', $person_object)); ?></a>
                      </span>
                    </footer>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>


    </div>

  </section>
  <?php
  if (get_field('about_content', $parentTerm)) :
  ?>
    <section class="vc_section section about-history white" id="sticky-stoperblk">
      <div class="vc_row wpb_row vc_row-fluid container">
        <div class="contact-details-outer wpb_column vc_column_container vc_col-sm-2/5">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <div class="wpb_text_column wpb_content_element ">
                <div class="wpb_wrapper">
                  <h2 id="over-stimulansz"><?php echo get_field('about_header', $parentTerm); ?></h2>
                  <p><?php echo get_field('about_content', $parentTerm); ?></p>
                </div>
              </div>
              <div class="wpb_text_column wpb_content_element  vc_custom_1617021612303">
                <div class="wpb_wrapper">
                  <?php
                  $read_more = get_field('read_more', $parentTerm);
                  if ($read_more) {
                    $read_more_url = $read_more['url'];
                    $read_more_title = $read_more['title'];
                    $read_more_target = $read_more['target'] ? $read_more['target'] : '_self';
                  }
                  ?>
                  <p style="text-align: left;"><a href="<?php echo esc_url($read_more_url); ?>" id="lees_meer_link" class="read-more-btn-link" target="<?php echo esc_attr($read_more_target); ?>"><i class="fas fa-plus"></i>&nbsp;<?php echo esc_html($read_more_title); ?></a></p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <?php
        if (have_rows('features', $parentTerm)) :
          while (have_rows('features', $parentTerm)) : the_row();
            $feature_count = get_sub_field('feature_count');
            $feature_content = get_sub_field('feature_content');
        ?>
            <div class="contact-details-outer wpb_column vc_column_container vc_col-sm-1/5">
              <div class="vc_column-inner">
                <div class="wpb_wrapper">
                  <div class="wpb_text_column wpb_content_element features-content  icon-block">
                    <div class="wpb_wrapper">
                      <?php
                      $feature_image = get_sub_field('feature_icon');
                      if (!empty($feature_image['url'])) :
                      ?>
                        <p><img class="alignnone size-full wp-image-27334 aligncenter" src="<?php echo esc_url($feature_image['url']); ?>" alt="<?php echo esc_attr($feature_image['alt']); ?>"></p>
                      <?php endif; ?>
                      <h1 id="000" style="text-align: center;"><?php echo  $feature_count ?></h1>
                      <p style="text-align: center;"><?php echo  $feature_content  ?></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        <?php
          endwhile;
        endif;
        ?>

      </div>
      <div class="vc_row wpb_row vc_row-fluid container hide-div" id="lees_meer_expand">
        <?php
        if (have_rows('about_block_description', $parentTerm)) :
          while (have_rows('about_block_description', $parentTerm)) : the_row();
            $about_description_title = get_sub_field('about_description_header');
            $about_description_content = get_sub_field('about_description_content');
        ?>
            <div class="block">
              <h2><?php echo  $about_description_title ?></h2>
              <p><?php echo $about_description_content ?></p>
            </div>
        <?php
          endwhile;
        endif;
        ?>
      </div>
    </section>
  <?php
  endif;
  ?>
</div>