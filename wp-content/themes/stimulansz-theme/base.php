<?php
use Roots\Sage\Setup;
use Roots\Sage\Wrapper;
?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
      do_action('get_header');
      get_template_part('templates/header');
    ?>
    <?php
    if ( is_front_page() ) {
    ?>
	<!-- banner section -->
        <section class="banner_section home_banner_section">
            <div class="container">
                <?php 
                $banner_image = get_field('banner_image',get_the_ID());
                $video_url   =  get_field('banner_video',get_the_ID());
                $banner_text =  get_field('banner_text',get_the_ID());
                if(isset($banner_image['url']) && empty($video_url)){
                ?>
                <div class="banner_image" style="background: url(<?php echo $banner_image['url']; ?>); background-size: cover; background-position: right center;">
                    <div class="banner_image_text"><h1 class="home_banner_text"><?php echo $banner_text;  ?></h1></div>
                    <div class="banner_search_wrapper">
                       <?php echo get_search_form(); ?>
					</div>
				</div>
                <?php }else{ ?>
					<div class="banner_video">
						<?php if(isset($video_url)&& !empty($video_url)): ?>
						 <iframe width="100%" height="400" src="<?php echo $video_url; ?>/?controls=0&showinfo=0&rel=0&autoplay=1&loop=1" frameborder="0" allowfullscreen></iframe> 
					 <?php endif; ?>
				  </div>
                <?php }?>

        </section>
        <!-- .banner section -->
    <?php } ?>
    <div class="wrap" role="document">
      <div class="content row">
        <main class="main">
          <?php include Wrapper\template_path(); ?>
        </main><!-- /.main -->
        <?php if (Setup\display_sidebar()) : ?>
          <aside class="sidebar">
            <?php //include Wrapper\sidebar_path(); ?>
          </aside><!-- /.sidebar -->
        <?php endif; ?>
      </div><!-- /.content -->
    </div><!-- /.wrap -->
    <?php
      do_action('get_footer');
         get_template_part('templates/footer');
      wp_footer();
    ?>
  </body>
</html>
