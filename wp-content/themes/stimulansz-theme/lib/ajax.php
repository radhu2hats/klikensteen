<?php
add_action('wp_ajax_get_training_items', 'get_training_items');
add_action('wp_ajax_nopriv_get_training_items', 'get_training_items');

function get_training_items() {

  $data = $_POST;
  $posts_per_page = sanitize_text_field( $data['posts_per_page'] );
  $paged = sanitize_text_field( $data['paged'] );
  $leerdoel = $data['leerdoel'];
  $uitvoering = $data['uitvoering'] ;
  $werkervaring_niveau = $data['werkervaring_niveau'] ;
  $parentWerkveldSlug = sanitize_text_field( $data['parentWerkveldSlug'] );
  $uploadUrl = wp_upload_dir();
  $parentTerm = get_term_by('slug', $parentWerkveldSlug , 'training_werkveld');
  $recipes = [];
  $tax_query = array('relation' => 'AND');

  if(!empty($leerdoel)){
    $tax_query[] = array(
      'taxonomy' => 'leerdoel',
      'field' => 'slug',
      'terms' => $leerdoel,
      'operator' => 'IN'
    );
  }

  if(!empty($uitvoering)){
    $tax_query[] = array(
      'taxonomy' => 'uitvoering',
      'field' => 'slug',
      'terms' => $uitvoering,
      'operator' => 'IN'
    );
  }

  if(!empty($werkervaring_niveau)){
    $tax_query[] = array(
      'taxonomy' => 'werkervaring_niveau',
      'field' => 'slug',
      'terms' => $werkervaring_niveau,
      'operator' => 'IN'
    );
  }

  $tax_query[] = array(
    'taxonomy' => 'training_werkveld',
    'field' => 'slug',
    'terms' => $parentWerkveldSlug
  );

  $args = array(
    'post_type' => 'training',
    'paged' => $paged,
    'posts_per_page' => $posts_per_page,
    'post_status' => 'publish',
    'orderby' => 'publish_date',
    'order' => 'ASC',
    'tax_query' => $tax_query
  );
  
  $queries = new WP_Query($args);
  $total_result = $queries->post_count;
  ob_start();
  ?>
        <h2>Er zijn <strong> <?php echo $queries->found_posts; ?> trainingen </strong> gevonden</h2>
        <div class="vc_row wpb_row vc_row-fluid post-repeater listing-blocks-outer">
          <?php
          $count = 1;
          foreach ($queries->posts as $query):
          $image = wp_get_attachment_url(get_post_thumbnail_id($query->ID), 'thumbnail');
          
          // $image = get_field('background_image', $query->ID);
          ?>

          <?php if($count == 1): ?>
            <div class="wpb_column vc_column_container vc_col-sm-8 item">
                    <div class="card card-block white_bg_block post-list-item wide">
                    <a href="<?php echo get_permalink($query->ID); ?>" >
                      <div class="card-inner">
                        <div class="img-wrap large-bllk">
                           <img src="<?php  echo $image; ?>" alt="<?php echo $query->post_title; ?>" onerror="src='<?php echo $uploadUrl['baseurl']; ?>/placeholder-large.png'">
                          <!-- <img src="<?php //echo $image['sizes']['stimulansz_card_wide']; ?>" /> -->
                        </div>
                        <div class="cnt-wrap">
                          <div class="title_wrap">
                            <div class="cat">
                              <?php
                              $selectedTerms = [];
                              foreach (wp_get_post_terms($query->ID, 'training_cat') as $term) {
                                $selectedTerms[] = $term->name;
                              }
                              echo implode(', ', $selectedTerms);
                              ?>
                            </div>
                            <div class="title"><?php echo $query->post_title ?></div>
                            <p><?php echo $query->post_excerpt; ?></p>
                          </div>
                          <div class="sub_title_wrap">
                            <div class="sub_title sub_title_1"><i class="far fa-clock"></i> <?php echo get_field('duration', $query->ID) ?></div>
                            <span class="readmore" tabindex="0"> <i class="fas fa-arrow-right">&nbsp;</i> </span>
                          </div>
                        </div>
                      </div>
                    </a>
                    </div>
                  </div>
                  <div class="wpb_column vc_column_container vc_col-sm-4 item">
                    <div class="vc_general vc_cta3 pink-cta vc_cta3-style-classic vc_cta3-shape-rounded vc_cta3-align-center vc_cta3-color-classic vc_cta3-icon-size-md vc_cta3-icons-top vc_cta3-actions-bottom vc_custom_1617022095910">
                      <div class="vc_cta3-icons">
                        <div class="vc_icon_element vc_icon_element-outer vc_icon_element-align-left">
                          <div class="vc_icon_element-inner vc_icon_element-color-blue vc_icon_element-size-md vc_icon_element-style- vc_icon_element-background-color-grey"><span class="vc_icon_element-icon vc_li vc_li-paperplane"></span></div>
                        </div>
                      </div>
                      <div class="vc_cta3_content-container">
                        <?php
                        $featured_static_content_title =  get_field('featured_static_content_title', $parentTerm);
                        $featured_static_content_description = get_field('featured_static_content_description', $parentTerm);
                        $featured_static_content_button = get_field('featured_static_content_button', $parentTerm);
                        ?>
                        <div class="vc_cta3-content">
                          <header class="vc_cta3-content-header">
                            <h3><?php echo $featured_static_content_title; ?></h3>
                            <h4><?php echo $featured_static_content_description; ?></h4>
                          </header>
                        </div>
                        <div class="vc_cta3-actions">
                          <div class="vc_btn3-container  btn-white vc_btn3-center"><a style="background-color:#ffffff; color:#af197d;" class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-custom" href="<?php echo $featured_static_content_button['url']; ?>" title=""><?php echo $featured_static_content_button['title']; ?></a></div>
                        </div>
                      </div>
                    </div>
                  </div>
          <?php else: ?>
          

          <div class="wpb_column vc_column_container vc_col-sm-4 item 2nd">
            <div class="card card-block white_bg_block post-list-item">
            <a href="<?php echo get_permalink($query->ID); ?>" >
              <div class="card-inner">
                <div class="img-wrap">
                <img src="<?php  echo $image;  ?>" alt="<?php  echo $query->post_title  ?>"  onerror="src='<?php echo $uploadUrl['baseurl']; ?>/placeholder-large.png'">
                  <!-- <img src="<?php // echo $image; ?>" alt="<?php // echo $query->post_title ?>">
                  <img src="<?php echo $image['sizes']['stimulansz_card_image']; ?>" /> -->
                </div>
                <div class="cnt-wrap">
                  <div class="title_wrap">
                    <div class="cat">
                      <?php
                      $selectedTerms = [];
                      foreach (wp_get_post_terms($query->ID, 'training_cat') as $term) {
                          $selectedTerms[] = $term->name;
                      }
                      echo implode(', ', $selectedTerms);
                      ?>
                    </div>
                    <div class="title"><?php echo $query->post_title ?></div>
                    <p><?php echo $query->post_excerpt; ?></p>
                  </div>
                  <div class="sub_title_wrap">
                    <div class="sub_title sub_title_1"><i class="far fa-clock"></i> <?php echo get_field('duration', $query->ID) ?></div>
                    <span class="readmore" tabindex="0"> <i class="fas fa-arrow-right">&nbsp;</i> </span>
                  </div>
                </div>
              </div>
            </a>
            </div>
          </div>
          <?php endif; ?>

          <?php
          $count++;
          endforeach;
          ?>
        </div>
        <?php wpex_pagination($queries->max_num_pages, $paged); ?>
  
  <?php

  $training_list = ob_get_clean();

  $training['training_list'] = $training_list;

  query_posts($args);
  $training['args'] = $args;
  // $recipes['pagination'] = Timber::get_pagination();

  echo json_encode($training);
  die();
    
}


// Numbered Pagination
if ( !function_exists( 'wpex_pagination' ) ) {
	
	function wpex_pagination($max_num_pages, $paged) {
		
		$prev_arrow = is_rtl() ? '→' : '←';
		$next_arrow = is_rtl() ? '←' : '→';
		
		global $wp_query;
		$total = $max_num_pages;
		$big = 999999999; // need an unlikely integer

		if( $total > 1 )  {
			 if( !$current_page = get_query_var('paged') )
				 $current_page = 1;
			echo paginate_links(array(
				'base'			=> str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format' => 'page/%#%',
				'current'		=> $paged,
				'total' 		=> $total,
				'mid_size'		=> 5,
				'type' 			=> 'list',
        'prev_next' => true,
				'prev_text'		=> $prev_arrow,
				'next_text'		=> $next_arrow,
        // 'aria_current' => page,
        'add_args' => array( 'page-num' => $paged ),
			 ) );
		}
	}
	
}