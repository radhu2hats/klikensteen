<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */
function body_class($classes) {
    // Add page slug if it doesn't exist
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    // Add class if sidebar is active
    if (Setup\display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    return $classes;
}

add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}

add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

/**
 * Add top bar menu
 */
function stimulanz_custom_new_menu() {
    register_nav_menus(
            array(
                'header-menu' => __('Header Menu'),
                'top-bar-menu' => __('Top bar menu'),
                'footer-werkvelden-menu' => __('Footer Werkvelden menu'),
                'footer-acties-menu' => __('Footer Acties menu'),
                'footer-producten-menu' => __('Footer Producten menu'),
                'bottom-footer-menu' => __('Bottom footer menu'),
                'theme_paginas' => __('Themapagina\'s menu'),
                'top-right-menu' => __('Login Menu'),
                'extra-menu' => __('Extra Menu'),
            )
    );
}

add_action('init', __NAMESPACE__ . '\\stimulanz_custom_new_menu');

/**
 * Get average review 
 * @param type $postid
 * @return type
 */
add_filter('stimulanz_get_average_preview', __NAMESPACE__ . '\\stimulanz_review', 10, 1);

function stimulanz_review($postid) {
    //Prepare Array
    if (!empty($postid)):
        $args = array(
            'post_id' => $postid,
            'status' => 'approve',
        );
        $comments = get_comments($args);
        $rating = array();
        foreach ($comments as $comment) :
            if (!empty(get_comment_meta($comment->comment_ID, 'rating', true))) {
                $rating[] = get_comment_meta($comment->comment_ID, 'rating', true);
            }
        endforeach;
        //Array sum
        if (count($rating) >= 1):
            $array_sum = array_sum($rating);
            $average_value = array();
            //Average rating count
            $average = $array_sum / count($rating);
            $round = round($average, 1);
            $total = count($rating);
            $average_value['total_comment'] = $total;
            $average_value['average'] = str_replace('.', ',', $round);
            return $average_value;
        //echo $average;
        endif;
    endif;
}

/**
 * Get term name by post id
 * @param type $postid
 * @return type
 */
add_action('stimulanz_categoryname', __NAMESPACE__ . '\\stimulanz_get_categoryname', 10, 3);

function stimulanz_get_categoryname($postid, $taxonomy) {
	global $wpdb;
	if(!empty($postid)){
		$postid = $postid;
	}else{
		$postid = get_the_ID();
	}
	
       //post type based select taxonomy
        $posttype = get_post_type( $postid );
        if($posttype == 'post' ){
            $taxonomy ='actueel';
        }elseif($posttype =='training'){
            $taxonomy ='training_cat';
        }elseif($posttype =='contactpersoon'){
            $taxonomy ='contactpersoon_cat';
        }elseif($posttype =='kennisbanken'){
            $taxonomy ='kennisbanken_cat';
        }elseif($posttype =='product'){
            $taxonomy ='product_cat';
        }elseif($posttype =='advies'){
            $taxonomy ='advies_cat';
        }elseif($posttype =='landingspagina'){
            $taxonomy ='landingspagina_cat';
        }
        
         
    if (isset($taxonomy) && !empty($taxonomy)):
        $terms = wp_get_post_terms($postid, $taxonomy, array('order' => 'DESC'));
		
        if ($terms && !is_wp_error($terms)) :
            $draught_links = array();
            foreach ($terms as $term) {
                $cate_name[] = $term->name;
            }
            //$cate_name = join( ", ", $cate_name );
            if (isset($cate_name[0])):
                echo '<div class="box-top-right-icon">';
                echo ucfirst($cate_name[0]);
                echo '</div>';
            endif;
        endif;
    endif;
}

/**
 * stimulanz lastest training post article
 * @hook stimulanz_traing_post
 */
add_action('stimulanz_traing_post', __NAMESPACE__ . '\\stimulanz_latest_traing_post', 10, 3);

function stimulanz_latest_traing_post() {

    global $post;
    $custom_taxterms = wp_get_object_terms($post->ID, 'training_cat', array('fields' => 'ids'));
    // arguments
    $args = array(
        'post_type' => 'training',
        'post_status' => 'publish',
        'posts_per_page' => 2, // you may edit this number
        'orderby' => 'rand',
        'tax_query' => array(
            array(
                'taxonomy' => 'training_cat',
                'field' => 'id',
                'terms' => $custom_taxterms
            )
        ),
        'post__not_in' => array($post->ID),
    );
    $training_query = new \WP_Query($args);
    ?>

    <?php if ($training_query->have_posts()) : ?>
        <div class="section_title"><h4><?php _e('Anderen bekeken ook', 'stimulansz') ?></h4></div>
        <div class="choose_training_area two_grid">
            <?php while ($training_query->have_posts()) : $training_query->the_post(); ?>
                <div class="col-md-6 col-sm-12 col-xs-12 training_block">
                    <div class="white_bg_block banner_block">
                        <?php do_action('stimulanz_categoryname', get_the_ID(), 'training_cat'); ?>
                       
                            <?php
                            if (has_post_thumbnail()) {
                                echo '<div class="banner_image">';
                                   the_post_thumbnail('stimulansz_related_post_image', ['class' => 'img-responsive responsive-full', 'title' => get_the_title()]);
                                echo '</div>';
                            }
                            
                            ?>

                        
                        <div class="banner_content">
                            <a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4> </a>
                            <div class="white_bg_block_container">
                                <div class="training_blog_points">
                                    <?php the_excerpt(); ?>
                                </div>
                                <div class="training_blog_points traning_schedule">
                                    <?php apply_filters('stimulanz_acf_icon_text', get_the_ID()); ?>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <?php
            endwhile;
            wp_reset_postdata();
            ?>
        </div>
    <?php endif; ?>
    <?php
}

/**
 * @hook stimulanz_acadmic_post
 * Latest three acadmic post details
 */
add_action('stimulanz_academic_post', __NAMESPACE__ . '\\stimulanz_latest_academic_post', 10, 3);

function stimulanz_latest_academic_post() {
    global $post;
    $custom_taxterms = wp_get_object_terms($post->ID, 'academie_cat', array('fields' => 'ids'));
    // arguments
    $args = array(
        'post_type' => 'academie',
        'post_status' => 'publish',
        'posts_per_page' => 3, // you may edit this number
        'orderby' => 'rand',
        'tax_query' => array(
            array(
                'taxonomy' => 'academie_cat',
                'field' => 'id',
                'terms' => $custom_taxterms
            )
        ),
        'post__not_in' => array($post->ID),
    );

    $academie_query = new \WP_Query($args);

    // $academie_query = new \ WP_Query($academieargs);
    ?>
    <?php if ($academie_query->have_posts()) : ?>
        <div class="section_title"><h4><?php _e('Geselecteerd op basis van dit onderwerp', 'stimulansz') ?></h4></div>
        <div class="choose_training_area">
            <?php while ($academie_query->have_posts()) : $academie_query->the_post(); ?>
                <div class="col-md-4 col-sm-12 col-xs-12 training_block">
                    <div class="white_bg_block banner_block">
                        <?php do_action('stimulanz_categoryname', get_the_ID(), 'academie_cat'); ?>
                        
                            <?php
                            if (has_post_thumbnail()) {
                                echo '<div class="banner_image">';
                                    the_post_thumbnail('blog-thumbnail', ['class' => 'img-responsive responsive-full', 'title' => get_the_title()]);
                                echo '</div>';
                            }
                            
                            ?>
                        
                        <div class="banner_content">
                            <div class="white_bg_block_container">
                                <a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4> </a>
                                <div class="training_blog_points">
                                    <?php 
                                    $intro = get_field('additional_banner_text');
                                        if(isset($intro) && !empty($intro)):
                                            echo $intro.' <a class="stimulansz-home-more-link" href="' . get_permalink() . '">' . esc_html__( 'Read more', 'stimulansz' ) . '</a>';
                                        else:
                                            the_excerpt();
                                        endif;
                                 ?>
                                    <?php //the_excerpt(); ?>
                                </div>
                                <div class="training_blog_points traning_schedule">
                                    <?php apply_filters('stimulanz_acf_icon_text', get_the_ID()); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            endwhile;
            wp_reset_postdata();
            ?>

        </div>
    <?php endif; ?>
    <?php
}

/**
 * @hook stimulanz_acadmic_post
 * Latest three acadmic post details
 */
add_action('stimulanz_landingspagina_post', __NAMESPACE__ . '\\stimulanz_landingspagina_post', 10, 3);

function stimulanz_landingspagina_post() {
    global $post;
    $custom_taxterms = wp_get_object_terms($post->ID, 'landingspagina_cat', array('fields' => 'ids'));
    // arguments
    $args = array(
        'post_type' => 'landingspagina',
        'post_status' => 'publish',
        'posts_per_page' => 3, // you may edit this number
        'orderby' => 'rand',
        'tax_query' => array(
            array(
                'taxonomy' => 'landingspagina_cat',
                'field' => 'id',
                'terms' => $custom_taxterms
            )
        ),
        'post__not_in' => array($post->ID),
    );

    $academie_query = new \WP_Query($args);
    ?>
    <?php if ($academie_query->have_posts()) : ?>
        <div class="section_title"><h4><?php _e('Geselecteerd op basis van dit onderwerp', 'stimulansz') ?></h4></div>
        <div class="choose_training_area">
            <?php while ($academie_query->have_posts()) : $academie_query->the_post(); ?>
                <div class="col-md-4 col-sm-12 col-xs-12 training_block">
                    <div class="white_bg_block banner_block">
                        <?php do_action('stimulanz_categoryname', get_the_ID(), 'landingspagina_cat'); ?>
                       
                            <?php
                            if (has_post_thumbnail()) {
                                 echo '<div class="banner_image">';
                                    the_post_thumbnail('stimulansz_block_image', ['class' => 'img-responsive responsive-full', 'title' => get_the_title()]);
                                echo '</div>';
                            }
                            
                            ?>
                       
                        <div class="banner_content">
                            <div class="white_bg_block_container">
                                
                                <a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4> </a>
                                <div class="training_blog_points">
                                    <?php the_excerpt(); ?>
                                </div>
                                <div class="training_blog_points traning_schedule">
                                    <?php apply_filters('stimulanz_acf_icon_text', get_the_ID()); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            endwhile;
            wp_reset_postdata();
            ?>

        </div>
    <?php endif; ?>
    <?php
}

/**
 * Excerpt content length
 * @param type $length
 * @return int
 */
function custom_excerpt_length($length) {
    return 20;
}

add_filter('excerpt_length', __NAMESPACE__ . '\\custom_excerpt_length', 999);
/**
 * 
 * @global int $paged
 * @global type $wp_query
 * @param type $pages
 * @param type $range
 */
add_action('stimulanz_pagination_number', __NAMESPACE__ . '\\stimulanz_pagination', 10, 3);

// numbered pagination
function stimulanz_pagination($pages = '', $range = 4) {
    $showitems = ($range * 2) + 1;
    global $paged;
    if (empty($paged))
        $paged = 1;
    if ($pages == '') {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if (!$pages) {
            $pages = 1;
        }
    }

    if (1 != $pages) {
        echo "<div class=\"pagination\"><span>Page " . $paged . " of " . $pages . "</span>";
        if ($paged > 2 && $paged > $range + 1 && $showitems < $pages)
            echo "<a href='" . get_pagenum_link(1) . "'>&laquo; First</a>";
        if ($paged > 1 && $showitems < $pages)
            echo "<a href='" . get_pagenum_link($paged - 1) . "'>&lsaquo; Previous</a>";

        for ($i = 1; $i <= $pages; $i++) {
            if (1 != $pages && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems )) {
                echo ($paged == $i) ? "<span class=\"current\">" . $i . "</span>" : "<a href='" . get_pagenum_link($i) . "' class=\"inactive\">" . $i . "</a>";
            }
        }

        if ($paged < $pages && $showitems < $pages)
            echo "<a href=\"" . get_pagenum_link($paged + 1) . "\">Next &rsaquo;</a>";
        if ($paged < $pages - 1 && $paged + $range - 1 < $pages && $showitems < $pages)
            echo "<a href='" . get_pagenum_link($pages) . "'>Last &raquo;</a>";
        echo "</div>\n";
    }
}

/**
 * Arrange comment form field
 * @param array $fields
 * @return type
 */
function stimulanz_move_comment_field_to_bottom($fields) {
    $comment_field = $fields['comment'];
    unset($fields['comment']);
    $fields['comment'] = $comment_field;
    return $fields;
}

add_filter('comment_form_fields', __NAMESPACE__ . '\stimulanz_move_comment_field_to_bottom');

add_action('stimulanz_instructieurs', __NAMESPACE__ . '\\stimulanz_instructor_list', 10, 1);

function stimulanz_instructor_list() {
      $instructieurs = get_field('over_de_instructeurs'); 
   ?>
    <?php if (is_array($instructieurs)): ?>
        <div class="section_title">
            <h4><?php echo get_field('instructieurs_title'); ?></h4>
        </div>
        <section class="stimulansz_experts_organization">
        <div class="vc_row wpb_row vc_row-fluid trainer-grid">
                  <?php foreach ($instructieurs as $key => $value):
                    $instructor_img_url = get_the_post_thumbnail_url($value->ID, 'instractor_image');
                    ?>
                    <div class="wpb_column vc_col-sm-4">
                      <div class="card card-block " id="">
                        <div class="card-inner">                          
                          <div class="img-wrap"><img width="264" height="148" src="<?php if (isset($instructor_img_url)) {  echo $instructor_img_url; }  ?>" class="attachment-stimulansz_card_image size-stimulansz_card_image" alt="">                        
                          </div>
                          <div class="cnt-wrap">
                            <div class="title_wrap">
                              <div class="title"><?php echo $value->post_title; ?></div>
                              <p>    <?php 
                                        if(!empty($value->post_excerpt)):
                                            echo get_the_excerpt($value->ID);
                                        endif;
                                        ?></p>
                            </div>
                            <div class="sub_title_wrap">
                            <?php if (get_field('telefoon', $value->ID)): ?>
                              <a href="tel:<?php echo get_field('telefoon', $value->ID); ?>" class="sub_title sub_title_1"><?php echo get_field('telefoon', $value->ID); ?></a>
                              <?php endif; ?>
                              <?php if (get_field('e_mail_adres', $value->ID)): ?>
                              <div class="sub_title sub_title_2"><button id="email_copyboard" data-text="<?php echo get_field('e_mail_adres', $value->ID); ?>" class="copyboard"><i class="fa fa-copy"></i> Kopieer e-mailadres</button></div>
                           <?php endif; ?>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  <?php 
                   endforeach;
                   wp_reset_postdata();
                   ?>
                    </div>
        </section>
    <?php endif; ?>
<?php
}

/**
 * stimulanz lastest training post article
 * @hook stimulanz_related_contactperson
 */
add_action('stimulanz_related_contactperson', __NAMESPACE__ . '\\stimulanz_related_contactperson', 10, 3);

function stimulanz_related_contactperson() {
    global $post;
    $custom_taxterms = wp_get_object_terms($post->ID, 'contactpersoon_cat', array('fields' => 'ids'));
    // arguments
    $args = array(
        'post_type' => 'contactpersoon',
        'post_status' => 'publish',
        'posts_per_page' => 3, // you may edit this number
        'orderby' => 'rand',
        'tax_query' => array(
            array(
                'taxonomy' => 'contactpersoon_cat',
                'field' => 'id',
                'terms' => $custom_taxterms
            )
        ),
        'post__not_in' => array($post->ID),
    );
    $training_query = new \WP_Query($args);
    ?>

    <?php if ($training_query->have_posts()) : ?>
        <div class="section_title"><h4><?php _e('Anderen bekeken ook', 'stimulansz') ?></h4></div>
        <div class="choose_training_area two_grid">
        <?php while ($training_query->have_posts()) : $training_query->the_post(); ?>
                <div class="col-md-6 col-sm-12 col-xs-12 training_block">
                    <div class="white_bg_block banner_block">
                        <?php do_action('stimulanz_categoryname', get_the_ID(), 'contactpersoon_cat'); ?>
                            <?php
                            if (has_post_thumbnail()) {
                                echo '<div class="banner_image">';
                                echo '<a href="'.get_permalink().'">'; 
                                 the_post_thumbnail('stimulansz_related_post_image', ['class' => 'img-responsive responsive-full', 'title' => get_the_title()]);
                                echo '</a></div>';
                            }
                            ?>
                        <div class="banner_content">
                            <a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4> </a>
                            <div class="white_bg_block_container">
                                <div class="training_blog_points">
                                        <?php the_excerpt(); ?>
                                </div>
                                <div class="training_blog_points traning_schedule">
                                <?php if (get_field('telephone')): ?>
                                        <div>
                                            <span><i class="fa fa-phone" aria-hidden="true"></i></span><span><a href="tel:<?php echo get_field('telephone'); ?>">T <?php echo get_field('telephone'); ?></a></span>
                                        </div>
                                    <?php endif; ?>
                                <?php if (get_field('email_address')): ?>
                                        <div>
                                            <span><i class="fa fa-envelope-o" aria-hidden="true"></i></i></span><span><a href="mailto:<?php echo get_field('email_address'); ?>"><?php echo get_field('email_address'); ?></a></span>
                                        </div>
                             <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            endwhile;
            wp_reset_postdata();
            ?>
        </div>
    <?php endif; ?>
    <?php
}

/**
 * stimulanz_fontawesome_icon
 */
add_filter('stimulanz_fontawesome_icon', __NAMESPACE__ . '\\stimulanz_fontawesome_icon', 10, 1);

function stimulanz_fontawesome_icon() {
    $icon_array = array('fa fa-clock-o', 'fa fa-play-circle-o', 'fa fa-chevron-circle-right', 'fa fa-building-o', 'fa fa-eur');

    return $icon_array;
}

/**
 * stimulanz_fontawesome_icon
 */
add_filter('stimulanz_acf_icon_text', __NAMESPACE__ . '\\stimulanz_acf_tiem_icon', 10, 1);

function stimulanz_acf_tiem_icon($post_id) {
    ?>
    <?php if (get_field('duration', $post_id)): ?>
        <div>
            <span><i class="fa fa-clock-o"></i></span><span><?php echo get_field('duration', $post_id); ?></span>
        </div>
    <?php endif; ?>
    <?php if (get_field('aantal_deelnemers', $post_id)): ?>
        <div>
            <span><i class="fa fa-user"></i></span><span><?php echo get_field('aantal_deelnemers', $post_id); ?> </span>
        </div>
    <?php endif; ?>
    <?php if (get_field('training_type', $post_id)): ?>
        <div>
            <span><i class="fa fa-building-o"></i></span><span><?php echo get_field('training_type', $post_id); ?></span>
        </div>
    <?php endif; ?>
    <?php if (get_field('price', $post_id)): ?>
        <div>
            <span><i class="fa fa-eur"></i></span><span><?php echo get_field('price', $post_id); ?></span>
        </div>
    <?php endif; ?>
        <?php  
       
        $html = apply_filters('stimulanz_event_dates',$post_id); 
        echo $html;
        ?>
         <?php /*if (get_field('eventstartdate', $post_id)): ?>
            <div>
                <span><i class="fa fa-calendar"></i></span><span><?php echo date_i18n("j F, Y", strtotime(get_field('eventstartdate', $post_id))); ?><?php if(get_field('eventenddate', $post_id)): ?> - <?php  echo  date_i18n("j F, Y", strtotime(get_field('eventenddate', $post_id))); ?><?php endif; ?></span>
            </div>
    <?php endif;*/ ?>
    <?php
    ?>
    <?php
}

/**
 * stimulanz related post article
 * @hook stimulanz_related_post_article
 */
add_action('stimulanz_related_post_article', __NAMESPACE__ . '\\stimulanz_custom_post_related_article', 10, 3);

function stimulanz_custom_post_related_article($custom_posttype, $taxonomy_name) {
    global $post;
    if (empty($custom_posttype)) {
        return;
    }
    $custom_taxterms = wp_get_object_terms($post->ID, $taxonomy_name, array('fields' => 'ids'));
    // arguments
    $args = array(
        'post_type' => $custom_posttype,
        'post_status' => 'publish',
        'posts_per_page' => 2, // you may edit this number
        'orderby' => 'rand',
        'tax_query' => array(
            array(
                'taxonomy' => $taxonomy_name,
                'field' => 'id',
                'terms' => $custom_taxterms
            )
        ),
        'post__not_in' => array($post->ID),
    );
    $training_query = new \WP_Query($args);
    ?>

    <?php if ($training_query->have_posts()) : ?>
        <div class="section_title"><h4><?php _e('Anderen bekeken ook', 'stimulansz') ?></h4></div>
        <div class="choose_training_area two_grid">
        <?php while ($training_query->have_posts()) : $training_query->the_post(); ?>
                <div class="col-md-6 col-sm-12 col-xs-12 training_block">
                    <div class="white_bg_block banner_block">
                        <?php do_action('stimulanz_categoryname', get_the_ID(), $taxonomy_name); ?>
                       
                            <?php
                            if (has_post_thumbnail()) {
                                echo '<div class="banner_image">';
                                 echo '<a href="'.get_permalink().'">';  
                                    the_post_thumbnail('stimulansz_related_post_image', ['class' => 'img-responsive responsive-full', 'title' => get_the_title()]);
                             echo '</a></div>';
                                
                            }
                            
                            ?>

                       
                        <div class="banner_content">
                            <a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4> </a>
                            <div class="white_bg_block_container">
                                <div class="training_blog_points">
									<?php the_excerpt(); ?>
                                </div>
                                <div class="training_blog_points traning_schedule">
                                    <?php apply_filters('stimulanz_acf_icon_text', get_the_ID()); ?>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            <?php
        endwhile;
        wp_reset_postdata();
        ?>
        </div>
        <?php endif; ?>
        <?php
    }
    
	
/**
 * stimulanz Upsell CPT article
 * @hook stimulanz_related_post_article
 */
add_action('stimulanz_custom_upsell_article', __NAMESPACE__ . '\\stimulanz_cpt_upsell_article', 10, 2);

function stimulanz_cpt_upsell_article($taxonomy_name) {
    global $post;
    $post_object= get_field('select_up_sell_catalogue_products');
   // print_r($post_object);
    ?>

   <?php if (is_array($post_object)): ?>
        <div class="section_title" id="training-sticky"><h4><?php echo get_field('catalogue_product_section_title'); //_e('Anderen bekeken ook', 'stimulansz'); ?></h4>
             <input type="hidden" id="check-visibility" value="a">
        </div>
        <div class="choose_training_area two_grid" id="div-under-training-sticky">
		 <?php
                // check if the repeater field has rows of data
                foreach ($post_object as $key => $value):
                    if($value->post_status=='publish'):
                    ?>
        
                <div class="col-md-6 col-sm-12 col-xs-12 training_block dddd">
                    <div class="white_bg_block banner_block">
                        <?php 
                                    /**
                                    * @hook for Category 
                                    */
                        
                                    do_action('stimulanz_categoryname', $value->ID, $taxonomy_name); ?>
                       
                            <?php
                            $img_url = get_the_post_thumbnail_url($value->ID, 'full');
                            if (!empty($img_url)) {
                            echo '<div class="banner_image">';
                             echo '<a class="upsell_article" href="'.get_permalink($value->ID).'">'; 
                                 echo '<img src="'.$img_url.'" class="img-responsive responsive-full" title ="'.get_the_title($value->ID).'">';
                            echo '</a></div>';
                                
                            }
                            ?>
                       
                        <div class="banner_content">
                            <a href="<?php echo get_permalink($value->ID); ?>"><h4><?php echo $value->post_title; ?></h4> </a>
                            <div class="white_bg_block_container">
                                <div class="training_blog_points">
                                    <?php 
                                        if(!empty($value->post_excerpt)):
                                            echo get_the_excerpt($value->ID);
                                        endif;
                                        ?>
                                </div>
                                <div class="training_blog_points traning_schedule">
                                    <?php apply_filters('stimulanz_acf_icon_text', $value->ID); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
                                    endif;
       endforeach;
        wp_reset_postdata();
        ?>
        </div>
        <?php endif; ?>
        <?php
    }
    
    
    
/**
 * @hook stimulanz_acf_icon_text_search
 * stimulanz_fontawesome_icon for search page
 */
add_filter('stimulanz_acf_icon_text_search', __NAMESPACE__ . '\\stimulanz_acf_tiem_icon_search', 10, 1);

function stimulanz_acf_tiem_icon_search($post_id) {
    
    $html = '';
    if(!empty($post_id)):
    //Duration
    if (get_field('duration', $post_id)): 
        $html='<div><span><i class="fa fa-clock-o"></i></span><span>'.get_field('duration', $post_id).'</span></div>';
     endif; 
     
     if (get_field('aantal_deelnemers', $post_id)): 
       $html .='<div><span><i class="fa fa-play-circle-o"></i></span><span> '.get_field('aantal_deelnemers', $post_id).' </span></div>';
    
     endif; 
     
   if (get_field('training_type', $post_id)): 
        $html .='<div><span><i class="fa fa-building-o"></i></span><span>'.get_field('training_type', $post_id).'</span></div>';
     endif; 
    if (get_field('price', $post_id)): 
        $html .='<div> <span><i class="fa fa-eur"></i></span><span>'.get_field('price', $post_id).'</span></div>';
     endif; 
    if (get_field('datum_data', $post_id)): 
        //$html .='<div><span><i class="fa fa-calendar"></i></span><span>'.get_field('datum_data', $post_id).'</span></div>';
    endif;
    //g:i a
   // $html .= apply_filters('stimulanz_event_dates',$post_id);
       
    return $html;
    endif;    
}


/**
 * Get term name by post id
 * @param type $postid
 * @return type string
 * @filter use for display category on search page
 */
add_filter('stimulanz_search_page_categoryname', __NAMESPACE__ . '\\stimulanz_search_page_categoryname', 10, 1);

function stimulanz_search_page_categoryname($postid) {
	global $wpdb;
	if(!empty($postid)){
		$postid = $postid;
	}else{
		$postid = get_the_ID();
	}
	
        //post type based select taxonomy
        $posttype = get_post_type( $postid );
        if($posttype == 'post' ){
            $taxonomy ='category';
        }elseif($posttype =='training'){
            $taxonomy ='training_cat';
        }elseif($posttype =='contactpersoon'){
            $taxonomy ='contactpersoon_cat';
        }elseif($posttype =='kennisbanken'){
            $taxonomy ='kennisbanken_cat';
        }elseif($posttype =='product'){
            $taxonomy ='product_cat';
        }elseif($posttype =='advies'){
            $taxonomy ='advies_cat';
        }elseif($posttype =='landingspagina'){
            $taxonomy ='landingspagina_cat';
        }
        
    if (isset($taxonomy) && !empty($taxonomy)):
        $terms = wp_get_post_terms($postid, $taxonomy, array('order' => 'DESC'));
		
        if ($terms && !is_wp_error($terms)) :
            $draught_links = array();
            foreach ($terms as $term) {
                $cate_name[] = $term->name;
            }
            $cate_name = '';
            //$cate_name = join( ", ", $cate_name );
            if (isset($cate_name[0])):
                $catename .='<div class="box-top-right-icon">';
                $catename .= ucfirst($cate_name[0]);
                $catename .= '</div>';
                return $catename;
            endif;
        endif;
    endif;
}

/**
 * @hook for change event cpt lebal name
 */
add_filter( 'register_post_type_args',__NAMESPACE__ . '\\change_cpt_labelname' ,10,2);
function change_cpt_labelname( $args, $post_type ){
 //filter for  post type
 if ('tribe_events' == $post_type ) {
     // Give other post_types their original arguments
      $args['labels']['name'] = 'Agenda';
      $args['labels']['singular_name'] = 'Agenda';
      return $args;
 }
  return $args;

}

/**
 * @hook for change event cpt lebal name
 */
add_filter( 'get_event_startdate',__NAMESPACE__ . '\\get_event_startdate' ,10);
function get_event_startdate(){
    global $wpdb;
    //Argument
    $event_args = array(
                        'post_type' => 'training',
                        'posts_per_page'=> -1,
                        'post_status ' =>'publish',
                        'meta_key' => 'eventstartdate',
                        'order'=> 'DESC'
                        );   
    
    $query = new \WP_Query( $event_args );
    
    if ( $query->have_posts() ) {
        $eventdate =array();
        //Current server time
         $current_time = date("F j Y");
        while ( $query->have_posts() ) {
            $query->the_post();
            // check if the repeater field has rows of data
            if( have_rows('event_dates') ):
                    // loop through the rows of data
                while ( have_rows('event_dates') ) : the_row();
                    $startdate = get_sub_field('eventstartdate',$query->post->ID);
                if(strtotime($startdate) >= strtotime($current_time)){
                   // display a sub field value
                   if(get_sub_field('eventstartdate',$query->post->ID)):
                        $eventdate[] = get_sub_field('eventstartdate', $query->post->ID);
                   endif;

                }
                    
                endwhile;

            else :

                // no rows found

            endif;
              
        }
        usort($eventdate, "date_sort");
        $array_count_values = array_count_values($eventdate);
        return $array_count_values;
    }
    // Reset the $post data to the current post in main query.
    wp_reset_postdata();
}
/**
 * 
 */
add_filter( 'stimulanz_event_dates',__NAMESPACE__ . '\\stimulanz_event_date' ,10,1);
function stimulanz_event_date($post_id){
    global $wpdb;
     $html = '';
     $eventdate =array();
     $current_time = date("d F Y");
      if( have_rows('event_dates',$post_id) ):
                    // loop through the rows of data
                while ( have_rows('event_dates',$post_id) ) : the_row();
      
                    $startdate = get_sub_field('eventstartdate',$post_id);
                    
                    if(!empty($startdate)):
                        $startdate = $startdate;
                        else:
                        $startdate= '';  
                    endif;
                    
                    $eventenddate = get_sub_field('event_end_date',$post_id);
                    $event_time = get_sub_field('event_time',$post_id);
                    if(isset($startdate) && !empty($eventenddate)){
                       $eventdate = $startdate .' - '.$eventenddate;
                        }else{
                          $eventdate = '<p>'.$startdate.'</p>';
                        }
                        
                        if(isset($eventdate) && !empty($event_time) ){
                            $eventdate= $eventdate.' - '.$event_time;
                        }
                        
                        if ($startdate): 
                            $html .='<div class="overview_page"><span><i class="fa fa-calendar"></i></span><span>'.$eventdate.'';
                             $html .='</span></div>';
                        endif; 
                endwhile;
                 return $html;

            else :

                // no rows found

            endif;
}


/**
 * Stimulanz Agenda traing
 */
add_filter( 'stimulanz_traing_agenda',__NAMESPACE__ . '\\sort_agenda_results',10,1);
function sort_agenda_results(){
	global $wpdb;
	$get_all_events = get_posts(array(
            'posts_per_page' => -1,
            'post_type' => 'training',
            'post_status' => 'publish',
            'suppress_filters' => true
    ));

    /* return if no event found */
    if (empty($get_all_events)) :
        return null;
    endif;
    
    /* store all event ids in one string to be passed to db query */
    $all_event_ids = '';
    foreach ($get_all_events as $single_event) : setup_postdata($single_event);
        $all_event_ids .= $single_event->ID . ',';
    endforeach;
    wp_reset_postdata();

    /* remove last comma from string of all event ids */
    if (substr($all_event_ids, -1) == ','):
        $all_event_ids = substr($all_event_ids, 0, -1);
    endif;

    /* get current date and date of next week in requird format */
    $current_date = date('Y-m-d');
  

    /* 
     * prepare a query which will get traingen post id , data sort by event start date
     * from post and post meta tables 
    * 
     */
	 $event_query ="SELECT post_id, MIN(meta_value) as meta_dt FROM st_postmeta WHERE post_id IN ($all_event_ids) and (meta_key like 'event_dates_%_eventstartdate' 
and DATE(meta_value) >='$current_date' ) GROUP BY post_id ORDER BY DATE(meta_value) ASC LIMIT 6";
 
    /* get all event data */
         //echo $event_query;
    $all_sorted_events = $wpdb->get_results($event_query);
  
	?>
<!-- 	  <h4><?php _e('Agenda','stimulansz'); ?></h4> -->
                            <div class="white_bg_block_container">
				<?php
                                if ( $all_sorted_events ) :
                                foreach ($all_sorted_events as $key=>$value):
                                    
                                     if( have_rows('event_dates',$value->post_id) ):
                                                // loop through the rows of data
                                            while ( have_rows('event_dates',$value->post_id) ) : the_row();
                                                 $startdate = get_sub_field('eventstartdate',$value->post_id);
                                                // echo $startdate.'===='.$value->meta_dt;
                                                 $startdate= convert_endate_nath($startdate);
                                                   if($_SERVER["REMOTE_ADDR"]=='144.48.250.178'){
                                                        //echo "Fielddate ===".$startdate; 
                                                         //echo "Query_Date ===".$value->meta_dt; 
                                                }
                                               if($startdate == strtotime($value->meta_dt)){
                                                   $event_time = get_sub_field('event_time',$value->post_id);
                                               }else{
                                                   
                                               }
                                            endwhile;
                                        endif;        
                                ?>
                                        
                                <div class="each_block">
                                   <div class="title"><a href="<?php echo get_the_permalink($value->post_id); ?>" class="home-agenda-title-link"><?php echo get_the_title($value->post_id); ?></a></div>
                                    <div class="date">
                                        <span> <?php 
                                         $dateStart = date_i18n("j F Y", strtotime($value->meta_dt));
                                        echo $dateStart;
                                           // date("d F Y",strtotime($value->meta_dt)); 
                                        ?></span>
                                        
                                        <span>&nbsp;<?php if(isset($event_time) && !empty($event_time)){ echo " - ".$event_time;}  ?></span>
                                        <span>
                                            | 
                                            <?php 
                                            $data = get_post_primary_category($value->post_id, 'training_cat');
                                            echo $data['primary_category']->name;
                                             ?>
                                        </span>
                                    </div>  
                                 </div>
                                 <?php 
                                  endforeach;
                               else : ?>
                                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
								
                            <?php endif; ?>
                           
<!--                                 <a href="<?php echo get_field('view_all_activities_link','option'); ?>" class="blue-btn"><?php _e('Bekijk alle activiteiten','stimulansz'); ?></a> -->
                        </div>
	<?php
        }