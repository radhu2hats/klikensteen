<?php

namespace Roots\Sage\WC;

use Roots\Sage\Utils;
use Roots\Sage\Dynamic;
use WC_Order;
use WC_Order_Item_Meta;

/**
 * Woocommerce extend functionality 
 */
/**
 * Function to display minicart.
 *
 * It uses woocommerce_mini_cart() function to display cart contents. It displays
 * cart items with quantity and cart/checkout buttons.
 *
 * @link https://docs.woocommerce.com/wc-apidocs/function-woocommerce_mini_cart.html
 * @return void	
 */
add_action('stimulanz_ajaxify_minicart', __NAMESPACE__ . '\\stimulanz_woo_mini_cart', 5);

function stimulanz_woo_mini_cart() {
    global $woocommerce;
    $myaccount_page_id = get_option('woocommerce_myaccount_page_id');
    $account_page_url = get_permalink($myaccount_page_id);
    $logout_url = $account_page_url;
    $logout_url .= get_option('woocommerce_logout_endpoint', '');
    if (get_option('woocommerce_force_ssl_checkout') == 'yes')
        $logout_url = str_replace('http:', 'https:', $logout_url);
    ?>
    <div class="stimulanz_mobilecart" id="mobile_minicart">
        <?php if (is_user_logged_in()): ?>
            <a href="<?php echo $woocommerce->cart->get_cart_url(); ?>"><span class="icon"><i class="fa fa-shopping-cart" aria-hidden="true"></i></span></a>
        <?php else: ?>
            <a href="<?php if (isset($account_page_url)) {
            echo $account_page_url;
        } ?>">
                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
            </a>
    <?php endif; ?>
    </div>
    <div class="dropdown minicart" id="minicart">
        <a href="<?php echo site_url(); ?>/winkelwagen/" class="dropdown-toggle">
            <i class="fa fa-shopping-cart" aria-hidden="true"></i>
            <span id="cart-contents" item-count="<?php echo $woocommerce->cart->cart_contents_count; ?>">
            </span>
        </a>
        <ul class="dropdown-menu mini_cart" id="mode-mini-cart">
                <?php woocommerce_mini_cart(array('list_class' => '')); ?>
        </ul>

    </div>
    <script>
        // Check if there are no items in cart then removeClass for proper design
        jQuery(document).ready(function () {
            var cart_content = jQuery('#minicart .cart-contents').attr("item-count");
            console.log(cart_content);
            if (cart_content == '0') {
                jQuery('#cart-contents').removeClass('cart-contents');
            }
        });
    </script>
    <?php
}

/**
 * Updates cart contents.
 *
 * This functions updates the cart cotents when user adds product to cart.
 * Updates cart item quantity, total price and item details.
 *
 * @link https://docs.woocommerce.com/document/show-cart-contents-total/
 * @return array returns cart div
 */
add_filter('add_to_cart_fragments', __NAMESPACE__ . '\\stimulanz_woo_header_add_to_cart_fragment', 5);

function stimulanz_woo_header_add_to_cart_fragment($fragments) {
    global $woocommerce;
    ob_start();
    ?>
    <div class="dropdown minicartd" id="minicart">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" id="prod-count">
            <i class="fa fa-shopping-cart" aria-hidden="true"></i>
            <a class="cart-customlocation" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>"><?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count); ?> - <?php echo $woocommerce->cart->get_cart_total(); ?></a>

        </a>
    <?php woocommerce_mini_cart(array('list_class' => ''));
    ?>

    </div>
    <?php
    $fragments['a.cart-customlocation'] = ob_get_clean();
    return $fragments;
}

add_action('stimulanz_login_menu_items', __NAMESPACE__ . '\\stimulanz_login_menu', 10, 2);

/**
 * Woocommerce user login profile list
 * Menu list when user is login or logout.
 */
function stimulanz_login_menu() {
    global $woocommerce;
    $myaccount_page_id = get_option('woocommerce_myaccount_page_id');
    $account_page_url = get_permalink($myaccount_page_id);
    $logout_url = $account_page_url;
    $logout_url .= get_option('woocommerce_logout_endpoint', '');

    $edit_acc_url = $account_page_url . get_option('woocommerce_myaccount_edit_account_endpoint', '');
    $edit_add_url = $account_page_url . get_option('woocommerce_myaccount_edit_address_endpoint', '');
    $orders_acc_url = $account_page_url . get_option('woocommerce_myaccount_view_order_endpoint', '');

    if (get_option('woocommerce_force_ssl_checkout') == 'yes')
        $logout_url = str_replace('http:', 'https:', $logout_url);

    $customer_id = get_current_user_id();
    $get_addresses = apply_filters('woocommerce_my_account_get_addresses', array(
        'billing' => __('Billing Address', 'woocommerce')
            ), $customer_id);
    ?>

    <?php if (is_user_logged_in()): ?>
        <!--    <div class="mobile-login-link login-icon">
                <a href="<?php //echo $logout_url;  ?>"><span><?php //echo _x('Logout', 'Header dropdown logout label', 'stimulanz');  ?></span><i class="fa fa-sign-out" aria-hidden="true"></i></a>
            </div>-->
        <div class="dropdown login-icon">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <span class="icon-login"></span>
                <span class="login-name text-uppercase">
                    <?php
                    $current_user = wp_get_current_user();
                    echo $current_user->display_name;
                    ?>
                </span>  <i class="fa fa-power-off" aria-hidden="true"></i>
            </a>
            <ul class="dropdown-menu fff">
                <li class="order-icon"><a href="<?php echo $account_page_url; ?>"><span class="icon"></span><?php echo _x('My orders', 'Header dropdown orders label', 'stimulanz'); ?></a></li>
                <li class="address-icon"><a href="<?php echo $edit_add_url . '/' . $get_addresses['billing']; ?>"><span class="icon"></span><?php echo _x('My addresses', 'Header dropdown addresses label', 'stimulanz'); ?></a></li>
                <li role="separator" class="divider"></li>
                <li class="logout-icon"><a href="<?php echo $logout_url; ?>"><span class="icon"></span><?php echo _x('Logout', 'Header dropdown logout label', 'stimulanz'); ?></a></li>
            </ul>
        </div>
            <?php else: ?>
        <a href="<?php echo $account_page_url; ?>"><span><?php _e('Inloggen', 'stimulanz'); ?></span><i class="fa fa-power-off" aria-hidden="true"></i></a>
            <?php
            endif;
        }

//Remove Default woocommerce breadcrumb
        remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);


        /**
         * Products Loop.
         *
         * @see woocommerce_result_count()
         * @see woocommerce_catalog_ordering()
         */
		 
		 remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart');

        remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
        remove_action('woocommerce_before_shop_loop', 'woocommerce_pagination', 10);
        remove_action('woocommerce_after_shop_loop', 'woocommerce_catalog_ordering', 30);

        add_action('woocommerce_before_shop_loop', __NAMESPACE__ . '\\stimulanz_woocommerce_resultcount', 20, 1);

        function stimulanz_woocommerce_resultcount() {
            global $wp_query;
            //Results count
            $paged = max(1, $wp_query->get('paged'));
            $per_page = $wp_query->get('posts_per_page');
            $total = $wp_query->found_posts;
            $first = ( $per_page * $paged ) - $per_page + 1;
            $last = min($total, $wp_query->get('posts_per_page') * $paged);
            echo '<div class="stimulanz_results_count woocommerce-result-count">';
            if($total=='1'){
                    echo '<span class="stimulanz_total_count">Resultaten ('.$total.')<span>';
                } else{    
                    if ( $total <= $per_page || -1 === $per_page ) {
                            /* translators: %d: total results */
                            //printf( _n( 'Showing the single result', 'Showing all %d results', $total, 'woocommerce' ), $total );
                         printf(_n('Resultaten', '<span class="stimulanz_total_count">Resultaten (%d)</span>', $total, 'woocommerce'), $total);

                    } else {
                          printf(_nx('Resultaten', '<span class="stimulanz_total_count">Resultaten (%3$d)</span>', $total, 'with first and last result', 'woocommerce'), $first, $last, $total);
                            /* translators: 1: first result 2: last result 3: total results */
                            //printf( _nx( 'Showing the single result', 'Showing %1$d&ndash;%2$d of %3$d results', $total, 'with first and last result', 'woocommerce' ), $first, $last, $total );
                    }
                }
               echo "</div>"; 
               
        }

        /**
         * @hook override pagination hook
         *  
         */
        add_action('woocommerce_before_shop_loop', __NAMESPACE__ . '\\stimulanz_woocommerce_pagination', 30, 1);

        function stimulanz_woocommerce_pagination() {
           global $wp_query;
            if ($wp_query->max_num_pages <= 1) {
                return;
            }
            echo '<div class="stimulanz_shop_pagination">';
            echo '<nav class="woocommerce-pagination">';
            echo paginate_links(apply_filters('woocommerce_pagination_args', array(
                'base' => esc_url_raw(str_replace(999999999, '%#%', remove_query_arg('add-to-cart', get_pagenum_link(999999999, false)))),
                'format' => '',
                'add_args' => false,
                'current' => max(1, get_query_var('paged')),
                'total' => $wp_query->max_num_pages,
                'prev_text' => '&larr;',
                'next_text' => '&rarr;',
                'type' => 'list',
                'end_size' => 3,
                'mid_size' => 3,
            )));
            echo '</nav>';
            
        }

        /**
         * @hook remove action from single page template
         */
        remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10);
        remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20);

       // remove_action('woocommerce_before_shop_loop', 'woocommerce_pagination', 10);
		//remove_action( 'woocommerce_before_single_product', 'wc_print_notices', 10 );
        remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10);
        remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20);
        remove_action('woocommerce_product_thumbnails', 'woocommerce_show_product_thumbnails', 20);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50);
		//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
        remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
       // remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
        remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
	// remove default sorting dropdown
 
        //remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );	
	//woocommerce_template_loop_price
        remove_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_price', 15 );
        add_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_price', 7);
      
/**
 * 
 * @global type $product
 * @global array $woocommerce_loop
 * @return type
 */ 
// add_action('stimulanz_single_product_summary', __NAMESPACE__ . '\\stimulanz_woocommerce_output_related_products', 25);
    function stimulanz_woocommerce_output_related_products() {
            global $product, $woocommerce_loop;
           
            
            $related = $product->get_related(2);
            if (sizeof($related) == 0)
                return;

            $args = apply_filters('woocommerce_related_products_args', array(
                'post_type' => 'product',
                'ignore_sticky_posts' => 1,
                'no_found_rows' => 1,
                'posts_per_page' => 2,
                'orderby' => $orderby,
                'post__in' => $related,
                'post__not_in' => array($product->id)
                    ));
            $products = new \ WP_Query($args);

            $woocommerce_loop['columns'] = 2;

            if ($products->have_posts()) :
                ?>
       
        
            <?php woocommerce_product_loop_start(); ?>
           
            <?php while ($products->have_posts()) : $products->the_post(); ?>
                <?php wc_get_template_part('content', 'product'); ?>

            <?php endwhile; // end of the loop.  ?>

            <?php woocommerce_product_loop_end(); ?>

            

    <?php
    endif;

    wp_reset_postdata();
}
/**
 * @hooks stimulanz_woocommerce_upsell_products 
 * Upsell product display single product page
 */
add_action('stimulanz_single_product_summary', __NAMESPACE__ . '\\stimulanz_woocommerce_upsell_products', 25);
function stimulanz_woocommerce_upsell_products(){
    
    global $wpdb,$woocommerce,$product, $woocommerce_loop;
    
    $upsells = $product->get_upsells();
       if (sizeof($upsells) == 0){
                     return;
                }
           
        $meta_query = $woocommerce->query->get_meta_query();
           $args = array(
               'post_type'           => 'product',
               'ignore_sticky_posts' => 1,
               'no_found_rows'       => 1,
               'posts_per_page'      => $posts_per_page,
               'post_status'         => 'publish',
               'orderby'             => $orderby,
               'post__in'            => $upsells,
               'post__not_in'        => array( $product->id ),
               'meta_query'          => $meta_query
           );
        $query = new \ WP_Query( $args );
           
            $woocommerce_loop['columns'] = 2;
                if ( $query->have_posts() ): ?>
        
            <?php woocommerce_product_loop_start(); ?>
        <div class="section_title">
                        <h4><?php _e('Anderen bekeken ook', 'stimulansz'); ?></h4>
                    </div>
        
           <?php while ($query->have_posts()) : $query->the_post(); ?>

               <?php wc_get_template_part('content', 'product'); ?>

           <?php endwhile; // end of the loop.  ?>

           <?php woocommerce_product_loop_end(); ?>
                   
                <?php 
            
            endif; 

             wp_reset_postdata(); 
}
/**
 * Change Proceed To Checkout Button Text In WooCommerce
 * */
add_filter('woocommerce_product_add_to_cart_text', __NAMESPACE__ . '\\ld_woo_custom_cart_button_text');
add_filter('woocommerce_product_single_add_to_cart_text', __NAMESPACE__ . '\\ld_woo_custom_cart_button_text');

function ld_woo_custom_cart_button_text() {
    return __('Bestellen', 'stimulansz');
}


// remove woocommerce mini cart action
remove_action( 'woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_proceed_to_checkout', 20, 2 ); 
remove_action( 'woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_button_view_cart', 10,2 ); 

/**
 * Overwrite Continue Shopping button name
 */
add_action( 'woocommerce_widget_shopping_cart_buttons',  __NAMESPACE__ . '\\stimulansz_checkout_minicart', 20 );
function stimulansz_checkout_minicart(){ ?>
        <a href="<?php echo esc_url( wc_get_page_permalink( 'cart' ) ); ?>" class="button checkout wc-forward"><?php echo _x('Continue Shopping', 'Header Minicart Continue shopping label', 'stimulanz'); ?></a>
<?php }
/**
 * Overwrite button name
 */
add_action( 'woocommerce_widget_shopping_cart_buttons', __NAMESPACE__ . '\\stimulansz_view_minicart', 20 );
function stimulansz_view_minicart(){ ?>
        <a href="<?php echo esc_url( wc_get_checkout_url() ); ?>" class="button checkout wc-forward"><?php  echo _x('Checkout', 'Header Minicart Checkout label', 'stimulanz'); ?></a>
<?php }



/**
 * Remove Province field from checkout page
 */
add_filter( 'woocommerce_checkout_fields' , __NAMESPACE__ . '\\stimulansz_override_checkout_fields' );
function stimulansz_override_checkout_fields( $fields ) {
    unset($fields['billing']['billing_state']);
    unset($fields['shipping']['shipping_state']);
    return $fields;
}

/**
 * Replace woocommerce checkout place holder
 */
add_filter('woocommerce_default_address_fields',  __NAMESPACE__ . '\\stimulansz_override_address_fields');
function stimulansz_override_address_fields( $address_fields ) {
    $address_fields['address_2']['placeholder'] = 'optioneel';
    return $address_fields;
}

/** 
 *Reduce the strength requirement on the woocommerce password.
 * 
 * Strength Settings
 * 3 = Strong (default)
 * 2 = Medium
 * 1 = Weak
 * 0 = Very Weak / Anything
 */
function stimulansz_reduce_woocommerce_min_strength_requirement( $strength ) {
    return 2;
}
add_filter( 'woocommerce_min_password_strength',  __NAMESPACE__ . '\\stimulansz_reduce_woocommerce_min_strength_requirement' );


/*
* Change password hint tex
*/

function stimulansz_strength_meter_custom_strings( $data ) {
    $data_new = array(
        'i18n_password_hint'    => esc_attr__( 'Hint: The password must be at least 8 characters long. Use uppercase and lowercase letters, numbers and characters like! "? $% ^ &) to make it stronger.', 'woocommerce' )
    );
    return array_merge( $data, $data_new );
}
add_filter( 'wc_password_strength_meter_params',  __NAMESPACE__ . '\\stimulansz_strength_meter_custom_strings' );