<?php

namespace Roots\Sage\Custompost;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*
* Creating a function to create training CPT 
* 
*/

function training_post_type() {
// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Training', 'Post Type General Name', 'stimulansz' ),
		'singular_name'       => _x( 'Training', 'Post Type Singular Name', 'stimulansz' ),
		'menu_name'           => __( 'Training', 'stimulansz' ),
		'parent_item_colon'   => __( 'Parent training', 'stimulansz' ),
		'all_items'           => __( 'Alle Trainingen', 'stimulansz' ),
		'view_item'           => __( 'View training', 'stimulansz' ),
		'add_new_item'        => __( 'Add New training', 'stimulansz' ),
		'add_new'             => __( 'Nieuwe Training', 'stimulansz' ),
		'edit_item'           => __( 'Training bewerken', 'stimulansz' ),
		'update_item'         => __( 'Update training', 'stimulansz' ),
		'search_items'        => __( 'Search training', 'stimulansz' ),
		'not_found'           => __( 'Not Found', 'stimulansz' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'stimulansz' ),
	);
	
// Set other options for Custom Post Type
	$args = array(
		'label'               => __( 'training', 'stimulansz' ),
		'description'         => __( 'training news and reviews', 'stimulansz' ),
		'labels'              => $labels,
		// Features this CPT supports in Post Editor
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
		// You can associate this CPT with a taxonomy or custom taxonomy. 
		//'taxonomies'          => array( 'training-category' ),
		/* A hierarchical CPT is like Pages and can have
		* Parent and child items. A non-hierarchical CPT
		* is like Posts.
		*/	
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'training',
                'map_meta_cap'        => true,
               
                
	);
	
	// Registering your Custom Post Type
	register_post_type( 'training', $args );
        
        $category_labels = array(
            'name' => _x('Categorieën', 'taxonomy general name'),
            'singular_name' => _x('Categorieën', 'taxonomy singular name'),
            'search_items' => __('Search Training'),
            'all_items' => __('All training'),
            'parent_item' => __('Parent training'),
            'parent_item_colon' => __('Parent training:'),
            'edit_item' => __('Training bewerken'),
            'update_item' => __('Update training'),
            'add_new_item' => __('Nieuwe training'),
            'new_item_name' => __('Nieuwe training Name'),
            'menu_name' => __('Categorieën'),
        );

        // Now register the taxonomy

        register_taxonomy('training_cat', array('training'), array(
            'hierarchical' => true,
            'labels' => $category_labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'training_cat'),
        ));

		//Register taxonomay for workfield
		 $workfield_category_labels = array(
            'name' => _x('Werkveld', 'taxonomy general name'),
            'singular_name' => _x('Werkveld', 'taxonomy singular name'),
            'search_items' => __('Search Werkveld'),
            'all_items' => __('All Werkveld'),
            'parent_item' => __('Parent Werkveld'),
            'parent_item_colon' => __('Parent Werkveld'),
            'edit_item' => __('Edit Werkveld'),
            'update_item' => __('Update Werkveld'),
            'add_new_item' => __('Add New Werkveld'),
            'new_item_name' => __('New Werkveld Name'),
            'menu_name' => __('Werkveld'),
        );

        // Now register the taxonomy for workfield
        register_taxonomy('training_werkveld', array('training'), array(
            'hierarchical' => true,
            'labels' => $workfield_category_labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'training_werkveld'),
        ));
		
		
		
        //Register taxonomay for Region
         $region_category_labels = array(
        'name' => _x('Regio', 'taxonomy general name'),
        'singular_name' => _x('Regio', 'taxonomy singular name'),
        'search_items' => __('Search Regio'),
        'all_items' => __('All Regio'),
        'parent_item' => __('Parent Regio'),
        'parent_item_colon' => __('Parent Regio:'),
        'edit_item' => __('Edit Regio'),
        'update_item' => __('Update Regio'),
        'add_new_item' => __('Add New Regio'),
        'new_item_name' => __('New Regio Name'),
        'menu_name' => __('Regio'),
);

        // Now register the taxonomy for Region
        register_taxonomy('training_regio', array('training'),array(
            'hierarchical' => true,
            'labels' => $region_category_labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'training_regio'),
        ));
         //Register taxonomay for Region
         $functie_category_labels = array(
        'name' => _x('Functie', 'taxonomy general name'),
        'singular_name' => _x('Functie', 'taxonomy singular name'),
        'search_items' => __('Search Functie'),
        'all_items' => __('All Functie'),
        'parent_item' => __('Parent Functie'),
        'parent_item_colon' => __('Parent Functie:'),
        'edit_item' => __('Edit Regio'),
        'update_item' => __('Update Functie'),
        'add_new_item' => __('Add Functie'),
        'new_item_name' => __('New Functie Name'),
        'menu_name' => __('Functie'),
);

        // Now register the taxonomy for Region
        register_taxonomy('training_functie', array('training'),array(
            'hierarchical' => true,
            'labels' => $functie_category_labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'training_functie'),
        ));
		
		
}
add_action( 'init',  __NAMESPACE__ .'\\training_post_type', 0 );


/* Hook into the 'init' action so that the function
* Containing our post type landingspagina 
* 
*/

add_action( 'init',  __NAMESPACE__ .'\\landingspagina_post_type', 0 );

function landingspagina_post_type() {

    $Landingspagina_labels = array(
		'name'                => _x( 'Landingspagina', 'Post Type General Name', 'stimulansz' ),
		'singular_name'       => _x( 'Landingspagina', 'Post Type Singular Name', 'stimulansz' ),
		'menu_name'           => __( 'Landingspagina', 'stimulansz' ),
		'parent_item_colon'   => __( 'Parent Landingspagina', 'stimulansz' ),
		'all_items'           => __( 'Alle Landingspagina’s', 'stimulansz' ),
		'view_item'           => __( 'View Landingspagina', 'stimulansz' ),
		'add_new_item'        => __( 'Nieuwe Landingspagina', 'stimulansz' ),
		'add_new'             => __( 'Nieuwe Landingspagina', 'stimulansz' ),
		'edit_item'           => __( 'Landingspagina bewerken', 'stimulansz' ),
		'update_item'         => __( 'Update Landingspagina', 'stimulansz' ),
		'search_items'        => __( 'Search Landingspagina', 'stimulansz' ),
		'not_found'           => __( 'Not Found', 'stimulansz' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'stimulansz' ),
	);
    
    // Set other options for Custom Post Type
	$Landingspagina_args = array(
		'label'               => __( 'Landingspagina', 'stimulansz' ),
		'description'         => __( 'Landingspagina', 'stimulansz' ),
		'labels'              => $Landingspagina_labels,
		// Features this CPT supports in Post Editor
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields' ),
		// You can associate this CPT with a taxonomy or custom taxonomy. 
		/* A hierarchical CPT is like Pages and can have
		* Parent and child items. A non-hierarchical CPT
		* is like Posts.
		*/	
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 6,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'landingspagina',
                'map_meta_cap'        => true,
              // 'rewrite'             => array('slug' => '/'),
	);
    // Registering your Custom Post Type
    register_post_type('landingspagina', $Landingspagina_args );
    
    
     $academice_category_labels = array(
            'name' => _x('Categorieën', 'taxonomy general name'),
            'singular_name' => _x('Categorieën', 'taxonomy singular name'),
            'search_items' => __('Search Landingspagina'),
            'all_items' => __('Alle Landingspagina’s'),
            'parent_item' => __('Parent landingspagina'),
            'parent_item_colon' => __('Parent landingspagina'),
            'edit_item' => __('Edit Landingspagina'),
            'update_item' => __('Update Landingspagina'),
            'add_new_item' => __('Nieuwe Landingspagina'),
            'new_item_name' => __('New Landingspagina Name'),
            'menu_name' => __('Categorieën'),
        );

        // Now register the taxonomy

        register_taxonomy('landingspagina_cat', array('landingspagina'), array(
            'hierarchical' => true,
            'labels' => $academice_category_labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'landingspagina_cat'),
        ));
		
		
		
		
}
/* 
* Hook into the 'init' action so that the function
* Containing our post type instructeur  
* 
*/

add_action( 'init',  __NAMESPACE__ .'\\instructeur_post_type', 0 );

function instructeur_post_type() {

    $Instructeur_labels = array(
		'name'                => _x( 'Contactpersoon', 'Post Type General Name', 'stimulansz' ),
		'singular_name'       => _x( 'Contactpersoon', 'Post Type Singular Name', 'stimulansz' ),
		'menu_name'           => __( 'Contactpersoon', 'stimulansz' ),
		'parent_item_colon'   => __( 'Parent Contactpersoon', 'stimulansz' ),
		'all_items'           => __( 'Alle Contactpersonen', 'stimulansz' ),
		'view_item'           => __( 'View Contactpersoon', 'stimulansz' ),
		'add_new_item'        => __( 'Nieuw Contactpersoon', 'stimulansz' ),
		'add_new'             => __( 'Nieuw Contactpersoon', 'stimulansz' ),
		'edit_item'           => __( 'Contactpersoon bewerken', 'stimulansz' ),
		'update_item'         => __( 'Update Contactpersoon', 'stimulansz' ),
		'search_items'        => __( 'Search Contactpersoon', 'stimulansz' ),
		'not_found'           => __( 'Not Found', 'stimulansz' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'stimulansz' ),
	);
    
    // Set other options for Custom Post Type
	$Instructeur_args = array(
		'label'               => __( 'Contactpersoon', 'stimulansz' ),
		'description'         => __( 'Contactpersoon', 'stimulansz' ),
		'labels'              => $Instructeur_labels,
		// Features this CPT supports in Post Editor
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields' ),
		// You can associate this CPT with a taxonomy or custom taxonomy. 
		/* A hierarchical CPT is like Pages and can have
		* Parent and child items. A non-hierarchical CPT
		* is like Posts.
		*/	
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 6,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
                'capability_type'     => 'contactpersoon',
                'map_meta_cap'        => true,
            
	);
    // Registering your Custom Post Type
    register_post_type('contactpersoon', $Instructeur_args );
    
    $instructeur_category_labels = array(
            'name' => _x('Categorieën', 'taxonomy general name'),
            'singular_name' => _x('Categorieën', 'taxonomy singular name'),
            'search_items' => __('Search Contactpersoon'),
            'all_items' => __('All Contactpersoon'),
            'parent_item' => __('Parent Contactpersoon'),
            'parent_item_colon' => __('Parent Contactpersoon:'),
            'edit_item' => __('Contactpersoon bewerken'),
            'update_item' => __('Update Contactpersoon'),
            'add_new_item' => __('Nieuw Contactpersoon'),
            'new_item_name' => __('New Contactpersoon Name'),
            'menu_name' => __('Categorieën'),
        );

        // Now register the taxonomy
        register_taxonomy('contactpersoon_cat', array('contactpersoon'), array(
            'hierarchical' => true,
            'labels' => $instructeur_category_labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'landingspagina_cat'),
        ));
}

/* 
* Hook into the 'init' action so that the function
* Containing our post type Kennisbanken 
*
*/

add_action( 'init',  __NAMESPACE__ .'\\stimulansz_kennisbanken_post_type', 0 );

function stimulansz_kennisbanken_post_type() {

    $kennisbanken_labels = array(
		'name'                => _x( 'Kennisbanken', 'Post Type General Name', 'stimulansz' ),
		'singular_name'       => _x( 'Kennisbanken', 'Post Type Singular Name', 'stimulansz' ),
		'menu_name'           => __( 'Kennisbanken', 'stimulansz' ),
		'parent_item_colon'   => __( 'Parent Kennisbanken', 'stimulansz' ),
		'all_items'           => __( 'Alle Kennisbanken', 'stimulansz' ),
		'view_item'           => __( 'View Kennisbanken', 'stimulansz' ),
		'add_new_item'        => __( 'Nieuwe Kennisbank', 'stimulansz' ),
		'add_new'             => __( 'Nieuwe Kennisbank', 'stimulansz' ),
		'edit_item'           => __( 'Edit Kennisbanken', 'stimulansz' ),
		'update_item'         => __( 'Update Kennisbanken', 'stimulansz' ),
		'search_items'        => __( 'Search Kennisbanken', 'stimulansz' ),
		'not_found'           => __( 'Not Found', 'stimulansz' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'stimulansz' ),
	);
    
    // Set other options for Custom Post Type
	$kennisbanken_args = array(
		'label'               => __( 'Kennisbanken', 'stimulansz' ),
		'description'         => __( 'Kennisbanken', 'stimulansz' ),
		'labels'              => $kennisbanken_labels,
		// Features this CPT supports in Post Editor
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields' ),
		// You can associate this CPT with a taxonomy or custom taxonomy. 
		/* A hierarchical CPT is like Pages and can have
		* Parent and child items. A non-hierarchical CPT
		* is like Posts.
		*/	
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 6,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'kennisbanken',
                'map_meta_cap'        => true,
              
	);
    // Registering your Custom Post Type
    register_post_type('kennisbanken', $kennisbanken_args );
    
    
     $kennisbanken_category_labels = array(
            'name' => _x('Categorieën', 'taxonomy general name'),
            'singular_name' => _x('Categorieën', 'taxonomy singular name'),
            'search_items' => __('Search Kennisbanken'),
            'all_items' => __('All Kennisbanken'),
            'parent_item' => __('Parent Kennisbanken'),
            'parent_item_colon' => __('Parent Kennisbanken:'),
            'edit_item' => __('Kennisbank bewerken'),
            'update_item' => __('Update Kennisbanken'),
            'add_new_item' => __('Add New Kennisbanken'),
            'new_item_name' => __('New Kennisbanken Name'),
            'menu_name' => __('Categorieën'),
        );

        // Now register the taxonomy

        register_taxonomy('kennisbanken_cat', array('kennisbanken'), array(
            'hierarchical' => true,
            'labels' => $kennisbanken_category_labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'kennisbanken_cat'),
        ));
        
       //Register taxonomay for Werkveld
    $werkveld_kennisbanken_category_labels = array(
            'name' => _x('Werkveld', 'taxonomy general name'),
            'singular_name' => _x('Werkveld', 'taxonomy singular name'),
            'search_items' => __('Search Werkveld'),
            'all_items' => __('All Werkveld'),
            'parent_item' => __('Parent Werkveld'),
            'parent_item_colon' => __('Parent Werkveld:'),
            'edit_item' => __('Edit Werkveld'),
            'update_item' => __('Update Werkveld'),
            'add_new_item' => __('Add New Werkveld'),
            'new_item_name' => __('New Werkveld Name'),
            'menu_name' => __('Werkveld'),
        );

        // Now register the taxonomy for Werkveld
        register_taxonomy('kennisbanken_werkveld', array('kennisbanken'), array(
            'hierarchical' => true,
            'labels' => $werkveld_kennisbanken_category_labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'kennisbanken_werkveld'),
        ));
		
	
	//Register taxonomay for Niveau
	// Temporarily deactivated
	
   /* $niveau_kennisbanken_category_labels = array(
            'name' => _x('Niveau', 'taxonomy general name'),
            'singular_name' => _x('Niveau', 'taxonomy singular name'),
            'search_items' => __('Search Niveau'),
            'all_items' => __('All Niveau'),
            'parent_item' => __('Parent Niveau'),
            'parent_item_colon' => __('Parent Niveau:'),
            'edit_item' => __('Edit Niveau'),
            'update_item' => __('Update Niveau'),
            'add_new_item' => __('Add New Niveau'),
            'new_item_name' => __('New Niveau Name'),
            'menu_name' => __('Niveau'),
        );

        // Now register the taxonomy for Niveau
        register_taxonomy('kennisbanken_niveau', array('kennisbanken'), array(
            'hierarchical' => true,
            'labels' => $niveau_kennisbanken_category_labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'rewrite' => array('slug' =>'kennisbanken_niveau'),
        ));
		
		//Register taxonomay for Regio
        $regio_kennisbanken_category_labels = array(
            'name' => _x('Regio', 'taxonomy general name'),
            'singular_name' => _x('Regio', 'taxonomy singular name'),
            'search_items' => __('Search Regio'),
            'all_items' => __('All Regio'),
            'parent_item' => __('Parent Regio'),
            'parent_item_colon' => __('Parent Regio:'),
            'edit_item' => __('Edit Regio'),
            'update_item' => __('Update Regio'),
            'add_new_item' => __('Add New Regio'),
            'new_item_name' => __('New Regio Name'),
            'menu_name' => __('Regio'),
        );

        // Now register the taxonomy for Regio
        register_taxonomy('kennisbanken_regio', array('kennisbanken'),array(
            'hierarchical' => true,
            'labels' => $regio_kennisbanken_category_labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'kennisbanken_regio'),
        ));
		 */
 
        
}

/* 
* Hook into the 'init' action so that the function
* Containing our post type Advies
* 
*/

add_action( 'init',  __NAMESPACE__ .'\\stimulansz_advies_post_type', 0 );

function stimulansz_advies_post_type() {

    $advies_labels = array(
		'name'                => _x( 'Advies', 'Post Type General Name', 'stimulansz' ),
		'singular_name'       => _x( 'Advies', 'Post Type Singular Name', 'stimulansz' ),
		'menu_name'           => __( 'Advies', 'stimulansz' ),
		'parent_item_colon'   => __( 'Parent Advies', 'stimulansz' ),
		'all_items'           => __( 'Alle Adviezen', 'stimulansz' ),
		'view_item'           => __( 'View Advies', 'stimulansz' ),
		'add_new_item'        => __( 'Nieuw Advies', 'stimulansz' ),
		'add_new'             => __( 'Nieuw Advies', 'stimulansz' ),
		'edit_item'           => __( 'Adviesbewerken', 'stimulansz' ),
		'update_item'         => __( 'Update Advies', 'stimulansz' ),
		'search_items'        => __( 'Search Advies', 'stimulansz' ),
		'not_found'           => __( 'Not Found', 'stimulansz' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'stimulansz' ),
	);
    
    // Set other options for Custom Post Type
	$advies_args = array(
		'label'               => __( 'Advies', 'stimulansz' ),
		'description'         => __( 'Advies', 'stimulansz' ),
		'labels'              => $advies_labels,
		// Features this CPT supports in Post Editor
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields' ),
		// You can associate this CPT with a taxonomy or custom taxonomy. 
		/* A hierarchical CPT is like Pages and can have
		* Parent and child items. A non-hierarchical CPT
		* is like Posts.
		*/	
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 7,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'advies',
                'map_meta_cap'        => true,
            
	);
    // Registering your Custom Post Type
    register_post_type('advies', $advies_args );
    
    
     $advies_category_labels = array(
            'name' => _x('Categorieën', 'taxonomy general name'),
            'singular_name' => _x('Categorieën', 'taxonomy singular name'),
            'search_items' => __('Search Advies'),
            'all_items' => __('All Advies'),
            'parent_item' => __('Parent Advies'),
            'parent_item_colon' => __('Parent Advies:'),
            'edit_item' => __('Adviesbewerken'),
            'update_item' => __('Update Advies'),
            'add_new_item' => __('Add New Advies'),
            'new_item_name' => __('New Advies Name'),
            'menu_name' => __('Categorieën'),
        );

        // Now register the taxonomy

        register_taxonomy('advies_cat', array('advies'), array(
            'hierarchical' => true,
            'labels' => $advies_category_labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'advies_cat'),
        ));
        
        
        //Register taxonomay for Werkveld
    $werkveld_advies_category_labels = array(
            'name' => _x('Werkveld', 'taxonomy general name'),
            'singular_name' => _x('Werkveld', 'taxonomy singular name'),
            'search_items' => __('Search Werkveld'),
            'all_items' => __('All Werkveld'),
            'parent_item' => __('Parent Werkveld'),
            'parent_item_colon' => __('Parent Werkveld:'),
            'edit_item' => __('Edit Werkveld'),
            'update_item' => __('Update Werkveld'),
            'add_new_item' => __('Add New Werkveld'),
            'new_item_name' => __('New Werkveld Name'),
            'menu_name' => __('Werkveld'),
        );

        // Now register the taxonomy for Werkveld
        register_taxonomy('advies_werkveld', array('advies'), array(
            'hierarchical' => true,
            'labels' => $werkveld_advies_category_labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'advies_werkveld'),
        ));
		
	
	//Register taxonomay for Niveau
    $niveau_advies_category_labels = array(
            'name' => _x('Niveau', 'taxonomy general name'),
            'singular_name' => _x('Niveau', 'taxonomy singular name'),
            'search_items' => __('Search Niveau'),
            'all_items' => __('All Niveau'),
            'parent_item' => __('Parent Niveau'),
            'parent_item_colon' => __('Parent Niveau:'),
            'edit_item' => __('Edit Niveau'),
            'update_item' => __('Update Niveau'),
            'add_new_item' => __('Add New Niveau'),
            'new_item_name' => __('New Niveau Name'),
            'menu_name' => __('Niveau'),
        );

        // Now register the taxonomy for Niveau
        register_taxonomy('advies_niveau', array('advies'), array(
            'hierarchical' => true,
            'labels' => $niveau_advies_category_labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'rewrite' => array('slug' =>'advies_niveau'),
        ));
		
        //Register taxonomay for Regio
        $regio_advies_category_labels = array(
            'name' => _x('Regio', 'taxonomy general name'),
            'singular_name' => _x('Regio', 'taxonomy singular name'),
            'search_items' => __('Search Regio'),
            'all_items' => __('All Regio'),
            'parent_item' => __('Parent Regio'),
            'parent_item_colon' => __('Parent Regio:'),
            'edit_item' => __('Edit Regio'),
            'update_item' => __('Update Regio'),
            'add_new_item' => __('Add New Regio'),
            'new_item_name' => __('New Regio Name'),
            'menu_name' => __('Regio'),
        );

        // Now register the taxonomy for Regio
        register_taxonomy('advies_regio', array('advies'),array(
            'hierarchical' => true,
            'labels' => $regio_advies_category_labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'advies_regio'),
        ));
		
        //Blog Extra taxonomy
        $actueel_category_labels = array(
            'name' => _x('Actueel type', 'taxonomy general name'),
            'singular_name' => _x('Actueel type', 'taxonomy singular name'),
            'search_items' => __('Search Actueel'),
            'all_items' => __('All Actueel'),
            'parent_item' => __('Parent Actueel'),
            'parent_item_colon' => __('Parent Actueel:'),
            'edit_item' => __('Edit Actueel'),
            'update_item' => __('Update Actueel'),
            'add_new_item' => __('Add New Actueel'),
            'new_item_name' => __('New Actueel Name'),
            'menu_name' => __('Actueel'),
        );

        // Now register the taxonomy for Regio
        register_taxonomy('actueel', array('post'),array(
            'hierarchical' => true,
            'labels' => $actueel_category_labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'actueel'),
        ));
		
		
}
