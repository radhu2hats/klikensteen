<!-- responsive menu mobile -->
<nav id="menu" class="menu slideout-menu slideout-menu-right">

    <?php
    if (has_nav_menu('header-menu')) :
        wp_nav_menu(
            array(
                'theme_location' => 'header-menu',
                'container_class' => 'top_menu_wraper header_section_top',
                'menu_class' => 'list-inline',
                'menu_id' => 'top_menu_header_wrapper',
                'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            )
        );
    endif;
    ?>

</nav>
<!-- .responsive menu mobile -->
<div class="wp">
    <script type="text/javascript">
        jQuery(window).load(function() {
            jQuery("html, body").animate({
                scrollTop: 0
            }, 10);
            jQuery('.wp').css("display", "block");
            //setTimeout(function(){$('.wp').css("display","none")},2000)
        });
    </script>

    <div class="header-form">
        <?php echo do_shortcode('[mailplusform formid=12091 /]'); ?>
    </div>
    <script>
        jQuery(window).on('beforeunload', function() {
            jQuery(window).scrollTop(0);
        });
    </script>
    <!-- header -->
    <header class="site-header" id="masterhead">

        <!-- top navigation -->
        <div class="header-top-block">
            <div class="container-fluid">
                <div class="top-menu-conatiner">
                    <?php
                    if (has_nav_menu('top-bar-menu')) :
                        wp_nav_menu(
                            array(
                                'theme_location' => 'top-bar-menu',
                                'container_class' => 'top_menu_wraper header_section_top',
                                'menu_class' => 'list-inline',
                                'menu_id' => 'top_menu_header_wrapper',
                                'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                            )
                        );
                    endif;
                    ?>
                    <div class="user-status header_section_top">
                        <?php do_action('stimulanz_login_menu_items'); ?>
                    </div>
                    <div class="top-menu-cart header_section_top">
                        <?php do_action('stimulanz_ajaxify_minicart');  ?>
                    </div>
                    <div class="js-slideout-toggle">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>

        </div>
        <!-- top navigation -->
        <!-- main headerblock -->
        <div class="header-bottom-block">
            <div class="container-fluid site-header-wrapper ">
                <div class="logo-navcontainer">
                    <!-- Upload logo  start section -->
                    <div class="logo-wrapper header_section">
                        <div id="site-logo" class="site-branding clr header-one-logo">
                            <div id="site-logo-inner" class="clr">
                                <?php if (get_theme_mod('m1_logo')) : ?>
                                    <a class="upload-logo-link" href="<?php echo esc_url(home_url('/')); ?>" id="site-logo" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
                                        <img class="upload-logo-img img-responsive" src="<?php echo get_theme_mod('m1_logo'); ?>" alt="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>">
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <!-- Upload logo  End section -->
                    <div class="nav-outer" >
                            <?php
                            wp_nav_menu(
                                array(
                                    'menu' => 'header-menu',
                                    'container' => '',
                                    'theme_location' => 'header-menu',
                                    'items_wrap' => '<ul>%3$s</ul>'
                                )
                            );
                            ?>
                        <button class="headsearch-btn" type="button"><i class="fas fa-search"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- main headerblock -->
<div class="overlay-outerblock"></div>




        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <meta name="google-site-verification" content="eoWq3UnL6GUZfAVzPiCrXHCk95rqOZSZiJNdwGTOQqQ" />
        <meta name="google-site-verification" content="hk3Rip0yu6F1LYqRlb1grSNkBO_Ccy8qrY8oOi7k8Rc" />
        <link href="<?php echo get_template_directory_uri(); ?>/assets/page-style.css" rel="stylesheet" type="text/css">
        <link href="<?php echo get_template_directory_uri(); ?>/dist/styles/overide.css" rel="stylesheet" type="text/css">
        <link href="<?php echo get_template_directory_uri(); ?>/dist/styles/responsive.css" rel="stylesheet" type="text/css">




    </header>
    <!-- .header -->

    <!-- breadcrumbs and search area -->
    <div class="breadcrumbs_search_area">
        <div class="container">
            <div class="breadcrumbs_wrapper breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">

                <?php
                if (function_exists('bcn_display')) {
                    bcn_display();
                }
                ?>
            </div>
            <!--  
 <div class="top_search">
    <?php echo get_search_form(); ?>
</div> -->

        </div>
    </div>
    <!-- .breadcrumbs and search area -->

    <!-- login popup -->
    <?php
    if (!is_user_logged_in()) {
    ?>
        <div id="login-popup" class="login-popup-container">
            <div class="login-popup-block">
                <div class="close-login-popup"><i class="fa fa-times" aria-hidden="true"></i></div>
                <div class="login-content">
                    <div class="login-option">
                        <div class="logo">
                            <img class="img-responsive" src="https://stimulansz-dev.klikensteen.nl/wp-content/uploads/2017/08/Stimulansz_logo_digitaal_blauw_rgb-1.svg" alt="stimulansz">
                        </div>
                        <div class="title"><?php _e('Juridische Kennisbanken Inzicht sociaal domein', 'stimulansz'); ?></div>
                        <div class="button">
                            <a href="<?php echo get_field('external_portal_link', 'option'); ?>" class="blue-btn"><?php _e('', 'stimulansz'); ?>inloggen</a>
                        </div>
                    </div>
                    <div class="login-option">
                        <div class="logo">
                            <img class="img-responsive" src="https://stimulansz-dev.klikensteen.nl/wp-content/uploads/2017/08/Stimulansz_logo_digitaal_blauw_rgb-1.svg" alt="stimulansz">
                        </div>
                        <div class="title"><?php _e('Winkelaccount website Stimulansz', 'stimulansz'); ?></div>
                        <div class="button">
                            <a href="<?php echo get_field('aanmelden', 'option'); ?>" class="blue-btn"><?php _e('inloggen', 'stimulansz'); ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <!-- .login popup -->