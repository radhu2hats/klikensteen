<?php
if ( ! class_exists( 'VcCourses' ) ) {

  class VcCourses extends WPBakeryShortCode {

      //Initialize Component
      function __construct() {
          add_action( 'init', array( $this, 'create_shortcode' ), 999 );            
          add_shortcode( 'vc_courses', array( $this, 'render_shortcode' ) );

      }        

      //Map Component
      public function create_shortcode() {
        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }        
      
        // Map courses with vc_map()
        vc_map(
          array(
            "name" => __("Courses Repeater", "stimulansz"), // Element name
            "base" => "vc_courses", // Element shortcode
            "class" => "box-repeater",
            "category" => __('Content', 'stimulansz'),
            'params' => array(
              array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "admin_label" => true,
                "heading" => __("Heading", "stimulansz"),
                "param_name" => "box_repeater_heading",
                "value" => __("", "stimulansz"),
                "description" => __('Add heading here', "stimulansz")
              ),
              // array(
              //   'type' => 'param_group',
              //   'param_name' => 'box_repeater_items',
              //   'params' => array(
              //     array(
              //       "type" => "textfield",
              //       "holder" => "div",
              //       "class" => "",
              //       "admin_label" => true,
              //       "heading" => __("Title", "stimulansz"),
              //       "param_name" => "box_repeater_items_title",
              //       "value" => __("", "stimulansz"),
              //     ),
              //     array(
              //       "type" => "textfield",                            
              //       "class" => "",
              //       "admin_label" => true,
              //       "heading" => __("Sub Title", "stimulansz"),
              //       "param_name" => "box_repeater_items_subtitle",
              //       "value" => __("", "stimulansz"),
              //     ),
              //     array(
              //       "type" => "vc_link",
              //       "class" => "",
              //       "heading" => __( "Link", "stimulansz" ),
              //       "param_name" => "box_repeater_items_url",
              //       "value" => '',
              //       "description" => __( "Select Link", "stimulansz" )
              //     ),
              //     array(
              //       "type" => "colorpicker",
              //       "class" => "",
              //       "heading" => __( "Box color", "my-text-domain" ),
              //       "param_name" => "box_repeater_items_bgcolor",
              //       "value" => '#FF0000', //Default Red color
              //       "description" => __( "Choose box color", "stimulansz" )
              //     ),
              //     array(
              //       "type" => "colorpicker",
              //       "class" => "",
              //       "heading" => __( "Text color", "my-text-domain" ),
              //       "param_name" => "box_repeater_items_textcolor",
              //       "value" => '#FFFFFF', //Default White color
              //       "description" => __( "Choose text color", "stimulansz" )
              //     )
              //   )
              // )
            )
          )
        );           
      
      }

      //Render Component
      public function render_shortcode( $atts ) {

        // print_r($atts);

        ob_start();

        $atts = (shortcode_atts(array(
            'box_repeater_heading'=>'',
            'box_repeater_items' =>'', 
            'extra_class'       => '',
            'element_id'        => ''
        ), $atts));
    
    
        //Content 
        // $content = wpb_js_remove_wpautop($content, true);
        $heading = $atts['box_repeater_heading'];  
        $items = vc_param_group_parse_atts($atts['box_repeater_items']);

        $terms = get_terms(
          array(
              'taxonomy'   => 'training_werkveld',
              'hide_empty' => true,
          )
        );
    
        //Class and Id
        $extra_class        = esc_attr($atts['extra_class']);
        $element_id         = esc_attr($atts['element_id']);

        ?>
        <div class="box-repeater">

            <?php  echo (!empty($heading))? '<h2>'.$heading.'</h2>': ''; ?>

            <?php if($terms) { ?>
              <div class="slider box-repeater-items">
                  <?php 
                  foreach ($terms as $term):
                  $term_link = get_term_link( $term );
                  $werkveld_background_color = get_field('werkveld_background_color', $term);
                  $werkveld_text_color = get_field('werkveld_text_color', $term);
                  $clr = 'black';
                  if($werkveld_text_color == '#ffffff'){
                    $clr = 'white';
                  }
                  ?>
                  <div class="item-box">
                    <div class="info-box">
                      <a href="<?php echo $term_link; ?>" title="<?php echo $term->name; ?>" style="background-color: <?php echo $werkveld_background_color; ?>; color: <?php echo $werkveld_text_color; ?>">
                        <div class="title" style="color: <?php echo $werkveld_text_color; ?>"><?php echo $term->name; ?></div>
                        <div class="subtitle <?php echo $clr; ?>" style="color: <?php echo $werkveld_text_color; ?>"><?php echo $term->count; ?> trainingen</div>
                      </a>
                    </div>
                  </div>
                  <?php
                  endforeach;
                  ?>
              </div>
              <div class="progress" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                <span class="slider__label sr-only">
              </div>
            <?php } ?>

        </div>
        <?php
        $result = ob_get_clean();
        return $result;
    
    
        // $output = '';
        // $output .= '<div class="score-block ' . $extra_class . '" id="' . $element_id . '" >';
        // $output .= '<div class="score-block-inner">';
        // $output .= '<div class="title">'.$content.'</div>';
        // $output .= '<div class="score">'.$score.'</div>';
        // $output .= '</div>';
        // $output .= '</div>';
    
        // return $output;                  
    
    }

  }

  new VcCourses();

}

