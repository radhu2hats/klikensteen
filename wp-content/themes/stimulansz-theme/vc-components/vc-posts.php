<?php
if ( ! class_exists( 'VcPosts' ) ) {

  class VcPosts extends WPBakeryShortCode {

      //Initialize Component
      function __construct() {
          add_action( 'init', array( $this, 'create_shortcode' ), 999 );            
          add_shortcode( 'vc_posts', array( $this, 'render_shortcode' ) );

      }        

      //Map Component
      public function create_shortcode() {
        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }        
      
        // Map courses with vc_map()
        vc_map(
          array(
            "name" => __("Add Posts", "stimulansz"), // Element name
            "base" => "vc_posts", // Element shortcode
            "class" => "box-repeater",
            "category" => __('Content', 'stimulansz'),
            'params' => array(
              array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "admin_label" => true,
                "heading" => __("Heading", "stimulansz"),
                "param_name" => "post_repeater_heading",
                "value" => __("", "stimulansz"),
                "description" => __('Add heading here', "stimulansz")
              ),
              array(
                "type" => "vc_link",
                "class" => "",
                "heading" => __( "Link", "stimulansz" ),
                "param_name" => "post_repeater_linnk",
                "value" => '',
                "description" => __( "Select Link", "stimulansz" )
              ),
              array(
                'type' => 'param_group',
                'param_name' => 'post_repeater_items',
                'params' => array(
                  array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "admin_label" => true,
                    "heading" => __("Post ID", "stimulansz"),
                    "param_name" => "post_repeater_items_title",
                    "value" => __("", "stimulansz"),
                  )
                )
              )
            )
          )
        );           
      
      }

      //Render Component
      public function render_shortcode( $atts ) {

        ob_start();

        $atts = (shortcode_atts(array(
            'post_repeater_heading'=>'',
            'post_repeater_linnk'=>'',
            'post_repeater_items' =>'', 
            'extra_class'       => '',
            'element_id'        => ''
        ), $atts));
    
    
        //Content 
        $heading = $atts['post_repeater_heading'];  
        $link = vc_build_link($atts['post_repeater_linnk']);  
        $items = vc_param_group_parse_atts($atts['post_repeater_items']);

        // print_r($link);
    
        //Class and Id
        $extra_class        = esc_attr($atts['extra_class']);
        $element_id         = esc_attr($atts['element_id']);

        $posts = array();

        if($items):
        foreach ($items as  $item):
        array_push($posts, $item['post_repeater_items_title']);
        endforeach;  
        endif; 

        ?>
        <div class="post-repeater">

            <div class="title">
              <?php  
              echo (!empty($heading))? '<h2>'.$heading.'</h2>': '';
              echo '<a class="slide-right-arrow" href="'.$link['url'].'" title="'.$link['title'].'">'.$link['title'].'</a>';
              ?>
            </div>        
            
            <?php
            $args = array(
              'post__in' => $posts,
              // 'orderby' => 'post__in',
              'post_type' => 'training',
            );
            $the_query = new WP_Query( $args );

            // The Loop
            if ( $the_query->have_posts() ) :
              echo '<div class="card-blocks-wrap slider-cards-wrap '.$extra_class.'" id="'.$element_id.'">';
              while ( $the_query->have_posts() ) :
              $the_query->the_post();
              $image = wp_get_attachment_url(get_post_thumbnail_id($the_query->ID), 'thumbnail');
              // $image = get_field('background_image', $the_query->ID);
              $term_obj_list = get_the_terms( $the_query->ID, 'training_cat' );
              $terms_string = join(', ', wp_list_pluck($term_obj_list, 'name'));
              ?>

              <div class="card card-block white_bg_block post-list-item">
                <div class="card-inner">
                <a href="<?php echo get_the_permalink(); ?>" >
                  <div class="img-wrap">
                  <img src="<?php  echo $image; 
                                          ?>" alt="<?php  echo $the_query->post_title   ?>">
                        <?php 
                        $new_label = get_field('training_post_new_label');
                        if($new_label): ?> 
                    <label class="new-sticker"> <?php echo $new_label[0]; ?></label>
                 <?php endif;?>
                  </div>
                  <div class="cnt-wrap">
                    <div class="title_wrap">
                      <div class="cat"><?php echo $terms_string; ?></div>
                      <div class="title"><?php echo get_the_title(); ?></div>
                      <p><?php echo get_the_excerpt(); ?></p>
                    </div>
                    <div class="sub_title_wrap">
                      <div class="sub_title sub_title_1"><?php echo get_field('duration', $query->ID) ?></div>
                      <span class="readmore"> <i class="fas fa-arrow-right">&nbsp;</i> </span>
                    </div>
                  </div>
                        </a>
                </div>
              </div>
                    
              <?php
              endwhile;
              echo '</div>';
            endif;
            /* Restore original Post Data */
            wp_reset_postdata();
            ?>


        </div>
        <div class="progress" role="progressbar" aria-valuemin="0" aria-valuemax="100">
          <span class="slider__label sr-only">
        </div>
        <div class="button-mobile">
            <?php
            echo '<a class="slide-right-arrow" href="'.$link['url'].'" title="'.$link['title'].'">'.$link['title'].'</a>';
            ?>
            </div>
        <?php
        $result = ob_get_clean();
        return $result;               
    
    }

  }

  new VcPosts();

}