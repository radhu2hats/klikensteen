<?php
if ( ! class_exists( 'VcCard' ) ) {
  class VcCard extends WPBakeryShortCode {

    function __construct() {
        add_action( 'init', array( $this, 'create_shortcode' ), 999 );            
        add_shortcode( 'vc_card', array( $this, 'render_shortcode' ) );

    }        

    public function create_shortcode() {
        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }        

        // Map card with vc_map()
        vc_map( array(
            'name'          => __('Card', 'stimulansz'),
            'base'          => 'vc_card',
            'description'  	=> __( '', 'stimulansz' ),
            'category'      => __( 'Stimulanz Element', 'stimulansz'),                
            'params' => array(

                array(
                  'type'          => 'attach_image',
                  'holder'        => 'div',
                  'heading'       => __( 'Image', 'stimulansz' ),
                  'param_name'    => 'card_image',
                  'value'         => __( '', 'stimulansz' ),
                  'description'   => __( 'Add Card Image.', 'stimulansz' ),
                ),

                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'heading'       => __( 'Title', 'stimulansz' ),
                    'param_name'    => 'title',
                    'value'         => __( '', 'stimulansz' ),
                    'description'   => __( 'Add Title.', 'stimulansz' ),
                ),

                array(
                  "type" => "textarea_html",
                  "holder" => "div",
                  "class" => "",                     
                  "heading" => __( "Card Content", 'stimulansz' ),
                  "param_name" => "content", // Important: Only one textarea_html param per content element allowed and it should have "content" as a "param_name"
                  "value" => __( "<p>I am test text block. Click edit button to change this text.</p>", 'stimulansz' ),
                  "description" => __( "Enter content.", 'stimulansz' )
                ),

                array(
                  'type'          => 'textfield',
                  'holder'        => 'div',
                  'heading'       => __( 'Sub title 1', 'stimulansz' ),
                  'param_name'    => 'sub_title_1',
                  'value'         => __( '', 'stimulansz' ),
                  'description'   => __( 'Add Sub title 1.', 'stimulansz' ),
                ),

                array(
                  'type'          => 'textfield',
                  'holder'        => 'div',
                  'heading'       => __( 'Sub title 2', 'stimulansz' ),
                  'param_name'    => 'sub_title_2',
                  'value'         => __( '', 'stimulansz' ),
                  'description'   => __( 'Add Sub title 2.', 'stimulansz' ),
                ),

                array(
                    'type'          => 'textfield',
                    'heading'       => __( 'Element ID', 'stimulansz' ),
                    'param_name'    => 'element_id',
                    'value'             => __( '', 'stimulansz' ),
                    'description'   => __( 'Enter element ID (Note: make sure it is unique and valid).', 'stimulansz' ),
                    'group'         => __( 'Extra', 'stimulansz'),
                ),

                array(
                    'type'          => 'textfield',
                    'heading'       => __( 'Extra class name', 'stimulansz' ),
                    'param_name'    => 'extra_class',
                    'value'             => __( '', 'stimulansz' ),
                    'description'   => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'stimulansz' ),
                    'group'         => __( 'Extra', 'stimulansz'),
                ),               
            ),
        ));             

    }

    public function render_shortcode( $atts, $content, $tag ) {
        $atts = (shortcode_atts(array(
            'card_image'   => '',
            'title'      => '',
            'sub_title_1'      => '',
            'sub_title_2'      => '',
            'extra_class'       => '',
            'element_id'        => ''
        ), $atts));


        //Content 
        $content            = wpb_js_remove_wpautop($content, true);
        $title       = esc_html($atts['title']);
        $card_image       = esc_html($atts['card_image']);
        $sub_title_1       = esc_html($atts['sub_title_1']);
        $sub_title_2       = esc_html($atts['sub_title_2']);
        // $img_atts = wp_get_attachment_image_src($card_image, 'stimulansz_card_image');

        //Class and Id
        $extra_class        = esc_attr($atts['extra_class']);
        $element_id         = esc_attr($atts['element_id']);

        $output = '';
        $output .= '<div class="card card-block ' . $extra_class . '" id="' . $element_id . '" >';
          $output .= '<div class="card-inner">';
            // $output .= '<div class="img-wrap"><img src="' .$img_atts[0]. '" /></div>';
            $output .= '<div class="img-wrap">'. wp_get_attachment_image($card_image, 'stimulansz_card_image') .'</div>';
            $output .= '<div class="cnt-wrap">';
              $output .= '<div class="title_wrap">';
                $output .= '<div class="title">' . $title . '</div>';
                $output .= $content;
              $output .= '</div>';
              if($sub_title_1 !='' || $sub_title_2 != ''):
              $output .= '<div class="sub_title_wrap">';
                $output .= '<div class="sub_title sub_title_1">' . $sub_title_1 . '</div>';
                $output .= '<div class="sub_title sub_title_2">' . $sub_title_2 . '</div>';
              $output .= '</div>';
              endif;
            $output .= '</div>';
          $output .= '</div>';
        $output .= '</div>';

        return $output;                  

    }

  }
}

new VcCard();