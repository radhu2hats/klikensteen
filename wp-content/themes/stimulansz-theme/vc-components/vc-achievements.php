<?php
if ( ! class_exists( 'VcAchievements' ) ) {

  class VcAchievements extends WPBakeryShortCode {

      //Initialize Component
      function __construct() {
          add_action( 'init', array( $this, 'create_shortcode' ), 999 );            
          add_shortcode( 'vc_achievements', array( $this, 'render_shortcode' ) );

      }        

      //Map Component
      public function create_shortcode() {
        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }        
      
        // Map blockquote with vc_map()
        vc_map( array(
            'name'          => __('Achievements', 'stimulansz'),
            'base'          => 'vc_achievements',
            'description'  	=> __( '', 'stimulansz' ),
            'category'      => __( 'Stimulanz Element', 'stimulansz'),                
            'params' => array(
      
                array(
                    "type" => "textarea_html",
                    "holder" => "div",
                    "class" => "",                     
                    "heading" => __( "Content", 'stimulansz' ),
                    "param_name" => "content", // Important: Only one textarea_html param per content element allowed and it should have "content" as a "param_name"
                    "value" => __( "<p>I am test text block. Click edit button to change this text.</p>", 'stimulansz' ),
                    "description" => __( "Enter content.", 'stimulansz' )
                ),    
      
                array(
                    'type'          => 'textfield',
                    'heading'       => __( 'Score', 'stimulansz' ),
                    'param_name'    => 'score',
                    'value'         => __( '', 'stimulansz' ),
                    'description'   => __( 'Add Score.', 'stimulansz' ),
                )        
            ),
        ));             
      
      }

      //Render Component
      public function render_shortcode( $atts, $content, $tag ) {
        $atts = (shortcode_atts(array(
            'score'      => '',
            'extra_class'       => '',
            'element_id'        => ''
        ), $atts));
    
    
        //Content 
        $content            = wpb_js_remove_wpautop($content, true);
        $score       = esc_html($atts['score']);
    
        //Class and Id
        $extra_class        = esc_attr($atts['extra_class']);
        $element_id         = esc_attr($atts['element_id']);
    
    
        $output = '';
        $output .= '<div class="score-block ' . $extra_class . '" id="' . $element_id . '" >';
        $output .= '<div class="score-block-inner">';
        $output .= '<div class="title">'.$content.'</div>';
        $output .= '<div class="score">'.$score.'</div>';
        $output .= '</div>';
        $output .= '</div>';
    
        return $output;                  
    
    }

  }

  new VcAchievements();

}

