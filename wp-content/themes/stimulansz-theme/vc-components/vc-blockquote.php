<?php
if ( ! class_exists( 'VcBlockquote' ) ) {
  class VcBlockquote extends WPBakeryShortCode {

    function __construct() {
        add_action( 'init', array( $this, 'create_shortcode' ), 999 );            
        add_shortcode( 'vc_blockquote', array( $this, 'render_shortcode' ) );

    }        

    public function create_shortcode() {
        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }        

        // Map blockquote with vc_map()
        vc_map( array(
            'name'          => __('Blockquote', 'stimulansz'),
            'base'          => 'vc_blockquote',
            'description'  	=> __( '', 'stimulansz' ),
            'category'      => __( 'Stimulanz Element', 'stimulansz'),                
            'params' => array(

                array(
                    "type" => "textarea_html",
                    "holder" => "div",
                    "class" => "",                     
                    "heading" => __( "Blockquote Content", 'stimulansz' ),
                    "param_name" => "content", // Important: Only one textarea_html param per content element allowed and it should have "content" as a "param_name"
                    "value" => __( "<p>I am test text block. Click edit button to change this text.</p>", 'stimulansz' ),
                    "description" => __( "Enter content.", 'stimulansz' )
                ),    

                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'heading'       => __( 'Author Name', 'stimulansz' ),
                    'param_name'    => 'name',
                    'value'         => __( '', 'stimulansz' ),
                    'description'   => __( 'Add Author Name.', 'stimulansz' ),
                ),

                array(
                  'type'          => 'textfield',
                  'holder'        => 'div',
                  'heading'       => __( 'Author Designation', 'stimulansz' ),
                  'param_name'    => 'designation',
                  'value'         => __( '', 'stimulansz' ),
                  'description'   => __( 'Add Author Designation.', 'stimulansz' ),
              ),

                array(
                    'type'          => 'textfield',
                    'heading'       => __( 'Element ID', 'stimulansz' ),
                    'param_name'    => 'element_id',
                    'value'             => __( '', 'stimulansz' ),
                    'description'   => __( 'Enter element ID (Note: make sure it is unique and valid).', 'stimulansz' ),
                    'group'         => __( 'Extra', 'stimulansz'),
                ),

                array(
                    'type'          => 'textfield',
                    'heading'       => __( 'Extra class name', 'stimulansz' ),
                    'param_name'    => 'extra_class',
                    'value'             => __( '', 'stimulansz' ),
                    'description'   => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'stimulansz' ),
                    'group'         => __( 'Extra', 'stimulansz'),
                ),               
            ),
        ));             

    }

    public function render_shortcode( $atts, $content, $tag ) {
        $atts = (shortcode_atts(array(
            'name'   => '',
            'designation'      => '',
            'extra_class'       => '',
            'element_id'        => ''
        ), $atts));


        //Content 
        $content            = wpb_js_remove_wpautop($content, true);
        $name       = esc_html($atts['name']);
        $designation       = esc_html($atts['designation']);

        //Class and Id
        $extra_class        = esc_attr($atts['extra_class']);
        $element_id         = esc_attr($atts['element_id']);
        


        $output = '';
        $output .= '<div class="blockquote white_bg_block ' . $extra_class . '" id="' . $element_id . '" >';
          $output .= '<div class="blockquote-inner">';
          $output .= $content;
          $output .= '<div class="name">' . $name . '</div>';
          $output .= '<div class="designation">' . $designation . '</div>';
          $output .= '</div>';
        $output .= '</div>';

        return $output;                  

    }

  }
}

new VcBlockquote();