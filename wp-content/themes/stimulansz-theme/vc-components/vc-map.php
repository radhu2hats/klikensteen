<?php
if ( ! class_exists( 'VcGmap' ) ) {
  class VcGmap extends WPBakeryShortCode {

    function __construct() {
        add_action( 'init', array( $this, 'create_shortcode' ), 999 );            
        add_shortcode( 'vc_gmap', array( $this, 'render_shortcode' ) );

    }        

    public function create_shortcode() {
        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }        

        // Map google map with vc_map()
        vc_map( array(
            'name'          => __('Gmap', 'stimulansz'),
            'base'          => 'vc_gmap',
            'description'  	=> __( '', 'stimulansz' ),
            'category'      => __( 'Stimulanz Element', 'stimulansz'),                
            'params' => array(

               
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'heading'       => __( 'Latitude', 'stimulansz' ),
                    'param_name'    => 'lat',
                    'value'         => __( '', 'stimulansz' ),
                    'description'   => __( 'Add Latitude.', 'stimulansz' ),
                ),

                array(
                  'type'          => 'textfield',
                  'holder'        => 'div',
                  'heading'       => __( 'Longitude', 'stimulansz' ),
                  'param_name'    => 'log',
                  'value'         => __( '', 'stimulansz' ),
                  'description'   => __( 'Add Longitude.', 'stimulansz' ),
              ),

                array(
                    'type'          => 'textfield',
                    'heading'       => __( 'Element ID', 'stimulansz' ),
                    'param_name'    => 'element_id',
                    'value'             => __( '', 'stimulansz' ),
                    'description'   => __( 'Enter element ID (Note: make sure it is unique and valid).', 'stimulansz' ),
                    'group'         => __( 'Extra', 'stimulansz'),
                ),

                array(
                    'type'          => 'textfield',
                    'heading'       => __( 'Extra class name', 'stimulansz' ),
                    'param_name'    => 'extra_class',
                    'value'             => __( '', 'stimulansz' ),
                    'description'   => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'stimulansz' ),
                    'group'         => __( 'Extra', 'stimulansz'),
                ),               
            ),
        ));             

    }

    public function render_shortcode( $atts, $content, $tag ) {
        $atts = (shortcode_atts(array(
            'lat'   => '',
            'log'      => '',
            'extra_class'       => '',
            'element_id'        => ''
        ), $atts));


        //Content 
        $content            = wpb_js_remove_wpautop($content, true);
        $latitude       = esc_html($atts['lat']);
        $longitude       = esc_html($atts['log']);

        //Class and Id
        $extra_class        = esc_attr($atts['extra_class']);
        $element_id         = esc_attr($atts['element_id']);
        


        $output = '';
        $output .= '<script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA03NsciIsIXYWojxFkqoM6ILmPaQgF3Pw&callback=initMap&libraries=&v=weekly"
        async
      ></script><script>';
        $output .= 'var lat = '. $latitude . ';';
        $output .= 'var lng = '. $longitude . ';';
        $output .= 'var map;';
        $output .= 'function initMap() {';
        $output .= 'map = new google.maps.Map(document.getElementById("map"), {';
        $output .= 'center: { lat: lat, lng: lng },';
        $output .= 'zoom: 16,';
        $output .= ' zoomControl: false,';
        $output .= 'fullscreenControl: false, ';
        $output .= '});';
        $output .= '}';
        $output .= '</script>'; 
        $output .= '<div id="map" style="width:264px; height:276px"></div>';
        // $output .= '<div class="gmap white_bg_block ' . $extra_class . '" id="' . $element_id . '" >';
        //   $output .= '<div class="gmap-inner">';
        //   $output .= $content;
        //   $output .= '</div>';
        // $output .= '</div>';

        return $output;                  

    }

  }
}

new VcGmap();