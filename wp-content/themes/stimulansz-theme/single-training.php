<?php
global $post;
$site_url = site_url();
?>
<link rel='stylesheet' id='vc_tta_style-css' href='<?php echo $site_url; ?>/wp-content/plugins/js_composer/assets/css/js_composer_tta.min.css?ver=6.0.5' type='text/css' media='all' />
<script type='text/javascript' src='<?php echo $site_url; ?>/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js?ver=6.0.5'></script>
<script type='text/javascript' src='<?php echo $site_url; ?>/wp-content/plugins/js_composer/assets/lib/vc_accordion/vc-accordion.min.js?ver=6.0.5'></script>
<script type='text/javascript' src='<?php echo $site_url; ?>/wp-content/plugins/js_composer/assets/lib/vc-tta-autoplay/vc-tta-autoplay.min.js?ver=6.0.5'></script>

<div class="page-template">
  <section class="section training-intro blue">
    <div class="vc_row wpb_row vc_row-fluid container">
      <div class="wpb_column vc_column_container vc_col-sm-8">
        <div class="left-block">
          <div class="cat"><?php
                            $terms = get_the_terms($post_id, 'training_cat');
                            $selectedTerms = [];
                            foreach ($terms as $term) {
                              $selectedTerms[] = $term->name;
                            }
                            echo implode(', ', $selectedTerms); ?>
          </div>
          <h1><?php echo get_the_title(); ?></h1>
          <?php if (get_field('duration') || get_field('aantal_deelnemers') || get_field('price')) { ?>
            <div class="training-intro-info white_bg_block float-none vc_row">
              <?php if (get_field('duration')) { ?><div class="wpb_column vc_column_container vc_col-sm-4"><i class="far fa-clock"></i> <?php echo get_field('duration') ?></div><?php } ?>
              <?php if (get_field('aantal_deelnemers')) { ?> <div class="wpb_column vc_column_container vc_col-sm-4"><i class="fas fa-user-friends"></i> <?php echo get_field('aantal_deelnemers') ?></div><?php } ?>
              <?php if (get_field('price')) { ?><div class="wpb_column vc_column_container vc_col-sm-4"><i class="fas fa-euro-sign"></i><?php echo get_field('price') ?> </div><?php } ?>
            </div>
          <?php }
          if (get_field('short_description')) { ?>
            <div class="intro-msg block">
              <?php echo get_field('short_description') ?>
            </div>
          <?php } ?>
          <div class="vc_btn3-container btn-white vc_btn3-inline vc_custom_1616657900407 download-brochure-btn">
            <?php
            if (get_field('download_file') != '' && get_field('download_form_id') != '') {
            ?>

              <a style="background-color:#ffffff; color:#0087d7;" class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-custom download-brochure" data-formId="<?php echo get_field('download_form_id') ?>" href="#" target="_self" title=""><i class="fas fa-download"></i>Download Brochure</a>
              <a href="<?php echo get_field('download_file'); ?>" download target="_blank" class="downloadButtonFile"></a>
            <?php } ?>
          </div>
        </div>
      </div>
      <div class="wpb_column vc_column_container vc_col-sm-4">
        <div class="vc_column-inner padding-left-100">
          <div class="wpb_wrapper">
            <section class="vc_cta3-container">
              <div class="vc_general vc_cta3 sidebar-widget-single-trainging orange-cta small-cta vc_cta3-style-classic vc_cta3-shape-rounded vc_cta3-align-left vc_cta3-color-classic vc_cta3-icon-size-md vc_cta3-actions-bottom">
                <div class="vc_cta3_content-container">
                  <div class="vc_cta3-content">
                    <header class="vc_cta3-content-header">
                      <?php $orange_text =  get_field('orange_block_text');
                      $alt_text  = get_field('alternative_text');
                      $start_date = get_field('event_start_date_info');
                      $time = get_field('event_time_info');
                      $location = get_field('event_location_info'); ?>
                      <p><?php echo $orange_text; ?></p>
                      <h2><?php if ($start_date) {
                            echo $start_date;
                          } else {
                            echo $alt_text;
                          }  ?></h2>
                      <p><?php if ($start_date) {
                            echo $time;
                          } ?><br /> <?php echo $location; ?></p>


                    </header>
                  </div>
                  <div class="vc_cta3-actions">
                    <div class="vc_btn3-container btn-white vc_btn3-center">
                      <?php $sign_up = get_field('sign_up_button');
                      if ($sign_up) :
                        $sign_up_url = $sign_up['url'];
                        $sign_up_title = $sign_up['title'];
                        $sign_up_target = $sign_up['target'] ? $sign_up['target'] : '_self';
                      endif; ?>
                      <a href="<?php echo esc_url($sign_up_url); ?>" target="<?php echo esc_attr($sign_up_target); ?>" style="background-color:#fcfcfc; color:#0087d7;" class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-custom" title=""><?php echo esc_html($sign_up_title); ?></a>

                    </div>
                    <?php
                    if (have_rows('event_info')) {
                    ?>
                      <a href="#Data_locatie" target="_self" class="btn-link navigate-btn">Bekijk alle data</a>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </section>
            <div class="wpb_widgetised_column wpb_content_element social-media-aside">
              <div class="wpb_wrapper">
                <section class="widget a2a_share_save_widget-2 widget_a2a_share_save_widget details-social-icons">
                  <h3>Training delen</h3>
                  <div class="a2a_kit a2a_kit_size_24 addtoany_list" style="line-height: 24px;">
                    <a class="a2a_button_linkedin" href="/#linkedin" title="LinkedIn" rel="nofollow noopener" target="_blank">
                      <span class="a2a_svg a2a_s__default a2a_s_linkedin" style="background-color: rgb(0, 123, 181); width: 24px; line-height: 24px; height: 24px; background-size: 24px; border-radius: 3px;">
                        <svg focusable="false" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32">
                          <path d="M6.227 12.61h4.19v13.48h-4.19V12.61zm2.095-6.7a2.43 2.43 0 0 1 0 4.86c-1.344 0-2.428-1.09-2.428-2.43s1.084-2.43 2.428-2.43m4.72 6.7h4.02v1.84h.058c.56-1.058 1.927-2.176 3.965-2.176 4.238 0 5.02 2.792 5.02 6.42v7.395h-4.183v-6.56c0-1.564-.03-3.574-2.178-3.574-2.18 0-2.514 1.7-2.514 3.46v6.668h-4.187V12.61z" fill="#FFF"></path>
                        </svg>
                      </span>
                      <span class="a2a_label">LinkedIn</span>
                    </a>
                    <a class="a2a_button_twitter" href="/#twitter" title="Twitter" rel="nofollow noopener" target="_blank">
                      <span class="a2a_svg a2a_s__default a2a_s_twitter" style="background-color: rgb(85, 172, 238); width: 24px; line-height: 24px; height: 24px; background-size: 24px; border-radius: 3px;">
                        <svg focusable="false" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32">
                          <path fill="#FFF" d="M28 8.557a9.913 9.913 0 0 1-2.828.775 4.93 4.93 0 0 0 2.166-2.725 9.738 9.738 0 0 1-3.13 1.194 4.92 4.92 0 0 0-3.593-1.55 4.924 4.924 0 0 0-4.794 6.049c-4.09-.21-7.72-2.17-10.15-5.15a4.942 4.942 0 0 0-.665 2.477c0 1.71.87 3.214 2.19 4.1a4.968 4.968 0 0 1-2.23-.616v.06c0 2.39 1.7 4.38 3.952 4.83-.414.115-.85.174-1.297.174-.318 0-.626-.03-.928-.086a4.935 4.935 0 0 0 4.6 3.42 9.893 9.893 0 0 1-6.114 2.107c-.398 0-.79-.023-1.175-.068a13.953 13.953 0 0 0 7.55 2.213c9.056 0 14.01-7.507 14.01-14.013 0-.213-.005-.426-.015-.637.96-.695 1.795-1.56 2.455-2.55z"></path>
                        </svg>
                      </span>
                      <span class="a2a_label">Twitter</span>
                    </a>
                    <a class="a2a_button_facebook" href="/#facebook" title="Facebook" rel="nofollow noopener" target="_blank">
                      <span class="a2a_svg a2a_s__default a2a_s_facebook" style="background-color: rgb(24, 119, 242); width: 24px; line-height: 24px; height: 24px; background-size: 24px; border-radius: 3px;">
                        <svg focusable="false" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32">
                          <path fill="#FFF" d="M17.78 27.5V17.008h3.522l.527-4.09h-4.05v-2.61c0-1.182.33-1.99 2.023-1.99h2.166V4.66c-.375-.05-1.66-.16-3.155-.16-3.123 0-5.26 1.905-5.26 5.405v3.016h-3.53v4.09h3.53V27.5h4.223z"></path>
                        </svg>
                      </span>
                      <span class="a2a_label">Facebook</span>
                    </a>
                    <a class="a2a_button_email" href="/#email" title="Email" rel="nofollow noopener" target="_blank">
                      <span class="a2a_svg a2a_s__default a2a_s_email" style="background-color: rgb(1, 102, 255); width: 24px; line-height: 24px; height: 24px; background-size: 24px; border-radius: 3px;">
                        <svg focusable="false" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32">
                          <path fill="#FFF" d="M26 21.25v-9s-9.1 6.35-9.984 6.68C15.144 18.616 6 12.25 6 12.25v9c0 1.25.266 1.5 1.5 1.5h17c1.266 0 1.5-.22 1.5-1.5zm-.015-10.765c0-.91-.265-1.235-1.485-1.235h-17c-1.255 0-1.5.39-1.5 1.3l.015.14s9.035 6.22 10 6.56c1.02-.395 9.985-6.7 9.985-6.7l-.015-.065z"></path>
                        </svg>
                      </span>
                      <span class="a2a_label">Email</span>
                    </a>
                    <a class="a2a_button_copy_link" href="/#copy_link" title="Copy Link" rel="nofollow noopener" target="_blank">
                      <span class="a2a_svg a2a_s__default a2a_s_link" style="background-color: rgb(1, 102, 255); width: 24px; line-height: 24px; height: 24px; background-size: 24px; border-radius: 3px;">
                        <svg focusable="false" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32">
                          <path fill="#FFF" d="M24.412 21.177c0-.36-.126-.665-.377-.917l-2.804-2.804a1.235 1.235 0 0 0-.913-.378c-.377 0-.7.144-.97.43.026.028.11.11.255.25.144.14.24.236.29.29s.117.14.2.256c.087.117.146.232.177.344.03.112.046.236.046.37 0 .36-.126.666-.377.918a1.25 1.25 0 0 1-.918.377 1.4 1.4 0 0 1-.373-.047 1.062 1.062 0 0 1-.345-.175 2.268 2.268 0 0 1-.256-.2 6.815 6.815 0 0 1-.29-.29c-.14-.142-.223-.23-.25-.254-.297.28-.445.607-.445.984 0 .36.126.664.377.916l2.778 2.79c.243.243.548.364.917.364.36 0 .665-.118.917-.35l1.982-1.97c.252-.25.378-.55.378-.9zm-9.477-9.504c0-.36-.126-.665-.377-.917l-2.777-2.79a1.235 1.235 0 0 0-.913-.378c-.35 0-.656.12-.917.364L7.967 9.92c-.254.252-.38.553-.38.903 0 .36.126.665.38.917l2.802 2.804c.242.243.547.364.916.364.377 0 .7-.14.97-.418-.026-.027-.11-.11-.255-.25s-.24-.235-.29-.29a2.675 2.675 0 0 1-.2-.255 1.052 1.052 0 0 1-.176-.344 1.396 1.396 0 0 1-.047-.37c0-.36.126-.662.377-.914.252-.252.557-.377.917-.377.136 0 .26.015.37.046.114.03.23.09.346.175.117.085.202.153.256.2.054.05.15.148.29.29.14.146.222.23.25.258.294-.278.442-.606.442-.983zM27 21.177c0 1.078-.382 1.99-1.146 2.736l-1.982 1.968c-.745.75-1.658 1.12-2.736 1.12-1.087 0-2.004-.38-2.75-1.143l-2.777-2.79c-.75-.747-1.12-1.66-1.12-2.737 0-1.106.392-2.046 1.183-2.818l-1.186-1.185c-.774.79-1.708 1.186-2.805 1.186-1.078 0-1.995-.376-2.75-1.13l-2.803-2.81C5.377 12.82 5 11.903 5 10.826c0-1.08.382-1.993 1.146-2.738L8.128 6.12C8.873 5.372 9.785 5 10.864 5c1.087 0 2.004.382 2.75 1.146l2.777 2.79c.75.747 1.12 1.66 1.12 2.737 0 1.105-.392 2.045-1.183 2.817l1.186 1.186c.774-.79 1.708-1.186 2.805-1.186 1.078 0 1.995.377 2.75 1.132l2.804 2.804c.754.755 1.13 1.672 1.13 2.75z"></path>
                        </svg>
                      </span>
                      <span class="a2a_label">Copy Link</span>
                    </a>
                  </div>
                </section>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="sticky-nav">
    <div class="container">
      <ul class="nav">
        <?php
        if (have_rows('page_nav_menu')) :
          while (have_rows('page_nav_menu')) : the_row();

            $navItem = get_sub_field('section_navs');
        ?>
            <li><a href="#<?php echo trim(preg_replace('/[^A-Za-z0-9\-]/', '_', str_replace(' ', '', $navItem)), '_'); ?>"><?php echo $navItem ?></a></li>
        <?php
          endwhile;

        // No value.
        else :

        endif;

        $sign_up = get_field('sign_up_button');
        if ($sign_up) :
          $sign_up_url = $sign_up['url'];
          $sign_up_title = $sign_up['title'];
          $sign_up_target = $sign_up['target'] ? $sign_up['target'] : '_self';
        endif; ?>
        <li><a href="<?php echo esc_url($sign_up_url); ?>" target="<?php echo esc_attr($sign_up_target); ?>" class="btn-sticky btn-orange end"><?php echo esc_html($sign_up_title); ?> </a> </li>

      </ul>
    </div>
  </div>


  <section class="details-pageoutercont-stz">
    <div class="vc_row wpb_row vc_inner vc_row-fluid container content-page-innerblock">

      <div class="wpb_column vc_column_container vc_col-sm-8 ">
        <?php
        $overzicht_heading = get_field('overzicht_heading');
        $overzicht_description = get_field('overzicht_description');
        if ($overzicht_heading || $overzicht_description || have_rows('overzicht_points') || get_field('ervaringen_left_description')) { ?>
          <section id="Overzicht" class="section overzicht white prefill-section">
            <div class="vc_row wpb_row vc_row-fluid container">
              <div class="wpb_column vc_column_container vc_col-sm-8 ">
                <h2 id="geef-goed-invulling-aan-het-so"><?php echo $overzicht_heading ?></h2>
                <p><?php echo $overzicht_description; ?></p>

                <ul>
                  <?php
                  if (have_rows('overzicht_points')) :
                    while (have_rows('overzicht_points')) : the_row();
                  ?>
                      <li><?php echo get_sub_field('points'); ?></li>
                  <?php
                    endwhile;

                  // No value.
                  else :

                  endif; ?>
                </ul>
                <?php
                if (have_rows('overzicht_question_section')) :
                  while (have_rows('overzicht_question_section')) : the_row();
                ?>
                    <div class="block">
                      <h4 id="voor-wie"><?php echo get_sub_field('overzicht_question'); ?></h4>

                      <!-- <ul> -->
                      <?php
                      /* if (have_rows('overzicht_answer_block')) :
                          while (have_rows('overzicht_answer_block')) : the_row();
                        ?>
                            <li><?php echo get_sub_field('overzicht_answer'); ?></li>
                        <?php
                          endwhile;

                        // No value.
                        else :

                        endif; */ ?>
                      <!-- </ul> -->

                      <p><?php echo get_sub_field('overzicht_answer');
                          ?></p>
                    </div>
                <?php
                  endwhile;

                // No value.
                else :

                endif; ?>

                <?php if (get_field('ervaringen_left_description')) { ?>
                  <div class="well cta blue">
                    <div class="vc_row wpb_row vc_row-fluid">
                      <div class="wpb_column vc_column_container vc_col-sm-4 ervaringen-blue-section">
                        <div class="col-left inner-col-left">
                          <h4 id="ervaringen">Alle ervaringen</h4>
                          <p><?php echo get_field('ervaringen_left_description'); ?>&nbsp;<strong><?php echo get_field('ervaringen_left_score'); ?></strong></p>
                          <?php if (have_rows('ervaringen_block')) { ?>

                            <div class="vc_btn3-container btn-white btn-sml vc_btn3-inline vc_custom_1616657900407">
                            <?php
                                          $experience_btn = get_field('ervaringen_btn_link');
                                          if ($experience_btn) :
                                            $experience_btn_url = $experience_btn['url'];
                                            $experience_btn_title = $experience_btn['title'];
                                            $experience_btn_target = $experience_btn['target'] ? $experience_btn['target'] : '_self';
                                          endif; ?>
                         
                              <a style="background-color:#ffffff; color:#0087d7;" id="experience_btn" class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-custom" href="#Ervaringen" target="<?php echo esc_attr($experience_btn_target); ?>" title=""><?php echo esc_html($experience_btn_title); ?></a>
                            </div>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="wpb_column vc_column_container vc_col-sm-8 ervaringen-blue">
                        <div class="card white_bg_block">
                          <p><?php echo get_field('review') ?></p>
                          <div class="name"><?php echo get_field('reviewer_name'); ?></div>
                          <div class="designation"><?php echo get_field('reviewer_designation') ?></div>
                        </div>
                        <a style="background-color:#ffffff; color:#0087d7;" id="experience_btn" class="mobile-blockbtn sign-up-btn-mobile vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-custom" href="#Ervaringen"  target="<?php echo esc_attr($experience_btn_target); ?>" title=""><?php echo esc_html($experience_btn_title); ?></a>
                      </div>
                    </div>
                  </div>
                <?php } ?>
              </div>
            </div>
          </section>
        <?php }

        if (get_field('programma_section_heading') || get_field('programma_section_content') || get_field('uitgelichte_video_content')) { ?>
          <section id="Programma" class="section programma blue blue-stzcontainer prefill-section">
            <div class="vc_row wpb_row vc_row-fluid container">
              <div class="wpb_column vc_column_container vc_col-sm-8">
                <h2 id="inhoud-van-de-opleiding"><?php echo get_field('programma_section_heading'); ?></h2>
                <p><?php echo get_field('programma_section_content'); ?></p>
                <?php if (get_field('uitgelichte_video_content')) { ?>
                  <div class="white_bg_block">
                    <div class="vc_row wpb_row vc_row-fluid">
                      <div class="wpb_column vc_column_container vc_col-sm-4">

                        <h3 id="van-visie-naar-organisatie"><?php echo get_field('uitgelichte_video_heading'); ?></h3>
                        <p><?php echo get_field('uitgelichte_video_content'); ?></p>
                      </div>
                      <div class="wpb_column vc_column_container vc_col-sm-8 video-white-block">
                        <div class="wpb_video_wrapper"> <?php echo the_field('white_trainer_block_right'); ?></div>
                      </div>
                    </div>
                  </div>
                <?php } ?>
                <div class="vc_tta-container vc_tts-cont" data-vc-action="collapseAll">
                  <div class="vc_general vc_tta vc_tta-accordion vc_tta-color-grey vc_tta-style-flat vc_tta-shape-rounded vc_tta-o-shape-group vc_tta-controls-align-left new-accord counter">
                    <div class="vc_tta-panels-container">
                      <div class="vc_tta-panels">

                        <?php
                        $count = 0;
                        if (have_rows('accordion_block')) :
                          while (have_rows('accordion_block')) : the_row();
                            $counter = $count++;
                            $accHeaderTitle = get_sub_field('accordion_header_title');
                            $accHeaderContent = get_sub_field('accordion_header_content');
                            $accTitle = get_sub_field('accordion_title');
                        ?>
                            <div class="accor-block">
                              <h3 id="van-visie-naar-organisatie"><?php echo  $accHeaderTitle ?></h3>
                              <p><?php echo  $accHeaderContent ?></p>
                              <div class="vc_tta-panel white_bg_tab" id="tab<?php echo $counter; ?>" data-vc-content="#tab<?php echo $counter; ?> .vc_tta-panel-body">
                                <div class="vc_tta-panel-heading">
                                  <h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right">
                                    <a href="#tab<?php echo $counter; ?>" data-vc-accordion="" data-vc-container=".vc_tts-cont">
                                      <span class="vc_tta-title-text">
                                        <?php echo $accTitle ?>
                                      </span>
                                      <i class="vc_tta-controls-icon vc_tta-controls-icon-plus"></i>
                                      </span>
                                    </a>
                                  </h4>
                                </div>
                                <div class="vc_tta-panel-body">
                                  <div class="wpb_text_column wpb_content_element ">
                                    <div class="wpb_wrapper">
                                      <ul>
                                        <?php
                                        if (have_rows('accordion_content')) :
                                          while (have_rows('accordion_content')) : the_row();
                                        ?>
                                            <li><?php echo get_sub_field('accordion_content_points'); ?></li>
                                        <?php
                                          endwhile;

                                        // No value.
                                        else :

                                        endif; ?>
                                      </ul>
                                      <!-- <?php //echo $accContent 
                                            ?> -->
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                        <?php
                          endwhile;

                        // No value.
                        else :

                        endif; ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="wpb_column vc_column_container vc_col-sm-4">
              </div>
            </div>
          </section>
        <?php }

        if (have_rows('event_info')) {
        ?>
          <section id="Data_locatie" class="section date-location white prefill-section">
            <div class="vc_row wpb_row vc_row-fluid container">
              <div class="wpb_column vc_column_container vc_col-sm-8">
                <h2>Data & Locatie</h2>
                <div class="vc_tta-container vc_tts-cont" data-vc-action="collapseAll">
                  <div class="vc_general vc_tta vc_tta-accordion vc_tta-color-grey vc_tta-style-flat vc_tta-shape-rounded vc_tta-o-shape-group vc_tta-controls-align-left event-accord  counter">
                    <div class="vc_tta-panels-container">
                      <div class="vc_tta-panels">
                        <?php
                        $i = 0;
                        if (have_rows('event_info')) :
                          while (have_rows('event_info')) : the_row();
                            $id = $i++;
                            $event_start_info = get_sub_field('start_date_&_location');
                            $event_dates = get_sub_field('event_dates_title');
                            $session_location_title = get_sub_field('event_location_title');
                            $session_location = get_sub_field('event_session_location'); ?>


                            <div class="accor-block">
                              <div class="vc_tta-panel" id="date_tab<?php echo $id ?>" data-vc-content="#date_tab<?php echo $id ?> .vc_tta-panel-body">
                                <div class="vc_tta-panel-heading">
                                  <h4 class="date-head vc_tta-panel-title vc_tta-controls-icon-position-right">
                                    <div class="title">
                                      <span class="lines vc_tta-title-text">
                                        <?php echo  $event_start_info  ?>
                                      </span>

                                      <?php
                                      $eventSignup = get_field('sign_up_button');
                                      if ($eventSignup) { ?>
                                        <div class="vc_btn3-container btn-white  sign-up-btn-mobile" style="display: none;">
                                          <?php
                                          if ($eventSignup) :
                                            $eventSignup_url = $eventSignup['url'];
                                            $eventSignup_title = $eventSignup['title'];
                                            $eventSignup_target = $eventSignup['target'] ? $eventSignup['target'] : '_self';
                                          endif; ?>
                                          <a style="background-color:#fcfcfc; color:#0087d7;" class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-custom" href="<?php echo esc_url($eventSignup_url); ?>" target="<?php echo esc_attr($eventSignup_target); ?>" title=""><?php echo esc_html($eventSignup_title); ?></a>
                                        </div>
                                      <?php } ?>

                                      <a href="#date_tab<?php echo $id ?>" class="lines expand" data-vc-accordion="" data-vc-container=".vc_tta-container">
                                        <span class="moreexpand">Toon lesdagen en locatie</span>
                                        <span class="lessexpand">Verberg lesdagen en locatie</span>
                                      </a>
                                    </div>
                                    <?php
                                    if ($eventSignup) { ?>
                                      <div class="vc_btn3-container btn-white  sign-up-btn-mobile-display">
                                        <?php
                                        if ($eventSignup) :
                                          $eventSignup_url = $eventSignup['url'];
                                          $eventSignup_title = $eventSignup['title'];
                                          $eventSignup_target = $eventSignup['target'] ? $eventSignup['target'] : '_self';
                                        endif; ?>
                                        <a style="background-color:#fcfcfc; color:#0087d7;" class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-custom" href="<?php echo esc_url($eventSignup_url); ?>" target="<?php echo esc_attr($eventSignup_target); ?>" title=""><?php echo esc_html($eventSignup_title); ?></a>
                                      </div>
                                    <?php } ?>
                                  </h4>
                                </div>
                                <div class="vc_tta-panel-body">
                                  <div class="wpb_text_column wpb_content_element ">
                                    <div class="wpb_wrapper">
                                      <div class="wpb_row vc_row-fluid">
                                        <div class="wpb_column date vc_column_container vc_col-sm-6">
                                          <h3><?php echo  $event_dates ?></h3>
                                          <?php
                                          if (have_rows('event_session_dates')) :
                                            while (have_rows('event_session_dates')) : the_row();
                                          ?>
                                              <div class="date-row">
                                                <div class="date"><?php echo get_sub_field('event_session_day'); ?> <?php echo get_sub_field('event_session_date'); ?></div>
                                                <div class="time"><?php echo get_sub_field('event_session_time') ?></div>
                                              </div>
                                          <?php
                                            endwhile;

                                          // No value.
                                          else :

                                          endif; ?>
                                        </div>
                                        <div class="wpb_column location vc_column_container vc_col-sm-6">
                                          <h3><?php echo $session_location_title ?></h3>
                                          <p><?php echo  $session_location ?></p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                        <?php
                          endwhile;

                        // No value.
                        else :

                        endif; ?>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="wpb_column vc_column_container vc_col-sm-4">
              </div>
            </div>
          </section>
        <?php }

        if (have_rows('ervaringen_block') || get_field('ervaringen_video_block')) {

        ?>
          <section id="Ervaringen" class="vc_section section blue video-outercont blue-stzcontainer prefill-section">
            <div class="vc_row wpb_row vc_row-fluid container">
              <div class="wpb_column vc_column_container vc_col-sm-8">
                <div class="vc_column-inner">
                  <div class="wpb_wrapper">
                    <div class="wpb_text_column wpb_content_element ">
                      <div class="wpb_wrapper">
                        <h2 id="ervaringen-over-beleidsadviseu"><?php echo get_field('ervaringen_header'); ?></h2>
                      </div>
                    </div>

                    <div class="vc_empty_space" style="height: 30px"><span class="vc_empty_space_inner"></span></div>
                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                      <div class="wpb_column vc_column_container vc_col-sm-4">
                        <div class="vc_column-inner">
                          <div class="wpb_wrapper">
                            <div class="score-block " id="">
                              <div class="score-block-inner">
                                <p> <?php echo get_field('ervaringen_green_block') ?></p>
                                <div class="score"><?php echo get_field('ervaringen_grade') ?></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="wpb_column vc_column_container vc_col-sm-8">
                        <div class="vc_column-inner">
                          <div class="wpb_wrapper">
                            <div class="wpb_video_widget wpb_content_element vc_clearfix   vc_video-aspect-ratio-169 vc_video-el-width-100 vc_video-align-left">
                              <div class="wpb_wrapper">
                                <div class="wpb_video_wrapper"><?php echo  get_field('ervaringen_video_block'); ?></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="vc_empty_space" style="height: 30px"><span class="vc_empty_space_inner"></span></div>
                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                      <?php
                      $review_count = 0;
                      if (have_rows('ervaringen_block')) :
                        while (have_rows('ervaringen_block')) : the_row();
                          $review_count++;
                          $hiddenClass = 'hide-ervaringen';
                          $experience = get_sub_field('experience');
                          $name = get_sub_field('name');
                          $designation = get_sub_field('designation');
                      ?>
                          <div class="listing-blks wpb_column vc_column_container vc_col-sm-4 ervaringen-div <?php if ($review_count > 3) {
                                                                                                                echo $hiddenClass;
                                                                                                              } ?>">
                            <div class="vc_column-inner">
                              <div class="wpb_wrapper">
                                <div class="blockquote white_bg_block " id="" style="height: 262px;">
                                  <div class="blockquote-inner">
                                    <p><?php echo $experience ?></p>
                                    <div class="name"><?php echo $name ?></div>
                                    <div class="designation"><?php echo $designation ?></div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                      <?php
                        endwhile;

                      // No value.
                      else :

                      endif; ?>

                    </div>
                    <?php
                    if ($review_count > 3) {
                    ?>
                      <div class="wpb_text_column wpb_content_element cta-link">
                        <div class="wpb_wrapper">

                          <p style="text-align: center;"><a id="ervaringen_btn" href="#" target="_self"><i class="fas fa-plus"></i> Meer ervaringen </a></p>

                        </div>
                      </div>
                    <?php
                    }
                    ?>
                  </div>
                </div>
              </div>
              <div class="wpb_column vc_column_container vc_col-sm-4">
                <div class="vc_column-inner">
                  <div class="wpb_wrapper"></div>
                </div>
              </div>
            </div>
          </section>
        <?php }

        if (get_field('trainer_header')) {
        ?>

          <section id="Trainer_s" class="section trainer white trainers-outer-container prefill-section">
            <div class="vc_row wpb_row vc_row-fluid container">
              <div class="wpb_column vc_column_container vc_col-sm-8">
                <h2 id="trainers"> <?php echo get_field('trainer_header'); ?></h2>
                <p> <?php echo get_field('trainer_header_content'); ?></p>
                <div class="vc_row wpb_row vc_row-fluid trainer-grid">
                  <?php $trainer_object = get_field('trainer');
                  if($trainer_object){
                  foreach ($trainer_object as $trainer) {
                  ?>
                    <div class="wpb_column vc_col-sm-4">
                      <div class="card card-block " id="">
                        <div class="card-inner">

                          <?php $trainer_icon = get_the_post_thumbnail_url($trainer->ID); ?>
                          <div class="img-wrap"><img width="264" height="148" src="<?php echo $trainer_icon ?>" class="attachment-stimulansz_card_image size-stimulansz_card_image" alt="">                        
                          </div>
                          <div class="cnt-wrap">
                            <div class="title_wrap">
                              <div class="title"><?php echo $trainer->post_title; ?></div>
                              <p><?php echo $trainer->post_excerpt ?></p>
                            </div>
                            <div class="sub_title_wrap">
                              <a href="tel:<?php print_r(get_field('telefoon', $trainer->ID)); ?>" class="sub_title sub_title_1"><?php print_r(get_field('telefoon', $trainer->ID)); ?></a>

                              <div class="sub_title sub_title_2"><button id="email_copyboard" data-text="<?php print_r(get_field('e_mail_adres', $trainer->ID)) ?>" class="copyboard"><i class="fa fa-copy"></i> Kopieer e-mailadres</button></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  <?php }
                  } ?>
                </div>
              </div>
              <div class="wpb_column vc_column_container vc_col-sm-4">
                <div class="vc_column-inner">
                  <div class="wpb_wrapper"></div>
                </div>
              </div>
            </div>
          </section>

        <?php }
        if (get_field('incompany_block_header') || get_field('incompany_block_description')) {
        ?>

          <section id="Incompany" class="section incompany blue blue-stzcontainer prefill-section">
            <div class="vc_row wpb_row vc_row-fluid container">
              <div class="wpb_column vc_column_container vc_col-sm-8">
                <div class="wpb_text_column wpb_content_element ">
                  <div class="wpb_wrapper">
                    <h2 id="parlementaire-behandeling-nieu"> <?php echo get_field('incompany_block_header'); ?></h2>
                    <p><?php echo get_field('incompany_block_description') ?></p>

                  </div>
                </div>
                <div class="vc_btn3-container  btn-white vc_btn3-inline margin-top-40">

                  <?php $IncompanyBtn = get_field('incompany_button');
                  if ($IncompanyBtn) :
                    $IncompanyBtn_url = $IncompanyBtn['url'];
                    $IncompanyBtn_title = $IncompanyBtn['title'];
                    $IncompanyBtn_target = $IncompanyBtn['target'] ? $IncompanyBtn['target'] : '_self';
                  endif;
                  if ($IncompanyBtn) { ?>
                    <a style="background-color:#ffffff; color:#0087d7;" target="<?php echo esc_attr($IncompanyBtn_target); ?>" id="incompany_btn" class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-custom" href="<?php echo esc_url($IncompanyBtn_url); ?>" title=""><?php echo esc_html($IncompanyBtn_title); ?> </a>
                  <?php } ?>
                </div>
              </div>
              <div class="wpb_column vc_column_container vc_col-sm-4">
                <div class="vc_column-inner">
                  <div class="wpb_wrapper"></div>
                </div>
              </div>
            </div>
          </section>
        <?php }
        if (have_rows('faq_block', 'option')) {
        ?>
          <section id="Faq_Section" class="vc_section section white bottom-last-block">
            <div class="vc_row wpb_row vc_inner vc_row-fluid container">
              <div class="wpb_column vc_column_container vc_col-sm-8">
                <div class="vc_column-inner">
                  <div class="wpb_wrapper">
                    <div class="wpb_text_column wpb_content_element ">
                      <div class="wpb_wrapper">
                        <h2>Veelgestelde vragen</h2>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="wpb_column vc_column_container vc_col-sm-4">
                <div class="vc_column-inner">
                  <div class="wpb_wrapper"></div>
                </div>
              </div>

            </div>
            <div class="vc_row wpb_row vc_row-fluid container">
              <div class="wpb_column vc_column_container vc_col-sm-8">
                <div class="vc_column-inner">
                  <div class="wpb_wrapper">
                    <div class="vc_tta-container" data-vc-action="collapseAll">
                      <div class="vc_general vc_tta vc_tta-accordion vc_tta-color-grey vc_tta-style-flat vc_tta-shape-rounded vc_tta-o-shape-group faq-accord vc_tta-controls-align-left  counter">
                        <div class="vc_tta-panels-container">
                          <div class="vc_tta-panels">
                            <?php
                            $counter = 0;
                            $className = "hide-div";
                            if (have_rows('faq_block', 'option')) :
                              while (have_rows('faq_block', 'option')) : the_row();
                                $count1  = $counter++;
                                $questions = get_sub_field('question');
                                $answers = get_sub_field('answer');
                            ?>

                                <div class="faq-div vc_tta-panel <?php if ($count1 > 3) {
                                                                    echo $className;
                                                                  } ?>" id="faq-tab<?php echo $count1 ?>" data-vc-content="#fab-tab<?php echo $count1 ?> .vc_tta-panel-body">
                                  <div class="vc_tta-panel-heading">
                                    <h4 class="vc_tta-panel-title vc_tta-controls-icon-position-right"><a href="#faq-tab<?php echo $count1 ?>" data-vc-accordion="" data-vc-container=".vc_tta-container"><span class="vc_tta-title-text"><?php echo $questions ?></span><i class="vc_tta-controls-icon vc_tta-controls-icon-plus"></i></a></h4>
                                  </div>
                                  <div class="vc_tta-panel-body">
                                    <div class="wpb_text_column wpb_content_element ">
                                      <div class="wpb_wrapper">
                                        <?php echo $answers ?>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            <?php
                              endwhile;

                            // No value.
                            else :

                            endif; ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php
                  if ($count1 > 3) {
                  ?>
                    <div class="wpb_text_column wpb_content_element cta-link">
                      <div class="wpb_wrapper">
                        <?php $FaqLink = get_field('faq_link', 'option');
                        if ($FaqLink) :
                          $FaqLink_url = $FaqLink['url'];
                          $FaqLink_title = $FaqLink['title'];
                          $FaqLink_target = $FaqLink['target'] ? $FaqLink['target'] : '_self';
                        endif; ?>
                        <p style="text-align: center;"><a id="faq_btn" href="<?php echo esc_url($FaqLink_url); ?>" target="<?php echo esc_attr($FaqLink_target); ?>"><i class="fas fa-plus"></i> <?php echo esc_html($FaqLink_title); ?> </a></p>
                      </div>
                    </div>
                  <?php
                  } ?>
                </div>
              </div>
              <div class="wpb_column vc_column_container vc_col-sm-4">
                <div class="vc_column-inner">
                  <div class="wpb_wrapper"></div>
                </div>
              </div>
            </div>
          </section>
        <?php } ?>
      </div>
      <div class="wpb_column vc_column_container vc_col-sm-4 sticky-advices">
        <div class="vc_column-inner padding-left-100 details-blue-block">
          <section class="vc_cta3-container">
            <div class="vc_general vc_cta3 blue-cta overflow-visible small-cta vc_cta3-style-classic vc_cta3-shape-rounded vc_cta3-align-left vc_cta3-color-classic vc_cta3-icon-size-md vc_cta3-actions-bottom">
              <div class="vc_cta3_content-container">
                <div class="vc_cta3-content">
                  <div class="avatar-wrap img-blockouter">
                    <?php $person_object = get_field('contact_content');
                    $person_icon = wp_get_attachment_url(get_post_thumbnail_id($person_object->ID)); ?>
                    <img src="<?php echo $person_icon ?>" alt="img" style="max-width : 200px">
                  </div>
                  <header class="vc_cta3-content-header">
                    <h3 id="vragen-of-advies-nodig"><span style="vertical-align: inherit;">Vragen of advies nodig?</span></h3>
                  </header>
                  <p> <?php print get_field('contactpersoon_rol', $person_object->ID) . '  ' . $person_object->post_title; ?> helpt u graag verder.</p>
                  <footer class="vc_cta3-content-footer vc_cta3-content-center">
                    <span>
                      <a href="tel:<?php print_r(get_field('telefoon', $person_object->ID)); ?>" class="phone"><?php print_r(get_field('telefoon', $person_object->ID)); ?></a>
                      <a href="mailto:<?php print_r(get_field('e_mail_adres', $person_object->ID)); ?>" class="email"><?php print_r(get_field('e_mail_adres', $person_object->ID)); ?></a>
                    </span>
                  </footer>
                </div>
              </div>
            </div>
          </section>

          <section class="vc_cta3-container">
            <div class="vc_general vc_cta3 white-cta overflow-visible small-cta vc_cta3-style-classic vc_cta3-shape-rounded vc_cta3-align-left vc_cta3-color-classic vc_cta3-icon-size-md vc_cta3-actions-bottom">
              <div class="vc_cta3_content-container">
                <div class="vc_cta3-content">
                  <?php if (get_field('download_form_id')) { ?>
                    <a href="#" class="btn-link download-brochure" data-formId="<?php echo get_field('download_form_id') ?>" target="_self">Download brochure</a>
                  <?php } ?>

                  <a href="#Faq_Section" class="btn-link navigate-btn">Veelgestelde vragen</a>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </section>
  <?php if (get_field('select_up_sell_catalogue_products')) { ?>
    <section class="section other-viewed posts post-repeater blue">
      <div class="vc_row wpb_row vc_row-fluid container">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <h2><?php echo get_field('catalogue_product_section_title') ?></h2>
          <div>
          </div>
          <div class="vc_row wpb_row vc_row-fluid container recent-posts-container">

            <?php $catalog_objects = get_field('select_up_sell_catalogue_products');

            foreach ($catalog_objects as $catalog_object) {
              $catalog_icon = wp_get_attachment_url(get_post_thumbnail_id($catalog_object->ID), 'thumbnail');
            ?>
              <div class="wpb_column vc_column_container vc_col-sm-3">
                <div class="card card-block white_bg_block post-list-item">
                  <div class="card-inner">
                    <a href="<?php echo get_permalink($catalog_object->ID); ?>">
                      <div class="img-wrap">
                        <img src="<?php echo $catalog_icon; ?>" alt="<?php echo $catalog_object->post_title ?>" />
                        <?php
                            $new_label = get_field('training_post_new_label');
                            if ($new_label) : ?>
                              <label class="new-sticker"> <?php echo $new_label[0]; ?></label>
                            <?php endif; ?>
                      </div>
                      <div class="cnt-wrap">
                        <div class="title_wrap">
                          <div class="cat"><?php
                                            $selectedTerms = [];
                                            foreach (wp_get_post_terms($catalog_object->ID, 'training_cat') as $term) {
                                              $selectedTerms[] = $term->name;
                                            }
                                            echo implode(', ', $selectedTerms);
                                            ?></div>
                          <div class="title"><?php echo $catalog_object->post_title ?></div>
                          <p><?php echo $catalog_object->post_excerpt ?></p>
                        </div>
                        <div class="sub_title_wrap ">
                          <div class="sub_title sub_title_1"><i class="far fa-clock"></i> <?php echo get_field('duration', $catalog_object->ID) ?></div>
                          <span class="readmore" tabindex="0"> <i class="fas fa-arrow-right">&nbsp;</i> </span>
                        </div>
                      </div>
                    </a>
                  </div>
                </div>
              </div>
            <?php } ?>

            <div class="wpb_column vc_column_container vc_col-sm-3">
              <div class="vc_general vc_cta3 pink-cta vc_cta3-style-classic vc_cta3-shape-rounded vc_cta3-align-center vc_cta3-color-classic vc_cta3-icon-size-md vc_cta3-icons-top vc_cta3-actions-bottom vc_custom_1617022095910">
                <div class="vc_cta3-icons">
                  <div class="vc_icon_element vc_icon_element-outer vc_icon_element-align-left">
                    <div class="vc_icon_element-inner vc_icon_element-color-blue vc_icon_element-size-md vc_icon_element-style- vc_icon_element-background-color-grey"><span class="vc_icon_element-icon vc_li vc_li-paperplane"></span></div>
                  </div>
                </div>
                <div class="vc_cta3_content-container">
                  <div class="vc_cta3-content">
                    <header class="vc_cta3-content-header">
                      <h2>Probeer ook onze Keuzehulp!</h2>
                      <h4>Met de handige keuzehulp vindt u snel de training die het best bij u past.</h4>
                    </header>
                  </div>
                  <div class="vc_cta3-actions">
                    <div class="vc_btn3-container  btn-white vc_btn3-center"><a style="background-color:#ffffff; color:#af197d;" class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-custom" href="<?php echo $site_url.'/keuzehulp/'?>" target="_self" title="">Start de Keuzehulp</a></div>
                  </div>
                </div>
              </div>
            </div>
          </div>

    </section>
  <?php } ?>


  <!-- <?php // the_content(); 
        ?> -->
</div>

<div id="downloadModalBox" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <?php
    echo  get_field('download_form_id');?>
    <?php echo do_shortcode('[mailplusform formid="' . get_field('download_form_id') . '" ssl="false" ]'); ?>
  </div>

</div>
<script>
  <?php
  if (isset($_POST['userId']) && isset($_GET['formid'])) {
  ?>
    jQuery(document).ready(function(e) {
      jQuery('.downloadButtonFile')[0].click();
    });
  <?php
  }
  ?>
</script>