<?php
//include('custom-shortcodes.php');

function webroom_wc_remove_password_strength()
{
	if (wp_script_is('wc - password - strength - meter', 'enqueued')) {
		wp_dequeue_script('wc - password - strength - meter');
	}
}
add_action('wp_print_scripts', 'webroom_wc_remove_password_strength', 100);

function getFeaturePosts1()
{
?>
	<ul class="related-posts">
		<?php
		$default_thumbnail = '/wp-content/uploads/2020/04/Stimulansz_logodescriptor_02.jpg';
		$the_query = new WP_Query('showposts=2&orderby=rand&category_name=uitgelicht');
		while ($the_query->have_posts()) : $the_query->the_post(); ?>
			<li>
				<div class="related_thumbnail">
					<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
						<?php
						if (has_post_thumbnail()) :
							the_post_thumbnail();
						else :
						?>
							<img src="<?php echo $default_thumbnail; ?>" alt="<?php the_title(); ?>" />
						<?php endif; ?>
					</a>
				</div>
				<div style="clear:both;"></div>
				<div class="related_permalink">
					<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
						<div class="link_text"><?php the_title(); ?></div>
						<div class="arrow-icon-permalink"><i class="fa fa-arrow-circle-right"></i></div>
					</a>
				</div>

			</li>

		<?php endwhile; ?>
		<?php wp_reset_query(); ?>
	</ul>
<?php
}


function agenda_lijst()
{
	echo apply_filters('stimulanz_traing_agenda', '');
}

function veel_gezocht_func()
{
	if (has_nav_menu('theme_paginas')) :
		wp_nav_menu(
			array(
				'theme_location' => 'theme_paginas',
				'container_class' => '',
				'menu_class' => 'list-inline theme_pages',
				'menu_id' => 'themepages_menu',
				'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			)
		);
	endif;
}

add_shortcode('Featured_post', 'getFeaturePosts1');

add_shortcode('agenda_lijst_home', 'agenda_lijst');

add_shortcode('veel_gezocht', 'veel_gezocht_func');


/**

 * Sage includes  

 *

 * The $sage_includes array determines the code library included in your theme.

 * Add or remove files to the array as needed. Supports child theme overrides.

 *

 * Please note that missing files will produce a fatal error.

 *

 * @link https://github.com/roots/sage/pull/1042

 */

//Register image size

add_image_size('stimulansz_block_image', 396, 179, true);

add_image_size('stimulansz_card_image', 264, 148, true);
add_image_size('stimulansz_card_wide', 264, 321, true);

//Register image for instructeur

add_image_size('stimulansz_instructeur_image', 200, 200, true);

//Register Training block image 

add_image_size('stimulansz_related_post_image', 407, 179, true);



add_image_size('stimulansz_single_blog_page_image', 350, 200, true);

//Register image for instractor

add_image_size('instractor_image', 130, 190, true);



add_image_size('stimulansz_instructeur_listing_image', 407, 300, array('center', 'bottom'));



$sage_includes = [

	'lib/assets.php', // Scripts and stylesheets

	'lib/extras.php', // Custom functions

	'lib/setup.php', // Theme setup

	'lib/titles.php', // Page titles

	'lib/wrapper.php', // Theme wrapper class

	'lib/customizer.php', // Theme customizer

	'lib/custompost.php', // Theme custom post type

	'lib/woocommerce.php'  // Theme custom post type

];





foreach ($sage_includes as $file) {

	if (!$filepath = locate_template($file)) {

		trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
	}



	require_once $filepath;
}

unset($file, $filepath);



/* * ******************************************************************************

 *

 *                               Upload logo from the backend 

 *

 * ******************************************************************************* */



function m1_customize_register($wp_customize)
{

	$wp_customize->add_setting('m1_logo'); // Add setting for logo uploader

	// Add control for logo uploader (actual uploader)

	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'm1_logo', array(

		'label' => __('Upload Logo', 'm1'),

		'section' => 'title_tagline',

		'settings' => 'm1_logo',

	)));
}



add_action('customize_register', 'm1_customize_register');



/**

 * Add top bar menu

 */

function wpb_custom_new_menu()
{

	//register_nav_menu('my-custom-menu', __('Top bar menu'));

	register_nav_menu('ipad-menu', __('Ipad Menu'));
}



add_action('init', 'wpb_custom_new_menu');



if (function_exists('acf_add_options_page')) {



	acf_add_options_page(array(

		'page_title' => 'Theme General Settings',

		'menu_title' => 'Theme Settings',

		'menu_slug' => 'theme-general-settings',

		'capability' => 'edit_posts',

		'redirect' => false

	));



	acf_add_options_sub_page(array(

		'page_title' => 'Theme Header Settings',

		'menu_title' => 'Header',

		'parent_slug' => 'theme-general-settings',

	));



	acf_add_options_sub_page(array(

		'page_title' => 'Theme Footer Settings',

		'menu_title' => 'Footer',

		'parent_slug' => 'theme-general-settings',

	));
}



/**

 * 

 * @param type $comment

 * @param type $args

 * @param type $depth

 */

function stimulansz_theme_comment($comment, $args, $depth)
{

	if ('div' === $args['style']) {

		$tag = 'ol';

		$add_below = 'comment';
	} else {

		$tag = 'li';

		$add_below = 'div-comment';
	}

?>

	<<?php echo $tag ?> <?php comment_class(empty($args['has_children']) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">

		<?php if ('div' != $args['style']) : ?>

			<article id="div-comment-<?php comment_ID() ?>" class="comment-body">

			<?php endif; ?>

			<div class="user_icon">

				<?php if ($args['avatar_size'] != 0) echo get_avatar($comment, $args['avatar_size']); ?>

			</div>

			<div class="comment_data">

				<div class="reviewrating">

					<?php

					$commentrating = get_comment_meta(get_comment_ID(), 'rating', true);

					if (isset($commentrating)) :

						echo str_replace('.', ',', $commentrating);

					endif;

					?>

				</div>



				<footer class="comment-meta">

					<div class="comment-author vcard">

						<?php //if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] );   
						?>

						<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()); ?>

					</div>

					<?php if ($comment->comment_approved == '0') : ?>

						<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.'); ?></em>

						<br />

					<?php endif; ?>

					<div class="comment-content-stimulzs">

						<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars(get_comment_link($comment->comment_ID)); ?>">

								<?php

								/* translators: 1: date, 2: time */

								printf(__('%1$s at %2$s'), get_comment_date(), get_comment_time());

								?></a><?php edit_comment_link(__('(Edit)'), '  ', '');

										?>

						</div>

					</div>

				</footer>

				<div class="comment-content">

					<?php comment_text(); ?>

				</div>

				<div class="reply">

					<?php comment_reply_link(array_merge($args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))); ?>

				</div>

			</div>

			<?php if ('div' != $args['style']) : ?>

			</article>

		<?php endif; ?>

	<?php

}



function exclude_single_posts_home($query)
{

	if ($query->is_main_query()) {

		$query->set('post__not_in', array(271, 268));
	}
}



add_action('pre_get_posts', 'exclude_single_posts_home');



/**

 * Remove the slug from published post permalinks. Only affect our custom post type, though.

 */

function stimulansz_remove_cpt_slug($post_link, $post, $leavename)
{



	if ('landingspagina' != $post->post_type) {

		return $post_link;
	}



	$post_link = str_replace('/' . $post->post_type . '/', '/', $post_link);



	return $post_link;
}



add_filter('post_type_link', 'stimulansz_remove_cpt_slug', 10, 3);



/**

 * Have WordPress match postname to any of our public post types (post, page, race)

 * All of our public post types can have /post-name/ as the slug, so they better be unique across all posts

 * By default, core only accounts for posts and pages where the slug is /post-name/

 */

function stimulansz_parse_request_trick($query)
{



	// Only noop the main query

	if (!$query->is_main_query())

		return;



	// Only noop our very specific rewrite rule match

	if (2 != count($query->query) || !isset($query->query['page'])) {

		return;
	}



	// 'name' will be set if post permalinks are just post_name, otherwise the page rule will match

	if (!empty($query->query['name'])) {

		$query->set('post_type', array('post', 'page', 'landingspagina'));
	}
}



add_action('pre_get_posts', 'stimulansz_parse_request_trick');



add_action('rss2_item', 'stimulansz_rss2_item');



function stimulansz_rss2_item()
{

	if (get_post_type() == 'post') {

		$taxonomies = array(

			'actueel',

		);

		// Loop through taxonomies

		foreach ($taxonomies as $taxonomy) {

			$terms = get_the_terms($post_id, $taxonomy);

			if (is_array($terms)) {

				// Loop through terms

				foreach ($terms as $term) {

					echo "<{$taxonomy}>{$term->name}</{$taxonomy}>\n";
				}
			}
		}
	}
}



//add SVG to allowed file uploads

function add_file_types_to_uploads($file_types)
{



	$new_filetypes = array();

	$new_filetypes['svg'] = 'image/svg+xml';

	$file_types = array_merge($file_types, $new_filetypes);



	return $file_types;
}



add_action('upload_mimes', 'add_file_types_to_uploads');



/**

 * Date sorting

 * @param type $a

 * @param type $b

 * @return type

 */

function date_sort($a, $b)
{

	return strtotime($a) - strtotime($b);
}



function stimulansz_author_blog_load_more_pagination()
{



	global $wp_query;



	// register our main script but do not enqueue it yet

	wp_register_script('ajaxloadmore', get_stylesheet_directory_uri() . '/assets/scripts/ajaxloadmore.js', array('jquery'));



	// now the most interesting part

	// we have to pass parameters to ajaxloadmore.js script but we can get the parameters values only in PHP

	// you can define variables directly in your HTML but I decided that the most proper way is wp_localize_script()

	wp_localize_script('ajaxloadmore', 'stimulansz_loadmore_params', array(

		'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX

		'posts' => serialize($wp_query->query_vars), // everything about your loop is here

		'current_page' => get_query_var('paged') ? get_query_var('paged') : 1,

		//'max_page' => $wp_query->max_num_pages

	));



	wp_enqueue_script('ajaxloadmore');
}



add_action('wp_enqueue_scripts', 'stimulansz_author_blog_load_more_pagination');



/**

 * 

 */

function stimulansz_loadmore_ajax_handler()
{

	?>



		<?php

		$args = unserialize(stripslashes($_POST['query']));



		$args['paged'] = $_POST['page'] + 1; // we need next page to be loaded

		$args['post_status'] = 'publish';

		$args['author'] = $_POST['author_id'];

		$args['post_type'] = 'post';

		$args['posts_per_page'] =  5;

		$query = new \WP_Query($args);



		?>

		<?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>

				<li>

					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

					<div class="blog_details">

						<div class="intro">

							<?php

							$post_date = get_the_date();

							if ($post_date) :

							?>

								<div class="date"><?php echo $post_date; ?></div>

							<?php endif; ?>

							<div class="content">

								<?php

								$intro = get_field('additional_banner_text');

								if (isset($intro) && !empty($intro)) :

									echo $intro . ' <a class="stimulansz-home-more-link" href="' . get_permalink() . '">' . esc_html__('Read more', 'stimulansz') . '</a>';

								else :

									the_excerpt();

								endif;



								?></div>

						</div>

					</div>

				</li>

		<?php endwhile;

		endif;

		?>



		<?php

		die; // here we exit the script and even no wp_reset_query() required!

	}



	add_action('wp_ajax_loadmore', 'stimulansz_loadmore_ajax_handler'); // wp_ajax_{action}

	add_action('wp_ajax_nopriv_loadmore', 'stimulansz_loadmore_ajax_handler'); // wp_ajax_nopriv_{action}



	/**

	 * Disable update for wpsolr

	 * @param type $value

	 * @return type

	 */

	function stimulansz_wpsolr_filter_plugin_updates($value)
	{

		unset($value->response['wpsolr-pro/wpsolr-pro.php']);

		return $value;
	}

	add_filter('site_transient_update_plugins', 'stimulansz_wpsolr_filter_plugin_updates');



	/**

	 * Sort posts on the blog posts page according to the custom sort order

	 * @param type $query

	 */

	function stimulansz_post_order_sort($query)
	{

		if ($query->is_main_query()) {

			$query->set('orderby', 'post_date');

			$query->set('order', 'DESC');
		}
	}

	add_action('pre_get_posts', 'stimulansz_post_order_sort');





	function stimulansz_read_more_link()
	{

		return '<a class="more-link" href="' . get_permalink() . '">Leesdd meer</a>';
	}

	add_filter('the_content_more_link', 'stimulansz_read_more_link');





	add_filter('event_date_sort_results', 'sort_traing_Date', 10, 1);



	/**

	 * 

	 * @param type $final_result

	 * @return string

	 */

	function sort_traing_Date($final_result = array())
	{



		// print_r($final_result);



		$data_array = isset($final_result[1]['eventstartdate_str']);

		//echo "check file";

		$datearray = $data_array['values'];

		$count = '';

		$current_time = strtotime(date("d-m-Y"));

		$strotime = array();

		if (count($datearray) >= 1) :





			for ($countdate = 0; $countdate < count($datearray); $countdate++) {

				$datearr = explode(' ', $datearray[$countdate]['value']);

				$count = '';

				$count = $datearray[$countdate]['count'];

				$replac_month = '';

				switch ($datearr[1]):

					case 'januari':

						$replac_month = str_replace($datearr[1], 'January', $datearray[$countdate]['value']);

						if (strtotime($replac_month) >= $current_time) :

							$strotime[] = array('value' => strtotime($replac_month), 'count' => $count);

						endif;



						break;

					case 'februari':

						$replac_month = str_replace($datearr[1], 'February', $datearray[$countdate]['value']);

						if (strtotime($replac_month) >= $current_time) :

							$strotime[] = array('value' => strtotime($replac_month), 'count' => $count);

						endif;

						break;

					case 'maart':

						$replac_month = str_replace($datearr[1], 'March', $datearray[$countdate]['value']);

						// $strotime[]=  strtotime($replac_month) ;

						if (strtotime($replac_month) >= $current_time) :

							$strotime[] = array('value' => strtotime($replac_month), 'count' => $count);

						endif;



						break;

					case 'april':

						$replac_month = str_replace($datearr[1], 'April', $datearray[$countdate]['value']);



						if (strtotime($replac_month) >= $current_time) :

							$strotime[] = array('value' => strtotime($replac_month), 'count' => $count);

						endif;



						break;

					case 'mei':

						$replac_month = str_replace($datearr[1], 'May', $datearray[$countdate]['value']);

						if (strtotime($replac_month) >= $current_time) :

							$strotime[] = array('value' => strtotime($replac_month), 'count' => $count);

						endif;



						break;



					case 'juni':

						$replac_month = str_replace($datearr[1], 'June', $datearray[$countdate]['value']);

						if (strtotime($replac_month) >= $current_time) :

							$strotime[] = array('value' => strtotime($replac_month), 'count' => $count);

						endif;

						break;

					case 'juli':

						$replac_month = str_replace($datearr[1], 'July', $datearray[$countdate]['value']);

						if (strtotime($replac_month) >= $current_time) :

							$strotime[] = array('value' => strtotime($replac_month), 'count' => $count);

						endif;

						break;

					case 'augustus':

						$replac_month = str_replace($datearr[1], 'August', $datearray[$countdate]['value']);

						if (strtotime($replac_month) >= $current_time) :

							$strotime[] = array('value' => strtotime($replac_month), 'count' => $count);

						endif;

						break;

					case 'september':

						$replac_month = str_replace($datearr[1], 'September', $datearray[$countdate]['value']);

						if (strtotime($replac_month) >= $current_time) :

							$strotime[] = array('value' => strtotime($replac_month), 'count' => $count);

						endif;

						break;

					case 'october':

						$replac_month = str_replace($datearr[1], 'October', $datearray[$countdate]['value']);

						if (strtotime($replac_month) >= $current_time) :

							$strotime[] = array('value' => strtotime($replac_month), 'count' => $count);

						endif;

						break;

					case 'november':

						$replac_month = str_replace($datearr[1], 'November', $datearray[$countdate]['value']);

						if (strtotime($replac_month) >= $current_time) :

							$strotime[] = array('value' => strtotime($replac_month), 'count' => $count);

						endif;

						break;

					case 'december':

						$replac_month = str_replace($datearr[1], 'December', $datearray[$countdate]['value']);

						if (strtotime($replac_month) >= $current_time) :

							$strotime[] = array('value' => strtotime($replac_month), 'count' => $count);

						endif;

						break;

				endswitch;
			}



			$sort_array = $strotime;

			sort($sort_array);

			$new_date_array = '';

			for ($countdates = 0; $countdates < count($sort_array); $countdates++) {

				$new_date_array[] = array("value" => date_i18n("j F Y", $sort_array[$countdates]['value']), 'count' => $sort_array[$countdates]['count']);
			}



			// print_r($data_array);

			$new_data_value['values'] = $new_date_array;

			$new_data_value['facet_type'] = 'facet_type_field';

			$final_result[1]['eventstartdate_str'] = $new_data_value;

			return $final_result[1];

		else :

			return $final_result[1];

		endif;
	}

	//add_filter('acf/settings/show_admin', '__return_false');

	add_filter('acf/settings/show_admin', 'my_acf_show_admin');



	function my_acf_show_admin($show)
	{



		return current_user_can('manage_options');
	}

	/**

	 * 

	 */



	function wporg_simple_role_caps()

	{

		// gets the simple_role role object

		$role = get_role('functioneel_beheerders');

		// add a new capability

		//$role->add_cap('edit_others_shop_order', true);



	}



	// add simple_role capabilities, priority must be after the initial role definition

	//add_action('init', 'wporg_simple_role_caps', 11);

	add_role(

		'eindredacteur',

		__('eindredacteur'),

		array(

			'read'         => true,  // true allows this capability

			'edit_posts'   => true,

		)

	);



	add_role(

		'functioneel_beheerders',

		__('functioneel beheerders'),

		array(

			'read'         => true,  // true allows this capability

			'edit_posts'   => true,

		)

	);



	add_role(

		'webredacteur',

		__('webredacteur'),

		array(

			'read'         => true,  // true allows this capability

			'edit_posts'   => true,

		)

	);





	function convert_endate_nath($event_date)
	{

		$datearr = explode(' ', $event_date);

		$orgidate = $event_date;

		$count = '';

		$current_time = strtotime(date("d-m-Y"));

		$replac_month = '';

		switch ($datearr[1]):

			case 'januari':

				$replac_month = str_replace($datearr[1], 'January', $orgidate);

				if (strtotime($replac_month) >= $current_time) :

					$strotime = strtotime($replac_month);

				endif;



				break;

			case 'februari':

				$replac_month = str_replace($datearr[1], 'February', $orgidate);

				if (strtotime($replac_month) >= $current_time) :

					$strotime = strtotime($replac_month);

				endif;

				break;

			case 'maart':

				$replac_month = str_replace($datearr[1], 'March', $orgidate);

				// $strotime[]=  strtotime($replac_month) ;

				if (strtotime($replac_month) >= $current_time) :

					$strotime = strtotime($replac_month);

				endif;



				break;

			case 'april':

				$replac_month = str_replace($datearr[1], 'April', $orgidate);



				if (strtotime($replac_month) >= $current_time) :

					$strotime = strtotime($replac_month);

				endif;



				break;

			case 'mei':

				$replac_month = str_replace($datearr[1], 'May', $orgidate);

				if (strtotime($replac_month) >= $current_time) :

					$strotime = strtotime($replac_month);

				endif;



				break;



			case 'juni':

				$replac_month = str_replace($datearr[1], 'June', $orgidate);

				if (strtotime($replac_month) >= $current_time) :

					$strotime = strtotime($replac_month);

				endif;

				break;

			case 'juli':

				$replac_month = str_replace($datearr[1], 'July', $orgidate);

				if (strtotime($replac_month) >= $current_time) :

					$strotime = array('value' => strtotime($replac_month), 'count' => $count);

				endif;

				break;

			case 'augustus':

				$replac_month = str_replace($datearr[1], 'August', $orgidate);

				if (strtotime($replac_month) >= $current_time) :

					$strotime = strtotime($replac_month);

				endif;

				break;

			case 'september':

				$replac_month = str_replace($datearr[1], 'September', $orgidate);

				if (strtotime($replac_month) >= $current_time) :

					$strotime = strtotime($replac_month);

				endif;

				break;

			case 'october':

				$replac_month = str_replace($datearr[1], 'October', $orgidate);

				if (strtotime($replac_month) >= $current_time) :

					$strotime = strtotime($replac_month);

				endif;

				break;

			case 'november':

				$replac_month = str_replace($datearr[1], 'November', $orgidate);

				if (strtotime($replac_month) >= $current_time) :

					$strotime = strtotime($replac_month);

				endif;

				break;

			case 'december':

				$replac_month = str_replace($datearr[1], 'December', $orgidate);

				if (strtotime($replac_month) >= $current_time) :

					$strotime = strtotime($replac_month);

				endif;

				break;

		endswitch;

		return $strotime;
	}

	add_filter('redirection_role', 'redirection_to_functioneel_beheerders');

	function redirection_to_functioneel_beheerders()
	{

		return 'edit_pages';
	}
	// Add number of product in cart
	function my_wc_cart_count()
	{

		if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {

			$count = WC()->cart->cart_contents_count;
		?><a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart'); ?>"><?php
																																	if ($count > 0) {
																																	?>
					<span class="cart-contents-count"><?php echo esc_html($count); ?></span>
				<?php
																																	} else {
																																		echo "0";
																																	}
				?></a><?php
					}
				}
				add_action('stimulanz_ajaxify_minicart', 'my_wc_cart_count');
				function my_header_add_to_cart_fragment($fragments)
				{

					ob_start();
					$count = WC()->cart->cart_contents_count;
						?><a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart'); ?>"><?php _e('0');
																																					if ($count > 0) {
																																					?>
				<span class="cart-contents-count"><?php echo esc_html($count); ?></span>
			<?php
																																					}
			?></a><?php

					$fragments['a.cart-contents'] = ob_get_clean();

					return $fragments;
				}
				add_filter('woocommerce_add_to_cart_fragments', 'my_header_add_to_cart_fragment');

				// Remove update notifications
				function remove_update_notifications($value)
				{

					if (isset($value) && is_object($value)) {
						unset($value->response['added-to-cart-popup-woocommerce/xoo-cp-main.php']);
					}

					return $value;
				}
				add_filter('site_transient_update_plugins', 'remove_update_notifications');


				// Home Slider (use)
				//add_shortcode('sliderh', 'sliderh_func');
				function sliderh_func($atts, $content = null)
				{
					extract(shortcode_atts(array(
						'body'  => '',
					), $atts));
					$body = (array) vc_param_group_parse_atts($body);

					ob_start(); ?>

		<div class=" responsive">
			<?php
					foreach ($body as $data) {
						$img = wp_get_attachment_image_src($data['first_caraousel_image'], 'full');
						$img1 = $img[0];

						$img2 = wp_get_attachment_image_src($data['second_caraousel_image'], 'full');
						$img3 = $img2[0];

			?>
				<div class="multiple">
					<div class="multiple-slide">
						<?php if ($data['title_heading_link_target'] == 'yes') {
							$target = 'target="_blank"';
						}
						?>
						<a <?php echo $target; ?> href="<?php echo $data['title_heading_link']; ?>" class="home-slider-image">
							<?php if (!empty($img1)) { ?>
								<img class="firstimg " src="<?php echo esc_url($img1); ?>" alt="images">
							<?php } ?>
							<?php if (!empty($img3)) { ?>
								<img class="sec-image" src="<?php echo esc_url($img3); ?>" alt="images">
							<?php } ?>
						</a>
						<h3><a <?php echo $target; ?> style="color:<?php echo $data['title_heading_color']; ?> !important" href="<?php echo $data['title_heading_link']; ?>" class="home-slider-title"><?php echo $data['title_heading']; ?></a></h3>
					</div>
				</div>
			<?php } ?>
		</div>
	<?php
					return ob_get_clean();
				}

				if (function_exists('vc_map')) {
					vc_map(
						array(
							"name"      => __("White caroussel", 'iotvcp-i10n'),
							"base"      => "sliderh",
							"class"     => "",
							"icon" => "icon-st",
							"category"  => 'Stimulanz Element',
							"params"    => array(
								// params group
								array(
									'type' => 'param_group',
									'value' => '',
									'param_name' => 'body',
									// Note params is mapped inside param-group:
									'params' => array(

										array(
											'type' => 'attach_image',
											"holder" => "div",
											"class" => "",
											'heading' => 'Image',
											'param_name' => 'first_caraousel_image',
											"value" => "",
											"description" => __("Upload image", 'otvcp-i10n')
										),
										array(
											'type' => 'attach_image',
											"holder" => "div",
											"class" => "",
											'heading' => 'Image',
											'param_name' => 'second_caraousel_image',
											"value" => "",
											"description" => __("Upload Second image", 'otvcp-i10n')
										),
										array(
											'type' => 'textfield',
											"class" => "",
											'heading' => 'Heading',
											'param_name' => 'title_heading',
											"value" => "",
											"description" => __("Add heading", 'iotvcp-i10n')
										),
										array(
											"type" => "colorpicker",
											"class" => "",
											"heading" => __("Heading Color", "my-text-domain"),
											"param_name" => "title_heading_color",
											"value" => '',
										),
										array(
											'type' => 'textfield',
											"class" => "",
											'heading' => 'Heading Link',
											'param_name' => 'title_heading_link',
											"value" => "",
											"description" => __("Add heading link", 'iotvcp-i10n')
										),
										array(
											'type' => 'dropdown',
											'heading' => __('Heading Link target Blank',  "my-text-domain"),
											'param_name' => 'title_heading_link_target',
											'value' => array(
												__('Yes',  "my-text-domain") => 'yes',
												__('No',  "my-text-domain") => 'no',
											),
										)
									)
								),

							)
						)
					);
				}




				add_shortcode('sliderh1', 'sliderh_func1');
				function sliderh_func1($atts, $content = null)
				{
					extract(shortcode_atts(array(
						'body1'  => '',
					), $atts));
					$body = (array) vc_param_group_parse_atts($body1);

					ob_start(); ?>

		<div class=" responsive version2 home-slider">
			<?php
					foreach ($body as $data) {
						$img = wp_get_attachment_image_src($data['first_caraousel_image1'], 'full');
						$img1 = $img[0];

						$img2 = wp_get_attachment_image_src($data['second_caraousel_image1'], 'full');
						$img3 = $img2[0];

			?>
				<div class="multiple">
					<div class="multiple-slide home-slider">
						<?php if ($data['title_heading_link_target1'] == 'yes') {
							$target = 'target="_blank"';
						}
						
						?>
						<a <?php echo $target; ?> href="<?php echo $data['title_heading_link1']; ?>" class="home-slider-image">
							<?php if (!empty($img1)) { ?>
								<img class="firstimg" src="<?php echo esc_url($img1); ?>" alt="images">
							<?php } ?>
							<?php if (!empty($img3)) { ?>
								<img class="sec-image" src="<?php echo esc_url($img3); ?>" alt="images">
							<?php } ?>
						</a>
						<h3><a <?php echo $target; ?> style="color:<?php echo $data['title_heading_color1']; ?> !important" href="<?php echo $data['title_heading_link1']; ?>" class="home-slider-title"><?php echo $data['title_heading1']; ?></a></h3>
					</div>
				</div>
			<?php } ?>
		</div>
	<?php
					return ob_get_clean();
				}

				if (function_exists('vc_map')) {
					vc_map(
						array(
							"name"      => __("Colored Caroussel", 'iotvcp-i10n'),
							"base"      => "sliderh1",
							"class"     => "",
							"icon" => "icon-st",
							"category"  => 'Stimulanz Element',
							"params"    => array(
								// params group
								array(
									'type' => 'param_group',
									'value' => '',
									'param_name' => 'body1',
									// Note params is mapped inside param-group:
									'params' => array(

										array(
											'type' => 'attach_image',
											"holder" => "div",
											"class" => "",
											'heading' => 'Image',
											'param_name' => 'first_caraousel_image1',
											"value" => "",
											"description" => __("Upload image", 'otvcp-i10n')
										),
										array(
											'type' => 'attach_image',
											"holder" => "div",
											"class" => "",
											'heading' => 'Image',
											'param_name' => 'second_caraousel_image1',
											"value" => "",
											"description" => __("Upload Second image", 'otvcp-i10n')
										),
										array(
											'type' => 'textfield',
											"class" => "",
											'heading' => 'Heading',
											'param_name' => 'title_heading1',
											"value" => "",
											"description" => __("Add heading", 'iotvcp-i10n')
										),
										array(
											"type" => "colorpicker",
											"class" => "",
											"heading" => __("Heading Color", "my-text-domain"),
											"param_name" => "title_heading_color1",
											"value" => '',
										),
										array(
											'type' => 'textfield',
											"class" => "",
											'heading' => 'Heading Link',
											'param_name' => 'title_heading_link1',
											"value" => "",
											"description" => __("Add heading link", 'iotvcp-i10n')
										),
										array(
											'type' => 'dropdown',
											'heading' => __('Heading Link target Blank',  "my-text-domain"),
											'param_name' => 'title_heading_link_target1',
											'value' => array(
												__('Yes',  "my-text-domain") => 'yes',
												__('No',  "my-text-domain") => 'no',
											),
										)
									)
								),

							)
						)
					);
				}

				if (!function_exists('wpcb_get_taxonomy_slug_by_id')) {
					/**
					 * wpcb_get_taxonomy_slug_by_id.
					 *
					 * @see https://wpcodebook.com/snippets/get-taxonomy-slug-by-id/
					 */
					function wpcb_get_taxonomy_slug_by_id($id, $taxonomy)
					{
						return (($term = get_term_by('slug', $id, $taxonomy)) ? $term->term_id : false);
					}
				}

				/*add_action( 'wp_enqueue_scripts', 'aj_enqueue_scripts' );
function aj_enqueue_scripts() {
  // Note that the first parameter of wp_enqueue_script() matches that of wp_localize_script.
  wp_enqueue_script( 'aj-demo', plugin_dir_url( __FILE__ ). 'dc-ajax-demo.js', array('jquery') );

  // The second parameter ('aj_ajax_url') will be used in the javascript code.
  wp_localize_script( 'aj-demo', 'aj_ajax_demo', array(
                      'ajax_url' => admin_url( 'admin-ajax.php' ),
  ));
}*/


				function add_theme_scripts()
				{
					//For style
					wp_enqueue_style('masterstyle', get_template_directory_uri() . '/dist/styles/master.css', array(), '1.1', 'all');
					wp_enqueue_style('multiselect', get_template_directory_uri() . '/dist/styles/multiselect.css', array(), '1.1', 'all');

					// wp_enqueue_style( 'overidestyle', get_template_directory_uri() . '/dist/styles/overide.css', array(), '1.1', 'all');

					//For Scripts
					wp_enqueue_script('sliderscript', get_template_directory_uri() . '/dist/scripts/slick.min.js', array('jquery'), 1.1, true);
					wp_enqueue_script('slidehelper', get_template_directory_uri() . '/dist/scripts/sliderscript.js', array('jquery'), 1.1, true);
					wp_enqueue_script('multiselect', get_template_directory_uri() . '/dist/scripts/jquery.multi-select.min.js', array('jquery'), 1.1, true);
					wp_enqueue_script('sticky', get_template_directory_uri() . '/dist/scripts/stickyMojo.js', array('jquery'), 1.1, true);

				}
				add_action('wp_enqueue_scripts', 'add_theme_scripts');




				add_action('wp_ajax_nopriv_aj_ajax_demo_get_count', 'aj_ajax_demo_process');
				add_action('wp_ajax_aj_ajax_demo_get_count', 'aj_ajax_demo_process');  // For logged in users.
				function aj_ajax_demo_process()
				{
					$post_type = $_POST['post_type'];
					$slug = get_term($post_type);
					$data  = get_field('custom_introduction', $slug->taxonomy . '_' . $slug->term_id);
					echo $data;
					wp_die();
				}


				// Press Releases


				function get_post_primary_category($post_id, $term = 'training_cat', $return_all_categories = false)
				{
					$return = array();

					if (class_exists('WPSEO_Primary_Term')) {
						// Show Primary category by Yoast if it is enabled & set
						$wpseo_primary_term = new WPSEO_Primary_Term($term, $post_id);
						$primary_term = get_term($wpseo_primary_term->get_primary_term());

						if (!is_wp_error($primary_term)) {
							$return['primary_category'] = $primary_term;
						}
					}

					if (empty($return['primary_category']) || $return_all_categories) {
						$categories_list = get_the_terms($post_id, $term);

						if (empty($return['primary_category']) && !empty($categories_list)) {
							$return['primary_category'] = $categories_list[0];  //get the first category
						}
						if ($return_all_categories) {
							$return['all_categories'] = array();

							if (!empty($categories_list)) {
								foreach ($categories_list as &$category) {
									$return['all_categories'][] = $category->term_id;
								}
							}
						}
					}

					return $return;
				}


				//function to modify default WordPress query
				function wpb_custom_query($query)
				{

					if ($query->is_main_query() && !is_admin() && is_post_type_archive('begrippenlijst')) {

						$query->set('orderby', 'title');
						$query->set('order', 'ASC');
						$query->set('suppress_filters', 'true');
					}
				}

				add_action('pre_get_posts', 'wpb_custom_query');

				// shortcode for about block
				add_shortcode('about_section', 'about_func');
				function about_func($atts, $content = null)
				{
					ob_start();
					$about_header = get_field('about_header','option'); 
					$about_description = get_field('about_content','option');
				?>

<section class="vc_section section about-history white">
    <div class="vc_row wpb_row vc_row-fluid container">
        <div class="contact-details-outer wpb_column vc_column_container vc_col-sm-2/5">
          <div class="vc_column-inner">
              <div class="wpb_wrapper">
                <div class="wpb_text_column wpb_content_element ">
                    <div class="wpb_wrapper">
                      <h2 id="over-stimulansz"><?php echo $about_header; ?></h2>
                      <p><?php echo $about_description ?></p>
                    </div>
                </div>
                <div class="wpb_text_column wpb_content_element  vc_custom_1617021612303">
                    <div class="wpb_wrapper">
                      <?php
                      $read_more = get_field('read_more', 'option');
                      if ($read_more) {
                        $read_more_url = $read_more['url'];
                        $read_more_title = $read_more['title'];
                        $read_more_target = $read_more['target'] ? $read_more['target'] : '_self';
                      }
                      ?>
                      <p style="text-align: left;"><a href="<?php echo esc_url($read_more_url); ?>" id="lees_meer_link" class="read-more-btn-link" target="<?php echo esc_attr($read_more_target); ?>"><i class="fas fa-plus"></i>&nbsp;<?php echo esc_html($read_more_title); ?></a></p>
                    </div>
                </div>
              </div>
          </div>
        </div>

        <?php
        if (have_rows('features', 'option')) :
        while (have_rows('features', 'option')) : the_row();
        $feature_count = get_sub_field('feature_count');
        $feature_content = get_sub_field('feature_content'); 
        ?>
        <div class="contact-details-outer wpb_column vc_column_container vc_col-sm-1/5">
          <div class="vc_column-inner">
              <div class="wpb_wrapper">
                <div class="wpb_text_column wpb_content_element features-content icon-block">
                    <div class="wpb_wrapper">
                      <?php
                      $feature_image = get_sub_field('feature_icon');
                      if (!empty($feature_image['url'])) :
                      ?>
                      <p><img class="alignnone size-full wp-image-27334 aligncenter" src="<?php echo esc_url($feature_image['url']); ?>" alt="<?php echo esc_attr($feature_image['alt']); ?>"></p>
                      <?php endif; ?>
                      <h1 id="000" style="text-align: center;"><?php echo  $feature_count ?></h1>
                      <p style="text-align: center;"><?php echo  $feature_content  ?></p>
                    </div>
                </div>
              </div>
          </div>
        </div>
        <?php
        endwhile;
        endif; 
        ?>
           </div>
		   <div class="vc_row wpb_row vc_row-fluid container hide-div" id="lees_meer_expand">
        <?php
        if (have_rows('about_block_description', 'option')) :
          while (have_rows('about_block_description','option')) : the_row();
            $about_description_title = get_sub_field('about_description_header');
            $about_description_content = get_sub_field('about_description_content');
        ?>
            <div class="block">
              <h2><?php echo  $about_description_title ?></h2>
              <p><?php echo $about_description_content ?></p>
            </div>
        <?php
          endwhile;
        endif;
        ?>
      </div>
  </section>
					

					<?php return ob_get_clean();
				}



				// contact section

				add_shortcode('contact_section', 'contact_func');
				function contact_func($atts, $content = null)
				{
					ob_start();
					print_r(get_field('contact_block_header'));
					$person_object = get_field('contact_content'); ?>
		<p>Onze studieadviseur <?php print_r($person_object->post_title); ?> helpt je graag verder.</p>
		<?php $person_icon = get_the_post_thumbnail_url($person_object->ID); ?>
		<div class="person-details-blk">
			<div>
				<img class="size-full wp-image-27516 alignleft" src="<?php echo $person_icon ?>" alt="img" width="100" height="104">
			</div>
			<div>
				<a href="tel:<?php print_r(get_field('telefoon', $person_object->ID)); ?>" class="phone"><?php print_r(get_field('telefoon', $person_object->ID)); ?></a>
				<a href="mailto:<?php print_r(get_field('e_mail_adres', $person_object->ID)); ?>" class="email"><?php print_r(get_field('e_mail_adres', $person_object->ID)); ?></a>
			</div>
		</div>
	<?php return ob_get_clean();
				}

	// 3 random posts for general landing page
	add_shortcode('random-training', 'random_training_func');
	function random_training_func($atts, $content = null)
				{
					ob_start();
					$args = array(
						'numberposts'  => 3,
						'post_type'    => 'training'
					  );
					  $trainings = get_posts($args);
					  foreach ($trainings as $training) {
						$image = wp_get_attachment_url(get_post_thumbnail_id($training->ID), 'thumbnail');
						// $image = get_field('background_image', $training->ID, true); ?>

<div class="details-listingblk wpb_column vc_column_container vc_col-sm-3">
    <div class="vc_column-inner">
        <div class="wpb_wrapper">
            <div class="card card-block white_bg_block" id="" style="height: 369px;">
                <div class="card-inner">
                    <div class="img-wrap">
					<img width="264" height="148" src="<?php echo $image ?>" class="attachment-stimulansz_card_image size-stimulansz_card_image" alt="<?php echo $training->post_title ?>"></div>
                    <div class="cnt-wrap">
                        <div class="title_wrap" style="height: 159px;">
                            <div class="title"><?php echo $training->post_title ?></div>
                            <p><?php echo substr(strip_tags($training->post_content), 0, 80) . '..' ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>

					<?php return ob_get_clean();
				}


	//general page trainer section
	add_shortcode('trainers_section', 'trainers_func');
	function trainers_func($atts, $content = null)
				{
					ob_start();
					$trainers_object = get_field('trainer'); ?>
		<div class="wpb_column vc_column_container vc_col-sm-9">
			<div class="vc_column-inner">
				<div class="wpb_wrapper">
					<div class="vc_row wpb_row vc_inner vc_row-fluid">
						<div class="description-block wpb_column vc_column_container vc_col-sm-4">
							<div class="vc_column-inner">
								<div class="wpb_wrapper">
									<div class="wpb_text_column wpb_content_element ">
										<div class="wpb_wrapper">
											<div id="heeft-u-nog-vragen">
											<h2 id="heeft-u-nog-vragen">	<?php print_r(get_field('trainer_header')); ?></h2>
											<p><?php print_r(get_field('trainer_header_content')); ?> </p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php
						foreach ($trainers_object as $trainer) {
						?>
							<div class="details-listingblk wpb_column vc_column_container vc_col-sm-4">
								<div class="vc_column-inner">
									<div class="wpb_wrapper">
										<div class="card card-block " id="" style="height: 414px;">
											<div class="card-inner">
												<div class="img-wrap">
													<?php $trainer_icon = get_the_post_thumbnail_url($trainer->ID); ?>
													<img width="264" height="148" src="<?php echo $trainer_icon ?>" alt="">
												</div>
												<div class="cnt-wrap">
													<div class="title_wrap" style="height: 132px;">
														<div class="title"><?php echo $trainer->post_title; ?></div>
														<p><?php echo $trainer->post_excerpt ?></p>
													</div>
													<div class="sub_title_wrap">
														<a href="tel:<?php print_r(get_field('telefoon', $trainer->ID)); ?>" class="sub_title sub_title_1"><?php print_r(get_field('telefoon', $trainer->ID)); ?></a>
														<div class="sub_title sub_title_2"><button title="<?php print_r(get_field('e_mail_adres', $trainer->ID)) ?>" data-text="<?php print_r(get_field('e_mail_adres', $trainer->ID)) ?>" class="copyboard"><i class="fa fa-copy"></i> Kopieer e-mailadres</button></div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	<?php return ob_get_clean();
				}

				//popular slider - training page

				add_shortcode('popular_trainings', 'popular_trainings_shortcode');

				function popular_trainings_shortcode()
				{

					ob_start();

	?>
		<!-- second-slider block -->
		<div class="sstnewslider-container">
			<div class="container-fluid">
				<div class="slider-innermaincont">
					<ul>
						<?php
						$args = array(
							'numberposts'	=> 20,
							'post_type'		=> 'training'
						);
						$trainings = get_posts($args);
						foreach ($trainings as $training) {
							$image = get_field('background_image', $training->ID, true); ?>

							<li>
								<a href="#">
									<div class="thumb-container">
										<img src="<?php echo $image['url']; ?>" alt="<?php echo $training->post_title ?>" />
									</div>
								</a>
								<div class="slder-contentcontainer">
									<span><?php
											$selectedTerms = [];
											foreach (wp_get_post_terms($training->ID, 'training_cat') as $term) {
												$selectedTerms[] = $term->name;
											}
											echo implode(', ', $selectedTerms);
											?></span>
									<h4><?php echo $training->post_title ?></h4>
									<p><?php echo substr(strip_tags($training->post_content), 0, 250) . '..' ?></p>
									<div class="date-blk"><?php echo get_field('duration', $training->ID) ?>
										<a href="#"><i class="dark-arrow"></i></a>
									</div>
								</div>

							</li>
						<?php } ?>
					</ul>

				</div>

			</div>
		</div>
		<!-- second-slider block -->
	    <?php
					return ob_get_clean();
		}
		//taxonomy slider - training page

				function slider()
				{
					$html  = '';
					$data = get_terms(array(
						'taxonomy' => 'training_werkveld',
						'hide_empty' => true,
					));
					if (!empty($data)) {
						$html .= '<div class="sstslider-outercontainer"><div class="container-fluid">
								  <div class="slider-innerblock">
								  <h2>Bekijk opleidingen per werkveld</h2>
								  <ul>';
						foreach ($data as $term) {
							$html .= '<li>
										<a href="#">
											<h6>' . $term->name . '</h6>
											<p>' . $term->count . ' trainingen<i class="light-arrow"></i></p>
										</a>
									</li>';
						}
						$html .= '</ul></div></div></div>';
					}

					return $html;
				}
				add_shortcode('slidertest', 'slider');

				function js_composer_front_load() {
					if(is_single()) {
						wp_enqueue_style('js_composer_front');
					}
				}
				
				add_action('wp_enqueue_scripts', 'js_composer_front_load');


				
				function hide_header_footer_for_result_page() {
					if( is_page( 27972 ) ) {
						  ?> <style>
							  
							  header#masterhead, div.site-header-mobileheader,
							  .footer-head-container, .footer-above, #footer_socialoutercont{ display:none; }
							 .footer-below-container{ margin-bottom: 110px;}
								  
							  </style>
						  <?php
					  }
				  }
				  add_action( 'wp', 'hide_header_footer_for_result_page' );


				// Require new custom Element
				require_once(get_template_directory() . '/vc-components/vc-achievements.php');
				require_once(get_template_directory() . '/vc-components/vc-blockquote.php');
				require_once(get_template_directory() . '/vc-components/vc-card.php');
				require_once(get_template_directory() . '/vc-components/vc-courses.php');
				require_once(get_template_directory() . '/vc-components/vc-posts.php');
				// require_once(get_template_directory() . '/vc-components/vc-map.php');
				require_once(get_template_directory() . '/lib/ajax.php');
			
				function get_trainings_count($term1,$taxonomy, $term2 ) {
					$args = array(
					  'post_type' => 'training',
					  'posts_per_page' => -1,
					  'post_status' => 'publish',
					  'orderby' => 'menu_order',
					  'order' => 'DESC',
					  'tax_query' => array(
					    array(
					      'taxonomy' => $term2->taxonomy,
					      'field' => 'slug',
					      'terms' => $term2->slug,
					    ),
					    array(
					      'taxonomy' => $taxonomy,
					      'field' => 'slug',
					      'terms' => $term1,
					    ),
					    'relation' => 'AND'
					  ),
					);
					$queries = new WP_Query($args);
					return $queries->post_count;
				  }