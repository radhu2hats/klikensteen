<?php

/**

 * Sage includes

 *

 * The $sage_includes array determines the code library included in your theme.

 * Add or remove files to the array as needed. Supports child theme overrides.

 *

 * Please note that missing files will produce a fatal error.

 *

 * @link https://github.com/roots/sage/pull/1042

 */

//Register image size

add_image_size('stimulansz_block_image', 396, 179, true);

//Register image for instructeur

add_image_size('stimulansz_instructeur_image', 200, 200, true);

//Register Training block image 

add_image_size('stimulansz_related_post_image', 407, 179, true);



add_image_size('stimulansz_single_blog_page_image', 350 , 200, true);

//Register image for instractor

add_image_size('instractor_image', 130, 190, true);



add_image_size( 'stimulansz_instructeur_listing_image', 407, 300, array( 'center', 'bottom' ) );



$sage_includes = [

    'lib/assets.php', // Scripts and stylesheets

    'lib/extras.php', // Custom functions

    'lib/setup.php', // Theme setup

    'lib/titles.php', // Page titles

    'lib/wrapper.php', // Theme wrapper class

    'lib/customizer.php', // Theme customizer

    'lib/custompost.php', // Theme custom post type

    'lib/woocommerce.php'  // Theme custom post type

];





foreach ($sage_includes as $file) {

    if (!$filepath = locate_template($file)) {

        trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);

    }



    require_once $filepath;

}

unset($file, $filepath);



/* * ******************************************************************************

 *

 *                               Upload logo from the backend 

 *

 * ******************************************************************************* */



function m1_customize_register($wp_customize) {

    $wp_customize->add_setting('m1_logo'); // Add setting for logo uploader

    // Add control for logo uploader (actual uploader)

    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'm1_logo', array(

        'label' => __('Upload Logo', 'm1'),

        'section' => 'title_tagline',

        'settings' => 'm1_logo',

    )));

}



add_action('customize_register', 'm1_customize_register');



/**

 * Add top bar menu

 */

function wpb_custom_new_menu() {

    register_nav_menu('my-custom-menu', __('Top bar menu'));

}



//add_action( 'init', 'wpb_custom_new_menu' );



if (function_exists('acf_add_options_page')) {



    acf_add_options_page(array(

        'page_title' => 'Theme General Settings',

        'menu_title' => 'Theme Settings',

        'menu_slug' => 'theme-general-settings',

        'capability' => 'edit_posts',

        'redirect' => false

    ));



    acf_add_options_sub_page(array(

        'page_title' => 'Theme Header Settings',

        'menu_title' => 'Header',

        'parent_slug' => 'theme-general-settings',

    ));



    acf_add_options_sub_page(array(

        'page_title' => 'Theme Footer Settings',

        'menu_title' => 'Footer',

        'parent_slug' => 'theme-general-settings',

    ));

}



/**

 * 

 * @param type $comment

 * @param type $args

 * @param type $depth

 */

function stimulansz_theme_comment($comment, $args, $depth) {

    if ('div' === $args['style']) {

        $tag = 'ol';

        $add_below = 'comment';

    } else {

        $tag = 'li';

        $add_below = 'div-comment';

    }

    ?>

    <<?php echo $tag ?> <?php comment_class(empty($args['has_children']) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">

    <?php if ('div' != $args['style']) : ?>

        <article id="div-comment-<?php comment_ID() ?>" class="comment-body">

        <?php endif; ?>

        <div class="user_icon">

            <?php if ($args['avatar_size'] != 0) echo get_avatar($comment, $args['avatar_size']); ?>

        </div>

        <div class="comment_data">

            <div class="reviewrating"> 

                <?php

                $commentrating = get_comment_meta(get_comment_ID(), 'rating', true);

                if (isset($commentrating)):

                    echo str_replace('.', ',', $commentrating);

                endif;

                ?>

            </div>



            <footer class="comment-meta">

                <div class="comment-author vcard">

                    <?php //if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] );   ?>

                    <?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()); ?>

                </div>

                <?php if ($comment->comment_approved == '0') : ?>

                    <em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.'); ?></em>

                    <br/>

                <?php endif; ?>

                <div class="comment-content-stimulzs">         

                    <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars(get_comment_link($comment->comment_ID)); ?>">

                            <?php

                            /* translators: 1: date, 2: time */

                            printf(__('%1$s at %2$s'), get_comment_date(), get_comment_time());

                            ?></a><?php edit_comment_link(__('(Edit)'), '  ', '');

                            ?>

                    </div>

                </div>

            </footer>

            <div class="comment-content">

                <?php comment_text(); ?>

            </div> 

            <div class="reply">

                <?php comment_reply_link(array_merge($args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))); ?>

            </div>

        </div> 

        <?php if ('div' != $args['style']) : ?>

        </article>

    <?php endif; ?>

    <?php

}



function exclude_single_posts_home($query) {

    if ($query->is_main_query()) {

        $query->set('post__not_in', array(271, 268));

    }

}



add_action('pre_get_posts', 'exclude_single_posts_home');



/**

 * Remove the slug from published post permalinks. Only affect our custom post type, though.

 */

function stimulansz_remove_cpt_slug($post_link, $post, $leavename) {



    if ('landingspagina' != $post->post_type ) {

        return $post_link;

    }



    $post_link = str_replace('/' . $post->post_type . '/', '/', $post_link);



    return $post_link;

}



add_filter('post_type_link', 'stimulansz_remove_cpt_slug', 10, 3);



/**

 * Have WordPress match postname to any of our public post types (post, page, race)

 * All of our public post types can have /post-name/ as the slug, so they better be unique across all posts

 * By default, core only accounts for posts and pages where the slug is /post-name/

 */

function stimulansz_parse_request_trick($query) {



    // Only noop the main query

    if (!$query->is_main_query())

        return;



    // Only noop our very specific rewrite rule match

    if (2 != count($query->query) || !isset($query->query['page'])) {

        return;

    }



    // 'name' will be set if post permalinks are just post_name, otherwise the page rule will match

    if (!empty($query->query['name'])) {

        $query->set('post_type', array('post', 'page', 'landingspagina'));

    }

}



add_action('pre_get_posts', 'stimulansz_parse_request_trick');



add_action('rss2_item', 'stimulansz_rss2_item');



function stimulansz_rss2_item() {

    if (get_post_type() == 'post') {

        $taxonomies = array(

            'actueel',

        );

        // Loop through taxonomies

        foreach ($taxonomies as $taxonomy) {

            $terms = get_the_terms($post_id, $taxonomy);

            if (is_array($terms)) {

                // Loop through terms

                foreach ($terms as $term) {

                    echo "<{$taxonomy}>{$term->name}</{$taxonomy}>\n";

                }

            }

        }

    }

}



//add SVG to allowed file uploads

function add_file_types_to_uploads($file_types) {



    $new_filetypes = array();

    $new_filetypes['svg'] = 'image/svg+xml';

    $file_types = array_merge($file_types, $new_filetypes);



    return $file_types;

}



add_action('upload_mimes', 'add_file_types_to_uploads');



/**

 * Date sorting

 * @param type $a

 * @param type $b

 * @return type

 */

function date_sort($a, $b) {

    return strtotime($a) - strtotime($b);

}

 

function stimulansz_author_blog_load_more_pagination() {



    global $wp_query;

   

    // register our main script but do not enqueue it yet

    wp_register_script('ajaxloadmore', get_stylesheet_directory_uri() . '/assets/scripts/ajaxloadmore.js', array('jquery'));



    // now the most interesting part

    // we have to pass parameters to ajaxloadmore.js script but we can get the parameters values only in PHP

    // you can define variables directly in your HTML but I decided that the most proper way is wp_localize_script()

    wp_localize_script('ajaxloadmore', 'stimulansz_loadmore_params', array(

        'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX

        'posts' => serialize( $wp_query->query_vars ), // everything about your loop is here

        'current_page' => get_query_var('paged') ? get_query_var('paged') : 1,

        //'max_page' => $wp_query->max_num_pages

    ));



    wp_enqueue_script('ajaxloadmore');

}



add_action('wp_enqueue_scripts', 'stimulansz_author_blog_load_more_pagination');



/**

 * 

 */

function stimulansz_loadmore_ajax_handler() {

    ?>



    <?php

    $args = unserialize(stripslashes($_POST['query']));

   

    $args['paged'] = $_POST['page'] + 1; // we need next page to be loaded

    $args['post_status'] = 'publish';

    $args['author'] = $_POST['author_id'];

    $args['post_type'] = 'post';

    $args['posts_per_page'] =  5;

    $query = new \WP_Query($args);

  

   ?>  

    <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?> 

            <li>

                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

                <div class="blog_details">

                    <div class="intro">

            <?php

            $post_date = get_the_date();

            if ($post_date):

                ?>

                            <div class="date"><?php echo $post_date; ?></div>

                        <?php endif; ?>

                        <div class="content">

                            <?php 

                            $intro = get_field('additional_banner_text');

                            if(isset($intro) && !empty($intro)):

                                echo $intro.' <a class="stimulansz-home-more-link" href="' . get_permalink() . '">' . esc_html__( 'Read more', 'stimulansz' ) . '</a>';

                            else:

                                the_excerpt();

                            endif;

                        

                        ?></div>

                    </div>

                </div>

            </li>    

        <?php endwhile;

    endif;

    ?>



    <?php

    die; // here we exit the script and even no wp_reset_query() required!

}



add_action('wp_ajax_loadmore', 'stimulansz_loadmore_ajax_handler'); // wp_ajax_{action}

add_action('wp_ajax_nopriv_loadmore', 'stimulansz_loadmore_ajax_handler'); // wp_ajax_nopriv_{action}



/**

 * Disable update for wpsolr

 * @param type $value

 * @return type

 */

function stimulansz_wpsolr_filter_plugin_updates( $value ) {

    unset( $value->response['wpsolr-pro/wpsolr-pro.php'] );

    return $value;

}

add_filter( 'site_transient_update_plugins', 'stimulansz_wpsolr_filter_plugin_updates' );



/**

 * Sort posts on the blog posts page according to the custom sort order

 * @param type $query

 */

function stimulansz_post_order_sort( $query ){

  if ( $query->is_main_query()){

    $query->set( 'orderby', 'post_date' );

    $query->set( 'order' , 'DESC' );

  }

}

add_action( 'pre_get_posts' , 'stimulansz_post_order_sort' );





function stimulansz_read_more_link() {

    return '<a class="more-link" href="'. get_permalink() . '">Leesdd meer</a>';

}

add_filter( 'the_content_more_link', 'stimulansz_read_more_link' );





add_filter('event_date_sort_results','sort_traing_Date',10,1);



/**

 * 

 * @param type $final_result

 * @return string

 */

function sort_traing_Date($final_result = array()) {

    

   // print_r($final_result);

    

    $data_array = isset($final_result[1]['eventstartdate_str']);

	//echo "check file";

    $datearray = $data_array['values'];

    $count = '';

    $current_time = strtotime(date("d-m-Y"));

    $strotime = array();

   if(count($datearray)>=1):

       

   

    for ($countdate = 0; $countdate < count($datearray); $countdate++) {

        $datearr = explode(' ', $datearray[$countdate]['value']);

        $count = '';

        $count = $datearray[$countdate]['count'];

        $replac_month = '';

        switch ($datearr[1]):

            case 'januari':

                $replac_month = str_replace($datearr[1], 'January', $datearray[$countdate]['value']);

                if (strtotime($replac_month) >= $current_time):

                    $strotime[] = array('value' => strtotime($replac_month), 'count' => $count);

                endif;



                break;

            case 'februari':

                $replac_month = str_replace($datearr[1], 'February', $datearray[$countdate]['value']);

                if (strtotime($replac_month) >= $current_time):

                    $strotime[] = array('value' => strtotime($replac_month), 'count' => $count);

                endif;

                break;

            case 'maart':

                $replac_month = str_replace($datearr[1], 'March', $datearray[$countdate]['value']);

                // $strotime[]=  strtotime($replac_month) ;

                if (strtotime($replac_month) >= $current_time):

                    $strotime[] = array('value' => strtotime($replac_month), 'count' => $count);

                endif;



                break;

            case 'april':

                $replac_month = str_replace($datearr[1], 'April', $datearray[$countdate]['value']);



                if (strtotime($replac_month) >= $current_time):

                    $strotime[] = array('value' => strtotime($replac_month), 'count' => $count);

                endif;



                break;

            case 'mei':

                $replac_month = str_replace($datearr[1], 'May', $datearray[$countdate]['value']);

                if (strtotime($replac_month) >= $current_time):

                    $strotime[] = array('value' => strtotime($replac_month), 'count' => $count);

                endif;



                break;



            case 'juni':

                $replac_month = str_replace($datearr[1], 'June', $datearray[$countdate]['value']);

                if (strtotime($replac_month) >= $current_time):

                    $strotime[] = array('value' => strtotime($replac_month), 'count' => $count);

                endif;

                break;

            case 'juli':

                $replac_month = str_replace($datearr[1], 'July', $datearray[$countdate]['value']);

                if (strtotime($replac_month) >= $current_time):

                    $strotime[] = array('value' => strtotime($replac_month), 'count' => $count);

                endif;

                break;

            case 'augustus':

                $replac_month = str_replace($datearr[1], 'August', $datearray[$countdate]['value']);

                if (strtotime($replac_month) >= $current_time):

                    $strotime[] = array('value' => strtotime($replac_month), 'count' => $count);

                endif;

                break;

            case 'september':

                $replac_month = str_replace($datearr[1], 'September', $datearray[$countdate]['value']);

                if (strtotime($replac_month) >= $current_time):

                    $strotime[] = array('value' => strtotime($replac_month), 'count' => $count);

                endif;

                break;

            case 'october':

                $replac_month = str_replace($datearr[1], 'October', $datearray[$countdate]['value']);

                if (strtotime($replac_month) >= $current_time):

                    $strotime[] = array('value' => strtotime($replac_month), 'count' => $count);

                endif;

                break;

            case 'november':

                $replac_month = str_replace($datearr[1], 'November', $datearray[$countdate]['value']);

                if (strtotime($replac_month) >= $current_time):

                    $strotime[] = array('value' => strtotime($replac_month), 'count' => $count);

                endif;

                break;

            case 'december':

                $replac_month = str_replace($datearr[1], 'December', $datearray[$countdate]['value']);

                if (strtotime($replac_month) >= $current_time):

                    $strotime[] = array('value' => strtotime($replac_month), 'count' => $count);

                endif;

                break;

        endswitch;

    }



    $sort_array = $strotime;

    sort($sort_array);

    $new_date_array = '';

    for ($countdates = 0; $countdates < count($sort_array); $countdates++) {

        $new_date_array[] = array("value" => date_i18n("j F Y", $sort_array[$countdates]['value']), 'count' => $sort_array[$countdates]['count']);

    }



    // print_r($data_array);

    $new_data_value['values'] = $new_date_array;

    $new_data_value['facet_type'] = 'facet_type_field';

    $final_result[1]['eventstartdate_str'] = $new_data_value;

     return $final_result[1];

    else:

     return $final_result[1];

    endif; 

    

    

}

//add_filter('acf/settings/show_admin', '__return_false');

add_filter('acf/settings/show_admin', 'my_acf_show_admin');



function my_acf_show_admin( $show ) {

    

    return current_user_can('manage_options');

    

}

/**

 * 

 */



function wporg_simple_role_caps()

{

    // gets the simple_role role object

    $role = get_role('functioneel_beheerders');

    // add a new capability

    //$role->add_cap('edit_others_shop_order', true);

    

}

 

// add simple_role capabilities, priority must be after the initial role definition

//add_action('init', 'wporg_simple_role_caps', 11);

add_role(

    'eindredacteur',

    __( 'eindredacteur' ),

    array(

        'read'         => true,  // true allows this capability

        'edit_posts'   => true,

    )

);



add_role(

    'functioneel_beheerders',

    __( 'functioneel beheerders' ),

    array(

        'read'         => true,  // true allows this capability

        'edit_posts'   => true,

    )

);



add_role(

    'webredacteur',

    __( 'webredacteur' ),

    array(

        'read'         => true,  // true allows this capability

        'edit_posts'   => true,

    )

);





function convert_endate_nath($event_date){

        $datearr = explode(' ', $event_date);

        $orgidate = $event_date;

        $count = '';

        $current_time = strtotime(date("d-m-Y"));

        $replac_month = '';

        switch ($datearr[1]):

            case 'januari':

                $replac_month = str_replace($datearr[1], 'January', $orgidate);

                if (strtotime($replac_month) >= $current_time):

                    $strotime = strtotime($replac_month);

                endif;



                break;

            case 'februari':

                $replac_month = str_replace($datearr[1], 'February', $orgidate);

                if (strtotime($replac_month) >= $current_time):

                    $strotime = strtotime($replac_month);

                endif;

                break;

            case 'maart':

                $replac_month = str_replace($datearr[1], 'March', $orgidate);

                // $strotime[]=  strtotime($replac_month) ;

                if (strtotime($replac_month) >= $current_time):

                    $strotime = strtotime($replac_month);

                endif;



                break;

            case 'april':

                $replac_month = str_replace($datearr[1], 'April',$orgidate);



                if (strtotime($replac_month) >= $current_time):

                    $strotime = strtotime($replac_month);

                endif;



                break;

            case 'mei':

                $replac_month = str_replace($datearr[1], 'May', $orgidate);

                if (strtotime($replac_month) >= $current_time):

                    $strotime = strtotime($replac_month);

                endif;



                break;



            case 'juni':

                $replac_month = str_replace($datearr[1], 'June',$orgidate);

                if (strtotime($replac_month) >= $current_time):

                    $strotime = strtotime($replac_month);

                endif;

                break;

            case 'juli':

                $replac_month = str_replace($datearr[1], 'July', $orgidate);

                if (strtotime($replac_month) >= $current_time):

                    $strotime = array('value' => strtotime($replac_month), 'count' => $count);

                endif;

                break;

            case 'augustus':

                $replac_month = str_replace($datearr[1], 'August',$orgidate);

                if (strtotime($replac_month) >= $current_time):

                    $strotime = strtotime($replac_month);

                endif;

                break;

            case 'september':

                $replac_month = str_replace($datearr[1], 'September', $orgidate);

                if (strtotime($replac_month) >= $current_time):

                    $strotime = strtotime($replac_month);

                endif;

                break;

            case 'october':

                $replac_month = str_replace($datearr[1], 'October', $orgidate);

                if (strtotime($replac_month) >= $current_time):

                    $strotime = strtotime($replac_month);

                endif;

                break;

            case 'november':

                $replac_month = str_replace($datearr[1], 'November', $orgidate);

                if (strtotime($replac_month) >= $current_time):

                    $strotime = strtotime($replac_month);

                endif;

                break;

            case 'december':

                $replac_month = str_replace($datearr[1], 'December', $orgidate);

                if (strtotime($replac_month) >= $current_time):

                    $strotime = strtotime($replac_month);

                endif;

                break;

        endswitch;

        return $strotime;

}

add_filter( 'redirection_role', 'redirection_to_functioneel_beheerders' );

function redirection_to_functioneel_beheerders() {

    return 'edit_pages';

}
// Add number of product in cart
function my_wc_cart_count() {
 
    if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
 
        $count = WC()->cart->cart_contents_count;
        ?><a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php 
        if ( $count > 0 ) {
            ?>
            <span class="cart-contents-count"><?php echo esc_html( $count ); ?></span>
            <?php
        } else {  echo "0"; }
                ?></a><?php
    }
 
}
add_action( 'stimulanz_ajaxify_minicart', 'my_wc_cart_count' );
function my_header_add_to_cart_fragment( $fragments ) {
 
    ob_start();
    $count = WC()->cart->cart_contents_count;
    ?><a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php _e( '0');
    if ( $count > 0 ) {
        ?>
        <span class="cart-contents-count"><?php echo esc_html( $count ); ?></span>
        <?php            
    }
        ?></a><?php
 
    $fragments['a.cart-contents'] = ob_get_clean();
     
    return $fragments;
}
add_filter( 'woocommerce_add_to_cart_fragments', 'my_header_add_to_cart_fragment' );

// Remove update notifications
function remove_update_notifications( $value ) {

    if ( isset( $value ) && is_object( $value ) ) {
        unset( $value->response[ 'added-to-cart-popup-woocommerce/xoo-cp-main.php' ] );
    }

    return $value;
}
add_filter( 'site_transient_update_plugins', 'remove_update_notifications' );