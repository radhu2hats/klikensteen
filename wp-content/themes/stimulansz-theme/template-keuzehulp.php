<?php
/**
 * Template Name: Keuzehulp Template
 */
$training_werkveld = get_terms('training_werkveld',array('fields'=>'all', 'parent'=>0, 'hide_empty' => true, 'number' => 8));
$leerdoel = get_terms('leerdoel',array('fields'=>'all', 'parent'=>0, 'hide_empty' => false, 'number' => 8));
$training_functie = get_terms('training_functie',array('fields'=>'all', 'parent'=>0, 'hide_empty' => true, 'number' => 8));
$werkervaring_niveau = get_terms('werkervaring_niveau',array('fields'=>'all', 'parent'=>0, 'hide_empty' => false, 'number' => 8));

?>

<div class="page-template">
  <?php echo do_shortcode('[custom-form-with-mailplus]'); ?>  
  <?php the_content(); ?>
</div>