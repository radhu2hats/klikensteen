<?php

/**
 * Template Name: Keuzehulp Result Page Template
 */
$site_url = site_url();
$werkervaring = $leerdoel = $werkveld = $huidige = array();

if (isset($_GET['werkervaring'])) {
    $werkervaring = explode(',', $_GET['werkervaring']);
}
if (isset($_GET['leerdoel'])) {
    $leerdoel = explode(',', $_GET['leerdoel']);
}
if (isset($_GET['werkveld'])) {
    $werkveld = explode(',', $_GET['werkveld']);
}
if (isset($_GET['huidige'])) {
    $huidige = explode(',', $_GET['huidige']);
}
if (isset($_GET['voornaam'])) {
    $url_query['voornaam'] = $_GET['voornaam'];
}
if (isset($_GET['tussenv'])) {
    $url_query['tussenv'] = $_GET['tussenv'];
}
if (isset($_GET['achternaam'])) {
    $url_query['achternaam'] = $_GET['achternaam'];
}
if (isset($_GET['email'])) {
    $url_query['email'] = $_GET['email'];
}
$help_page = get_permalink(get_page_by_path('keuzehulp')) . "?" . http_build_query($url_query);
?>

<link rel='stylesheet' id='js_composer' href='<?php echo $site_url; ?>/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=6.0.5' type='text/css' media='all' />
<link rel='stylesheet' id='vc_tta_style-css' href='<?php echo $site_url; ?>/wp-content/plugins/js_composer/assets/css/js_composer_tta.min.css?ver=6.0.5' type='text/css' media='all' />
<script type='text/javascript' src='<?php echo $site_url; ?>/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js?ver=6.0.5'></script>
<script type='text/javascript' src='<?php echo $site_url; ?>/wp-content/plugins/js_composer/assets/lib/vc_accordion/vc-accordion.min.js?ver=6.0.5'></script>
<script type='text/javascript' src='<?php echo $site_url; ?>/wp-content/plugins/js_composer/assets/lib/vc-tta-autoplay/vc-tta-autoplay.min.js?ver=6.0.5'></script>
<div class="page-template">
    <?php
    if (!empty($leerdoel)) {
        $tax_query[] = array(
            'taxonomy' => 'leerdoel',
            'field' => 'slug',
            'terms' => $leerdoel,
            'operator' => 'IN'
        );
    }

    if (!empty($werkervaring)) {
        $tax_query[] = array(
            'taxonomy' => 'werkervaring_niveau',
            'field' => 'slug',
            'terms' => $werkervaring,
            'operator' => 'IN'
        );
    }

    if (!empty($huidige)) {
        $tax_query[] = array(
            'taxonomy' => 'training_functie',
            'field' => 'slug',
            'terms' => $huidige,
            'operator' => 'IN'
        );
    }

    if ($werkveld) {
        $tax_query[] = array(
            'taxonomy' => 'training_werkveld',
            'field' => 'slug',
            'terms' => $werkveld,
            'operator' => 'IN'
        );
    }

    $args = array(
        'post_type' => 'training',
        'paged' => 1,
        'posts_per_page' => 8,
        'post_status' => 'publish',
        'orderby' => 'menu_order',
        'order' => 'DESC',
        'tax_query' => $tax_query
    );
    $training =  get_posts($args);
    ?>
    <section class="section workfield-wrapper blue top-section-block help-result-outer">

        <header>
            <div class="container-fluid site-header-wrapper">
                <div class="header-elements-outer">
                    <div class="logo-icon"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/logo_icon.svg" alt="mobi logo"></a></div>
                    <div class="logo-textblk"><i class="location-icon"></i> Keuzehulp</div>
                    <div class="linkt-block"><a href="<?php echo get_home_url(); ?>">Naar stimulansz.nl<i></i></a></div>
                </div>
            </div>
        </header>


        <div class="vc_row wpb_row vc_row-fluid container">
            <div class="wpb_column vc_column_container vc_col-sm-9" id="content-mainouterblock">

                <div class="section results-introblock training-intro training-cat-intro space-bottom-100 margin-sectionbottom">
                    <div class="vc_row wpb_row vc_row-fluid container">
                        <div class="wpb_column vc_column_container vc_col-sm-8">
                            <div class="left-block">
                                <h1 class="result-page-title"><?php echo get_field('result_page_header') ?></h1>
                                <div class="intro-msg block">
                                    <h4><?php echo get_field('result_page_text') ?> </h4>
                                </div>
                                <div class="vc_btn3-container btn-white vc_btn3-inline vc_custom_1616657900407" style="float: left;">
                                    <a style="background-color:#ffffff; color:#0087d7; font-family: 'Metropolis Semi';" class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-custom" onclick='scrollToResult("trainingData");' title="Bekijk aanbevolen trainingen">Bekijk aanbevolen trainingen</a>
                                </div>
                                <a href="<?php echo $help_page ?>" class="reload-btn-block"><i class="light-refresh"></i>Keuzehulp opnieuw doen</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="wpb_column vc_column_container vc_col-sm-12 padding-left-60">

                    <div class="wpb_column vc_column_container vc_col-sm-12 filter-search-result" id="trainingData">
                        <h2>Aanbevolen trainingen voor u</h2>
                        <?php $training_count =  count($training); ?>
                        <?php if ($training_count < 1) : ?>
                            <h4 class="text-left ms-1">Geen trainingen gevonden voor de selectie</h4>
                        <?php else : ?>
                            <div class="vc_row wpb_row vc_row-fluid listing-blocks-outer   post-repeater">
                                <?php
                                $count = 1;
                                foreach ($training as $query) :
                                    $image = wp_get_attachment_url(get_post_thumbnail_id($query->ID), 'thumbnail');
                                    // $image = get_field('background_image', $query->ID);
                                ?>

                                    <?php if ($count == 1) : ?>
                                        <div class="wpb_column vc_column_container vc_col-sm-8 item">
                                            <div class="card card-block white_bg_block post-list-item wide featured-result">
                                                <a href="<?php echo get_permalink($query->ID); ?>">
                                                    <div class="card-inner">
                                                        <div class="img-wrap large-bllk">
                                                            <img src="<?php echo $image;
                                                                        ?>" alt="<?php echo $query->post_title
                                                                                    ?>">
                                                            <!-- <img src="<?php //echo $image['sizes']['stimulansz_card_wide']; 
                                                                            ?>" /> -->
                                                        </div>
                                                        <div class="cnt-wrap">
                                                            <div class="title_wrap">
                                                                <div class="cat">
                                                                    <?php
                                                                    $selectedTerms = [];
                                                                    foreach (wp_get_post_terms($query->ID, 'training_cat') as $term) {
                                                                        $selectedTerms[] = $term->name;
                                                                    }
                                                                    echo implode(', ', $selectedTerms);
                                                                    ?>
                                                                </div>
                                                                <div class="title"><?php echo $query->post_title ?></div>
                                                                <p><?php echo $query->post_excerpt ?></p>
                                                            </div>
                                                            <div class="sub_title_wrap">
                                                                <div class="sub_title sub_title_1"><i class="far fa-clock"></i> <?php echo get_field('duration', $query->ID) ?></div>
                                                                <span class="readmore" tabindex="0"> <i class="fas fa-arrow-right">&nbsp;</i> </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>

                                    <?php else : ?>

                                        <div class="wpb_column vc_column_container vc_col-sm-4 item">
                                            <div class="card card-block white_bg_block post-list-item">
                                                <a href="<?php echo get_permalink($query->ID); ?>">
                                                    <div class="card-inner">
                                                        <div class="img-wrap">
                                                            <img src="<?php echo $image;
                                                                        ?>" alt="<?php echo $query->post_title
                                                                                    ?>">
                                                            <!-- <img src="<?php //echo $image['sizes']['stimulansz_card_image']; 
                                                                            ?>" /> -->
                                                        </div>
                                                        <div class="cnt-wrap">
                                                            <div class="title_wrap">
                                                                <div class="cat">
                                                                    <?php
                                                                    $selectedTerms = [];
                                                                    foreach (wp_get_post_terms($query->ID, 'training_cat') as $term) {
                                                                        $selectedTerms[] = $term->name;
                                                                    }
                                                                    echo implode(', ', $selectedTerms);
                                                                    ?>
                                                                </div>
                                                                <div class="title"><?php echo $query->post_title ?></div>
                                                                <p><?php echo $query->post_excerpt; ?></p>
                                                            </div>
                                                            <div class="sub_title_wrap">
                                                                <div class="sub_title sub_title_1"><i class="far fa-clock"></i> <?php echo get_field('duration', $query->ID) ?></div>
                                                                <span class="readmore" tabindex="0"> <i class="fas fa-arrow-right">&nbsp;</i> </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                            
                        <?php endif; ?>
                        <?php
                                    $count++;
                                endforeach;
                    ?>
                  
                    </div>
                <?php endif; ?>
                <div class="helpbottom-btn-outercont">
                    <?php if ($training_count) { ?>
                        <div class="vc_btn3-container btn-white vc_btn3-inline vc_custom_1616657900407">
                            <?php
                            $training_overview_link = get_permalink(get_page_by_path('training-overview'));

                            ?>
                            <a style="background-color:#ffffff; color:#0087d7 !important; font-family: 'Metropolis Semi';" class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-custom" href="<?php echo $training_overview_link ?>" target="_self" title="">Bekijk alle trainingen</a>
                        </div>
                    <?php } ?>
                    <a href="<?php echo $help_page ?>" class="reload-btn-block" style="color: #0087d7 !important;"><i class="blue-refresh"></i>Keuzehulp opnieuw doen</a>
                </div>


                </div>
            </div>
        </div>

        <div class="wpb_column vc_column_container vc_col-sm-3 sticky-sidebar" style="position: sticky; top:180px;">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <section class="vc_cta3-container" id="sticky-content">
                        <div class="vc_general vc_cta3 blue-cta overflow-visible small-cta vc_cta3-style-classic vc_cta3-shape-rounded vc_cta3-align-left vc_cta3-color-classic vc_cta3-icon-size-md vc_cta3-actions-bottom">
                            <div class="vc_cta3_content-container">
                                <div class="vc_cta3-content">

                                    <div class="avatar-wrap img-blockouter">
                                        <img src="https://sttest.klikensteen.nl/wp-content/uploads/2017/09/Evelien-0013.jpg" alt="img" style="max-width : 200px">
                                    </div>
                                    <header class="vc_cta3-content-header">
                                        <h3 id="vragen-of-advies-nodig"><span style="vertical-align: inherit;"><?php echo get_field('blue_sticky_block_header') ?></span></h3>
                                    </header>
                                    <p><?php echo get_field('blue_sticky_block_text') ?></p>
                                    <footer class="vc_cta3-content-footer vc_cta3-content-center">
                                        <span>
                                            <a href="#" class="phone"><?php echo get_field('blue_sticky_block_telephone') ?></a>
                                            <a href="#" class="email"><?php echo get_field('blue_sticky_block_mail') ?></a>
                                        </span>
                                    </footer>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>


</div>

</section>

</div>
<script>
    function scrollToResult(element) {
        document.getElementById(element).scrollIntoView({
            behavior: 'smooth'
        });
    }
</script>