jQuery('.slider-innermaincont').append(' <div class="progress" role="progressbar" aria-valuemin="0" aria-valuemax="100"><span  class="slider__label sr-only"></span> </div>   ');
/*  Slider script*/
var $slider = jQuery('.slider-innermaincont ul');
var $progressBar = jQuery('.progress');
var $progressBarLabel = jQuery('.slider__label');

$slider.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
    var calc = ((nextSlide) / (slick.slideCount - 1)) * 100;
    $progressBar
        .css('background-size', calc + '% 100%')
        .attr('aria-valuenow', calc);
    $progressBarLabel.text(calc + '% completed');
});

// jQuery('.keuzehulp-slider').slick({
//     draggable: false,
//     swipeToSlide: false,
//     adaptiveHeight: true
// });
console.log("1");
// jQuery('.box-repeater-items').slick({
//     infinite: false,
//     slidesToShow: 4,
//     slidesToScroll: 1,
//     speed: 400,
//     responsive: [{
//             breakpoint: 1200,
//             settings: {
//                 slidesToShow: 3
//             }
//         },
//         {
//             breakpoint: 992,
//             settings: {
//                 slidesToShow: 2
//             }
//         },

//         {
//             breakpoint: 768,
//             settings: {
//                 slidesToShow: 1
//             }
//         }
//     ]
// });
console.log("2");
// $slider.slick({ 
//     infinite: false,
//     slidesToShow: 6,
//     variableWidth: true,
//     slidesToScroll: 1,
//     speed: 400,
//     responsive: [{
//             breakpoint: 1200,
//             settings: {
//                 slidesToShow: 3
//             }
//         },
//         {
//             breakpoint: 992,
//             settings: {
//                 slidesToShow: 3
//             }
//         },

//         {
//             breakpoint: 768,
//             settings: {
//                 slidesToShow: 2
//             }
//         },

//         {
//             breakpoint: 480,
//             settings: {
//                 slidesToShow: 1
//             }
//         }
//     ]
// });
console.log("3");
/*  Slider script*/

shiftContent();

jQuery(window).resize(function() {
    shiftContent();
});

function shiftContent() {
    if (jQuery(window).width() <= 767) {
        jQuery('.sticky-sidebar').remove().insertAfter(jQuery('.training-intro.training-cat-intro'));
    } else {
        jQuery('.sticky-sidebar').remove().insertAfter(jQuery('#content-mainouterblock'));
    }
}

jQuery(function() {
    if (jQuery(window).width() < 1200) {
        jQuery(' li.mega-menu-item:has(ul)').append('<span class="mega-indicator-new" />');
        jQuery(".mobinavigation-blk .js-slideout-toggle").click(function() {
            jQuery(this).toggleClass("open");
            jQuery("body").toggleClass("mobilenav-activeblock");
            jQuery(".mega-menu-item-has-children").removeClass("mobilesubnav-active");
        });
        jQuery("li.mega-menu-item.mega-menu-item-has-children > span.mega-indicator-new").click(function(event) {
            event.preventDefault();
            jQuery(this).parent().addClass("mobilesubnav-active");
        });

        jQuery("li.mega-menu-item.back-to-menu a").click(function() {
            jQuery(this).parents(".mega-menu-item-has-children").removeClass("mobilesubnav-active");
        });
    }
});