! function(e) {
    var n = {
            common: {
                init: function() {
                    function e() {
                        if (jQuery(".banner_section .banner_image .banner_image_text").length && jQuery(window).width() < 1200 && !jQuery(".banner_section .banner_image_text_mobile").length) {
                            jQuery(".banner_image_text_mobile").html("");
                            var e = "";
                            if (e = jQuery(".banner_section .banner_image .banner_image_text").html(), jQuery(".banner_section .banner_image .author_name").length) {
                                var n = "";
                                n = jQuery(".banner_section .banner_image .author_name").html(), jQuery('<div class="banner_image_text_mobile">' + e + '<div class="author_name">' + n + "</div></div>").insertAfter(".banner_section .banner_image")
                            } else jQuery('<div class="banner_image_text_mobile">' + e + "</div>").insertAfter(".banner_section .banner_image")
                        }
                        if (jQuery(".resulted-grid").length && jQuery(".resulted-grid").find("ul.products").addClass("grid"), jQuery(".banner_section .banner_color .banner_equal").length) {
                            var t = jQuery(".banner_section .banner_color .banner_equal:first-child").height();
                            t > 183 ? jQuery(".banner_section .banner_color .banner_equal").css("height", t + "px") : jQuery(".banner_section .banner_color .banner_equal").css("height", "185px")
                        }
                        if (jQuery(".product_listing_section .products.grid .training_block .white_bg_block").matchHeight(), jQuery(".choose_training_area .training_block .white_bg_block").matchHeight(), jQuery(".stimulansz_experts_organization .white_bg_block").matchHeight(), jQuery(".stimulansz_experts_organization .white_bg_block h4").matchHeight(), jQuery(".stimulansz_experts_organization .white_bg_block .white_bg_block_container").matchHeight(), jQuery(window).width() > 767) {
                            var i = jQuery(".stimulansz_experts_organization .white_bg_block.padding-bottom .equal-block").height();
                            setTimeout(function() { jQuery(".stimulansz_experts_organization .white_bg_block.padding-bottom .equal-block").css("height", i + 18 + "px") }, 1e3)
                        }
                    }

                    function n() {
                        if (jQuery(window).width() < 1200) {
                            if (jQuery("#panel").length || jQuery("#masterhead").nextUntil("#masterfooter").next("#masterfooter").andSelf().wrapAll('<main id="panel" class="panel slideout-panel"></main>'), !jQuery(".mobile-nav").length) {
                                var e = "";
                                e = jQuery("#menu-header-menu-bar").html() + jQuery("#top_menu_header_wrapper").html(), jQuery(jQuery("#menu")).append("<ul class='mobile-nav'>" + e + "</ul>")
                            }
                            var n = new Slideout({ panel: document.getElementById("panel"), menu: document.getElementById("menu"), side: "right" });
                            document.querySelector(".js-slideout-toggle").addEventListener("click", function() { jQuery(this).toggleClass("open"), n.toggle() }), document.querySelector(".menu").addEventListener("click", function() { jQuery(".js-slideout-toggle").toggleClass("open"), n.close() })
                        } else {
                            var t = jQuery("#panel").contents();
                            jQuery("#panel").replaceWith(t)
                        }
                    }
                    jQuery("#commentform").validate({ rules: { rating: { required: !0, number: !0, max: 10 } }, messages: { rating: { required: "Please enter Review rating.", number: "Please enter only number value", max: "You can give your review point between 1 to 10." } }, submitHandler: function(e) { e.submit() } }), jQuery(".grid").isotope({ itemSelector: ".grid-item" }), e(), n(), jQuery(window).load(function() { e() }), jQuery(window).resize(function() { e() }), jQuery(document).on("wpsolr_on_ajax_success", function() { jQuery(".choose_training_area .training_block .white_bg_block").matchHeight({ remove: !0 }), jQuery(".choose_training_area .training_block .white_bg_block").matchHeight() })
                },
                finalize: function() {}
            },
            home: { init: function() {}, finalize: function() {} },
            about_us: { init: function() {} }
        },
        t = {
            fire: function(e, t, i) {
                var r, a = n;
                t = void 0 === t ? "init" : t, r = "" !== e, r = r && a[e], r = r && "function" == typeof a[e][t], r && a[e][t](i)
            },
            loadEvents: function() { t.fire("common"), e.each(document.body.className.replace(/-/g, "_").split(/\s+/), function(e, n) { t.fire(n), t.fire(n, "finalize") }), t.fire("common", "finalize") }
        };
    e(document).ready(t.loadEvents)
}(jQuery);
jQuery(document).ready(function($) {
    /* PS++ 04032018 - submenu design, start */


    jQuery(".mobile-nav li.menu-item-has-children > a").not(".mobile-nav li ul li > a").on("click", function(e) {
        e.stopPropagation();
        e.preventDefault();
        jQuery(this).siblings('ul').slideToggle();
    });
    /* PS++ 04032018 - submenu design, end */

    /* PS++ 18062018 - login popup open/close, start */
    jQuery("#login-popup .close-login-popup").on("click", function() {
        jQuery("#login-popup").fadeOut();
    });
    jQuery("#masterhead .user-status a").on("click", function(e) {
        e.preventDefault();
        jQuery("#login-popup").fadeIn();
    });
    jQuery("#masterhead .order-icon .logout-icon a, #masterhead .user-status .address-icon a, #masterhead .user-status .logout-icon a").on("click", function(e) {
        e.stopPropagation();
        window.open(jQuery(this).attr("href"), '_self');
    });
    /* PS++ 18062018 - login popup open/close, end */

    // kopieer email
    jQuery('.copyboard').on('click', function(e) {
        e.preventDefault();

        var copyText = jQuery(this).attr('data-text');
        jQuery(this).attr('title', 'Gekopieerd');
        var textarea = document.createElement("textarea");
        textarea.textContent = copyText;
        textarea.style.position = "fixed"; // Prevent scrolling to bottom of page in MS Edge.
        document.body.appendChild(textarea);
        textarea.select();
        document.execCommand("copy");
        document.body.removeChild(textarea);
        //console.log(copyText);
    });

    // help page input 
    function checkForInput(element) {



        if (jQuery(element).val() != "") {
            jQuery(element).parent().parent().find('label').addClass('activeInput');
        } else {
            jQuery(element).parent().parent().find('label').remove('activeInput');
        }
    }


    jQuery('input.help-form-input').each(function() {
        checkForInput(this);
    });

    jQuery('form[class="main-class-stz-outer"]').find('input[type=text]').focusin(function() {

        jQuery(this).parent().parent().find('label').addClass('activeInput');

    });

    jQuery('form[class="main-class-stz-outer"]').find('textarea').focusin(function() {

        jQuery(this).parent().parent().find('label').addClass('activeInput');

    });


    jQuery('form[class="main-class-stz-outer"]').find('input[type=text]').focusout(function() {
        if (!this.value) {
            jQuery(this).parent().parent().find('label').removeClass('activeInput');
        }


    });
    jQuery('form[class="main-class-stz-outer"]').find('textarea').focusout(function() {
        if (!this.value) {
            jQuery(this).parent().parent().find('label').removeClass('activeInput');
        }
    });



    jQuery('.sticky-nav ul li a').on('click', function(e) {
        var href = jQuery(this).attr('href');
        jQuery('html, body').animate({
            scrollTop: jQuery(href).offset().top - 100
        }, '300');
        jQuery('.active-tab').removeClass('active-tab');
        jQuery(this).addClass('active-tab');
        e.preventDefault();
    });

    jQuery('.navigate-btn').on('click', function(e) {
        var href = jQuery(this).attr('href');
        jQuery('html, body').animate({
            scrollTop: jQuery(href).offset().top - 200
        }, '300');
    });

    jQuery('#experience_btn').on('click', function(e) {
        var href = jQuery(this).attr('href');
        jQuery('html, body').animate({
            scrollTop: jQuery(href).offset().top - 200
        }, '300');
    });


    jQuery('body').on('click', '.page-numbers li', function() {
        jQuery('html, body').animate({ scrollTop: jQuery(".filter-wrapper").offset().top - 200 }, 'slow');

    });



});


jQuery('#lees_meer_link').on('click', function(e) {
    e.preventDefault();

    jQuery("#lees_meer_expand.hide-div").removeClass('hide-div');
    jQuery(this).hide();
});


jQuery("body").on('hover', '.mega-menu-item-has-children,.mega-icons-linkouter, .mega-sub-menu li .mega-menu-link, .mega-sub-menu-subtitle', function() {
    jQuery("body").toggleClass("result-hover");
});

jQuery("body").on('hover', '.overlay-outerblock', function() {
    jQuery("body").removeClass("result-hover");
});


//read more button class


jQuery('.read-more-btn').click(function() {
    if (jQuery('.about-details-left').hasClass('lees-meer-class')) {
        jQuery('.about-details-left').removeClass('lees-meer-class');
    } else {
        jQuery('.about-details-left .lees-meer-class').removeClass('lees-meer-class');
        jQuery('.about-details-left').addClass('lees-meer-class');
    }
});

// multiselect
jQuery('#leerdoel').multiSelect({
    'containerHTML': '<div class="multi-select-container">',
    'menuHTML': '<div class="multi-select-menu">',
    'menuItemsHTML': '<div class="multi-select-menuitems">',
    'menuItemHTML': '<label class="multi-select-menuitem">',
    'presetsHTML': '<div class="multi-select-presets">',
    'noneText': "Selecteer Leerdoel",

    // Active CSS class
    'activeClass': 'multi-select-container--open',
});
jQuery('#werkervaring_niveau').multiSelect({
    'containerHTML': '<div class="multi-select-container">',
    'menuHTML': '<div class="multi-select-menu">',
    'menuItemsHTML': '<div class="multi-select-menuitems">',
    'menuItemHTML': '<label class="multi-select-menuitem">',
    'presetsHTML': '<div class="multi-select-presets">',
    'noneText': "Selecteer Werkervaring",


    // Active CSS class
    'activeClass': 'multi-select-container--open',
});


jQuery('#uitvoering').multiSelect({
    'containerHTML': '<div class="multi-select-container">',
    'menuHTML': '<div class="multi-select-menu">',
    'menuItemsHTML': '<div class="multi-select-menuitems">',
    'menuItemHTML': '<label class="multi-select-menuitem">',
    'presetsHTML': '<div class="multi-select-presets">',
    'noneText': "Selecteer Uitvoering",

    // Active CSS class
    'activeClass': 'multi-select-container--open',
});


// Get the modal
var modal = document.getElementById("newsLetterModal");
var btn = document.getElementById("news_letter_popup");
var span = document.getElementsByClassName("close")[0];
btn.onclick = function() {
    modal.style.display = "block";
}
span.onclick = function() {

    modal.style.display = "none";
}
jQuery('.close').on("click", function() {
    modal.style.display = "none";
});
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

jQuery('.copyboard').on({
    "click": function() {
        jQuery(this).tooltip({ items: ".copyboard", content: "Gekopieerd" });
        jQuery(this).tooltip("open");
    },
    "mouseout": function() {
        jQuery(this).tooltip("disable");
    }
});



jQuery(document).on('change', '#training_cat', function(e) {
    loadTrainingData(parseInt(currentPage));
    updateSelectedOptions();
    jQuery("#trainingData").html("Loading...");
    e.preventDefault();
});

jQuery('#faq_btn').on('click', function(e) {
    e.preventDefault();

    jQuery(".faq-div.hide-div").removeClass('hide-div');
    jQuery(this).hide();
});


jQuery('#ervaringen_btn').on('click', function(e) {
    e.preventDefault();

    jQuery(".ervaringen-div.hide-ervaringen").removeClass('hide-ervaringen');
    jQuery(this).hide();
});


jQuery(window).scroll(function() {
    var windscroll = jQuery(window).scrollTop();
    // if (windscroll >= 100) {
    //     jQuery('.prefill-section').each(function(i) {
    //         if (jQuery(this).position().top <= windscroll - 766) {
    //             jQuery('.sticky-nav ul li a.active-tab').removeClass('active-tab');
    //             jQuery('.sticky-nav ul li a').eq(i).addClass('active-tab');
    //         }
    //     });

    // } else {

    //     jQuery('nav li.active-tab').removeClass('active');
    // }

    if (jQuery(this).scrollTop() > 0) {
        jQuery('.site-header').addClass('nav-stick');
    } else {
        jQuery('.site-header').removeClass('nav-stick');
    }
}).scroll();




let input_1 = document.querySelector("#input_4_21");
let input_2 = document.querySelector("#input_4_22");
let input_3 = document.querySelector("#input_4_23");
let input_4 = document.querySelector("#input_4_24");
let button = document.querySelector("#keuzehulp_btn");
let check_1 = document.querySelector("#choice_4_11_2");
if (button) {

    if (input_1.value === "" || input_3.value === "" || input_4.value === "" || !check_1.checked) {
        button.disabled = true;
    } else {
        button.disabled = false;
    }

    input_1.addEventListener("change", stateHandle);
    input_3.addEventListener("change", stateHandle);
    input_4.addEventListener("change", stateHandle);
    check_1.addEventListener("change", stateHandle);

    function stateHandle() {
        if (input_1.value === "" || input_3.value === "" || input_4.value === "" || !check_1.checked) {
            button.disabled = true;
        } else {
            button.disabled = false;
        }
    }

}
jQuery('#keuzehulp').find('input[type=text]').focusin(function() {

    jQuery(this).parent().parent().find('label').addClass('activeInput');


});


jQuery('#keuzehulp').find('input[type=text]').focusout(function() {
    if (!this.value) {
        jQuery(this).parent().parent().find('label').removeClass('activeInput');
    }


});
jQuery('#options_selected').click(function() {
    var checkboxes = jQuery('.filter-lists input:checkbox:checked').length;
    if (checkboxes > 0) {
        jQuery('#filter_count').html("(" + checkboxes + ")");
    } else {
        jQuery('#filter_count').html('');

    }
    loadLeerdoelMobileData(1);
    jQuery("body").removeClass("filter-open");
    jQuery('#display_filter_group').css('display', 'none');
    // updateMobileSelectedOptions();

});

function showDiv() {
    document.getElementById('display_filter_group').style.display = "block";
    jQuery("body").toggleClass("filter-open");

}


jQuery('#close_filter').click(function(e) {
    jQuery("body").removeClass("filter-open");
    jQuery('#display_filter_group').css('display', 'none');
});


jQuery('.clear-leerdoel-filter-btn').click(function() {
    jQuery('.leerdoel-mob-filter:checkbox').removeAttr('checked');
});
jQuery('.clear-uitvoering-filter-btn').click(function() {
    jQuery('.uitvoering-mob-filter:checkbox').removeAttr('checked');
});
jQuery('.clear-werkervaring-filter-btn').click(function() {
    jQuery('.werkervaring_niveau-mob-filter:checkbox').removeAttr('checked');
});


// $(".icons-linkouter .mega-menu-link").each(function(){
//     var span = jQuery("<span class='menu-bullet'></span>");
//     $(this).append(span);

//     var event_message = <?php the_field('werkveld_dot_color','jeugd'); ?>;
//     console.log(event_message);
// });