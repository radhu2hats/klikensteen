<?php
/*
Plugin Name: Visual Composer - Mailchimp by Webholics
Plugin URI: https://webholics.org
Description: Add Mailchimp signup forms from Visual Composer Editor.
Version: 5.0
Author: Webholics
Author URI:  https://webholics.org
 */

// don't load directly
if ( !defined( 'ABSPATH' ) ) die( '-1' );

define( "VC_MAILCHIMP_DIR", WP_PLUGIN_DIR . "/" . basename( dirname( __FILE__ ) ) );
define( "VC_MAILCHIMP_URL", plugins_url() . "/" . basename( dirname( __FILE__ ) ) );

//Add Admin class
require_once (VC_MAILCHIMP_DIR.'/lib/class-vc-mailchimp-admin.php');
add_action( 'plugins_loaded', array( 'VC_Mailchimp_Admin', 'setup' ) );

//Add Shortcode class
require_once (VC_MAILCHIMP_DIR.'/lib/class-vc-mailchimp-shortcode.php');
add_action( 'plugins_loaded', array( 'VC_Mailchimp_Shortcode', 'setup' ) );

// Functions File
require_once (VC_MAILCHIMP_DIR.'/lib/functions.php');


add_action( 'plugins_loaded', function(){
    add_filter( 'site_transient_update_plugins', function ( $value )
    {
        if( isset( $value->response['vc-mailchimp/vc-mailchimp.php'] ) )
            unset( $value->response['vc-mailchimp/vc-mailchimp.php'] );
        return $value;
    });
});

