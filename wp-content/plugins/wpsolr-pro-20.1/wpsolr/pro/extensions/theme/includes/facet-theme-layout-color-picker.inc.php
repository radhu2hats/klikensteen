<?php

use wpsolr\pro\extensions\theme\layout\color_picker\WPSOLR_UI_Layout_Color_Picker;

?>

<div style="display:none"
     class="wpsolr-remove-if-hidden wpsolr_facet_type <?php echo WPSOLR_UI_Layout_Color_Picker::CHILD_LAYOUT_ID; ?>">

    <p>
        The color picker will show the content of this facet as a set of colored icons.<br>
        Use the localizations button to associate a color to your facet contents. For instance 'white' content
        associated to #ffffff.
    </p>

</div>
