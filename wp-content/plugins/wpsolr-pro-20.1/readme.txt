=== WPSOLR PRO ===

Contributors: wpsolr

Author: wpsolr

Current Version: 20.1

Author URI: https://www.wpsolr.com/

Tags: search, TablePress search, Solr in WordPress, wordpress search, bbPress search, WooCommerce search, ACF search, coupon search, affiliate feed search, relevance, Solr search, fast search, wpsolr, apache solr, better search, site search, category search, search bar, comment search, filtering, relevant search, custom search, filters, page search, autocomplete, post search, online search, search, spell checking, search integration, did you mean, typeahead, search replacement, suggestions, search results, search by category, multi language, seo, lucene, solr, suggest, apache lucene

Requires at least: 3.7.1

Tested up to: 4.8.2

Stable tag: 20.1

License: GPLv2 or later

License URI: http://www.gnu.org/licenses/gpl-2.0.html


Search faster. When your Wordpress search fails, when your WooCommerce search or bbPress search gets stuck, you need a change of search technology.

== Description ==

Replace your Wordpress or WooCommerce search with Apache Solr and Elasticsearch.

Test WPSOLR features on <a href="https://www.wpsolr.com/?s=WOOCOMMERCE+OR+ACF+OR+BBPRESS+OR+WPML+OR+POLYLANG+OR+GROUPS+OR+s2MEMBER&post_type=&camp=3">our documentation search page</a>: live suggestions, facets with acf, geolocation.

You definitely need WPSOLR PRO search if you agree with one of:

- My current search page, my instant (live) product suggestions, are so slow that my visitors are leaving without buying anything, without subscribing to anything

- I have too many posts, products, visitors, comments, and I cannot afford hundred of dollars on external search hosted services

- Most of my data is stored in pdf files, word files, excel files. I need to search these formats too.

- My customers are international, they speak different languages. My search should be multilingual also.

- I want a modern search with tons of features. Ajax, facets, partial match search, fuzzy match search.

- I want to filter my woocommerce search results with any taxonomies, custom fields, attributes, or variations.

- I have several sites, unrelated, but I want to give my visitors a single search page combining all their content

- My bbPress search cannot handle thousands, hundreds of thousands of topics and replies.


If not, there are plenty of great search plugins out there to help you.

But, if you're really ready to unleash the beast, visit <a href='https://www.wpsolr.com?camp=2'>wpsolr.com</a>, ask us any question, or just download WPSOLR PRO search to give it a try.


== Installation ==

1. Upload the wpsolr-pro folder to the /wp-content/plugins/ directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Activate the license for your website: https://www.wpsolr.com/knowledgebase/how-to-activate-a-license-pack/.

Installation procedure for Apache Solr: see FAQ section.

== Changelog ==

= 20.0 =
* Fix index creation with Elasticsearch 6.x: "[include_in_all] is not allowed for indices created on or after version 6.0.0 as [_all] is deprecated"
* Add field type text "_t" to store text length > 32K. Fix error: Document contains at least one immense term in field=xxx (whose UTF8 encoding is longer than the max length 32766)
* Fix index creation after upgrading from WPSOLR (free) : "Cannot use string offset as an array in /wp-content/plugins/wpsolr-pro/wpsolr/core/classes/extensions/indexes/admin_options.inc.php:69"

= 19.9 =
* Security fix
* New Range Slider filter with <a href="http://ionden.com/a/plugins/ion.rangeSlider/en.html">ion.rangeSlider javascript library</a>
* New Select box filter with <a href="https://select2.org/">Select2 javascript library</a>
* New Select box filter
* Remove SQL syntax error in debug.log when uploading attachment types not selected in screen 2.2
* Fix filters/facets not responding with IE11

= 19.8 =
* New ACF field types are supported: layout (tab, clone).
* Fix metabox "Do not search" checkbox not removing posts from the index.
* Prevent real-time indexing of post types not selected in screen 2.2
* New WP action to activate/deactivate real-time indexing. Much faster to import data, and use cron indexing, without the real-time. Ex: add_action(WPSOLR_Events::WPSOLR_ACTION_OPTION_SET_REALTIME_INDEXING, true ).

= 19.7 =
* WooCommerce extension: fix sorting results.

= 19.6 =
* New extension to manage external crons.
* Fix an indexing error due to a watermark format change in recent versions
* Remove Listify extension dependency to PHP 7
* Tested with Solr 7.0.0
* Tested with Elasticsearch 5.6.2

= 19.5 =
* New extension for the theme <a href="https://themeforest.net/item/listify-wordpress-directory-theme/9602611">Listify - WordPress Directory Theme</a>. Speed up listing search, category filter, label filter, geolocation radius filter, with WPSOLR.
* 123 tests, 7218 assertions (phpunit + Selenium2)

= 19.4 =
* New extension for the free plugin <a href="https://wordpress.org/plugins/yith-woocommerce-ajax-search/">YITH WooCommerce Ajax Search</a>. Use WPSOLR search to retrieve the products shown in the plugin search box suggestions.
* 119 tests, 6872 assertions (phpunit + Selenium2)

= 19.3 =
* Compatibility with WPML Multilingual CMS 3.7.1 and WPML String Translation 2.5.4 (You might need to upgrade WPML)
* Compatibility with Polylang 2.2.1 (You might need to upgrade Polylang)
* Fix facet hierarchies bad display when the option Theme extension's "Collapse facet hierarchies" is off
* 117 tests, 6780 assertions (phpunit + Selenium2)

= 19.2 =
* Add parameters "shards" and "replicas" to the Elasticsearch indexes form
* 113 tests, 5766 assertions (phpunit + Selenium2)

= 19.1 =
* Fully automated SolrCloud indexes creation (no need to access SolrCloud admin UI or server filesystem)
* Semi-automated Solr indexes creation (detailed instructions with server filesystem commands to add configuration files)

= 19.0 =
* (Elasticsearch attachments) Use of https://www.elastic.co/guide/en/elasticsearch/plugins/current/ingest-attachment.html, instead of deprecated https://www.elastic.co/guide/en/elasticsearch/plugins/current/mapper-attachments.html
* Fix non clickable facets containing single quotes
* Tests: 101, Assertions: 5211 (phpunit + Selenium2)

= 18.8 =
* Test Elasticsearch indexes. With a push on a button, create a hosted Elasticsearch index, ready to use with your search.

= 18.7 =
* Fix to work with Elasticsearch 5.5.1.
* Upgrade Elastica library from 5.2.1 to 5.3.0.
* Remove automatically file Null.php from Elastica library, which caused PHP 7 sniffers fail.

= 18.6 =
* Fix Solr indexing errors when some post data contains control characters.

= 18.5 =
* New "Yoast SEO" extension - Build beautiful search urls with meta descriptions
* New "All In One SEO Pack" extension - Build beautiful search urls with meta descriptions
* New Advanced scoring extension (Elasticsearch only) - Boost recent results while keeping relevancy
* New import/export settings
* Improve the creation of a test Solr index inside the plugin
* Add sort on multi-value fields (requires downloading the new Solr schema, then restart Solr or reload the index)
* s2Member extension is now compatible with Elasticsearch
* Fix warnings on upgrade version preview
* Verify 4000 checkpoints with automatic user acceptance tests (phpunit + Selenium2).

= 18.3 =
* (WP All Import) First release of the "WP All Import" extension. Fix posts not deleted from the search engine index while deleted by import.
* Fix the auto-update when called by wp-cli.

= 18.2 =
* (SEO extension) In preparation. Generate beautiful SEO urls and metas for the facets.
* (Premium extension) Fix the Ajax Infinite scroll always showing the first page.
* (Premium extension) Display attachments in the current theme results.

= 18.1 =
* (Theme extension) New option to use the current theme with Ajax (screen 2.1).
* (Theme extension) New facet layout 'Color picker'. You can now filter results clicking on colored icons.
* (Theme extension) New grid selection (horizontal, 1 column, 2 columns) on facet layouts.
* (WooCommerce extension) Fix a front-end search error when the option "Replace orders search by wpsolr" is selected.
* (Premium extension) Fix the screen 2.4 (facets) freeze when a post type contains many terms.
* (Premium extension) Fix the partial match option when the search contains several keywords. For Apache Solr and Elasticsearch.
* (WPML extension) Fix taxonomies being indexed in the admin current WPML language, rather than the index WPML language.
* Fix Elasticsearch error on post deletion
* Fix Elasticsearch boosts
* Get attachments in the current theme results.

= 18.0 =
* (Theme extension) New ranges layout. Show numeric filters, like prices, as ranges.
* (Premium extension) New option to apply 'OR' on facets with multi-selection
* (Elasticsearch) Fix empty results for multi-word keywords
* Upgrade https://github.com/solariumphp/solarium from 3.4.1 to 3.8.1, to fix exclusion for interval facets
* Fix tab showing the index settings collapsed, preventing the creation of the test Solr index.

= 17.9 =
* (Groups extension): prevent Solr error on groups with empty capabilities (capabilities removed from Groups but still there).
* Remove duplicated layout list on facets tab.
* Fix error while indexing post authors without display_name

= 17.7 =
* Fix tab showing the index settings collapsed, preventing the creation of the test Solr index.

= 17.6 =
* (Extension Toolset types) Add a checkbox to the wpsolr metabox. When a post contains a Toolset field of type "file", the file content is added to the post body (indexed and searched).

= 17.3 =
* First release of Elasticsearch:
 1. Install Elasticsearch
 2. Choose Elastic search for your index
 3. WPSOLR create your index, and setup mappings and analysers. No manual action required.
 4. Enjoy all wpsolr features: full-text search, sort, facets, autocomplete.
* Elasticsearch indexes work with all extensions, but "s2Member" and "Groups". Work in progress.
* Improve indexing debugging by catching and displaying fatal php errors.
* Fix "did you mean ?" for Apache Solr > 5.3

= 17.1 =
* Fix facets with html caracters (&, >, <, ...) returning 0 results.
* Fix silent error while indexing attachments > 500 KB (files too big where not indexed).

= 17.0 =
* Fix autocomplete.
* Fix facets not responding on clicks with the Ajax template.

= 16.9 =
* Fix wrong search engine while setting up an index (Elasticsearch is set instead of Apache Solr).

= 16.8 =
* Preparation for Elasticsearch in addition to Apache Solr.
* New layout choice on facets: checkboxes, or radio boxes.
* New exclusion choice on facets: display facets count as if no other selection was made.
* Requires PHP >= 5.4 (previously 5.3).

= 16.7 =
* Premium pack: new option to filter a facet content by default.
* Fix bug in admin screen while drag&dropping the sort items (front sort items where correctly displayed).

= 16.6 =
* Fix missing documents in index when many posts have the exact same published date (imports). Re-index everything if you are concerned.

= 16.4 =
* Fix php warnings in logs.

= 16.3 =
* Theme extension: new option to customize the facets css.

= 16.2 =
* WooCommerce extension : automatic filter on catalog visibility status. Needs a full re-index.
* Speed up loading of extensions.

= 16.1 =
* Premium extension: can now localize, and translate, all facet contents in tab 2.4 (facets). For instance, the WooCommerce stock status value 'instock' in the more readable 'In stock'.
* WooCommerce extension : prevent WooCommerce product attributes with type 'Text' split on commas during indexing.
* Front: Add compatibility with WooCommerce Customizer plugin's infinite scroll (auto loading). Tested live with 140K products.
* Admin: Add better visuals to indicate which extensions are active.

= 16.0 =
* WooCommerce extension: Replace shop search with WPSOLR search.

= 15.9 =
* (New) Theme extension: Facet hierarchies collapsing option.
* Premium extension: Sort facets alphabetically option (instead of default sort count).

= 15.8 =
* Fix error with special (Solr) characters in keywords.

= 15.7 =
* Fix the Ajax InfiniteScroll for Firefox.

= 15.6 =
* Add an option to replace the Infinite Scroll javascript.
<a href="https://www.wpsolr.com/knowledgebase/infinite-scroll-search-navigation/">Documentation</a>


= 15.5 =
* Add a new filter to replace the default facets HTML with your own. Works with the Ajax shortcode, and the facets widget.
<a href="https://www.wpsolr.com/guide/actions-and-filters/search-results-replace-facets-html/">Documentation</a>

= 15.4 =
* Add two css classes to the facets html (header and list), so each facet can be styled individually.

= 15.3 =
* (WooCommerce extension) Fix the product categories search to support multilingual subcategories slugs.

= 15.2 =
* Security update.

= 15.1 =
* Fix HTML of the Ajax search form, which could cause side effects to the theme's rendering.
* Fix PHP warning on admin menu "Plugins".

= 15.0 =
* First release of WPSOLR PRO:
- includes an external license manager for paid extensions
- includes it's own automatic update manager


== Frequently Asked Questions ==

= Is there a trial for the extensions ? =

Yes, we added a 7 days trial for all packs (Premium, bbPress, Woocommerce, WPML, Polylang, S2member, Groups, Types, ACF). Download WPSOLR PRO trial, then follow https://www.wpsolr.com/knowledgebase/how-to-activate-a-license-pack/.

= What is the installation procedure for Solr on Windows ? =

!!! Important: always reload the index in your Solr admin UI after each install/change of file schema.xml

A tutorial at WPSOLR: [Solr 4.x](http://wpsolr.com/installation-guide/ "Apache Solr installation, Solr 4.x")

A tutorial at Wordpress support: [Windows, Solr 5.x/6.x](https://wordpress.org/support/topic/great-software-but-needs-some-documentation "Apache Solr installation, Windows, Solr 5.x/6.x")

= What is the installation procedure for Solr on linux ? =

!!! Important: always reload the index in your Solr admin UI after each install/change of file schema.xml

A tutorial at Wordpress support: [Linux, Solr 4.x](https://wordpress.org/support/topic/no-support-for-self-hosted-solr-and-not-working-for-self-hosted "Apache Solr installation, Linux, Solr 4.x")

A tutorial at Linode: [Linux, Solr 4.x](https://www.linode.com/docs/websites/cms/turbocharge-wordpress-search-with-solr "Apache Solr installation, Linux, Solr 4.x")

For Linux, Solr 6.1.0 (tested). Replace 6.1.0 with your current Solr version.
`
wget http://archive.apache.org/dist/lucene/solr/6.1.0/solr-6.1.0.tgz
tar xvf solr-6.1.0.tgz
solr-6.1.0/bin/solr start
solr-6.1.0/bin/solr create -c wpsolr-6.1.0
(download solr 5.xx config files from https://www.wpsolr.com/kb/apache-solr/apache-solr-configuration-files)
cp solrconfig.xml schema.xml solr-6.1.0/server/solr/wpsolr-6.1.0/conf/
(reload index with solr admin UI)
(configure a new index in wpsolr admin UI:
Index name: wpsolr - local 6.1.0
Solr Protocol: http
Solr host: localhost
Solr port: 8983
Solr path: /solr/wpsolr-6.1.0
)
(index posts on wpsolr admin UI, including a pdf file)
(search in posts, retrieve the pdf)
`

= What WPSOLR PRO can do to help my search ? =
Relevanssi, Better Search, Search Everything, are really great because they do not need other external softwares or services to work.

WPSOLR, on the other hand, requires Apache Solr, the worlds's most popular search engine on the planet, to index and search your data.

If you can manage to install Solr (or to buy a hosting Solr service), WPSOLR can really help you to:

* Search in many sites for aggregated searches

* Search in thousands or millions of posts/products

* Search in attached files (pdf, word, excel....)

* Filter results with dynamic facets

* Tweak your search in many many ways with Solr solrconfig.cml and schema.xml files (language analysers, stopwords, synonyms, stemmers ...)

= Do you offer a premium version ? =
Yes. Check out our <a href="https://wpsolr.com/pricing">Premium Packs</a>.

= Can you search in several sites and show results on one site ? =
Yes, there is a multisites extension in WPSOLR PRO.

You configure the sites belonging to the network search as "local", and one or several "global" sites to show results from "local" sites consolidated, while "Local" sites continue to search their own data.

As Solr manages the whole network search, there is almost no limits to the number of "local" sites, and number of posts indexed.
Contact us for more information on this multisites feature.

= Can you manage millions of products/attributes/variations ? =
Yes (Premium for attributes/variations). WPSOLR PRO is based on the mighty Apache Solr search engine. It can easily manage millions of posts, and fast.

= Why the search page does not show up ? =
You have to select the admin option "Replace standard WP search", and verify that your urls permalinks are activated.

= Which PHP version is required ? =

WPSOLR uses a Solr client library, Solarium, which requires namespaces.

Namespaces are supported by PHP >= 5.3.0

= Which Apache Solr version is supported ? =

Solr 4.x, Solr 5.x, Solr 6.x

WPSOLR PRO was tested till Solr 6.1.0

= Can I have my Apache Solr server hosted ? =

Yes, on <a href='http://gotosolr.com/en/'>Gotosolr Solr hosting</a>.

[Gotosolr Solr hosting tutorial](http://www.gotosolr.com/en/solr-tutorial-for-wordpress/ "Gotosolr Solr hosting tutorial")

[sitepoint tutorial on Gotosolr Solr hosting with WPSOLR](https://www.sitepoint.com/enterprise-search-with-apache-solr-and-wordpress/ "sitepoint tutorial on Gotosolr Solr hosting with WPSOLR")

= How do I install and configure my own Apache Solr server ? =

Please refer to our detailed <a href='http://wpsolr.com/installation-guide/'>Installation Guide</a>.


= What version of Solr does the WPSOLR PRO plugin need? =

WPSOLR PRO plugin is <a href="https://www.wpsolr.com/kb/apache-solr/apache-solr-configuration-files"> compatible with the following Solr versions</a>. But if you were going with a new installation, we would recommend installing Solr version 3.6.x or above.


= Does WPSOLR PRO Plugin work with any version of WordPress? =

As of now, the WPSOLR PRO Plugin works with WordPress version 3.8 or above.


= Can custom post type, custom taxonomies and custom fields be added filtered search? =

Yes (Premium feature). The WPSOLR PRO plugin provides option in dashboard, to select custom post types, custom taxonomies and custom fields, to be added in filtered search.


= Do you offer support? =

You can raise a support question for our plugin from wordpress.org.
Premium users can use our zendesk support.
