<?php

use wpsolr\core\classes\engines\solarium\WPSOLR_IndexSolariumClient;
use wpsolr\core\classes\engines\solarium\WPSOLR_SearchSolariumClient;
use wpsolr\core\classes\engines\WPSOLR_AbstractEngineClient;
use wpsolr\core\classes\engines\WPSOLR_AbstractIndexClient;
use wpsolr\core\classes\engines\WPSOLR_AbstractSearchClient;
use wpsolr\core\classes\extensions\localization\OptionLocalization;
use wpsolr\core\classes\extensions\WpSolrExtensions;
use wpsolr\core\classes\hosting_api\WPSOLR_Hosting_Api_Abstract;
use wpsolr\core\classes\hosting_api\WPSOLR_Hosting_Api_None;
use wpsolr\core\classes\models\WPSOLR_Model_Post_Type;
use wpsolr\core\classes\services\WPSOLR_Service_Container;
use wpsolr\core\classes\ui\WPSOLR_Data_Facets;
use wpsolr\core\classes\ui\WPSOLR_Data_Sort;
use wpsolr\core\classes\ui\WPSOLR_UI_Facets;
use wpsolr\core\classes\ui\WPSOLR_UI_Sort;
use wpsolr\core\classes\utilities\WPSOLR_Option;
use wpsolr\core\classes\WPSOLR_Events;

// Load localization class
WpSolrExtensions::require_once_wpsolr_extension( WpSolrExtensions::OPTION_LOCALIZATION, true );
//WpSolrExtensions::load();


/**
 * @param $thedate
 *
 * @return mixed
 */
function solr_format_date( $thedate ) {
	$datere  = '/(\d{4}-\d{2}-\d{2})\s(\d{2}:\d{2}:\d{2})/';
	$replstr = '${1}T${2}Z';

	return preg_replace( $datere, $replstr, $thedate );
}
function stimulansz_getObj(){
	return WPSOLR_Service_Container::get_query();
}

function fun_search_indexed_data() {

	$ad_url = admin_url();

	// Retrieve search form page url
	$get_page_info = WPSOLR_SearchSolariumClient::get_search_page();
	$url           = get_permalink( $get_page_info->ID );
	// Filter the search page url. Used for multi-language search forms.
	$url = apply_filters( WPSOLR_Events::WPSOLR_FILTER_SEARCH_PAGE_URL, $url, $get_page_info->ID );

	// Load localization options
	$localization_options = OptionLocalization::get_options();

	$wdm_typehead_request_handler = WPSOLR_AJAX_AUTO_COMPLETE_ACTION;

	echo "<div class='cls_search' style='width:100%'> <form action='$url' method='get'  class='search-frm' >";
	echo '<input type="hidden" value="' . $wdm_typehead_request_handler . '" id="path_to_fold">';
	echo '<input type="hidden" value="' . $ad_url . '" id="path_to_admin">';
	echo '<input type="hidden" value="' . WPSOLR_Service_Container::get_query()->get_wpsolr_query( '', true ) . '" id="search_opt">';

	$ajax_nonce = wp_create_nonce( "nonce_for_autocomplete" );

	echo '<div class="ui-widget">';
	echo '<input type="hidden"  id="ajax_nonce" value="' . $ajax_nonce . '">';
	echo '<input type="text" placeholder="' . OptionLocalization::get_term( $localization_options, 'search_form_edit_placeholder' ) . '" value="';
	echo WPSOLR_Service_Container::get_query()->get_wpsolr_query( '', true ) . '" name="search" id="search_que" class="' . WPSOLR_Option::OPTION_SEARCH_SUGGEST_CLASS_DEFAULT;
	echo ' sfl2" autocomplete="off"/>';
	echo '<input type="submit" value="' . OptionLocalization::get_term( $localization_options, 'search_form_button_label' ) . '" id="searchsubmit" style="position:relative;width:auto">';
	echo '<input type="hidden" value="' . WPSOLR_Service_Container::getOption()->get_search_after_autocomplete_block_submit() . '" id="is_after_autocomplete_block_submit">';
	echo '<input type="hidden" value="' . WPSOLR_Service_Container::get_query()->get_wpsolr_paged() . '" id="paginate">';

	parse_str( parse_url( $url, PHP_URL_QUERY ), $url_params );
	if ( ! empty( $url_params ) && isset( $url_params['lang'] ) ) {
		echo '<input type="hidden" value="' . esc_attr( $url_params['lang'] ) . '" name="lang">';
	}

	// Filter to add fields to the search form
	echo apply_filters( WPSOLR_Events::WPSOLR_FILTER_APPEND_FIELDS_TO_AJAX_SEARCH_FORM, '' );

	echo '<div style="clear:both"></div></div></form>';


	echo '</div>';
	echo "<div class='cls_results'>";

	try {

		try {

			$final_result = WPSOLR_Service_Container::get_solr_client()->display_results( WPSOLR_Service_Container::get_query() );

		} catch ( Exception $e ) {

			$message = $e->getMessage();
			echo "<span class='infor'>$message</span>";
			die();
		}

		if ( $final_result[2] == 0 ) {
			echo "<span class='infor'>" . sprintf( OptionLocalization::get_term( $localization_options, 'results_header_no_results_found' ), WPSOLR_Service_Container::get_query()->get_wpsolr_query( '', true ) ) . "</span>";
		} else {
			echo '<div class="wdm_resultContainer">
			<div class="wdm_list">';

			// Display sort list UI
			echo WPSOLR_UI_Sort::build(
				WPSOLR_Data_Sort::get_data(
					WPSOLR_Service_Container::getOption()->get_sortby_items_as_array(),
					WPSOLR_Service_Container::getOption()->get_sortby_items_labels(),
					WPSOLR_Service_Container::get_query()->get_wpsolr_sort(),
					$localization_options
					)
				);


			// Display facets UI
			echo '<div id="res_facets">' . WPSOLR_UI_Facets::Build(
				WPSOLR_Data_Facets::get_data(
					WPSOLR_Service_Container::get_query()->get_filter_query_fields_group_by_name(),
					WPSOLR_Service_Container::getOption()->get_facets_to_display(),
					$final_result[1] ),
				$localization_options,
				WPSOLR_Service_Container::getOption()->get_facets_layouts_ids() ) . '</div>';


			echo '</div>
			<div class="wdm_results">';
			if ( $final_result[0] != '0' ) {
				echo $final_result[0];
			}

			$ui_result_rows = $final_result[3];
			if ( WPSOLR_Service_Container::getOption()->get_search_is_display_results_info() && $ui_result_rows != 0 ) {
				echo '<div class="res_info">' . $final_result[4] . '</div>';
			}

			if ( $ui_result_rows != 0 ) {
				$img = plugins_url( 'images/gif-load.gif', __FILE__ );
				echo '<div class="loading_res"><img src="' . $img . '"></div>';
				echo "<div class='results-by-facets'>";
				foreach ( $ui_result_rows as $resarr ) {
					echo $resarr;
				}
				echo "</div>";
				echo "<div class='paginate_div'>";
				$total         = $final_result[2];
				$number_of_res = WPSOLR_Service_Container::getOption()->get_search_max_nb_results_by_page();
				if ( $total > $number_of_res ) {
					$pages = ceil( $total / $number_of_res );
					echo '<ul id="pagination-flickr" class="wdm_ul">';
					for ( $k = 1; $k <= $pages; $k ++ ) {
						echo "<li ><a class='paginate' href='javascript:void(0)' id='$k'>$k</a></li>";
					}
				}
				echo '</ul></div>';

			}


			echo '</div>';
			echo '</div><div style="clear:both;"></div>';
		}
	} catch
	( Exception $e ) {

		echo sprintf( 'The search could not be performed. An error occured while trying to connect to the Apache Solr server. <br/><br/>%s<br/>', $e->getMessage() );
	}

	echo '</div>';
}

/**
 * Shortcode for search general data
 * Use short code in page or post
 */
function stimulansz_general_search_indexed_data() {
	
	$ad_url = admin_url();


	// Retrieve search form page url
	$get_page_info = WPSOLR_SearchSolariumClient::get_search_page();
	$url           = get_permalink( $get_page_info->ID );
	// Filter the search page url. Used for multi-language search forms.
	$url = apply_filters( WPSOLR_Events::WPSOLR_FILTER_SEARCH_PAGE_URL, $url, $get_page_info->ID );

	// Load localization options
	$localization_options = OptionLocalization::get_options();

	$wdm_typehead_request_handler = WPSOLR_AJAX_AUTO_COMPLETE_ACTION;

	echo "<div class='cls_search' style='width:100%; display:none'> <form action='$url' method='get'  class='search-frm' >";
	echo '<input type="hidden" value="' . $wdm_typehead_request_handler . '" id="path_to_fold">';
	echo '<input type="hidden" value="' . $ad_url . '" id="path_to_admin">';
	echo '<input type="hidden" value="' . WPSOLR_Service_Container::get_query()->get_wpsolr_query() . '" id="search_opt">';

	$ajax_nonce = wp_create_nonce( "nonce_for_autocomplete" );

	echo '<div class="ui-widget">';
	echo '<input type="hidden"  id="ajax_nonce" value="' . $ajax_nonce . '">';
	echo '<input type="text" placeholder="' . OptionLocalization::get_term( $localization_options, 'search_form_edit_placeholder' ) . '" value="';
	echo esc_attr( WPSOLR_Service_Container::get_query()->get_wpsolr_query() ) . '" name="search" id="search_que" class="' . WPSOLR_Option::OPTION_SEARCH_SUGGEST_CLASS_DEFAULT;
	echo ' sfl2" autocomplete="off"/>';
	echo '<input type="submit" value="' . OptionLocalization::get_term( $localization_options, 'search_form_button_label' ) . '" id="searchsubmit" style="position:relative;width:auto">';
	echo '<input type="hidden" value="' . WPSOLR_Service_Container::getOption()->get_search_after_autocomplete_block_submit() . '" id="is_after_autocomplete_block_submit">';
	echo '<input type="hidden" value="' . WPSOLR_Service_Container::get_query()->get_wpsolr_paged() . '" id="paginate">';
	// Filter to add fields to the search form
	echo apply_filters( WPSOLR_Events::WPSOLR_FILTER_APPEND_FIELDS_TO_AJAX_SEARCH_FORM, '' );

	echo '<div style="clear:both"></div></div></form>';


	echo '</div>';
	echo "<div class='cls_results'>";

	try {

		try {

			$final_result = WPSOLR_Service_Container::get_solr_client()->display_results( WPSOLR_Service_Container::get_query() );


		} catch ( Exception $e ) {

			$message = $e->getMessage();
			echo "<span class='infor'>$message</span>";
			die();
		}

		if ( $final_result[2] == 0 ) {
			echo "<span class='infor'>" . sprintf( OptionLocalization::get_term( $localization_options, 'results_header_no_results_found' ), WPSOLR_Service_Container::get_query()->get_wpsolr_query() ) . "</span>";
		} else {
			echo '<div class="wdm_resultContainer_d">
			<div class="wdm_list_d">';

			// Display sort list UI
			/*echo WPSOLR_UI_Sort::build(
				WPSOLR_Data_Sort::get_data(
					WPSOLR_Service_Container::getOption()->get_sortby_items_as_array(),
					WPSOLR_Service_Container::getOption()->get_sortby_items_labels(),
					WPSOLR_Service_Container::get_query()->get_wpsolr_sort(),
					$localization_options
				)
); */ 

$finalfactlist = apply_filters('event_date_sort_results',$final_result);
			// Display facets UI
echo ' <div class="col-md-3 col-sm-4 col-xs-12">
<div class="white_bg_block">
<div class="white_bg_block_container"><div class="checkboxCustom_content">
<div class="checkboxCustom">
<div class="filter_wrapper"><div id="res_facets">' . WPSOLR_UI_Facets::Build(
	WPSOLR_Data_Facets::get_data(
		WPSOLR_Service_Container::get_query()->get_filter_query_fields_group_by_name(),
		WPSOLR_Service_Container::getOption()->get_facets_to_display(),
		$final_result[1] ),
	$localization_options,
	WPSOLR_Service_Container::getOption()->get_facets_layouts_ids() ) . '</div>';


echo '</div></div></div></div></div></div></div>
<div class="col-md-9 col-sm-8 col-xs-12">';

if ( $final_result[0] != '0' ) {
	echo $final_result[0];
}

$ui_result_rows = $final_result[3];
if ( WPSOLR_Service_Container::getOption()->get_search_is_display_results_info() && $ui_result_rows != 0 ) {
	echo '<div class="stimulanz_total_search_count"><div class="res_info">' . $final_result[4] . '</div></div>';
}

if ( $ui_result_rows != 0 ) {

	$cate = $_GET['wpsolr_fq']['0'];
	if (isset($cate)):
		$cat_explode = explode(':', $cate);
	else:
		$cat_explode = '';
	endif;
                               //print_r($cat_explode);
                               // print_r($ui_result_rows);
                                //die();
	$img = plugins_url( 'images/gif-load.gif', __FILE__ );
	echo '<div class="loading_res"><img src="' . $img . '"></div>';
	echo "<div class='choose_training_area results-by-facets'>";
	foreach ( $ui_result_rows as $resarr ) {

                                  // echo '<a href="'.get_post_permalink($resarr[$count][0]).'"><h4>'.$ui_result_rows[$count][1].'</h4></a>';
		echo '<div class="white_bg_block banner_block">';
		echo $resarr;
		echo "</div>";    
	}

	if (empty($cat_explode[1])) {
		?>
		<script> 
		jQuery(document).on('wpsolr_on_ajax_success', function () {
			jQuery('.page-template-template-search .wpsolr_facet_categories,.page-template-template-search .wpsolr_facet_training_cat_str,.page-template-template-search .wpsolr_facet_product_cat_str,.page-template-template-search .wpsolr_facet_training_workfield_str,\n\
				.page-template-template-search .wpsolr_facet_training_level_str,.page-template-template-search .wpsolr_facet_training_region_str,.page-template-template-search .wpsolr_facet_kennisbanken_cat_str,\n\
				.page-template-template-search .wpsolr_facet_kennisbanken_werkveld_str,.page-template-template-search .wpsolr_facet_kennisbanken_niveau_str,.page-template-template-search .wpsolr_facet_kennisbanken_regio_str,.page-template-template-search .wpsolr_facet_advies_cat_str,\n\
				.page-template-template-search .wpsolr_facet_advies_werkveld_str,.page-template-template-search .wpsolr_facet_advies_niveau_str,.page-template-template-search .wpsolr_facet_advies_regio_str,.page-template-template-search .wpsolr_facet_pa_werkveld_str,.page-template-template-search .wpsolr_facet_pa_regio_str,.page-template-template-search .wpsolr_facet_event_start_date_str,.page-template-template-search .wpsolr_facet_training_werkveld_str,.page-template-template-search .wpsolr_facet_training_regio_str,.page-template-template-search .wpsolr_facet_landingspagina_cat_str').hide();
}); 
jQuery('.page-template-template-search div#type\\:acf-field,.page-template-template-search div#type\\:page,.page-template-template-search div#type\\:shop_order,.page-template-template-search div#type\\:tribe_events,.page-template-template-search div#type\\:tribe_organizer').hide();
</script>
<?php
}else{ ?>
<script> 
jQuery(document).on('wpsolr_on_ajax_success', function () {
	jQuery('.page-template-template-search .wpsolr_facet_categories,.page-template-template-search .wpsolr_facet_training_cat_str,.page-template-template-search .wpsolr_facet_product_cat_str,.page-template-template-search .wpsolr_facet_training_workfield_str,\n\
		.page-template-template-search .wpsolr_facet_training_level_str,.page-template-template-search .wpsolr_facet_training_region_str,.page-template-template-search .wpsolr_facet_kennisbanken_cat_str,\n\
		.page-template-template-search .wpsolr_facet_kennisbanken_werkveld_str,.page-template-template-search .wpsolr_facet_kennisbanken_niveau_str,.page-template-template-search .wpsolr_facet_kennisbanken_regio_str,.page-template-template-search .wpsolr_facet_advies_cat_str,\n\
		.page-template-template-search .wpsolr_facet_advies_werkveld_str,.page-template-template-search .wpsolr_facet_advies_niveau_str,.page-template-template-search .wpsolr_facet_advies_regio_str,.page-template-template-search .wpsolr_facet_pa_werkveld_str,.page-template-template-search .wpsolr_facet_pa_regio_str,.page-template-template-search .wpsolr_facet_event_start_date_str,.page-template-template-search .wpsolr_facet_training_werkveld_str,.page-template-template-search .wpsolr_facet_training_regio_str,.page-template-template-search .wpsolr_facet_landingspagina_cat_str').show();
}); 
jQuery('.page-template-template-search div#type\\:acf-field,.page-template-template-search div#type\\:page,.page-template-template-search div#type\\:shop_order,.page-template-template-search div#type\\:tribe_events,.page-template-template-search div#type\\:tribe_organizer').hide();
</script>
<?php   }
echo "</div>";
echo  "</div>";
echo "<div class='paginate_div'>";
$wpsolr_page = $_GET['wpsolr_page'];
$total         = $final_result[2];
$number_of_res = WPSOLR_Service_Container::getOption()->get_search_max_nb_results_by_page();
if ( $total > $number_of_res ) {
	$pages = ceil( $total / $number_of_res );
	echo '<ul id="pagination-flickr" class="wdm_ul">';
	for ( $k = 1; $k <= $pages; $k ++ ) {
		if(($wpsolr_page == $k) && !empty($wpsolr_page)){
			$active = 'current';
		}else if(('1' == $k) && empty($wpsolr_page)){
			$active = 'current';
		}else{
			$active ='';
		}

		echo "<li><a class='paginate $active' href='javascript:void(0)' id='$k'>$k</a></li>";
	}
}
echo '</ul></div>';

}


echo '</div>';
echo '</div><div style="clear:both;"></div>';
echo "<script type='text/javascript'>
jQuery(document).ready(function(){
	jQuery('.choose_training_area .training_block .white_bg_block').matchHeight();
});
</script>";
}
} catch
( Exception $e ) {

	echo sprintf( 'The search could not be performed. An error occured while trying to connect to the Apache Solr server. <br/><br/>%s<br/>', $e->getMessage() );
}

echo '</div>';
}

/**
 * Custom stimulansz search filter
 * @custom object
 */
function stimulansz_custom_search_indexed_data($custom_object) {

	$ad_url = admin_url();
	// Retrieve search form page url
	$get_page_info = WPSOLR_SearchSolariumClient::get_search_page();
	$url = get_permalink( $get_page_info->ID );
	$cat_taxonomy='';
	$title = '';  
        //Page template is template-kennisbanken   
	if( is_page_template( 'template-kennisbanken.php' )):
		$cat_taxonomy ='kennisbanken_cat';
	$workfield ='kennisbanken_werkveld';
	$level = '';
	$region = '';
	$title = 'Direct naar:';
                //$level ='kennisbanken_niveau';
                //$region ='kennisbanken_regio';
	endif;

           //Page template is template-kennisbanken   
	if( is_page_template( 'template-advies.php' )):
		$cat_taxonomy ='advies_cat';
	$workfield ='advies_werkveld';
	$level ='advies_niveau';
	$region ='advies_regio';
	$title = 'Direct naar:';
	endif;

          // if( is_page('trainingen')):
	if ( is_page_template( 'template-training-list.php' ) ) :
		$cat_taxonomy ='training_cat';
	$workfield ='training_werkveld';
	$level ='';
	$region ='training_regio';
	$title = 'Direct naar:';
	endif;

          //For archive kennisbanken page
	if ( is_post_type_archive('kennisbanken') ) {
		$cat_taxonomy ='kennisbanken_cat';
		$workfield ='kennisbanken_werkveld';
		$level = '';
		$region = '';
		$title = '';
               // $level ='kennisbanken_niveau';
               // $region ='kennisbanken_regio';
	}
          //For archive kennisbanken page
	if ( is_post_type_archive('advies') ) {
		$cat_taxonomy ='advies_cat';
		$workfield ='advies_werkveld';
		$level ='advies_niveau';
		$region ='advies_regio';
		$title = '';
	}
          //For archive training page
	if ( is_post_type_archive('training') ) {
		$cat_taxonomy ='training_cat';
		$workfield ='training_werkveld';
		$level ='';
		$region ='training_regio';
		$title = '';
	}


        // Filter the search page url. Used for multi-language search forms.
	//$url = apply_filters( WPSOLR_Events::WPSOLR_FILTER_SEARCH_PAGE_URL, $url, $get_page_info->ID );

	// Load localization options
        // Filter the search page url. Used for multi-language search forms.
	$url = apply_filters( WPSOLR_Events::WPSOLR_FILTER_SEARCH_PAGE_URL, $url, $get_page_info->ID );

	// Load localization options
	$localization_options = OptionLocalization::get_options();

	$wdm_typehead_request_handler = WPSOLR_AJAX_AUTO_COMPLETE_ACTION;

	echo "<div class='cls_search' style='width:100%;display:none;'> <form action='$url' method='get'  class='search-frm' >";
	echo '<input type="hidden" value="' . $wdm_typehead_request_handler . '" id="path_to_fold">';
	echo '<input type="hidden" value="' . $ad_url . '" id="path_to_admin">';
	echo '<input type="hidden" value="' . WPSOLR_Service_Container::get_query()->get_wpsolr_query() . '" id="search_opt">';

	$ajax_nonce = wp_create_nonce( "nonce_for_autocomplete" );

	echo '<div class="ui-widget">';
	echo '<input type="hidden"  id="ajax_nonce" value="' . $ajax_nonce . '">';
	echo '<input type="text" placeholder="' . OptionLocalization::get_term( $localization_options, 'search_form_edit_placeholder' ) . '" value="';
	echo esc_attr( WPSOLR_Service_Container::get_query()->get_wpsolr_query() ) . '" name="search" id="search_que" class="' . WPSOLR_Option::OPTION_SEARCH_SUGGEST_CLASS_DEFAULT;
	echo ' sfl2" autocomplete="off"/>';
	echo '<input type="submit" value="' . OptionLocalization::get_term( $localization_options, 'search_form_button_label' ) . '" id="searchsubmit" style="position:relative;width:auto">';
	echo '<input type="hidden" value="' . WPSOLR_Service_Container::getOption()->get_search_after_autocomplete_block_submit() . '" id="is_after_autocomplete_block_submit">';
	echo '<input type="hidden" value="' . WPSOLR_Service_Container::get_query()->get_wpsolr_paged() . '" id="paginate">';
	// Filter to add fields to the search form
	echo apply_filters( WPSOLR_Events::WPSOLR_FILTER_APPEND_FIELDS_TO_AJAX_SEARCH_FORM, '' );

	echo '<div style="clear:both"></div></div></form>';


	echo '</div>';


       // echo "<div class='cls_results'>";
	try {

		try {
			$final_result = WPSOLR_Service_Container::get_solr_client()->display_results( $custom_object );
            
		} catch ( Exception $e ) {
			$message = $e->getMessage();
			echo "<span class='infor'>$message</span>";
			die();
		}

		if ( $final_result[2] == 0 ) {
			echo "<span class='infor'>" . sprintf( OptionLocalization::get_term( $localization_options, 'results_header_no_results_found' ), WPSOLR_Service_Container::get_query()->get_wpsolr_query() ) . "</span>";
		} else {
                   // echo '<div class="wdm_results">';
			if ( $final_result[0] != '0' ) {
				echo $final_result[0];
			}


			$ui_result_rows = $final_result[3];

			if ( WPSOLR_Service_Container::getOption()->get_search_is_display_results_info() && $ui_result_rows != 0 ) {
				//echo '<div class="res_info">' . $final_result[4] . '</div>';
			}
                      // Display facets UI
                        // Display sort list UI
			/*echo WPSOLR_UI_Sort::build(
				WPSOLR_Data_Sort::get_data(
					WPSOLR_Service_Container::getOption()->get_sortby_items_as_array(),
					WPSOLR_Service_Container::getOption()->get_sortby_items_labels(),
					WPSOLR_Service_Container::get_query()->get_wpsolr_sort(),
					$localization_options
				)
); */ 

$finalfactlist = apply_filters('event_date_sort_results',$final_result);

if(!is_page('26')){
	echo '<div class="col-md-3 col-sm-4 col-xs-12">
	<div class="white_bg_block">
	<div class="white_bg_block_container"><div class="checkboxCustom_content">
	<div class="checkboxCustom">
	<div class="filter_wrapper"><div id="res_facets">' . WPSOLR_UI_Facets::Build(
		WPSOLR_Data_Facets::get_data(
			WPSOLR_Service_Container::get_query()->get_filter_query_fields_group_by_name(),
			WPSOLR_Service_Container::getOption()->get_facets_to_display(),
			$finalfactlist ), 
		$localization_options,
		WPSOLR_Service_Container::getOption()->get_facets_layouts_ids() ) . '</div>';
}

echo '</div></div></div></div></div></div>';



if ( $ui_result_rows != 0 ) {
	$img = plugins_url( 'images/gif-load.gif', __FILE__ );
	if(is_page('26')){  
		echo '<div class="container"><div class="row"><div class="col-md-12 col-sm-12 col-xs-12"><div class="section_title"><h2 style="color:#0087d7;">'.$title.'</h2><div class="loading_res"><img src="' . $img . '"></div><div class="row">'; 
	}else{
		echo '<div class="col-md-9 col-sm-8 col-xs-12"><div class="section_title"><h2>'.$title.'</h2><div class="loading_res"><img src="' . $img . '"></div>';
	}
	echo "<div class='choose_training_area results-by-facets'>";

	for($count=0;$count<count($ui_result_rows);$count++ ){
		$terms ='';
		$post_id = $ui_result_rows[$count][0];
		$post_url = get_permalink($ui_result_rows[$count][0]);
		$terms = wp_get_post_terms($ui_result_rows[$count][0],$cat_taxonomy, array('order' => 'DESC'));
		$term_workfield = wp_get_post_terms($ui_result_rows[$count][0],$workfield, array('order' => 'DESC'));
		$term_level = wp_get_post_terms($ui_result_rows[$count][0],$level, array('order' => 'DESC'));
		$term_region = wp_get_post_terms($ui_result_rows[$count][0],$region, array('order' => 'DESC'));
                                    //echo "<a href='".get_permalink($ui_result_rows[$count][0])."'>";
		if (is_page('300') ) {
			echo "<div class='col-md-4 col-sm-6 col-xs-12 training_block blocklink' urllink='$post_url'><div class='white_bg_block banner_block'><div class='banner_image'>";
                                     // $featured_img_url = get_the_post_thumbnail_url($ui_result_rows[$count][0],'full'); 
			$featured_img_url = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'full' ); 
			if(!empty($featured_img_url[0])):
				echo '<a href="'.get_permalink($post_id).'"><img class="img-responsive" src="'.$featured_img_url[0].'"></a>';
			endif;
			echo'</div>';       
			echo '<div class="banner_content">';
			if(isset($terms[0]->name)){
				echo'<a class="blue-rabion" href="'.get_permalink($post_id).'"><div class="box-top-right-icon">'.ucfirst($terms[0]->name).'</div></a>';
			}
			echo '<a href="'.get_post_permalink($ui_result_rows[$count][0]).'"><h4>'.$ui_result_rows[$count][1].'</h4></a>';
			echo '<div class="white_bg_block_container"><div class="training_blog_points">';
			echo  $ui_result_rows[$count][3];
			echo '</div><div class="training_blog_points traning_schedule">';
                                    //Extra feature list item
			apply_filters('stimulanz_acf_icon_text',$ui_result_rows[$count][0]);


			if($feedback_rating['average']):
				echo'<div><span class="reviews">'.$feedback_rating['average'].'</span><span>beoordelingen</span></div>';
			endif;
                                    // if(isset($term_workfield[0]->name)){ echo '<div><span><i class="fa fa-cog" aria-hidden="true"></i></span><span> '.ucfirst($term_workfield[0]->name).'</span></div>'; }
                                  //   if(isset($term_level[0]->name)){ echo '<div><span><i class="fa fa-cog" aria-hidden="true"></i></span><span> '.ucfirst($term_level[0]->name).'</span></div>';}
                                   //  if(isset($term_region[0]->name)){  echo '<div><span><i class="fa fa-map-marker" aria-hidden="true"></i></span><span> '.ucfirst($term_region[0]->name).'</span></div>';}
			echo "</div></div></div>"; ?>
			<?php  if( get_field('sticker_on_/_off', $ui_result_rows[$count][0]) ) { 
				$sticker_color =  get_post_meta($ui_result_rows[$count][0],'sticker_color',true); 
				if($sticker_color){
					?>
					<style>
					.training-sticker-<?php echo $ui_result_rows[$count][0]; ?>{
						background: <?php echo $sticker_color; ?> !important;
					}
					.training-sticker-<?php echo $ui_result_rows[$count][0]; ?>::before, .training-sticker-<?php echo $ui_result_rows[$count][0]; ?>::after{
						background: <?php echo $sticker_color; ?> !important; 
					}
					</style>
					<?php } ?>
					<div id='burst-12' class="training-sticker-<?php echo $ui_result_rows[$count][0]; ?>"><span><?php echo get_post_meta($ui_result_rows[$count][0],'sticker_text',true); ?></span></div><?php } echo "</div></div>";
				}
				else {

					if (is_page('26') ) {
						echo "<div class='col-md-3 col-sm-6 col-xs-12 training_block blocklink' urllink='$post_url'><div class='white_bg_block banner_block'><div class='banner_image'>";
					}else{
						echo "<div class='col-md-4 col-sm-6 col-xs-12 training_block blocklink' urllink='$post_url'><div class='white_bg_block banner_block'><div class='banner_image'>"; 
					}
					$featured_img_url = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'full' ); 
                                     // $featured_img_url = get_the_post_thumbnail_url($ui_result_rows[$count][0],'full'); 
					if(!empty($featured_img_url[0])):
						echo '<a href="'.get_permalink($post_id).'"><img class="img-responsive" src="'.$featured_img_url[0].'"></a>';
					endif;
					echo'</div>';       
					echo '<div class="banner_content">';
					if(isset($terms[0]->name)){
						echo'<a class="blue-rabion" href="'.get_permalink($post_id).'"><div class="box-top-right-icon">'.ucfirst($terms[0]->name).'</div></a>';
					}
					echo '<a href="'.get_post_permalink($ui_result_rows[$count][0]).'"><h4>'.$ui_result_rows[$count][1].'</h4></a>';
					echo '<div class="white_bg_block_container"><div class="training_blog_points">';
					echo  $ui_result_rows[$count][3];
					echo '</div><div class="training_blog_points traning_schedule">';
                                    //Extra feature list item
					apply_filters('stimulanz_acf_icon_text',$ui_result_rows[$count][0]);

					$feedback_rating = apply_filters( 'stimulanz_get_average_preview', $ui_result_rows[$count][0]);
					if($feedback_rating['average']):
						echo'<div><span class="reviews">'.$feedback_rating['average'].'</span><span>beoordelingen</span></div>';
					endif;
                                    // if(isset($term_workfield[0]->name)){ echo '<div><span><i class="fa fa-cog" aria-hidden="true"></i></span><span> '.ucfirst($term_workfield[0]->name).'</span></div>'; }
                                  //   if(isset($term_level[0]->name)){ echo '<div><span><i class="fa fa-cog" aria-hidden="true"></i></span><span> '.ucfirst($term_level[0]->name).'</span></div>';}
                                   //  if(isset($term_region[0]->name)){  echo '<div><span><i class="fa fa-map-marker" aria-hidden="true"></i></span><span> '.ucfirst($term_region[0]->name).'</span></div>';}
					echo "</div></div></div></div></div>";

				}

			}

			echo "<div class='paginate_div stimulansz_custom_pagination'>";
			?>

			<script>
			jQuery( document ).ready(function() {
				jQuery(".blocklink").on('click',function(){
                                                            //alert(jQuery(this).attr('urllink'));
                                                            var links = jQuery(this).attr('urllink');
                                                            window.location.href = links;
                                                           // window.open(jQuery(this).attr('urllink'));
                                                       });
			});


			</script>
			<?php
			$wpsolr_page = $_GET['wpsolr_page'];
			$total         = $final_result[2];
			$number_of_res = WPSOLR_Service_Container::getOption()->get_search_max_nb_results_by_page();
			if ( $total > $number_of_res ) {
				$pages = ceil( $total / $number_of_res );
				echo '<ul id="pagination-flickr" class="wdm_ul">';
				for ( $k = 1; $k <= $pages; $k ++ ) {
					if(($wpsolr_page == $k) && !empty($wpsolr_page)){
						$active = 'current';
					}else if(('1' == $k) && empty($wpsolr_page)){
						$active = 'current';
					}else{
						$active ='';
					}

					echo "<li><a class='paginate $active' href='javascript:void(0)' id='$k'>$k</a></li>";
				}
			}
			echo '</ul></div></div></div>'; 


		}
                       // echo '</div>';
		echo '<div style="clear:both;"></div></div>';
	}
} catch
( Exception $e ) {
	echo sprintf( 'The search could not be performed. An error occured while trying to connect to the Apache Solr server. <br/><br/>%s<br/>', $e->getMessage() );
}
	//echo '</div>';
}




/**
 * @throws Exception
 */
function return_solr_instance() {

	$status      = '0';
	$message     = '';
	$output_data = [];

	if ( isset( $_POST['security'] ) && wp_verify_nonce( $_POST['security'], WPSOLR_NONCE_FOR_DASHBOARD ) ) {

		try {

			$path = plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';
			require_once $path;


			$index_engine = isset( $_POST['sindex_engine'] ) ? $_POST['sindex_engine'] : WPSOLR_AbstractEngineClient::ENGINE_SOLR;

			$index_hosting_api_id = isset( $_POST['sindex_hosting_api_id'] ) ? $_POST['sindex_hosting_api_id'] : WPSOLR_Hosting_Api_None::HOSTING_API_ID;
			$index_region_id      = isset( $_POST['sindex_region_id'] ) ? $_POST['sindex_region_id'] : '';
			$hosting_api          = WPSOLR_Hosting_Api_Abstract::get_hosting_api_by_id( $index_hosting_api_id );

			$slabel   = $_POST['slabel'];
			$spath    = ! empty( $_POST['spath'] ) ? $_POST['spath'] : $hosting_api->get_data_by_id( WPSOLR_Hosting_Api_Abstract::DATA_PATH, $slabel, '' );
			$port     = ! empty( $_POST['sport'] ) ? $_POST['sport'] : $hosting_api->get_data_by_id( WPSOLR_Hosting_Api_Abstract::DATA_PORT, 'donotcare', '' );
			$host     = ! empty( $_POST['shost'] ) ? $_POST['shost'] : $hosting_api->get_data_by_id( WPSOLR_Hosting_Api_Abstract::DATA_HOST_BY_REGION_ID, $index_region_id, '' );
			$username = $_POST['skey'];
			$password = $_POST['spwd'];
			$protocol = ! empty( $_POST['sproto'] ) ? $_POST['sproto'] : $hosting_api->get_data_by_id( WPSOLR_Hosting_Api_Abstract::DATA_SCHEME, $index_region_id, $_POST['sproto'] );

			$solr_cloud_extra_parameters = [];
			switch ( $index_engine ) {
				case WPSOLR_AbstractEngineClient::ENGINE_SOLR_CLOUD:
				$solr_cloud_extra_parameters['index_solr_cloud_shards']             = isset( $_POST['index_solr_cloud_shards'] ) ? $_POST['index_solr_cloud_shards'] : '2';
				$solr_cloud_extra_parameters['index_solr_cloud_replication_factor'] = isset( $_POST['index_solr_cloud_replication_factor'] ) ? $_POST['index_solr_cloud_replication_factor'] : '2';
				$solr_cloud_extra_parameters['index_solr_cloud_max_shards_node']    = isset( $_POST['index_solr_cloud_max_shards_node'] ) ? $_POST['index_solr_cloud_max_shards_node'] : '2';
				break;

				case WPSOLR_AbstractEngineClient::ENGINE_ELASTICSEARCH:
				$solr_cloud_extra_parameters['index_elasticsearch_shards']   = isset( $_POST['index_elasticsearch_shards'] ) ? $_POST['index_elasticsearch_shards'] : '5';
				$solr_cloud_extra_parameters['index_elasticsearch_replicas'] = isset( $_POST['index_elasticsearch_replicas'] ) ? $_POST['index_elasticsearch_replicas'] : '1';
				break;
			}

			$client = WPSOLR_AbstractSearchClient::create_from_config( [
				'index_engine'     => $index_engine,
				'index_label'      => $slabel,
				'scheme'           => $protocol,
				'host'             => $host,
				'port'             => $port,
				'path'             => $spath,
				'username'         => $username,
				'password'         => $password,
				'timeout'          => WPSOLR_AbstractSearchClient::DEFAULT_SEARCH_ENGINE_TIMEOUT_IN_SECOND,
				'extra_parameters' => [
				$index_engine          => $solr_cloud_extra_parameters,
				'index_hosting_api_id' => $index_hosting_api_id,
				'index_email'          => isset( $_POST['sindex_email'] ) ? $_POST['sindex_email'] : '',
				'index_api_key'        => isset( $_POST['sindex_api_key'] ) ? $_POST['sindex_api_key'] : '',
				'index_region_id'      => $index_region_id,
				]
				]
				);

			$action = isset( $_POST['wpsolr_index_action'] ) ? $_POST['wpsolr_index_action'] : 'wpsolr_index_action_ping';


			if ( 'wpsolr_index_action_delete' === $action ) {

				// Delete the index and it's data.
				$client->admin_delete_index();

			} else {

				// Just trigger an exception if bad ping.
				$client->admin_ping( $output_data );
			}

		} catch ( Exception $e ) {

			$str_err      = '';
			$solr_code    = $e->getCode();
			$solr_message = $e->getMessage();

			switch ( $e->getCode() ) {

				case 401:
				$str_err .= "<br /><span>The server authentification failed. Please check your user/password (Solr code http $solr_code)</span><br />";
				break;

				case 400:
				case 404:

				$str_err .= "<br /><span>We could not join your Solr server. Your Solr path could be malformed, or your Solr server down (Solr code $solr_code)</span><br />";
				break;

				default:

					// Try to interpret some special errors with code "0"
				if ( ( method_exists( $e, 'getStatusMessage' ) ) && ( strpos( $e->getStatusMessage(), 'Failed to connect' ) > 0 ) && ( strpos( $e->getStatusMessage(), 'Connection refused' ) > 0 ) ) {

					$str_err .= "<br /><span>We could not connect to your Solr server. It's probably because the port is blocked. Please try another port, for instance 443, or contact your hosting provider/network administrator to unblock your port.</span><br />";

				}

				break;

			}

			$status  = '2';
			$message = sprintf( '%s<br>%s', $str_err, $solr_message );
		}
	}

	echo wp_json_encode( [ 'status' => $status, 'message' => $message, 'return' => $output_data ] );
	die();
}

add_action( 'wp_ajax_' . 'return_solr_instance', 'return_solr_instance' );


function return_solr_status() {

	if ( isset( $_POST['security'] ) && wp_verify_nonce( $_POST['security'], WPSOLR_NONCE_FOR_DASHBOARD ) ) {
		echo WPSOLR_Service_Container::get_solr_client()->get_solr_status();
	}

	die();
}

add_action( 'wp_ajax_' . 'return_solr_status', 'return_solr_status' );


function return_solr_results() {

	$final_result = WPSOLR_Service_Container::get_solr_client()->display_results( WPSOLR_Service_Container::get_query() );

	// Add result rows as html
	$res1[] = $final_result[3];

	// Add pagination html
	$total         = $final_result[2];
	$number_of_res = WPSOLR_Service_Container::getOption()->get_search_max_nb_results_by_page();
	$paginat_var   = '';
	if ( $total > $number_of_res ) {
		$pages       = ceil( $total / $number_of_res );
		$paginat_var .= '<ul id="pagination-flickr"class="wdm_ul">';
		for ( $k = 1; $k <= $pages; $k ++ ) {
			$paginat_var .= "<li ><a class='paginate' href='javascript:void(0)' id='$k'>$k</a></li>";
		}
		$paginat_var .= '</ul>';
	}
	$res1[] = $paginat_var;

	// Add results infos html ('showing x to y results out of n')
	$res1[] = $final_result[4];

	// Add facets data
	$res1[] = WPSOLR_UI_Facets::Build(
		WPSOLR_Data_Facets::get_data(
			WPSOLR_Service_Container::get_query()->get_filter_query_fields_group_by_name(),
			WPSOLR_Service_Container::getOption()->get_facets_to_display(),
			$final_result[1] ),
		OptionLocalization::get_options(),
		WPSOLR_Service_Container::getOption()->get_facets_layouts_ids()
		);

	// Output Json response to Ajax call
	echo json_encode( $res1 );


	die();
}

add_action( 'wp_ajax_nopriv_' . 'return_solr_results', 'return_solr_results' );
add_action( 'wp_ajax_' . 'return_solr_results', 'return_solr_results' );

/**
 * Fatal errors not captured by try/catch in Ajax calls.
 */
/**
 * Handler for fatal errors.
 *
 * @param $code
 * @param $message
 * @param $file
 * @param $line
 */
function wpsolr_my_error_handler( $code, $message, $file, $line ) {

	echo wp_json_encode(
		[
		'nb_results'        => 0,
		'status'            => $code,
		'message'           => sprintf( 'Error on line %s of file %s: %s', $line, $file, $message ),
		'indexing_complete' => false,
		]
		);

	die();
}

/**
 * Catch fatal errors, and call the handler.
 */
function wpsolr_fatal_error_shutdown_handler() {

	$last_error = error_get_last();
	if ( E_ERROR === $last_error['type'] ) {
		// fatal error
		wpsolr_my_error_handler( E_ERROR, $last_error['message'], $last_error['file'], $last_error['line'] );
	}
}

/*
 * Ajax call to index Solr documents
 */
function return_solr_index_data() {

	if ( isset( $_POST['security'] ) && wp_verify_nonce( $_POST['security'], WPSOLR_NONCE_FOR_DASHBOARD ) ) {
		try {

			set_error_handler( 'wpsolr_my_error_handler' );
			register_shutdown_function( 'wpsolr_fatal_error_shutdown_handler' );

			// Indice of Solr index to index
			$solr_index_indice = $_POST['solr_index_indice'];

			// Batch size
			$batch_size = intval( $_POST['batch_size'] );

			// nb of document sent until now
			$nb_results = intval( $_POST['nb_results'] );

			// Debug infos displayed on screen ?
			$is_debug_indexing = isset( $_POST['is_debug_indexing'] ) && ( 'true' === $_POST['is_debug_indexing'] );

			// Re-index all the data ?
			$is_reindexing_all_posts = isset( $_POST['is_reindexing_all_posts'] ) && ( 'true' === $_POST['is_reindexing_all_posts'] );

			// Post types to reindex
			$post_types = $_POST['post_types'];

			// Stop indexing ?
			$is_stopping = isset( $_POST['is_stopping'] ) & ( 'true' === $_POST['is_stopping'] ) ? true : false;

			$solr = WPSOLR_IndexSolariumClient::create( $solr_index_indice );

			$current_user = wp_get_current_user();

			// Reset documents if requested
			if ( $is_reindexing_all_posts ) {
				$solr->reset_documents( $current_user->user_email );
			}

			$res_final = $solr->index_data( $is_stopping, $current_user->user_email, WPSOLR_Model_Post_Type::create_models( $post_types ), $batch_size, null, $is_debug_indexing );

			// Increment nb of document sent until now
			$res_final['nb_results'] += $nb_results;

			echo wp_json_encode( $res_final );

		} catch ( Exception $e ) {

			echo wp_json_encode(
				[
				'nb_results'        => 0,
				'status'            => $e->getCode(),
				'message'           => htmlentities( $e->getMessage() ),
				'indexing_complete' => false,
				]
				);

		}
	}

	die();
}

add_action( 'wp_ajax_' . 'return_solr_index_data', 'return_solr_index_data' );


/*
 * Ajax call to clear Solr documents
 */
function return_solr_delete_index() {

	set_error_handler( 'wpsolr_my_error_handler' );
	register_shutdown_function( 'wpsolr_fatal_error_shutdown_handler' );

	if ( isset( $_POST['security'] ) && wp_verify_nonce( $_POST['security'], WPSOLR_NONCE_FOR_DASHBOARD ) ) {
		try {

			// Indice of Solr index to delete
			$solr_index_indice = $_POST['solr_index_indice'];

			// Post types to delete
			$post_types = isset( $_POST['post_types'] ) ? $_POST['post_types'] : null;

			$models = null;
			if ( ! is_null( $post_types ) ) {
				$models = WPSOLR_Model_Post_Type::create_models( $post_types );
			}

			$current_user = wp_get_current_user();

			$solr = WPSOLR_IndexSolariumClient::create( $solr_index_indice );
			$solr->delete_documents( $current_user->user_email, $models );

		} catch ( Exception $e ) {

			echo wp_json_encode(
				[
				'nb_results'        => 0,
				'status'            => $e->getCode(),
				'message'           => htmlentities( $e->getMessage() ),
				'indexing_complete' => false,
				]
				);

		}
	}

	die();
}

add_action( 'wp_ajax_' . 'return_solr_delete_index', 'return_solr_delete_index' );

/**
 * Ajax call to clear an indexing lock
 **/
function wpsolr_ajax_remove_process_lock() {

	try {

		if ( ! isset( $_POST['security'] ) || ! wp_verify_nonce( $_POST['security'], WPSOLR_NONCE_FOR_DASHBOARD ) ) {
			throw new \Exception( 'Unauthorized Ajax call.' );
		}

		// Process to stop/unlock
		$process_id = isset( $_POST['process_id'] ) ? sanitize_text_field( $_POST['process_id'] ) : '';

		WPSOLR_AbstractIndexClient::unlock_process( $process_id );


		echo wp_json_encode(
			[
			'status'  => 0,
			'message' => 'Indexing process has been stopped.',
			]
			);


	} catch ( \Exception $e ) {

		echo wp_json_encode(
			[
			'status'  => $e->getCode(),
			'message' => htmlentities( $e->getMessage() ),
			]
			);
	}

	die();
}

// Ajax to remove locks
add_action( 'wp_ajax_' . 'wpsolr_ajax_remove_process_lock', 'wpsolr_ajax_remove_process_lock' );
