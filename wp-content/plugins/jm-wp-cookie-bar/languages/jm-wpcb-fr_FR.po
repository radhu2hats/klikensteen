msgid ""
msgstr ""
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Project-Id-Version: JM WP Cookei Bar\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: jmlapam <contact@tweetpress.fr>\n"
"Language-Team: jmlapam <contact@tweetpress.fr>\n"
"MIME-Version: 1.0\n"
"X-Generator: Poedit 1.6.4\n"
"Language: fr_FR\n"
"X-Poedit-SourceCharset: UTF-8\n"

msgid ""
"Which CSS class do you want to add to style the close button? (default is ."
"wpcb-close-btn)"
msgstr "Quelle classe CSS pour le bouton OK (par défaut .wpcb-close-btn)?"

msgid "Set the cookie expiration time? (default is 365 days)"
msgstr "Régler la durée des cookies (par défaut 365 jours)"

msgid "The text to be shown. (120 chars at most)"
msgstr "Le texte à afficher. (120 caractère maximum)"

msgid "URL for your cookie rules"
msgstr "URL vers a la page contenant vos règles"

msgid "Which position?"
msgstr "Quelle position?"

msgid "Default is top of the page"
msgstr "Par défaut en haut de page"

msgid "top of the page"
msgstr "Haut de page"

msgid "bottom of the page"
msgstr "Bas de page"

msgid ""
"Default is yes. Be careful, this could mess up with bar position so do not "
"forget to add required styles in your own stylesheet."
msgstr ""
"Oui par défaut. Attention si vous désactivez cela peut interférer avec la "
"position de la barre de cookies donc ne pas oublier de préciser les styles "
"requis dans votre propre feuille de style."

msgid "Include basic styles?"
msgstr "Inclure les styles basiques?"

msgid "yes"
msgstr "oui"

msgid "no"
msgstr "non"

msgid ""
"Cookies help us deliver our services. By using our services, you agree to "
"our use of cookies."
msgstr ""
"Les cookies nous permettent de délivrer notre service. En utilisant nos "
"services, vous approuvez notre utilisation des cookies."

msgid "Read more"
msgstr "En savoir plus"
