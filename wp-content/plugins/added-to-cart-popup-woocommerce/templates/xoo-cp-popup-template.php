<?php

//Exit if accessed directly
if(!defined('ABSPATH')){
	return; 	
}

?>

<div class="xoo-cp-opac"></div>
<div class="xoo-cp-modal">
	<div class="xoo-cp-container popup-content">
		<div class="xoo-cp-outer ">
			<div class="xoo-cp-cont-opac"></div>
			<span class="xoo-cp-preloader xoo-cp-icon-spinner"></span>
		</div>
		<img src="<?php echo get_template_directory_uri(); ?>/img/new-icon.jpg" class="xoo-cp-close xoo-cp-icon-cross">
<!--<span class="xoo-cp-close xoo-cp-icon-cross"><i class="fa fa-close" aria-hidden="true"></i></span>-->

		<div class="xoo-cp-content"></div>
			
		<?php do_action('xoo_cp_before_btns'); ?>	
		<div class="xoo-cp-btns">
			<table class="xoo-cp-pdetails clearfix">
	<tr data-xoo_cp_key="<?php echo $cart_item_key; ?>">
		
		<td class="xoo-cp-pimg"><img class="upload-logo-img img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/Stimulansz_logo_digitaal_blauw_rgb-1.svg" alt="stimulansz"></a></td>
		<td class="xoo-cp-ptitle"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
<p>is toegevoegd aan je winkelmand</p>
		</td>

		

		


		
	</tr>
</table>
			
			<a class="xoo-cp-close xcp-btn" href="#">Verder winkelen</a>
			<a class="xoo-cp-btn-ch xcp-btn" href="<?php echo site_url(); ?>/winkelwagen/">Naar bestellen</a>
		</div>
		<?php do_action('xoo_cp_after_btns'); ?>
	</div>
</div>