<?php

namespace ILJ\Helper;

use  ILJ\Database\Linkindex ;
/**
 * Statistics toolset
 *
 * Methods for providing statistics
 *
 * @package ILJ\Helper
 * @since   1.0.0
 */
class Statistic
{
    /**
     * A configureable wrapper for the aggregation of columns of the linkindex
     *
     * @since  1.0.0
     * @param  array $args Configuration of the selection
     * @return array
     */
    public static function getAggregatedCount( $args = array() )
    {
        $defaults = [
            "type"  => "link_from",
            "limit" => 10,
        ];
        $args = wp_parse_args( $args, $defaults );
        extract( $args );
        if ( !is_numeric( $limit ) ) {
            $limit = $defaults['limit'];
        }
        $inlinks = Linkindex::getGroupedCount( $type );
        return array_slice( $inlinks, 0, $limit );
    }

}