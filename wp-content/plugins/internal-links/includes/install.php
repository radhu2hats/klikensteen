<?php

/**
 * @package ILJ\Includes
 */
use  ILJ\Core\Options ;
use  ILJ\Database\Linkindex ;
use  ILJ\Backend\Environment ;
/**
 * Responsible for creating the database tables
 *
 * @since  1.0.0
 * @return void
 */
function ilj_install_db()
{
    global  $wpdb ;
    $charset_collate = $wpdb->get_charset_collate();
    $query_linkindex = "CREATE TABLE " . $wpdb->prefix . Linkindex::ILJ_DATABASE_TABLE_LINKINDEX . " (\n        `link_from` BIGINT(20) NULL,\n        `link_to` BIGINT(20) NULL,\n        `type_from` VARCHAR(45) NULL,\n        `type_to` VARCHAR(45) NULL,\n        `anchor` TEXT NULL,\n        INDEX `link_from` (`link_from` ASC),\n        INDEX `type_from` (`type_from` ASC),\n        INDEX `type_to` (`type_to` ASC),\n        INDEX `link_to` (`link_to` ASC))" . $charset_collate . ";";
    include_once ABSPATH . 'wp-admin/includes/upgrade.php';
    dbDelta( $query_linkindex );
    Environment::update( "last_version", ILJ_VERSION );
}

/**
 * Installs the default options for the plugin
 *
 * @since  1.0.0
 * @return void
 */
function ilj_install_options()
{
    global  $ilj_fs ;
    Options::setOptionsDefault();
}

register_activation_hook( ILJ_FILE, '\\ilj_install_db' );
register_activation_hook( ILJ_FILE, '\\ilj_install_options' );