<?php
namespace ILJ\Core;

use ILJ\Enumeration\KeywordOrder;
use ILJ\Enumeration\TagExclusion;

/**
 * Options Wrapper
 *
 * Holds all the options, which are configured by the site administrator
 *
 * @package ILJ\Core
 * @since   1.0.0
 */

class Options
{

    /**
     * Settings sections
     */
    const ILJ_OPTION_SECTION_CONTENT = 'content';
    const ILJ_OPTION_SECTION_LINKS   = 'links';

    /**
     * Setting fields, could be administrated
     */
    const ILJ_OPTION_KEY_BLACKLIST            = 'ilj_settings_field_blacklist';
    const ILJ_OPTION_KEY_WHITELIST            = 'ilj_settings_field_whitelist';
    const ILJ_OPTION_KEY_KEYWORD_ORDER        = 'ilj_settings_field_keyword_order';
    const ILJ_OPTION_KEY_LINKS_PER_PAGE       = 'ilj_settings_field_links_per_page';
    const ILJ_OPTION_KEY_LINKS_PER_TARGET     = 'ilj_settings_field_links_per_target';
    const ILJ_OPTION_KEY_MULTIPLE_KEYWORDS    = 'ilj_settings_field_multiple_keywords';
    const ILJ_OPTION_KEY_LINK_OUTPUT_INTERNAL = 'ilj_settings_field_link_output_internal';
    const ILJ_OPTION_KEY_INTERNAL_NOFOLLOW    = 'ilj_settings_field_internal_nofollow';
    const ILJ_OPTION_KEY_NO_LINK_TAGS         = 'ilj_settings_field_no_link_tags';

    /**
     * Other (internal) options
     */
    const ILJ_OPTION_KEY_ENVIRONMENT  = 'ilj_environment';
    const ILJ_OPTION_KEY_INDEX_NOTIFY = 'ilj_option_index_notify';

    /**
     * Prefixes
     */
    const ILJ_OPTION_PREFIX_PAGE = 'ilj_settings_section_';
    const ILJ_OPTION_PREFIX_ID   = 'ilj_settings_';

    /**
     * Initializes the plugin options
     *
     * @since  1.0.0
     * @return void
     */
    public static function init()
    {
        self::addSettingsSections();
        self::addSettingsFields();
        self::registerSettings();
    }

    /**
     * Responsible for the registration of settings sections
     *
     * @since  1.0.0
     * @return void
     */
    protected static function addSettingsSections()
    {
        add_settings_section(
            self::ILJ_OPTION_PREFIX_ID . self::ILJ_OPTION_SECTION_CONTENT,
            __('Content Settings Section', 'ILJ'),
            (function () {
                echo '<p>' . __('Configure how the plugin should behave regarding the internal linking.', 'ILJ') . '</p>';
            }),
            self::ILJ_OPTION_PREFIX_PAGE . self::ILJ_OPTION_SECTION_CONTENT
        );

        add_settings_section(
            self::ILJ_OPTION_PREFIX_ID . self::ILJ_OPTION_SECTION_LINKS,
            __('Links Settings Section', 'ILJ'),
            (function () {
                echo '<p>' . __('Setting options for the output of the generated links.', 'ILJ') . '</p>';
            }),
            self::ILJ_OPTION_PREFIX_PAGE . self::ILJ_OPTION_SECTION_LINKS
        );
    }

    /**
     * Responsible for registering the settings fields
     *
     * @since  1.0.0
     * @return void
     */
    protected static function addSettingsFields()
    {
        add_settings_field(
            self::ILJ_OPTION_KEY_BLACKLIST,
            __('Blacklist of posts that should not be used for linking', 'ILJ'),
            (function () {
                $values = self::getOption(self::ILJ_OPTION_KEY_BLACKLIST);
                if ($values == "") {
                    $values = [];
                }
                echo '<select name="' . self::ILJ_OPTION_KEY_BLACKLIST . '[]" id="' . self::ILJ_OPTION_KEY_BLACKLIST . '" multiple="multiple">';
                foreach ($values as $value) {
                    echo '<option value="' . $value . '" selected="selected">' . get_the_title($value) . '</option>';
                }
                echo '</select> ' . self::getHelpLink('whitelist-blacklist/', 'blacklist', 'blacklist');
                echo '<p class="description">' . __('Posts that get configured here do not link to others automatically.', 'ILJ') . '</p>';
            }),
            self::ILJ_OPTION_PREFIX_PAGE . self::ILJ_OPTION_SECTION_CONTENT,
            self::ILJ_OPTION_PREFIX_ID . self::ILJ_OPTION_SECTION_CONTENT
        );

        add_settings_field(
            self::ILJ_OPTION_KEY_WHITELIST,
            __('Whitelist of post types, that should be used for linking', 'ILJ'),
            (function () {
                $values = self::getOption(self::ILJ_OPTION_KEY_WHITELIST, []);
                if ($values == "") {
                    $values = [];
                }

                $post_types_public = get_post_types(
                    [
                        'public'   => true,
                        '_builtin' => false
                    ], 'objects', 'or'
                );

                $post_types_with_editor = get_post_types_by_support(
                    ['editor']
                );

                if (count($post_types_public)) {
                    echo '<select name="' . self::ILJ_OPTION_KEY_WHITELIST . '[]" id="' . self::ILJ_OPTION_KEY_WHITELIST . '" multiple="multiple">';
                    foreach ($post_types_public as $post_type) {
                        if (in_array($post_type->name, $post_types_with_editor)) {
                            echo '<option value="' . $post_type->name . '"' . (in_array($post_type->name, $values) ? ' selected' : '') . '>' . $post_type->label . '</option>';
                        }
                    }
                    echo '</select> ' . self::getHelpLink('whitelist-blacklist/', 'whitelist', 'whitelist');
                    echo '<p class="description">' . __('All posts within the allowed post types can link to other posts automatically.', 'ILJ') . '</p>';
                }
            }),
            self::ILJ_OPTION_PREFIX_PAGE . self::ILJ_OPTION_SECTION_CONTENT,
            self::ILJ_OPTION_PREFIX_ID . self::ILJ_OPTION_SECTION_CONTENT
        );

        add_settings_field(
            self::ILJ_OPTION_KEY_KEYWORD_ORDER,
            __('Order for configured keywords while linking', 'ILJ'),
            (function () {
                $value = self::getOption(self::ILJ_OPTION_KEY_KEYWORD_ORDER, KeywordOrder::FIFO);

                echo '<select name="' . self::ILJ_OPTION_KEY_KEYWORD_ORDER . '" id="' . self::ILJ_OPTION_KEY_KEYWORD_ORDER . '">';
                $order_types = KeywordOrder::getValues();

                foreach ($order_types as $order_type) {
                    echo '<option value="' . $order_type . '"' . ($order_type == $value ? ' selected' : '') . '>' . KeywordOrder::translate($order_type) . '</option>';
                }
                echo '</select> ';
                echo '<p class="description">' . __('Set the order of how your set keywords get used for building links.', 'ILJ') . '</p>';
            }),
            self::ILJ_OPTION_PREFIX_PAGE . self::ILJ_OPTION_SECTION_CONTENT,
            self::ILJ_OPTION_PREFIX_ID . self::ILJ_OPTION_SECTION_CONTENT
        );

        add_settings_field(
            self::ILJ_OPTION_KEY_LINKS_PER_PAGE,
            __('Maximum amount of links per post', 'ILJ'),
            (function () {
                $multiple_keywords = self::getOption(self::ILJ_OPTION_KEY_MULTIPLE_KEYWORDS);
                $value             = self::getOption(self::ILJ_OPTION_KEY_LINKS_PER_PAGE);
                echo '<input type="number" name="' . self::ILJ_OPTION_KEY_LINKS_PER_PAGE . '" id="' . self::ILJ_OPTION_KEY_LINKS_PER_PAGE . '" value="' . $value . '"' . ($multiple_keywords ? ' disabled="disabled"' : '') . ' /> ' . self::getHelpLink('link-countings/', 'links-per-post-amount', 'links per post');
                echo '<p class="description">' . __('For an unlimited number of links, set this value to <code>0</code> .', 'ILJ') . '</p>';
            }),
            self::ILJ_OPTION_PREFIX_PAGE . self::ILJ_OPTION_SECTION_CONTENT,
            self::ILJ_OPTION_PREFIX_ID . self::ILJ_OPTION_SECTION_CONTENT
        );

        add_settings_field(
            self::ILJ_OPTION_KEY_LINKS_PER_TARGET,
            __('Maximum frequency of how often a post gets linked within another one', 'ILJ'),
            (function () {
                $multiple_keywords = self::getOption(self::ILJ_OPTION_KEY_MULTIPLE_KEYWORDS);
                $value             = self::getOption(self::ILJ_OPTION_KEY_LINKS_PER_TARGET);
                echo '<input type="number" name="' . self::ILJ_OPTION_KEY_LINKS_PER_TARGET . '" id="' . self::ILJ_OPTION_KEY_LINKS_PER_TARGET . '" value="' . $value . '"' . ($multiple_keywords ? ' disabled="disabled"' : '') . ' /> ' . self::getHelpLink('link-countings/', 'post-frequency', 'post frequency');
                echo '<p class="description">' . __('For an unlimited number of links, set this value to <code>0</code> .', 'ILJ') . '</p>';
            }),
            self::ILJ_OPTION_PREFIX_PAGE . self::ILJ_OPTION_SECTION_CONTENT,
            self::ILJ_OPTION_PREFIX_ID . self::ILJ_OPTION_SECTION_CONTENT
        );

        add_settings_field(
            self::ILJ_OPTION_KEY_MULTIPLE_KEYWORDS,
            __('Link as often as possible', 'ILJ'),
            (function () {
                echo '<label for="' . self::ILJ_OPTION_KEY_MULTIPLE_KEYWORDS . '">';
                echo '<input name="' . self::ILJ_OPTION_KEY_MULTIPLE_KEYWORDS . '" id="' . self::ILJ_OPTION_KEY_MULTIPLE_KEYWORDS . '" type="checkbox" value="1" class="code" ' . checked(1, self::getOption(self::ILJ_OPTION_KEY_MULTIPLE_KEYWORDS), false) . ' /> ';
                echo __('Allows posts and keywords to get linked as often as possible.', 'ILJ');
                echo '</label> ' . self::getHelpLink('link-countings/', 'greedy-mode', 'greedy mode');
                echo '<p class="description">(' . __('Deactivates all other restrictions', 'ILJ') . ')</p>';
            }),
            self::ILJ_OPTION_PREFIX_PAGE . self::ILJ_OPTION_SECTION_CONTENT,
            self::ILJ_OPTION_PREFIX_ID . self::ILJ_OPTION_SECTION_CONTENT
        );

        add_settings_field(
            self::ILJ_OPTION_KEY_NO_LINK_TAGS,
            __('Exclude HTML areas from linking', 'ILJ'),
            (function () {
                $values = self::getOption(self::ILJ_OPTION_KEY_NO_LINK_TAGS);
                if ($values == "") {
                    $values = [];
                }
                echo '<select name="' . self::ILJ_OPTION_KEY_NO_LINK_TAGS . '[]" id="' . self::ILJ_OPTION_KEY_NO_LINK_TAGS . '" multiple="multiple">';
                foreach (TagExclusion::getValues() as $value) {
                    $is_pro = (bool) !(TagExclusion::getRegex($value));
                    echo '<option value="' . $value . '"' . (!$is_pro && in_array($value, $values) ? ' selected' : '') . ($is_pro ? ' disabled' : '') . '>' . TagExclusion::translate($value) . ($is_pro ? ' (' . __('only available in Pro', 'ILJ') . ')' : '') . '</option>';
                }
                echo '</select>';
                echo '<p class="description">' . __('Content within the HTML tags that are configured here do not get used for linking.', 'ILJ') . '</p>';
            }),
            self::ILJ_OPTION_PREFIX_PAGE . self::ILJ_OPTION_SECTION_CONTENT,
            self::ILJ_OPTION_PREFIX_ID . self::ILJ_OPTION_SECTION_CONTENT
        );

        add_settings_field(
            self::ILJ_OPTION_KEY_LINK_OUTPUT_INTERNAL,
            __('Template for the link output (keyword links)', 'ILJ'),
            (function () {
                $value = self::getOption(self::ILJ_OPTION_KEY_LINK_OUTPUT_INTERNAL);
                echo '<input type="text" name="' . self::ILJ_OPTION_KEY_LINK_OUTPUT_INTERNAL . '" id="' . self::ILJ_OPTION_KEY_LINK_OUTPUT_INTERNAL . '" value="' . $value . '" /> ' . self::getHelpLink('link-templates/', '', 'link templates');
                echo '<p class="description">' . __('Markup for the output of generated internal links.', 'ILJ') . '</p>';
                echo '<p>' . __('You can use the placeholders <code>{{url}}</code> for the target and <code>{{anchor}}</code> for the generated anchor text.', 'ILJ') . '</p>';
            }),
            self::ILJ_OPTION_PREFIX_PAGE . self::ILJ_OPTION_SECTION_LINKS,
            self::ILJ_OPTION_PREFIX_ID . self::ILJ_OPTION_SECTION_LINKS
        );

        add_settings_field(
            self::ILJ_OPTION_KEY_INTERNAL_NOFOLLOW,
            __('NoFollow for internal keyword links', 'ILJ'),
            (function () {
                echo '<label for="' . self::ILJ_OPTION_KEY_INTERNAL_NOFOLLOW . '">';
                echo '<input name="' . self::ILJ_OPTION_KEY_INTERNAL_NOFOLLOW . '" id="' . self::ILJ_OPTION_KEY_INTERNAL_NOFOLLOW . '" type="checkbox" value="1" class="code" ' . checked(1, self::getOption(self::ILJ_OPTION_KEY_INTERNAL_NOFOLLOW), false) . ' />';
                echo __('Sets the <code>rel="nofollow"</code> attribute for keyword links (<strong>not recommended</strong>).', 'ILJ');
                echo '</label>';
            }),
            self::ILJ_OPTION_PREFIX_PAGE . self::ILJ_OPTION_SECTION_LINKS,
            self::ILJ_OPTION_PREFIX_ID . self::ILJ_OPTION_SECTION_LINKS
        );
    }

    /**
     * Registers all plugin settings
     *
     * @since  1.0.0
     * @return void
     */
    protected static function registerSettings()
    {
        register_setting(self::ILJ_OPTION_PREFIX_PAGE . self::ILJ_OPTION_SECTION_CONTENT, self::ILJ_OPTION_KEY_BLACKLIST);
        register_setting(self::ILJ_OPTION_PREFIX_PAGE . self::ILJ_OPTION_SECTION_CONTENT, self::ILJ_OPTION_KEY_WHITELIST);
        register_setting(self::ILJ_OPTION_PREFIX_PAGE . self::ILJ_OPTION_SECTION_CONTENT, self::ILJ_OPTION_KEY_KEYWORD_ORDER);
        register_setting(self::ILJ_OPTION_PREFIX_PAGE . self::ILJ_OPTION_SECTION_CONTENT, self::ILJ_OPTION_KEY_LINKS_PER_PAGE);
        register_setting(self::ILJ_OPTION_PREFIX_PAGE . self::ILJ_OPTION_SECTION_CONTENT, self::ILJ_OPTION_KEY_LINKS_PER_TARGET);
        register_setting(self::ILJ_OPTION_PREFIX_PAGE . self::ILJ_OPTION_SECTION_CONTENT, self::ILJ_OPTION_KEY_MULTIPLE_KEYWORDS);
        register_setting(self::ILJ_OPTION_PREFIX_PAGE . self::ILJ_OPTION_SECTION_CONTENT, self::ILJ_OPTION_KEY_NO_LINK_TAGS);
        register_setting(
            self::ILJ_OPTION_PREFIX_PAGE . self::ILJ_OPTION_SECTION_LINKS, self::ILJ_OPTION_KEY_LINK_OUTPUT_INTERNAL, [
                'type'              => 'string',
                'sanitize_callback' => 'htmlentities'
            ]
        );
        register_setting(self::ILJ_OPTION_PREFIX_PAGE . self::ILJ_OPTION_SECTION_LINKS, self::ILJ_OPTION_KEY_INTERNAL_NOFOLLOW);
    }

    /**
     * Retrieves the internal option value with different defaults
     *
     * @since  1.0.0
     * @param  string $option The option value which should be returned
     * @return mixed
     */
    public static function getOption($option)
    {
        $options = self::getConstants();

        switch ($option) {
            case self::ILJ_OPTION_KEY_WHITELIST:
            case self::ILJ_OPTION_KEY_BLACKLIST:
            case self::ILJ_OPTION_KEY_NO_LINK_TAGS:
                $value = get_option($option, []);
                if (!is_array($value)) {
                    return [];
                }
                return $value;
                break;
            case in_array($option, $options):
                return get_option($option, '');
            default:
                return get_option($option, false);
                break;
        }
    }

    /**
     * Sets the value of a plugin option
     *
     * @since  1.0.0
     * @param  string $option The option name
     * @param  string $value  The option value
     * @return bool
     */
    public static function setOption($option, $value)
    {
        $options = self::getConstants();

        if (!in_array($option, $options)) {
            return false;
        }

        return update_option($option, $value);
    }

    /**
     * Returns the class constants
     *
     * @since  1.0.0
     * @return array
     */
    protected static function getConstants()
    {
        $reflection = new \ReflectionClass(get_class());
        return $reflection->getConstants();
    }

    /**
     * Returns the default value of a single option
     *
     * @since  1.0.0
     * @param  string $option The option name
     * @return string|null
     */
    public static function getOptionDefault($option)
    {
        $defaults = self::getDefaults();

        if (isset($defaults[$option])) {
            return $defaults[$option];
        }

        return null;
    }

    /**
     * Internal set of default values
     *
     * @since  1.0.1
     * @return type
     */
    protected static function _getDefaults()
    {
        return [
            self::ILJ_OPTION_KEY_BLACKLIST            => [],
            self::ILJ_OPTION_KEY_WHITELIST            => ['page', 'post'],
            self::ILJ_OPTION_KEY_KEYWORD_ORDER        => KeywordOrder::FIFO,
            self::ILJ_OPTION_KEY_LINKS_PER_PAGE       => 0,
            self::ILJ_OPTION_KEY_LINKS_PER_TARGET     => 1,
            self::ILJ_OPTION_KEY_MULTIPLE_KEYWORDS    => false,
            self::ILJ_OPTION_KEY_LINK_OUTPUT_INTERNAL => '<a href="{{url}}">{{anchor}}</a>',
            self::ILJ_OPTION_KEY_INTERNAL_NOFOLLOW    => false,
            self::ILJ_OPTION_KEY_NO_LINK_TAGS         => [TagExclusion::HEADLINE]
        ];
    }

    /**
     * Returns all default values for options
     *
     * @since  1.0.0
     * @return array
     */
    public static function getDefaults()
    {
        return self::_getDefaults();
    }

    /**
     * Sets the default options
     *
     * @since 1.1.0
     *
     * @return void
     */
    public static function setOptionsDefault()
    {
        $defaults = self::getDefaults();

        foreach ($defaults as $option => $default) {
            if (is_string($default)) {
                $default = esc_html($default);
            }

            $existant_option = get_option($option, false);

            if (!$existant_option) {
                add_option($option, $default);
            }
        }
    }

    /**
     * Generates and returns the help link for manual pages
     *
     * @since  1.0.1
     * @param  string $docpath The path on the docs archive
     * @param  string $anchor  The anchored link within the doc page to jump to
     * @param  string $medium  The tracking medium
     * @return string
     */
    protected static function getHelpLink($docpath, $anchor, $medium)
    {
        $url = 'https://internallinkjuicer.com/docs/' . $docpath . '?utm_source=settings&utm_medium=' . urlencode($medium) . '&utm_campaign=plugin';
        $url = ($anchor != '') ? $url . '#' . $anchor : $url;
        return '<a href="' . $url . '" class="help tip" rel="noopener" target="_blank" title="' . __('Get help', 'ILJ') . '"><span class="dashicons dashicons-editor-help"></span></a>';
    }
}
