<?php

namespace ILJ\Core;

use  ILJ\Core\App ;
use  ILJ\Core\Options ;
use  ILJ\Backend\Editor ;
use  ILJ\Helper\Gutenberg ;
use  ILJ\Backend\AdminMenu ;
use  ILJ\Helper\Replacement ;
use  ILJ\Backend\Environment ;
use  ILJ\Helper\Capabilities ;
use  ILJ\Enumeration\TagExclusion ;
use  ILJ\Backend\Menupage\Settings ;
/**
 * The main app
 *
 * Coordinates all steps for the plugin usage
 *
 * @package ILJ\Core
 *
 * @since 1.0.1
 */
class App
{
    private static  $instance = null ;
    /**
     * Initializes the construction of the app
     *
     * @static
     * @since  1.0.1
     *
     * @return void
     */
    public static function init()
    {
        if ( null !== self::$instance ) {
            return;
        }
        global  $ilj_fs ;
        self::$instance = new self();
        $last_version = Environment::get( 'last_version' );
        
        if ( $last_version != ILJ_VERSION ) {
            ilj_install_db();
            Options::setOptionsDefault();
        }
    
    }
    
    protected function __construct()
    {
        global  $ilj_fs ;
        $this->initSettings();
        $this->loadIncludes();
        add_action( 'plugins_loaded', [ $this, 'afterPluginsLoad' ] );
        add_action( 'admin_init', [ '\\ILJ\\Backend\\Editor', 'addAssets' ] );
        add_action( 'admin_init', [ '\\ILJ\\Core\\Options', 'init' ] );
    }
    
    /**
     * Initialising all menu and settings related stuff
     *
     * @since 1.0.1
     *
     * @return type
     */
    protected function initSettings()
    {
        add_action( 'admin_menu', [ '\\ILJ\\Backend\\AdminMenu', 'init' ] );
        add_filter( 'plugin_action_links_' . ILJ_NAME, [ $this, 'addSettingsLink' ] );
    }
    
    /**
     * Loads all include files
     *
     * @since 1.0.1
     *
     * @return void
     */
    public function loadIncludes()
    {
        $include_files = [ 'install' ];
        global  $ilj_fs ;
        foreach ( $include_files as $file ) {
            include_once ILJ_PATH . 'includes/' . $file . '.php';
        }
    }
    
    /**
     * Gets called after all plugins are loaded for registering actions and filter
     *
     * @since 1.0.1
     *
     * @return void
     */
    public function afterPluginsLoad()
    {
        $this->registerActions();
        $this->registerFilter();
        load_plugin_textdomain( 'ILJ', false, dirname( ILJ_NAME ) . '/languages/' );
        global  $ilj_fs ;
    }
    
    /**
     * @inheritdoc
     */
    protected function registerActions()
    {
        $capability = current_user_can( 'administrator' );
        global  $ilj_fs ;
        
        if ( $capability ) {
            add_action( 'load-post.php', [ '\\ILJ\\Backend\\Editor', 'addKeywordMetaBox' ] );
            add_action( 'load-post-new.php', [ '\\ILJ\\Backend\\Editor', 'addKeywordMetaBox' ] );
            add_action(
                'save_post',
                [ '\\ILJ\\Backend\\Editor', 'saveKeywordMeta' ],
                10,
                2
            );
            add_action( 'wp_ajax_ilj_search_posts', [ '\\ILJ\\Helper\\Ajax', 'searchPostsAction' ] );
            $this->automaticIndexBuildingMode();
        }
    
    }
    
    /**
     * Triggers alle actions for automatic index building mode.
     *
     * @since  1.1.0
     * @return void
     */
    protected function automaticIndexBuildingMode()
    {
        
        if ( Gutenberg::isActive() ) {
            add_action(
                'updated_post_meta',
                function (
                $meta_id,
                $post_id,
                $meta_key,
                $meta_value
            ) {
                $this->triggerRebuildIndex( $post_id );
            },
                10,
                4
            );
        } else {
            add_action( 'save_post', [ $this, 'triggerRebuildIndex' ], 20 );
        }
    
    }
    
    /**
     * Triggers the recreation of the linkindex
     *
     * @since  1.1.0
     * @param  string  $new_status New status, after transition
     * @param  string  $old_status Old status, before transition
     * @param  WP_Post $post       Hooked post object
     * @return void
     */
    public function triggerRebuildIndex( $post_id )
    {
        $post = get_post( $post_id );
        $linkindex_environment = Environment::get( 'linkindex' );
        if ( !in_array( $post->post_status, [ 'publish', 'trash' ] ) ) {
            return;
        }
        add_action( 'shutdown', function () {
            $index_builder = new IndexBuilder();
            $index_builder->buildIndex();
        } );
    }
    
    /**
     * @inheritdoc
     * @since      1.0.0
     */
    public function registerFilter()
    {
        add_filter( 'the_content', function ( $content ) {
            $link_builder = new LinkBuilder( get_the_ID(), 'post' );
            return $link_builder->linkContent( $content );
        } );
        $tag_exclusions = Options::getOption( Options::ILJ_OPTION_KEY_NO_LINK_TAGS );
        if ( count( $tag_exclusions ) ) {
            add_filter( Replacement::ILJ_FILTER_EXCLUDE_TEXT_PARTS, function ( $search_parts ) use( $tag_exclusions ) {
                foreach ( $tag_exclusions as $tag_exclusion ) {
                    global  $ilj_fs ;
                    if ( !isset( $regex ) ) {
                        $regex = TagExclusion::getRegex( $tag_exclusion );
                    }
                    if ( $regex ) {
                        $search_parts[] = $regex;
                    }
                }
                return $search_parts;
            } );
        }
        global  $ilj_fs ;
    }
    
    /**
     * Adds a link to the plugins settings page on plugins overview
     *
     * @since 1.0.0
     *
     * @param  array $links All links that get displayed
     * @return array
     */
    public function addSettingsLink( $links )
    {
        $settings_link = '<a href="admin.php?page=' . AdminMenu::ILJ_MENUPAGE_SLUG . '_' . Settings::ILJ_MENUPAGE_SETTINGS_SLUG . '">' . __( 'Settings' ) . '</a>';
        array_unshift( $links, $settings_link );
        return $links;
    }

}