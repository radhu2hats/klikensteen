��    V      �      |      |  
   }  ~   �  .        6  <   K  ~   �                      	   +     5  
   F  1   Q  �   �           $     )     2     >  s   ^     �     �     �     	     	  -   (	     V	     f	  '   v	  �   �	     (
     1
     8
     V
     ]
     b
     j
     �
     �
     �
     �
     �
     �
     �
     �
  "   �
     �
     �
                    '     /     <     H     N     \     m     �     �     �     �     �       5   .  	   d     n     |     �  %   �     �     �  (   �       A     B   R  =   �  B   �  e        |  !   �     �     �  4   �  �  �     �  s   �  2   /     b  =   |  �   �     Q     W     `     i     z     �  
   �  %   �  q   �     8     <     B  !   K  !   m  �   �     7  ]   H     �     �     �  7   �          '  .   8  �   g          
          0     9     >  $   F     k          �     �     �     �     �     �     �     �     �     �          	               *     8     A     O     `     p     �     �     �     �     �  =   �  	   3     =     L     T  #   Z     ~     �  (   �     �  ?   �  @   "  <   c  N   �  [   �     K     X     x     }  4   �   %s created <a href="https://www.sisow.nl/Sisow/down.aspx?doc=HandleidingWooCommerce.pdf" target="_blank">Click to open documentation</a>. Activate this module to accept %s transactions Add utm_nooverride=1 Add utm_nooverride=1 recommended if you use Google Analytics An additional contract is required for this payment method, please contact <a href="mailto:sales@sisow.nl">sales@sisow.nl</a>. April August Bankcode Banks in list Birthdate Choose your bank CoC number Create AfterPay invoice direct after credit check Curl is not installed.<br />In order to use the Sisow payment methods, you must install install CURL.<br />Ask your system administrator to install php_curl Day Days December Description Description on the bank account Description on the bank account, can be overwritten at every payment method. Use {orderid} to include order number. Disable B2B Disable B2B transactions Display banks in list Documentation Don't cancel Don't cancel the order after a failed payment Enable Sisow %s Enable testmode Enable testmode for all payment methods Extra settings are required for this payment method, contact <a href="mailto:support@sisow.nl">support@sisow.nl</a> for more information. February Female Fields with an * are required Gender IBAN Include Include bank account details Include paylink January July June Mail to admin Make AfterPay invoice Male March Mark the order direct as completed May Merchant ID Merchant Key Month November October Only for B2B Pay with %s Phone Please Choose Please choose... Please fill in your IBAN Please fill in your coc number Please fill in your phonenumber Please insert a bank code Please select your bank Please select your birthdate Please select your gender Send e-mail to shop administrator instead of consumer September Set completed Shop ID Sisow Sisow payment methods for woocommerce Sisow settings Status recieved from Sisow: %s Status recieved from Sisow: %s, IBAN: %s Testmode The Merchant ID from Sisow, you can find it in your Sisow account The Merchant Key from Sisow, you can find it in your Sisow account The Shop ID from Sisow, you can find it in your Sisow account The Sisow payment methods plugin requires woocommerce to be active The following options are required to use the Sisow Gateway and are used by all Sisow Payment Methods Warning Woocommerce Sisow Payment Methods Year http://www.sisow.nl https://wordpress.org/plugins/sisow-for-woocommerce/ Project-Id-Version: Woocommerce Sisow Payment Methods
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-04-30 07:49+0000
POT-Revision-Date: Thu Nov 12 2015 13:09:23 GMT+0100
PO-Revision-Date: 2019-04-30 07:52+0000
Last-Translator: admin <shop@mvhaaren.nl>
Language-Team: Nederlands (Formeel)
Language: nl_NL_formal
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: .
X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.2.2; wp-5.0.4 %s aangemaakt <a href="https://www.sisow.nl/Sisow/down.aspx?doc=HandleidingWooCommerce.pdf" target="_blank">Open documentatie</a> Activeer de module om %s transacties te accepteren Voeg utm_nooverride=1 toe Voeg de paramater utm_nooverride=1 toe voor google Analytics. Voor deze betaalmethode is een aanvullend contract nodig, neem contact op met <a href="mailto:sales@sisow.nl">sales@sisow.nl</a> voor meer informatie. April Augustus Bankcode Banken als lijst Geboortedatum Kies uw bank KvK nummer Maak Afterpay factuur automatisch aan CURL is niet actief op de server, deze dient te worden geactiveerd<br/>Neem hiervoor contact op met uw beheerder. Dag Dagen December Omschrijving op het bankafschrift Omschrijving op het bankafschrift Omschrijving op het rekeningafschrift van de consument, kan per betaalmethode worden overschreven. Gebruik {orderid} om het ordernummer op te nemen in de omschrijving. B2B uitschakelen B2B transacties uitschakelen, het veld KvK nummer wordt niet meer weergegeven in de checkout. Banken als lijst weergeven Documentatie Niet annuleren De order niet annuleren wanneer een betaling is mislukt Activeer Sisow %s Zet testmode aan Testmodus voor ALLE betaalmethoden inschakelen Voor deze betaalmethode zijn aanvullende instellingen nodig, neem contact op met <a href="mailto:sales@sisow.nl">sales@sisow.nl</a> voor meer informatie. Februari Vrouw Velden met een * zijn verplicht Geslacht IBAN Include Voeg uw bankgegevens toe aan de mail Voeg betaallink toe Januari Juli Juni E-mail beheerder Maak Afterpay factuur aan Man Maart Zet de order direct op afgerond Mei Merchant ID Merchant Key Maand November Oktober Alleen voor B2B Betaal met %s Telefoon Maak uw keuze Maak uw keuze... Voer uw IBAN in Vul uw KvK nummer in Vul uw telefoonnummer in Voer uw bankcode in Selecteer uw bank Selecteer uw geboortedatum Selecteer uw geslacht Stuur e-mail naar de shop eigenaar in plaats van de consument September Order afgerond Shop ID Sisow Sisow betaalopties voor WooCommerce Sisow Instellingen Status ontvangen van Sisow: %s Status ontvangen van Sisow: %s, IBAN: %s Testmode De Merchant ID van Sisow, deze vind u terug in uw Sisow profiel De Merchant Key van Sisow, deze vind u terug in uw Sisow profiel Het Shop ID van Sisow, deze vind u terug in uw Sisow profiel Om de Sisow betaalwijzen te gebruiken dient WooCommerce te zijn geïnstalleerd De onderstaande instellingen zijn verplicht, deze worden door alle betaalmethoden gebruikt. Waarschuwing WooCommerce Sisow Betaal Opties Jaar http://www.sisow.nl https://wordpress.org/plugins/sisow-for-woocommerce/ 