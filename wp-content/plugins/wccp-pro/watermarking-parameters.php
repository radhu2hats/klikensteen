<?php
$watermark_type = "dw";
$watermark_position = "center-center";
$watermark_r_text = "www.stimulansz.nl";
$r_font_size_factor = "55";
$watermark_text = "WATERMARKED";
$font_size_factor = "90";
$pure_watermark_stamp_image = "https://www.stimulansz.nl/wp-content/plugins/wccp-pro/images/testing-logo.png";
$margin_left_factor = "50";
$margin_top_factor = "50";
$watermark_color = "#000000";
$watermark_r_color = "#efefef";
$watermark_transparency = "65";
$watermark_rotation = "40";
$watermark_imagefilter = "None";
$watermark_signature = "This image is protected";
$home_path = "/home/stimulansz/public_html/";
?>