<?php
class wccp_pro_controls{
	//---------------------------------------------------------------------
	//Layout builders
	//---------------------------------------------------------------------
	public static function open_tab()
	{
		echo '<div class="simpleTabsContent"><!-- Tab Opened -->';
	}
	public static function add_tab_heading($text)
	{
		echo '<div class="row"><div class="col-lg-12"><b><font size="5">'. $text .'</font></b></div></div>';
	}
	public static function close_tab()
	{
		echo '</div><!-- Tab Closed -->';
	}
	public static function open_row()
	{
		echo '<div class="row">';
	}
	public static function close_row()
	{
		echo '</div><!-- Row Closed -->';
	}
	public static function add_label($text)
	{
		echo '<div class="col-md-2 col-xs-12"><div class="welling"><span>'. $text .'</span></div></div>';
	}
	public static function add_inner_label($text)
	{
		echo '<div class="col-md-2 col-xs-12"><div class="inner-label"><span>'. $text .'</span></div></div>';
	}
	public static function open_controls_container($id)
	{
	    echo '<div id="container_'.$id.'" style="display: table-cell; vertical-align: middle;" class="col-md-7 col-xs-7 controls_container">';
	}
	public static function close_controls_container()
	{
	    echo '</div>';
	}
	public static function add_help_container($text)
	{
	    echo '<div class="col-md-3 col-xs-5"><div class="welling"><span>'. $text .'</span></div></div><!-- Help container closed -->';
	}
	public static function add_line()
	{
	    echo '<hr style="margin-bottom: 5px; margin-top: 5px;">';
	}
	public static function add_section($title,$color)
	{
		if($color == '') $color = '#1ABC9C';
		echo '<div class="col-lg-12 section"><h4><strong><font size="3" color="$color">'.$title.'</font></strong></h4></div>';
		
		echo "<style>.col-lg-12.section {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background: rgba(0, 0, 0, 0) linear-gradient(to right bottom, #f8f8f8, #fff) repeat scroll 0 0;
    border-bottom: 1px solid #f1f1f1;
    border-image: none;
    border-left: 7px solid;
    border-right: 1px solid #f1f1f1;
    color: $color;
    margin: 8px 0;}</style>";
	}
	public static function close_div()
	{
		echo '</div>';
	}
	//---------------------------------------------------------------------
	//Function to show a message (alert - success - fail) after saving
	//---------------------------------------------------------------------
	public static function save_changes_message()
	{
		echo 'Settings saved successfully';
	}
	//---------------------------------------------------------------------
	//Function to add a static photo behind any control
	//---------------------------------------------------------------------
	public static function add_photo_help_container($path, $class)
	{
		$pluginsurl = plugins_url( '', __FILE__ );
		
		$img = '<img id="'.$class.'" border="0" src="'. $pluginsurl .'/'.$path.'">';
		
		echo '<div class="col-md-3 col-xs-5"><div class="welling"><span>'. $img .'</span></div></div><!-- Photo container closed -->';
	}
	//---------------------------------------------------------------------
	//Function to get settings from the main options array
	//---------------------------------------------------------------------
	public static function wccp_pro_get_setting($name)
	{
		$wccp_pro_settings = wccp_pro_read_options();
		
		if (array_key_exists($name,$wccp_pro_settings))
		{
			$option_value = $wccp_pro_settings["$name"];
		}
		else
		{
			$option_value = '';
		}
		return $option_value;
	}
	//---------------------------------------------------------------------
	//add dropdown control
	//---------------------------------------------------------------------
	public static function add_dropdown($name , $options_array , $default_value)
	{
		wccp_pro_save_setting($name, $default_value);
		
		$choosed_option_value = self::wccp_pro_get_setting($name);
		
		echo '<div id="div_'.$name.'" class="col-md-5 col-xs-12 row">';
		
	    	echo '<select size="1" id="'.$name.'" name="'.$name.'">';
	    	
	    	$arrlength = count($options_array);
	    	
	    	for($x = 0; $x < $arrlength; $x++)
	    	{
	    		if ($options_array[$x] == $choosed_option_value)
	    		
	    			echo '<option selected>'.$options_array[$x].'</option>';
	    			
	    		else
	    		
	    			echo '<option>'.$options_array[$x].'</option>';
	    	}
	    	
	    	echo '</select>';
	    	
	    	echo '<script>$("select").selectpicker({style: "btn-hg btn-primary", menuStyle: "dropdown-inverse"});</script>';

		echo '</div>';
	}
	//---------------------------------------------------------------------
	//add textbox control
	//---------------------------------------------------------------------
	public static function add_textbox($name , $placeholder, $class, $default_value)
	{
		wccp_pro_save_setting($name, $default_value);
		
		$choosed_option_value = self::wccp_pro_get_setting($name);
		
		echo "<div style=\"padding-bottom: 5px;\" class=\"$class\">";
		
		echo "<input type=\"text\" placeholder=\"$placeholder\" class=\"form-control textbox_custom\" name=\"$name\" id=\"$name\"   value=\"$choosed_option_value\" size=\"25\">";
		
		echo '</div>';
	}
	//---------------------------------------------------------------------
	//add bottom hint under any control
	//---------------------------------------------------------------------
	public static function add_bottom_hint($bottom_hint)
	{
		echo '<div class="row col-md-12 col-xs-12"><div class="">';
		
		echo "<span>$bottom_hint</span>";
		
		echo '</div></div>';
	}
	//---------------------------------------------------------------------
	//add textarea
	//---------------------------------------------------------------------
	public static function add_textarea($name , $placeholder, $class, $bottom_hint, $default_value)
	{
		wccp_pro_save_setting($name, $default_value);
		
		$choosed_option_value = self::wccp_pro_get_setting($name);
		
		echo "<div style=\"padding-bottom: 5px;\" class=\"$class\">";
		
		echo "<textarea placeholder=\"$placeholder\" class=\"form-control textbox_custom\" name=\"$name\" id=\"$name\">$choosed_option_value</textarea>";
		
		echo '</div>';
		
		echo '<div class="row col-md-12 col-xs-12"><div class="">';
		
		echo "<span>$bottom_hint</span>";
		
		echo '</div></div>';
	}
	//---------------------------------------------------------------------
	//add colorpicker control whitch belongs to wordpress
	//---------------------------------------------------------------------
	public static function add_colorpicker($name, $behind_text, $default_color)
	{
		wccp_pro_save_setting($name, $default_color);
		
		$choosed_option_value = self::wccp_pro_get_setting($name);
		
		echo '<div class="col-md-5 col-xs-12 row"><div>'.$behind_text.'</div>';
		
		if ($choosed_option_value == '') $choosed_option_value = $default_color;
			
			echo "<input name=\"$name\" type=\"text\" value=\"$choosed_option_value\" class=\"nrcw-colorpicker-field\" data-default-color=\"$default_color\" />";
		
		echo "<style>.wp-picker-input-wrap,.wp-picker-holder{position: absolute;z-index:9999999;background:#ffffff;}</style>";
		
		echo "<script>jQuery(document).ready(function($){
		$('.nrcw-colorpicker-field').wpColorPicker();});</script>";
		
		echo '</div>';
	}
	//---------------------------------------------------------------------
	//add dismissable alert anywhere
	//---------------------------------------------------------------------
	public static function add_dismissable_box($name, $behind_text, $default_color)
	{
		//https://premium.wpmudev.org/blog/adding-admin-notices/
	}
	//---------------------------------------------------------------------
	//add Slider control
	//---------------------------------------------------------------------
	public static function add_slider($name, $default_value, $min, $max, $factor, $orientation, $show_array)
	{
		wccp_pro_save_setting($name, $default_value);
		
		$choosed_option_value = self::wccp_pro_get_setting($name);
		
		$pluginsurl = plugins_url( '', __FILE__ );
		
		if(!array_key_exists('class', $show_array)) $show_array["class"] = 'col-md-4 col-xs-12 row';
		
		echo '<div class="'. $show_array["class"] .'" style="margin-top: 20px;">';

		echo '<div class="'.$name.'_tooltip"></div><div onmousemove="getslidervalue_'.$name.'();" id="'.$name.'_slider_div"></div>';
		
		echo '</div>';
		
		if ($show_array["counter"] == 1)
		{
			echo '<div class="col-md-1 col-xs-3" style="margin-top: 13px;">';
			
			echo '<input id="'.$name.'" name="'.$name.'" value="'.$choosed_option_value.'" type="number" size="5" style="border: 1px solid #FFFFFF; border-radius: 44px;text-align: center;width: 45px;">';
			
			echo '</div>';
		}
		else
		{
			echo '<input hidden id="'.$name.'" name="'.$name.'" value="'.$choosed_option_value.'" readonly type="text" size="5" style="border: 1px solid #FFFFFF; border-radius: 44px;text-align: center;width: 45px;">';
		}

		if ($show_array["tansparency_meter"] == 1)
		{
			echo '<div class="col-md-1 col-xs-4" style="margin-top: 12px;">';
			
			$opacity = 1 - ($choosed_option_value/$max);
			
			echo '<img border="0" style="opacity: '. $opacity .'" src="'.$pluginsurl.'/framework/images/tansparency_meter.png" id="tansparency_meter_'.$name.'"/>';
			
			echo '</div>';
		}
		
		if (array_key_exists('behind_text', $show_array)) {
			
			if ($show_array["behind_text"] != '')
			{
				echo '<div class="col-md-1 col-xs-4" style="margin-top: 12px;">';
				
					echo $show_array["behind_text"];
				
				echo '</div>';
			}
			$show_array["behind_text"] = '';
		}
		echo'
			<script>var $slider_'.$name.' = $("#'.$name.'_slider_div");
			tooltip = $(".'.$name.'_tooltip");
			//tooltip.hide();
			if ($slider_'.$name.'.length > 0) {
			  $slider_'.$name.'.slider({
			    min: '.$min * $factor.',
			    max: '.$max * $factor.',
			    value: '.$choosed_option_value * $factor.',
			    orientation: "horizontal",
			    range: "min",
			    start: function(event,ui) {
			          tooltip.fadeIn("fast");
			        },
			
			        stop: function(event,ui) {
			          tooltip.fadeOut("fast");
			        }
			  }).addSliderSegments($slider_'.$name.'.slider("option").max);
			}
			$( document ).ready(function() {
			    getslidervalue_'.$name.'();
			});

			function getslidervalue_'.$name.'()
			{
				document.getElementById("'.$name.'").value = parseInt($slider_'.$name.'.slider("option").value/'.$factor.');
				var element = document.getElementById("tansparency_meter_'.$name.'");
				var op = parseInt($slider_'.$name.'.slider("option").value/'.$factor.');
				element.style.opacity = 1 - (op/'.$max.');
			}
			</script>';
	}
	//---------------------------------------------------------------------
	// Function to add a set of photos and select one of them
	//---------------------------------------------------------------------
	public static function add_image_picker($name, $settings, $options_array, $folder_path, $default_value)
	{
		wccp_pro_save_setting($name, $default_value);
		
		//$settings = 'multiple="multiple" data-limit="2"';
		
		$choosed_option_value = self::wccp_pro_get_setting($name);
		
		$pluginsurl = plugins_url( '', __FILE__ );
		
		echo '<div style="margin: 1px 0 -9px;">';
		
		echo '<select '.$settings.' id="'.$name.'" name="'.$name.'" hidden class="image-picker show-html">';
		
		$arrlength = count($options_array[0]);
	    	
	    	for($x = 0; $x < $arrlength; $x++)
	    	{
				$value = $options_array[0][$x];
				
				$title = $options_array[1][$x];
				
	    		if ($value == $choosed_option_value)
					
					echo "<option selected data-img-src=\"$pluginsurl/$folder_path/$value.png\" value=\"$value\">$title</option>";
					
	    		else
					
	    			echo "<option data-img-src=\"$pluginsurl/$folder_path/$value.png\" value=\"$value\">$title</option>";
	    	}
		
		echo '</select>';
		
		echo '</div>';

		echo '
			<script type="text/javascript">
			jQuery("select.image-picker").imagepicker({
			  hide_select:  false,
			});

			jQuery("select.image-picker.show-labels").imagepicker({
			  hide_select:  false,
			  show_label:   true,
			});

			jQuery("select.image-picker.limit_callback").imagepicker({
			  limit_reached:  function(){alert("We are full!")},
			  hide_select:    false
			});

			var container = jQuery("select.image-picker.masonry").next("ul.thumbnails");
			container.imagesLoaded(function(){
			  container.masonry({
				itemSelector:   "li",
			  });
			});
		  </script>';
	}
	//---------------------------------------------------------------------
	//add checkbox control 
	//---------------------------------------------------------------------
	public static function add_media_uploader($name, $default_image)
	{		
		wccp_pro_save_setting($name, $default_image);
		
		$choosed_option_value = self::wccp_pro_get_setting($name);
		
		include 'media_uploader_script.php';
	}
	//---------------------------------------------------------------------
	//add checkbox control 
	//---------------------------------------------------------------------
	public static function add_checkbox($name , $behind_text , $default_value, $js_function)
	{
		
		wccp_pro_save_setting($name, $default_value);
		
		$choosed_option_value = self::wccp_pro_get_setting($name);
		
		//if(!isset($choosed_option_value) || $choosed_option_value == '') $choosed_option_value = $default_value;
		
		echo '<div class="col-md-4 col-xs-12 row">';
		
		echo '<label class="checkbox" for="checkbox1">';
		
		echo '<input data-toggle="checkbox" type="checkbox" '.$js_function.' name="'.$name.'" id="'.$name.'" value="checked" ' .$choosed_option_value.'>';
		
		echo '<font face="Tahoma" size="2">'. $behind_text .'</font>';
		
		echo '</label></div>';
	}

}//Class End
?>