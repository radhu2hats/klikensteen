<?php
$pluginsurl = plugins_url( '', __FILE__ );

$wccp_pro_settings = wccp_pro_read_options();

function wccp_pro_save_setting($name, $default_value)
{
	$wccp_pro_settings = wccp_pro_read_options();
	
	if (!array_key_exists("$name",$wccp_pro_settings) && !isset( $_POST['Save_settings'] ) && isset( $_POST['Restore_defaults'] ))
	{
		$option_new_value = $default_value;
		
		$wccp_pro_settings["$name"] = $option_new_value;
		
		update_option( 'wccp_pro_settings' , $wccp_pro_settings );
	}
	
	if (!array_key_exists("$name",$wccp_pro_settings) && !isset( $_POST['Save_settings'] ) && !isset( $_POST['Restore_defaults'] ))
	{
		$option_new_value = $default_value;
		
		$wccp_pro_settings["$name"] = $option_new_value;
		
		update_option( 'wccp_pro_settings' , $wccp_pro_settings );
	}
	
	if(isset( $_POST['Save_settings'] ))
	{
		$wccp_pro_settings = wccp_pro_read_options();
		
		$option_new_value = '';
		
		if(isset($_POST["$name"])) $option_new_value = $_POST["$name"];
		
		if (!isset($_POST["$name"]) && $wccp_pro_settings["$name"] != '') $option_new_value = $wccp_pro_settings["$name"];
		
		if(!isset($_POST["$name"]) && $wccp_pro_settings["$name"] == 'checked') $option_new_value = '';
		
		$wccp_pro_settings["$name"] = $option_new_value;
		
		update_option( 'wccp_pro_settings' , $wccp_pro_settings );
		
		$wccp_pro_settings = wccp_pro_read_options();
	}
}
//---------------------------------------------------------------------------------------------
//Function to read options from the database
//---------------------------------------------------------------------------------------------
function wccp_pro_read_options()
{
	if (get_option('wccp_pro_settings'))
		$wccp_pro_settings = get_option('wccp_pro_settings');
	else
		$wccp_pro_settings = array();

	return $wccp_pro_settings;
}
function wccp_pro_cover_images( $content ) {
	
	global $wccp_pro_settings;
	
	//if($wccp_pro_settings["mysite_rule"] == "Fast Watermark (Recommended)"){

		$regexp = '<img[^>]*>';

		if(preg_match_all("/$regexp/iU", $content, $matches, PREG_SET_ORDER)) {

			if( !empty($matches) ) {

				$srcUrl = get_permalink();

				for ($i=0; $i <= count($matches); $i++)
				{
					if (isset($matches[$i]) && isset($matches[$i][0]))
					{
						$tag = $matches[$i][0];
					}
					else
					{
						$tag = '';
					}
					$tag2 = '';
					
					echo htmlentities($tag, ENT_QUOTES) . '<br><br>';
					
					$tag2 = '<div class="cover-container">'.'<div class="glass-cover"></div>'.$tag.'</div>';
					
					$tag2 = '<div class="cover-container">'.
					''.$tag.'<h2><span>A Movie in the Park:<span class="spacer">&nbsp;</span><br><span class="spacer">&nbsp;</span>Kung Fu Panda</span></h2></div>';
					
					$content = str_replace($tag,$tag2,$content);

				}

			}
		}
	//}
	return $content;
}
////////////////////////////////////////////////////////////////////////
function wccp_pro_find_image_urls( $content ) {
	
	global $wccp_pro_settings;
	
	if($wccp_pro_settings["remove_img_urls"] == "Yes"){

	$regexp = '(href=\"http)(.*)(.jpg|.jpeg|.png)';

	if(preg_match_all("/$regexp/iU", $content, $matches, PREG_SET_ORDER)) {

		if( !empty($matches) ) {

			$srcUrl = get_permalink();

			for ($i=0; $i <= count($matches); $i++)
			{
				if (isset($matches[$i]) && isset($matches[$i][0]))

					$tag = $matches[$i][0];

				else

					$tag = '';

				$tag2 = '';

				$content = str_replace($tag,$tag2,$content);
			}
		}
	}
	}
	return $content;
}

////////////////////////////////////////////////////////////////////////
function wccp_pro_add_style() {
	global $wccp_pro_pluginsurl;
	echo '<link rel="stylesheet" id="wccp_pro_add_style"  href="'.$wccp_pro_pluginsurl.'/hide-saving.css" type="text/css" media="all" />';
}

////////////////////////////////////////////////////////////////////////
function wccp_pro_alert_message()
{
	global $wccp_pro_settings;
?>
	<div id='wccp_pro_mask'></div>
	<div id="wpcp-error-message" class="msgmsg-box-wpcp warning-wpcp hideme"><span>error: </span><?php echo $wccp_pro_settings['smessage'];?></div>
	<script>
	var timeout_result;
	function show_wccp_pro_message(smessage)
	{
		timeout = <?php echo $wccp_pro_settings['message_show_time'];?>*1000;

		if (smessage !== "" && timeout!=0)
		{
			var smessage_text = smessage;
			jquery_fadeTo();
			document.getElementById("wpcp-error-message").innerHTML = smessage_text;
			document.getElementById("wpcp-error-message").className = "msgmsg-box-wpcp warning-wpcp showme";
			clearTimeout(timeout_result);
			timeout_result = setTimeout(hide_message, timeout);
		}
		else
		{
			clearTimeout(timeout_result);
			timeout_result = setTimeout(hide_message, timeout);
		}
	}
	function hide_message()
	{
		jquery_fadeOut();
		document.getElementById("wpcp-error-message").className = "msgmsg-box-wpcp warning-wpcp hideme";
	}
	function jquery_fadeTo()
	{
		try {
			jQuery("#wccp_pro_mask").fadeTo("slow", 0.3);
		}
		catch(err) {
			//alert(err.message);
			}
	}
	function jquery_fadeOut()
	{
		try {
			jQuery("#wccp_pro_mask").fadeOut( "slow" );
		}
		catch(err) {}
	}
	</script>
	<style type="text/css">
	#wccp_pro_mask
	{
		position: absolute;
		bottom: 0;
		left: 0;
		position: fixed;
		right: 0;
		top: 0;
		background-color: #000;
		pointer-events: none;
		display: none;
		z-index: 10000;
		animation: 0.5s ease 0s normal none 1 running ngdialog-fadein;
		background: rgba(0, 0, 0, 0.4) none repeat scroll 0 0;
	}
	#wpcp-error-message {
	    direction: ltr;
	    text-align: center;
	    transition: opacity 900ms ease 0s;
		pointer-events: none;
	    z-index: 99999999;
	}
	.hideme {
    	opacity:0;
    	visibility: hidden;
	}
	.showme {
    	opacity:1;
    	visibility: visible;
	}
	.msgmsg-box-wpcp {
		border-radius: 10px;
		color: <?php echo $wccp_pro_settings['font_color'];?>;
		font-family: Tahoma;
		font-size: 11px;
		margin: 10px;
		padding: 10px 36px;
		position: fixed;
		width: 255px;
		top: 50%;
  		left: 50%;
  		margin-top: -10px;
  		margin-left: -130px;
  		-webkit-box-shadow: 0px 0px 34px 2px <?php echo $wccp_pro_settings['shadow_color'];?>;
		-moz-box-shadow: 0px 0px 34px 2px <?php echo $wccp_pro_settings['shadow_color'];?>;
		box-shadow: 0px 0px 34px 2px <?php echo $wccp_pro_settings['shadow_color'];?>;
	}
	.msgmsg-box-wpcp b {
		font-weight:bold;
		text-transform:uppercase;
	}
	.error-wpcp {<?php global $pluginsurl; ?>
		background:#ffecec url('<?php echo $pluginsurl ?>/images/error.png') no-repeat 10px 50%;
		border:1px solid #f5aca6;
	}
	.success {
		background:#e9ffd9 url('<?php echo $pluginsurl ?>/images/success.png') no-repeat 10px 50%;
		border:1px solid #a6ca8a;
	}
	.warning-wpcp {
		background:<?php echo $wccp_pro_settings['msg_color'];?> url('<?php echo $pluginsurl ?>/images/warning.png') no-repeat 10px 50%;
		border:1px solid <?php echo $wccp_pro_settings['shadow_color'];?>;
	}
	.notice {
		background:#e3f7fc url('<?php echo $pluginsurl ?>/images/notice.png') no-repeat 10px 50%;
		border:1px solid #8ed9f6;
	}
    </style>
	<input class="hideme" style="position:fixed" id="prntscr_disable_field" type="url" value="No Copy"><!-- This input used to replace any printscreen value with "No Copy" word in the clipboard -->
<?php
}
?>