<?php
//delete_option('wccp_pro_settings');
$wccp_pro_settings = wccp_pro_read_options();
//delete_option('wccp_pro_settings');
session_unset();
if ( isset( $_POST['Restore_defaults'] ) ) 
{
	delete_option('wccp_pro_settings');
}
?>
<style>
#aio_admin_main {
	text-align:left;
	direction:ltr;
	padding:10px;
	margin: 10px;
	background-color: #ffffff;
	border:1px solid #EBDDE2;
	display: relative;
	overflow: auto;
}
.inner_block{
	height: 370px;
	display: inline;
	min-width:770px;
}
#donate{
    background-color: #EEFFEE;
    border: 1px solid #66DD66;
    border-radius: 10px 10px 10px 10px;
    height: 58px;
    padding: 10px;
    margin: 15px;
    }
.text-font {
    color: #1ABC9C;
    font-size: 14px;
    line-height: 1.5;
    padding-left: 3px;
    transition: color 0.25s linear 0s;
}
.text-font:hover {
    opacity: 1;
    transition: color 0.25s linear 0s;
}
.simpleTabsContent{
	border: 1px solid #E9E9E9;
	padding: 4px;
}
div.simpleTabsContent{
	margin-top:0;
	border: 1px solid #E0E0E0;
    display: none;
    height: 100%;
    min-height: 400px;
    padding: 5px 15px 15px;
}
html {
	background: #FFFFFF;
}
.welling {
    background-color: rgba(255, 253, 255, 1);
    border-radius: 3px;
    min-height: 54px;
    padding: 5px;
    font-family: tahoma;
    font-size: 11px;
}
.inner-label {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: #f6f6f6;
    border-image: none;
    border-radius: 3px;
    border-style: solid;
    border-width: 1px 1px 1px 7px;
    font-family: tahoma;
    font-size: 11px;
    padding: 13px;
    text-align: center;
}
.controls_container {
    border-radius: 4px;
    min-height: 54px;
    padding-top: 7px;
    font-family: tahoma;
    font-size: 11px;
}
.welling p {
    font-family: tahoma;
    font-size: 11px;
}
.welling > span {
    display: table-cell;
    height: 44px;
    vertical-align: middle;
}
.textbox_custom{
    width: 80% !important;
}
</style>
<?php
$c = new wccp_pro_controls();

if($c -> wccp_pro_get_setting('show_rating_message') == 'Yes')
{
	echo wpccp_rate_us('https://wordpress.org/support/plugin/wp-content-copy-protector/reviews/?filter=5', '#191E23');
	
	echo "<p></p>";
}
?>
<form method="POST">
<input type="hidden" value="update" name="action">
<div class="simpleTabs">
<ul class="simpleTabsNavigation">
    <li><a href="#">Main Settings</a></li>
	<li><a href="#">Premium RightClick Protection</a></li>
	<li><a href="#">Premium Protection by CSS</a></li>
	<li><a href="#">Smart Protection</a></li>
	<li><a href="#">Watermark</a></li>
	<li><a href="#">Exclusion</a></li>
	<li><a href="#">Custom Settings</a></li>
    <li><a href="#">Help</a></li>
</ul>
<?php
//---------------------------------------------------------------------
//Basic Protection 
//---------------------------------------------------------------------
$c->open_tab();
	$c->add_tab_heading('<strong>Basic Protection (<font color="#008000">Basic Layer</font>):</strong>');
		$c->open_row();
	    $c->add_label("Disable Selection on");
	    $c->open_controls_container('');
			$c->add_checkbox('single_posts_protection' , 'Posts' , 'checked', '');
			$c->add_checkbox('home_page_protection' , 'HomePage' , 'checked', '');
			$c->add_checkbox('page_protection' , 'Static pages' , 'checked', '');
		$c->close_controls_container();
	    $c->add_help_container('To choose where to apply the protection');
	$c->close_row();
    $c->add_line();
    
    $c->open_row();
        $c->add_label("Selection disabled message");
        $c->open_controls_container('');
			$default_text = '<b>Alert:</b> Content is protected !!';
	        $c->add_textbox('smessage' , 'Selection disabled message here', 'col-md-7 col-xs-12 row', $default_text);
		$c->close_controls_container();
        $c->add_help_container('');
    $c->close_row();
    $c->add_line();
	
	$c->open_row();
        $c->add_label("CTRL + key disabled message");
        $c->open_controls_container('ctrl_message_container');
			$default_text = '<b>Alert:</b> You are not allowed to copy content or view source';
	        $c->add_textbox('ctrl_message' , 'Write a message for CTRL + keys', 'col-md-7 col-xs-12 row', $default_text);
		$c->close_controls_container();
        $c->add_help_container('Message for disable the keys  CTRL+A , CTRL+C , CTRL+X , CTRL+S or CTRL+V , CTRL+U');
    $c->close_row();
    $c->add_line();
    
	$c->open_row();
	    $c->add_label("Disable special keys");
	    $c->open_controls_container('');
			$c->add_checkbox('prntscr_protection' , 'Print Screen Key' , 'checked', '');
			$c->add_checkbox('ctrl_p_protection' , 'Ctrl+P Key' , 'checked', '');
			$c->add_checkbox('ctrl_s_protection' , 'Ctrl+S Key' , 'checked', '');
		$c->close_controls_container();
	    $c->add_help_container('Disable PrintScreen key & Printing key & Save page key<p>Note: this may not work on all browsers</p>');
	$c->close_row();
    $c->add_line();
	
	$c->open_row();
	    $c->add_label("Special keys disabled message");
	    $c->open_controls_container('');
			$default_text = 'You are not allowed to print or save this page!!';
	        $c->add_textbox('custom_keys_message' , 'Write a message for PrintScreen key', 'col-md-7 col-xs-12 row', $default_text);
		$c->close_controls_container();
	    $c->add_help_container('');
	$c->close_row();
    $c->add_line();
	
    $c->open_row();
        $c->add_label("Message inner style");
        $c->open_controls_container('');
	        $c->add_colorpicker('msg_color', 'Message background', '#ffecec');
	        $c->add_colorpicker('font_color', 'Font color', '#555555');
		$c->close_controls_container();
        $c->add_help_container('You may use CTRL+F5 after saving to preview the message');
    $c->close_row();
    $c->add_line();
    
    $c->open_row();
        $c->add_label("Message outer style");
        $c->open_controls_container('');
	        $c->add_colorpicker('border_color', 'Border color', '#f5aca6');
	        $c->add_colorpicker('shadow_color', 'Shadow color', '#f2bfbf');
		$c->close_controls_container();
        $c->add_help_container('You may use CTRL+F5 after saving to preview the message');
    $c->close_row();
    $c->add_line();
    
    $c->open_row();
        $c->add_label("Message Show Time");
        $c->open_controls_container('');
			$show_array[] = array();
			$show_array["class"] = 'col-md-4 col-xs-12 row';$show_array["counter"] = 1;
			$show_array["tansparency_meter"] = 0;$show_array["behind_text"] = '';
			$c->add_slider('message_show_time', 3, 0, 10, 1, 'horizontal', $show_array);
		$c->close_controls_container();
        $c->add_help_container('By seconds, the alert message show time<p>0 value will hide the message');
    $c->close_row();

$c->close_tab();

//---------------------------------------------------------------------
// Copy Protection on RightClick 
//---------------------------------------------------------------------
$c->open_tab();
	$c->add_tab_heading('<strong>Copy Protection on RightClick (<font color="#008000">Premium Layer 1</font>):</strong>');
	$c->open_row();
	    $c->add_label("Disable RightClick on");
	    $c->open_controls_container('');
			$c->add_checkbox('right_click_protection_posts' , 'Posts' , 'checked', '');
			$c->add_checkbox('right_click_protection_homepage' , 'HomePage' , 'checked', '');
			$c->add_checkbox('right_click_protection_pages' , 'Static pages' , 'checked', '');
		$c->close_controls_container();
	    $c->add_help_container('To choose where to apply the protection');
	$c->close_row();
	$c->add_line();
	
	$c->open_row();
	    $c->add_label("Disables RightClick for HTML tags");
	    $c->open_controls_container('');
			$c->add_checkbox('img' , 'Images ' , 'checked', '');
			$c->add_checkbox('a' , 'links' , 'checked', '');
			$c->add_checkbox('pb' , 'Text content' , 'checked', '');
			$c->add_checkbox('h' , 'Headlines' , 'checked', '');
			$c->add_checkbox('textarea' , 'Text area' , 'checked', '');
			$c->add_checkbox('input' , 'Text fields' , 'checked', '');
			$c->add_checkbox('emptyspaces' , 'Empty spaces' , 'checked', '');
			$c->add_checkbox('videos' , 'Videos (Not Recommended)' , '', '');
		$c->close_controls_container();
	    $c->add_help_container('Video protection is not recommended because video players are too different');
	$c->close_row();
	$c->add_line();
	
	$c->open_row();
	    $c->add_label("Right click disabled messages");
	    $c->open_controls_container('');
			$c->add_textbox('alert_msg_img' , 'For Images', 'col-md-4 col-xs-12 row', '<b>Alert:</b> Protected image');
			$c->add_textbox('alert_msg_a' , 'For Links', 'col-md-4 col-xs-12 row', '<b>Alert:</b> This link is protected');
			$c->add_textbox('alert_msg_pb' , 'For Text', 'col-md-4 col-xs-12 row', '<b>Alert:</b> Right click on text is disabled');
			$c->add_textbox('alert_msg_h' , 'For Headlines', 'col-md-4 col-xs-12 row', '<b>Alert:</b> Right click on headlines is disabled');
			$c->add_textbox('alert_msg_textarea' , 'For Text Area', 'col-md-4 col-xs-12 row', '<b>Alert:</b> Right click is disabled');
			$c->add_textbox('alert_msg_input' , 'For Text fields', 'col-md-4 col-xs-12 row', '<b>Alert:</b> Right click is disabled');
			$c->add_textbox('alert_msg_emptyspaces' , 'For Empty Spaces', 'col-md-4 col-xs-12 row', '<b>Alert:</b> Right click on empty spaces is disabled');
			$c->add_textbox('alert_msg_videos' , 'For videos', 'col-md-4 col-xs-12 row', '<b>Alert:</b> Right click on videos is disabled');
		$c->close_controls_container();
	    $c->add_help_container('&lt;b&gt;some text&lt;/b&gt; shows the text in <b>bold</b> format');
	$c->close_row();
	$c->add_line();
$c->close_tab();

//---------------------------------------------------------------------
//Protection by CSS Techniques 
//---------------------------------------------------------------------
$c->open_tab();
	$c->add_tab_heading('<strong>Protection by CSS Techniques (<font color="#008000">Premium Layer 2</font>):</strong>');
	$c->open_row();
        $c->add_label('Home Page Protection by CSS');
        $c->open_controls_container('');
	        $options_array = array('Yes','No');
	        $default_value = 'Yes';
	        $c->add_dropdown('home_css_protection', $options_array, $default_value);
		$c->close_controls_container();
		$c->add_help_container('Protect your Homepage by CSS tricks');
	$c->close_row();
    $c->add_line();
	
	$c->open_row();
        $c->add_label('Posts Protection by CSS');
        $c->open_controls_container('');
	        $options_array = array('Yes','No');
	        $default_value = 'Yes';
	        $c->add_dropdown('posts_css_protection', $options_array, $default_value);
		$c->close_controls_container();
		$c->add_help_container('Protect your single posts by CSS tricks');
	$c->close_row();
	$c->add_line();
    
    $c->open_row();
        $c->add_label('Pages Protection by CSS');
        $c->open_controls_container('');
	        $options_array = array('Yes','No');
	        $default_value = 'Yes';
	        $c->add_dropdown('pages_css_protection', $options_array, $default_value);
		$c->close_controls_container();
		$c->add_help_container('Protect your static pages by CSS tricks');
	$c->close_row();
		
	$c->open_row();
	    $c->add_label("Add custom CSS code");
	    $c->open_controls_container('custom_css_code_container');
			$bottom_hint = '';
			$default_value = "<style><!-- Start your code after this line -->\n \n<!-- End your code before this line --></style>";
			$c->add_textarea('custom_css_code', 'Insert your custom code here', 'col-md-10 col-xs-12 row',$bottom_hint , $default_value);
		$c->close_controls_container();
	    $c->add_help_container('');
	$c->close_row();
$c->close_tab();

//---------------------------------------------------------------------
// Smart phones protection
//---------------------------------------------------------------------
$c->open_tab();
$c->add_tab_heading('<strong>Smart phones protection (<font color="#008000">Premium Layer 3</font>):</strong>');
	$c->open_row();
	    $c->add_label("Auto overlay a transparent image over the real images on:");
	    $c->open_controls_container('');
			$c->add_checkbox('protection_overlay_posts' , 'Posts' , '', '');
			$c->add_checkbox('protection_overlay_homepage' , 'HomePage' , '', '');
			$c->add_checkbox('protection_overlay_pages' , 'Static pages' , '', '');
		$c->close_controls_container();
	    $c->add_photo_help_container('images/tansparent.png', '');
	$c->close_row();
	$c->add_line();

	$c->open_row();
	    $c->add_label("Auto remove image attachment links");
	    $c->open_controls_container('Auto-remove-image-attachment-links');
			$options_array = array('Yes', 'No');
				$default_value = 'No';
				$c->add_dropdown('remove_img_urls', $options_array, $default_value);
				$c->add_bottom_hint("<p>Not recommended when you are using any lightbox plugin</p>");
		$c->close_controls_container();
	    $c->add_help_container('All images will be without hover links');
	$c->close_row();
	$c->add_line();
	
	$c->open_row();
	    $c->add_label("Action when JavaScript is disabled");
	    $c->open_controls_container('hotlinking_rule4');
			$options_array = array('Nothing', 'Watermark all');
			$default_value = 'Nothing';
			$c->add_dropdown('no_js_action', $options_array, $default_value);
		$c->close_controls_container();
	    $c->add_help_container('The browser will do it if the user disable JavaScript');
	$c->close_row();

$c->close_tab();
//---------------------------------------------------------------------
//Protection by watermarking
//---------------------------------------------------------------------
$c->open_tab();
	$c->add_tab_heading('<strong>Watermark your images with full control (<font color="#008000">Premium Layer 4</font>):</strong>');
	if (!extension_loaded('gd') && !function_exists('gd_info')) {
		//echo "PHP GD library is NOT installed on your web server";
		$c->add_section('Warning: PHP GD library is NOT installed on your web server','red');
	}
	//$c->add_section('Default watremark','#1ABC9C');
	$c->open_row();
	    $c->add_label("Hotlinking Rule");
	    $c->open_controls_container('hotlinking_rule');
			$options_array = array('Watermark', 'No Action');
				$default_value = 'Watermark';
				$c->add_dropdown('hotlinking_rule', $options_array, $default_value);
		$c->close_controls_container();
	    $c->add_help_container('Action when a thief copy your images to his site or try to download them');
	$c->close_row();
	$c->add_line();
	
	$c->open_row();
	    $c->add_label("My Site Rule");
	    $c->open_controls_container('my_site_rule');
			$options_array = array('Watermark', 'No Action');
				$default_value = 'No Action';
				$c->add_dropdown('mysite_rule', $options_array, $default_value);
		$c->close_controls_container();
	    $c->add_help_container('What will happen to images on my site');
	$c->close_row();
	$c->add_line();
	
	$c->open_row();
	    $c->add_label("Watermark logo");
	    $c->open_controls_container('Watermark-logo');
			$pluginsurl = plugins_url( '', __FILE__ );
			$default_image = $pluginsurl . '/images/testing-logo.png';
			$c->add_media_uploader('dw_logo',$default_image);
		$c->close_controls_container();
	    $c->add_help_container('You can use a transparent logo in a png format<br>Best size: 128x128 px');
	$c->close_row();
	$c->add_line();
	
	$c->open_row();
	    $c->add_label("Logo Margins");
	    $c->open_controls_container('margin_factors');
			$c->open_row();
			$show_array[] = array();
				$c->add_inner_label("From Top");
				$show_array["class"] = 'col-md-4 col-xs-12 row';$show_array["counter"] = 1;
				$show_array["tansparency_meter"] = 0;$show_array["behind_text"] = '%';
				$c->add_slider('dw_margin_top_factor', 50, 1, 100, 1, 'horizontal', $show_array);//Text size
			$c->close_row();
			
			$c->open_row();
				$c->add_inner_label("From Left");
				$show_array[] = array();
				$show_array["class"] = 'col-md-4 col-xs-12 row';$show_array["counter"] = 1;
				$show_array["tansparency_meter"] = 0;$show_array["behind_text"] = '%';
				$c->add_slider('dw_margin_left_factor', 50, 1, 100, 2, 'horizontal', $show_array);//Text size
			$c->close_row();
		$c->close_controls_container();
	    $c->add_photo_help_container('images/logo-positioning.png', '');
	$c->close_row();
	$c->add_line();
	
	$c->open_row();
	    $c->add_label("Watermark Central text");
	    $c->open_controls_container('Watermark-text');
			$default_text = 'WATERMARKED';
			$c->add_textbox('dw_text' , 'Watermark text', 'col-md-6 col-xs-12 row', $default_text);
			$c->add_colorpicker('dw_font_color', 'Font color', '#000000');
		$c->close_controls_container();
	    $c->add_help_container('Write a short text');
	$c->close_row();
	$c->add_line();
	
	$c->open_row();
	    $c->add_label("Watermark text position");
	    $c->open_controls_container('watermark-position');
			$options_array = 
			array(
				array('top-left','top-center','top-right','center-left','center-center','center-right','bottom-left','bottom-center','bottom-right'),
				array('Top Left','Top Center','Top Rright','Center Left','Center','Center Right','Bottom Left','Bottom Center','Bottom Right')
			);
			$c->add_image_picker('dw_position', '', $options_array, 'images/img-picker-1', 'center-center');
	    $c->close_controls_container();
		$c->add_help_container('Choose a watermark text position');
	$c->close_row();
	$c->add_line();
	
	$c->open_row();
	    $c->add_label("Central text font size");
	    $c->open_controls_container('Watermark-font-size');
			$show_array[] = array();
			$show_array["class"] = 'col-md-4 col-xs-12 row';$show_array["counter"] = 1;
			$show_array["tansparency_meter"] = 0;$show_array["behind_text"] = '%';
			//add_slider($name, $default_value, $min, $max, $factor, $orientation, $show_array)
			$c->add_slider('dw_font_size_factor', 90, 1, 100, 1, 'horizontal', $show_array);//Text size
		$c->close_controls_container();
	    $c->add_help_container('Depend on image size');
	$c->close_row();
	$c->add_line();
	
	$c->open_row();
	    $c->add_label("Watermark Repeated text");
	    $c->open_controls_container('Watermark-r-text');
			$default_text = 'your-site.com';
			$default_text = $_SERVER["SERVER_NAME"];
			$c->add_textbox('dw_r_text' , 'Watermark text', 'col-md-6 col-xs-12 row', $default_text);
			$c->add_colorpicker('dw_r_font_color', 'Font color', '#efefef');
		$c->close_controls_container();
	    $c->add_help_container('Repeated as a grid on the image');
	$c->close_row();
	$c->add_line();
	
	$c->open_row();
	    $c->add_label("Repeated text font size");
	    $c->open_controls_container('repeated-text-font-size');
			$show_array[] = array();
			$show_array["class"] = 'col-md-4 col-xs-12 row';$show_array["counter"] = 1;
			$show_array["tansparency_meter"] = 0;$show_array["behind_text"] = '%';
			//add_slider($name, $default_value, $min, $max, $factor, $orientation, $show_array)
			$c->add_slider('dw_r_font_size_factor', 55, 1, 100, 1, 'horizontal', $show_array);//Text size
		$c->close_controls_container();
	    $c->add_help_container('Depend on image size');
	$c->close_row();
	$c->add_line();
	
	$c->open_row();
	    $c->add_label("Text transparency");
	    $c->open_controls_container('Watermark-transparency');
			$show_array[] = array();
			$show_array["class"] = 'col-md-4 col-xs-12 row';$show_array["counter"] = 1;
			$show_array["tansparency_meter"] = 1;$show_array["behind_text"] = '';
			$c->add_slider('dw_text_transparency', 65, 1, 100, 1, 'horizontal', $show_array);//Text transparency
			$c->add_inner_label("Rotation");
			$c->add_textbox('dw_rotation', 'Rotation value', 'col-md-4 col-xs-12 row', '40');//Text Rotation
			// $c->add_filedropdown();
		$c->close_controls_container();
	    $c->add_help_container('Rotation value + or - (0 to 360)');
	$c->close_row();
	$c->add_line();
	
	$c->open_row();
	    $c->add_label("Watermark image filter");
	    $c->open_controls_container('Watermark image filter');
			$options_array = array('Blur','Grayscale','Negate','Britness','None');
			$default_value = 'None';
			$c->add_dropdown('dw_imagefilter', $options_array, $default_value);
		$c->close_controls_container();
	    $c->add_help_container('Chosse any filter you want');
	$c->close_row();
	$c->add_line();
	
	$c->open_row();
	    $c->add_label("Signature");
	    $c->open_controls_container('watermark_effect_3');
			$default_text = 'This image is protected';
			$c->add_textbox('dw_signature' , 'Signature', 'col-md-6 col-xs-12 row', $default_text);
		$c->close_controls_container();
	    $c->add_help_container('Will added at the bottom area of any image');
	$c->close_row();
	$c->add_line();
$c->close_tab();
//---------------------------------------------------------------------
// Exclude URLs (pages or posts) form protection
//---------------------------------------------------------------------
$c->open_tab();
$c->add_tab_heading('<strong>Exclude URLs (pages or posts) form protection:</strong>');
	$c->open_row();
	    $c->add_label("URL Exclude List");
	    $c->open_controls_container('url_exclude_list');
			$bottom_hint = "Example: http://www.mysite.com/mypage.html";
			$c->add_textarea('url_exclude_list', 'Exclude list', 'col-md-10 col-xs-12 row',$bottom_hint, '');
		$c->close_controls_container();
	    $c->add_help_container("Please enter URL's line by line<p><b>Note:</b> Watermarking is out of exclusion</p>");
	$c->close_row();
	
	$c->open_row();
	    $c->add_label("Selection Exclude by class name");
	    $c->open_controls_container('selection_exclude_classes');
			$bottom_hint = "Example: class1";
			$c->add_textarea('selection_exclude_classes', 'Exclude list', 'col-md-10 col-xs-12 row',$bottom_hint, '');
		$c->close_controls_container();
	    $c->add_help_container("Please excludeed calsses from selection line by line<p><b>Note:</b> This type of exclusion has some special rules</p>");
	$c->close_row();

$c->close_tab();

//---------------------------------------------------------------------
//Custom Settings
//---------------------------------------------------------------------
$c->open_tab();
$c->add_tab_heading('<strong>Custom Settings:</strong>');

    $c->open_row();
        $c->add_label("Exclude Admin from protection");
        $c->open_controls_container('');
			$options_array = array('Yes','No');
			$default_value = 'No';
			$c->add_dropdown('exclude_admin_from_protection', $options_array, $default_value);
		$c->close_controls_container();
        $c->add_help_container('If Yes, The protection functions will be inactive for the admin when he is logged in');
    $c->close_row();
	$c->add_line();
	
	$c->open_row();
        $c->add_label('Show the plugin icon in the top admin bar');
        $c->open_controls_container('plugin-icon-top-admin-bar');
	        $options_array = array('Yes','No');
	        $default_value = 'Yes';
	        $c->add_dropdown('show_admin_bar_icon', $options_array, $default_value);
		$c->close_controls_container();
	$c->add_help_container('Used for going to plugin settings page fast');
    $c->close_row();
	
	$c->open_row();
        $c->add_label('Show the plugin rating bar');
        $c->open_controls_container('plugin-rating-bar');
	        $options_array = array('Yes','No');
	        $default_value = 'Yes';
	        $c->add_dropdown('show_rating_message', $options_array, $default_value);
		$c->close_controls_container();
	$c->add_help_container('Used for show/hide plugin rating page');
    $c->close_row();
$c->close_tab();

//---------------------------------------------------------------------
//About tab
//---------------------------------------------------------------------
$c->open_tab();
//echo '<img style="float:left" src="http://www.wp-buy.com/wp-content/uploads/2015/05/wp-buy-new-logo.png">';
$c->add_tab_heading('<strong>Contact US</strong>');
	$c->open_row();
	    $c->add_label("Getting Support:");
	    $c->open_controls_container('Getting-Support');
			echo '<img style="" src="'.$pluginsurl.'/images/wp-buy-new-logo.png"><p></p>';
			echo "<font size=4pt>";
			echo "We would love to hear from you! Please write any question you want and we will get in touch with you shortly.";
			echo "<p></p>Have your own issue? Support Center will help you:";
			echo '<p></p><a class=" current " href="http://www.wp-buy.com/support-center/">Create A New Ticket *</a>';
			echo "</font>";
		$c->close_controls_container();
	$c->close_row();

$c->close_tab();
?>
</div><!-- simple tabs div end -->
<p align="right">
<input type="submit" class="btn btn-default" value="Restore defaults" style="width: 193; height: 29;" name="Restore_defaults">&nbsp;&nbsp;
<input type="button" class="btn btn-warning" alt="Use CTRL+F5 after saving" onclick="show_wccp_pro_message('This is a preview message (save changes first)');" value="Preview alert message" style="width: 193; height: 29;" name="B5">&nbsp;&nbsp; 
<input class="btn btn-success" type="submit" value="   Save Settings   " name="Save_settings" style="width: 193; height: 29;">&nbsp;&nbsp;</p>
</form>
<div class="msgmsg-box-wpcp warning-wpcp hideme" id="wpcp-error-message"><b>Alert: </b>Content is protected !!</div>


<style type="text/css">
	#wpcp-error-message {
	    direction: ltr;
	    text-align: center;
	    transition: opacity 900ms ease 0s;
	    z-index: 99999999;
	}
	.hideme {
    	opacity:0;
    	visibility: hidden;
	}
	.showme {
    	opacity:1;
    	visibility: visible;
	}
	.enable-me{
		
	}
	.disable-me{
	
	}
	.msgmsg-box-wpcp {
		border-radius: 10px;
		color: <?php echo $wccp_pro_settings['font_color'];?>;
		font-family: Tahoma;
		font-size: 11px;
		margin: 10px;
		padding: 10px 36px;
		position: fixed;
		width: 255px;
		top: 50%;
  		left: 50%;
  		margin-top: -10px;
  		margin-left: -130px;
  		-webkit-box-shadow: 0px 0px 34px 2px <?php echo $wccp_pro_settings['shadow_color'];?>;
		-moz-box-shadow: 0px 0px 34px 2px <?php echo $wccp_pro_settings['shadow_color'];?>;
		box-shadow: 0px 0px 34px 2px <?php echo $wccp_pro_settings['shadow_color'];?>;
	}
	.msgmsg-box-wpcp span {
		font-weight:bold;
		text-transform:uppercase;
	}
	.error-wpcp {<?php global $pluginsurl; ?>
		background:#ffecec url('<?php echo $pluginsurl ?>/images/error.png') no-repeat 10px 50%;
		border:1px solid #f5aca6;
	}
	.success {
		background:#e9ffd9 url('<?php echo $pluginsurl ?>/images/success.png') no-repeat 10px 50%;
		border:1px solid #a6ca8a;
	}
	.warning-wpcp {
		background:<?php echo $wccp_pro_settings['msg_color'];?> url('<?php echo $pluginsurl ?>/images/warning.png') no-repeat 10px 50%;
		border:1px solid <?php echo $wccp_pro_settings['shadow_color'];?>;
	}
	.notice {
		background:#e3f7fc url('<?php echo $pluginsurl ?>/images/notice.png') no-repeat 10px 50%;
		border:1px solid #8ed9f6;
	}
</style>

<?php
########## function creation ##################

function wpccp_rate_us($plugin_url, $box_color='#1D1F21'){
	
	$ret = '
	<style type="text/css">
	
	.rate_box{
		width:700px;
		background-color:'.$box_color.';
		color:#ffffff;
		margin:10px;
		-webkit-border-radius: 7px;
		-moz-border-radius: 7px;
		border-radius: 7px;
		padding:5px;
		direction: ltr;

	}
	.rating {
	  unicode-bidi: bidi-override;
	  direction: rtl;
	  
	  
	}
	.link_wp{
		
		color:#EDAE42 !important
	}
	.rating > span {
	  display: inline-block;
	  position: relative;
	  width: 1.1em;
	  font-size:22px;
	}
	.rating > span:hover:before,
	.rating > span:hover ~ span:before {
	   content: "\2605";
	   position: absolute;
	   color:yellow;
	}
	</style>';
	
	$ret .= '<div class="row rate_box">
<div class="col-md-8">
<p>
<strong>Do you like this plugin?</strong><br /> Please take a few seconds to <a class="link_wp" href="'.$plugin_url.'" target="_blank">rate it on WordPress.org!</a></p>
</div>
<div class="col-md-4">
<div class="rating">';

	for($r=1; $r<=5; $r++)
	{
		
		$ret .= '<span onclick="window.open(\''.$plugin_url.'\',\'_blank\')">☆</span>';
	}

$ret .= '</div>
</div>
</div>';
return $ret;
}


//---------------------------------------------------------------------------------------------
//Hotlinking
//---------------------------------------------------------------------------------------------
if(isset( $_POST['Save_settings'] ))
{
	if($_POST['hotlinking_rule'] == "Watermark" || $_POST['mysite_rule'] == "Watermark")
	{
		wccp_pro_modify_htaccess();
	}
	if($_POST['hotlinking_rule'] == "No Action" && $_POST['mysite_rule'] == "No Action")
	{
		wccp_pro_clear_htaccess();
	}
}
if ( isset( $_POST['Restore_defaults'] ) ) 
{
	wccp_pro_modify_htaccess();
	
	$wccpadminurl = get_admin_url();
	
	$settings_url = $wccpadminurl.'options-general.php?page=wccp-options-pro';
	
	header("Location: ".$settings_url);
}
?>