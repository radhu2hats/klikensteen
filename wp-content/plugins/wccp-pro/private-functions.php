<?php
//---------------------------------------------------------------------------------------------
//Register libraries using new wordpress register_script & enqueue_script functions
//---------------------------------------------------------------------------------------------
$pluginsurl = plugins_url( '', __FILE__ );

$c = new wccp_pro_controls();

function wccp_pro_enqueue_scripts() {

	$pluginsurl = plugins_url( '', __FILE__ );
	
	$admincore = '';
	
	if (isset($_GET['page'])) $admincore = $_GET['page'];
	
	if( is_admin() && $admincore == 'wccp-options-pro') {
	
	wp_enqueue_script('jquery');
	
	wp_register_script('simpletabsjs', $pluginsurl.'/js/simpletabs_1.3.js');
	wp_enqueue_script('simpletabsjs');
	
	wp_register_style('simpletabscss', $pluginsurl.'/css/simpletabs.css');
	wp_enqueue_style('simpletabscss');
	
	wp_register_style('bootstrapcss', $pluginsurl.'/flat-ui/css/bootstrap.css');
	wp_enqueue_style('bootstrapcss');
	
	wp_register_style('flat-ui-css', $pluginsurl.'/flat-ui/css/flat-ui.css');
	wp_enqueue_style('flat-ui-css');
	
	wp_register_script('jquery-1.8.3.min.js', $pluginsurl.'/flat-ui/js/jquery-1.8.3.min.js');
	wp_enqueue_script('jquery-1.8.3.min.js');
	
	wp_register_script('jquery-ui-1.10.3.custom.min.js', $pluginsurl.'/flat-ui/js/jquery-ui-1.10.3.custom.min.js');
	wp_enqueue_script('jquery-ui-1.10.3.custom.min.js');
	
	wp_register_script('jquery.ui.touch-punch.min.js', $pluginsurl.'/flat-ui/js/jquery.ui.touch-punch.min.js');
	wp_enqueue_script('jquery.ui.touch-punch.min.js');
	
	wp_register_script('bootstrap.min.js', $pluginsurl.'/flat-ui/js/bootstrap.min.js');
	wp_enqueue_script('bootstrap.min.js');
	
	wp_register_script('bootstrap-select.js', $pluginsurl.'/flat-ui/js/bootstrap-select.js');
	wp_enqueue_script('bootstrap-select.js');
	
	wp_register_script('bootstrap-switch.js', $pluginsurl.'/flat-ui/js/bootstrap-switch.js');
	wp_enqueue_script('bootstrap-switch.js');
	
	wp_register_script('flatui-checkbox.js', $pluginsurl.'/flat-ui/js/flatui-checkbox.js');
	wp_enqueue_script('flatui-checkbox.js');
	
	wp_register_script('flatui-radio.js', $pluginsurl.'/flat-ui/js/flatui-radio.js');
	wp_enqueue_script('flatui-radio.js');
	
	wp_register_script('jquery.tagsinput.js', $pluginsurl.'/flat-ui/js/jquery.tagsinput.js');
	wp_enqueue_script('jquery.tagsinput.js');
	
	wp_register_script('jquery.placeholder.js', $pluginsurl.'/flat-ui/js/jquery.placeholder.js');
	wp_enqueue_script('jquery.placeholder.js');
	
	wp_register_script('image-picker.js', $pluginsurl.'/image-picker/image-picker.js');
	wp_enqueue_script('image-picker.js');
	
	wp_register_style('image-picker.css', $pluginsurl.'/image-picker/image-picker.css');
	wp_enqueue_style('image-picker.css');
	
	wp_enqueue_style( 'wp-color-picker' );
    wp_enqueue_script( 'my-script-handle', plugins_url('my-script.js', __FILE__ ), array( 'wp-color-picker' ), false, true );
	
	wp_enqueue_script('media-upload');
	wp_enqueue_script('thickbox');
	wp_enqueue_media();
	}
}
add_action('admin_enqueue_scripts', 'wccp_pro_enqueue_scripts');

function wccp_pro_enqueue_pages_scripts() {
	wp_enqueue_script('jquery');
}
if (!is_admin()) add_action("wp_enqueue_scripts", "wccp_pro_enqueue_pages_scripts", 11);
//---------------------------------------------------------------------------------------------
//Add and icon to the top admin bar
//---------------------------------------------------------------------------------------------
function wpccp_add_items($admin_bar)
{
	$pluginsurl = plugins_url( '', __FILE__ );
	
	$wccpadminurl = get_admin_url();
	
	//The properties of the new item. Read More about the missing 'parent' parameter below

    $args = array(
            'id'    => 'Protection',
            'title' => __('<img src="'.$pluginsurl.'/images/adminbaricon.png" style="vertical-align:middle;margin-right:5px;width: 22px;" alt="Protection" title="Protection" />Protection' ),
            'href'  => $wccpadminurl.'options-general.php?page=wccp-options-pro',
            'meta'  => array('title' => __('WP Content Copy Protection'),)
            );
 
    //This is where the magic works.
    
    $admin_bar->add_menu( $args);
}

if($c -> wccp_pro_get_setting('show_admin_bar_icon') == 'Yes')
{
	add_action('admin_bar_menu', 'wpccp_add_items',  40);
}

//---------------------------------------------------------------------------------------------
//Show settings page
//---------------------------------------------------------------------------------------------
function wccp_pro_options_page_pro()
{
	if( is_plugin_active( 'wp-content-copy-protector/preventer-index.php' ) )
	{
		echo '<p align="center" dir="ltr">&nbsp;</p>
				<p align="center" dir="ltr">&nbsp;</p>
				<p align="center" dir="ltr">&nbsp;</p>
				<p align="center" dir="ltr"><font size="5" color="#FF0000">Alert!</font></p>
				<p align="center" dir="ltr"><font size="5">The free version of WP Content Copy 
				Protection is still active</font></p>
				<p align="center" dir="ltr"><font size="5">Please deactivate it to start using the pro version</font></p>';
	}
	else
	{
		include 'admin_settings.php';
	}
}

//---------------------------------------------------------------------------------------------
//Make our function to call the WordPress function to add to the correct menu
//---------------------------------------------------------------------------------------------
function wccp_pro_add_options() {

	add_options_page('WP Content Copy Protection (pro)', 'WP Content Copy Protection (pro)', 'manage_options', 'wccp-options-pro', 'wccp_pro_options_page_pro');
}
add_action('admin_menu', 'wccp_pro_add_options');
?>