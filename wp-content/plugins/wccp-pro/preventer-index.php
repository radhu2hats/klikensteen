<?php
ob_start();
session_start();
/*
Plugin Name: WP Content Copy Protection & No Right Click (premium)

Plugin URI: http://www.wp-buy.com/

Description: This wp plugin protect the posts content from being copied by any other web site author , you dont want your content to spread without your permission!!

Version: 5.0.0.1

Author: wp-buy

Author URI: http://www.wp-buy.com/
*/

/////Version number changed at 2/4/2016/////

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = PucFactory::buildUpdateChecker(
    'http://www.wp-buy.com/wp-update-server-php7/?action=get_metadata&slug=wccp-pro',
    __FILE__,
	'wccp-pro'
);

//---------------------------------------------------------------------------------------------
//define all variables the needed alot
//---------------------------------------------------------------------------------------------
$wpccp_pluginsurl = plugins_url( '', __FILE__ );

include "common-functions.php";
include "controls-functions.php";
include "private-functions.php";
include "js_functions.php";
include "css_functions.php";
include "play_functions.php";

$wccp_pro_settings = wccp_pro_read_options();

$dw_query = '';

function wccp_pro_add_htaccess($insertion) {
    $htaccess_file = ABSPATH.'.htaccess';
	wccp_pro_insert_with_markers_htaccess($htaccess_file, 'wccp_image_protection', '');//This will always clear the old watermarking rules
    return wccp_pro_insert_with_markers_htaccess($htaccess_file, 'wccp_pro_image_protection', (array) $insertion);
}
//---------------------------------------------------------------------------------------------
//Register libraries using new wordpress register_script & enqueue_script functions
//---------------------------------------------------------------------------------------------
function wccp_pro_modify_htaccess() {
	
	$wccp_pro_settings = wccp_pro_read_options();
	$pluginsurl = plugins_url( '', __FILE__ );
	$url = site_url();
	$url = wccp_pro_get_domain($url);
	$hotlinking_rule_text = 'RewriteRule ^.*$ - [NC,L]';
	$mysite_rule_text = 'RewriteRule ^.*$ - [NC,L]';
	
	$type = 'dw';
	$dw_position = $wccp_pro_settings['dw_position'];
	$dw_text = $wccp_pro_settings['dw_text'];
	$dw_r_text = $wccp_pro_settings['dw_r_text'];
	$dw_font_color = $wccp_pro_settings['dw_font_color'];
	$dw_r_font_color = $wccp_pro_settings['dw_r_font_color'];
	$dw_font_size_factor = $wccp_pro_settings['dw_font_size_factor'];
	$dw_r_font_size_factor = $wccp_pro_settings['dw_r_font_size_factor'];
	$dw_text_transparency = $wccp_pro_settings['dw_text_transparency'];
	$dw_rotation = $wccp_pro_settings['dw_rotation'];
	$dw_imagefilter = $wccp_pro_settings['dw_imagefilter'];
	$dw_signature = $wccp_pro_settings['dw_signature'];
	$dw_logo = $wccp_pro_settings['dw_logo'];
	$dw_margin_left_factor = $wccp_pro_settings['dw_margin_left_factor'];
	$dw_margin_top_factor = $wccp_pro_settings['dw_margin_top_factor'];
	$upload_dir = wp_upload_dir();
	$basedir = $upload_dir['basedir'];  //   /home3/usexpert/hostgator-best-coupon.com/wp-content/uploads
	$home_path = get_home_path();
	
	$file_content = '<?php' . "\n";
	$file_content .= '$watermark_type = "' .$type. '";' . "\n";
	$file_content .= '$watermark_position = "' .$dw_position. '";' . "\n";
	$file_content .= '$watermark_r_text = "' .$dw_r_text. '";' . "\n";
	$file_content .= '$r_font_size_factor = "' .$dw_r_font_size_factor. '";' . "\n";
	$file_content .= '$watermark_text = "' .$dw_text. '";' . "\n";
	$file_content .= '$font_size_factor = "' .$dw_font_size_factor. '";' . "\n";
	$file_content .= '$pure_watermark_stamp_image = "' .$dw_logo. '";' . "\n";
	
	$file_content .= '$margin_left_factor = "' .$dw_margin_left_factor. '";' . "\n";
	$file_content .= '$margin_top_factor = "' .$dw_margin_top_factor. '";' . "\n";
	$file_content .= '$watermark_color = "' .$dw_font_color. '";' . "\n";
	$file_content .= '$watermark_r_color = "' .$dw_r_font_color. '";' . "\n";
	$file_content .= '$watermark_transparency = "' .$dw_text_transparency. '";' . "\n";
	$file_content .= '$watermark_rotation = "' .$dw_rotation. '";' . "\n";
	$file_content .= '$watermark_imagefilter = "' .$dw_imagefilter. '";' . "\n";
	$file_content .= '$watermark_signature = "' .$dw_signature. '";' . "\n";
	$file_content .= '$home_path = "' .$home_path. '";' . "\n";
	$file_content .= '?>';
	
	$plugin_dir_path = plugin_dir_path( __FILE__ );
	$file = $plugin_dir_path . 'watermarking-parameters.php';  // (Can write to this file)
	
	// Write the contents back to the file
	file_put_contents($file, $file_content);
	
	$dw_query = "type=dw&position=$dw_position&text=$dw_text&font_color=$dw_font_color&r_text=$dw_r_text&r_font_color=$dw_r_font_color&font_size_factor=$dw_font_size_factor&r_font_size_factor=$dw_r_font_size_factor&text_transparency=$dw_text_transparency&rotation=$dw_rotation&imagefilter=$dw_imagefilter&signature=$dw_signature&stamp=$dw_logo&margin_left_factor=$dw_margin_left_factor&margin_top_factor=$dw_margin_top_factor&home_path=$home_path";
	$dw_query = '';
	$hotlinking_rule = $wccp_pro_settings['hotlinking_rule'];
	if($hotlinking_rule == "Watermark"){
		$hotlinking_rule_text = 'RewriteRule ^(.*)\.(jpg|png|jpeg)$ ' . $pluginsurl . '/watermark.php?'. $dw_query . '&src=/$1.$2 [R=301,NC,L]';
	}else if ($hotlinking_rule == "No Action"){
		$hotlinking_rule_text = 'RewriteRule ^.*$ - [NC,L]';
	}
	
	$mysite_rule = $wccp_pro_settings['mysite_rule'];
	if($mysite_rule == "Watermark"){
		$mysite_rule_text = 'RewriteRule ^(.*)\.(jpg|png|jpeg)$ ' . $pluginsurl . '/watermark.php?'. $dw_query . '&src=/$1.$2 [R=301,NC,L]';
	}
	else
	{
		$mysite_rule_text = 'RewriteRule ^.*$ - [NC,L]';
	}
	
	$prevented_agents_rule_text = 'RewriteRule ^.*$ '. $pluginsurl . '/watermark.php [R=301,L]';
	
	$ruls[] = <<<EOT
	<IfModule mod_rewrite.c>
	RewriteEngine on
EOT;
	
	$ruls[] = <<<EOT
	RewriteCond %{HTTP_USER_AGENT} (PrintFriendly.com)
	$prevented_agents_rule_text
	
	RewriteCond %{QUERY_STRING} (wccp_pro_watermark_pass) [NC,OR]
	RewriteCond %{REQUEST_URI} (wp-content/plugins) [NC,OR]
	RewriteCond %{REQUEST_URI} (logo) [NC,OR]
	RewriteCond %{REQUEST_URI} (featured-listing-bg.png) [NC,OR]
	RewriteCond %{REQUEST_URI} (wp-content/themes) [NC]

	RewriteRule ^.*$ - [NC,L]
	
	# What happen to images on my site
	#RewriteCond %{HTTP_ACCEPT} (image|png) [NC]
	RewriteCond %{HTTP_REFERER} ^http(s)?://(www\.)?$url [NC,OR]
	RewriteCond %{HTTP_REFERER} ^(.*)$url [NC]
	$mysite_rule_text
	
	#Save as or Click on View image after right click or without any referer
	RewriteCond %{HTTP_ACCEPT} (text|html|application) [NC]
	$hotlinking_rule_text
	
	RewriteCond %{REQUEST_URI} \.(jpg|jpeg|png)$ [NC]
	RewriteCond %{REMOTE_ADDR} !^(127.0.0.1|162.144.5.62)$ [NC]
	RewriteCond %{REMOTE_ADDR} !^66.6.(32|33|36|44|45|46|40). [NC]
	RewriteCond %{HTTP_REFERER} !^http(s)?://(www\.)?(www.$url|$url|pinterest.com|tumblr.com|facebook.com|plus.google|twitter.com|googleapis.com|googleusercontent.com|ytimg.com|gstatic.com) [NC]
	RewriteCond %{HTTP_USER_AGENT} !(googlebot|msnbot|baiduspider|slurp|webcrawler|teoma|photon|facebookexternalhit|facebookplatform|pinterest|feedfetcher|ggpht) [NC]
	RewriteCond %{HTTP_USER_AGENT} !(photon|smush.it|akamai|cloudfront|netdna|bitgravity|maxcdn|edgecast|limelight|tineye) [NC]
	RewriteCond %{HTTP_USER_AGENT} !(developers|gstatic|googleapis|googleusercontent|google|ytimg) [NC]
	$hotlinking_rule_text
	
</ifModule>
EOT;

	wccp_pro_add_htaccess($ruls);
}
register_activation_hook( __FILE__, 'wccp_pro_modify_htaccess' );
add_action( 'upgrader_process_complete', 'wccp_pro_modify_htaccess',10, 2);

//---------------------------------------------------------------------------------------------
//Register libraries using new wordpress register_script & enqueue_script functions
//---------------------------------------------------------------------------------------------
function wccp_pro_clear_htaccess()
{
	$htaccess_file = ABSPATH.'.htaccess';
	
	wccp_pro_insert_with_markers_htaccess($htaccess_file, 'wccp_pro_image_protection', "");
}
register_deactivation_hook( __FILE__, 'wccp_pro_clear_htaccess' );

function wccp_pro_insert_with_markers_htaccess( $filename, $marker, $insertion ) {
    if (!file_exists( $filename ) || is_writeable( $filename ) ) {
        if (!file_exists( $filename ) ) {
            $markerdata = '';
        } else {
            $markerdata = explode( "\n", implode( '', file( $filename ) ) );
        }
 
        if ( !$f = @fopen( $filename, 'w' ) )
            return false;
 
        $foundit = false;
        if ( $markerdata ) {
            $state = true;
            foreach ( $markerdata as $n => $markerline ) {
                if (strpos($markerline, '# BEGIN ' . $marker) !== false)
                    $state = false;
                if ( $state ) {
                    if ( $n + 1 < count( $markerdata ) )
                        fwrite( $f, "{$markerline}\n" );
                    else
                        fwrite( $f, "{$markerline}" );
                }
                if (strpos($markerline, '# END ' . $marker) !== false) {
                    fwrite( $f, "# BEGIN {$marker}\n" );
                    if ( is_array( $insertion ))
                        foreach ( $insertion as $insertline )
                            fwrite( $f, "{$insertline}\n" );
                    fwrite( $f, "# END {$marker}\n" );
                    $state = true;
                    $foundit = true;
                }
            }
        }
        if (!$foundit) {
            fwrite( $f, "\n# BEGIN {$marker}\n" );
            foreach ( $insertion as $insertline )
                fwrite( $f, "{$insertline}\n" );
            fwrite( $f, "# END {$marker}\n" );
        }
        fclose( $f );
        return true;
    } else {
        return false;
    }
}

//---------------------------------------------------------------------------------------------
//Register libraries using new wordpress register_script & enqueue_script functions
//---------------------------------------------------------------------------------------------
function wccp_pro_get_domain($url)
{
	$nowww = preg_replace('/www\./','',$url);
	
	$domain = parse_url($nowww);
	
	preg_match("/[^\.\/]+\.[^\.\/]+$/", $nowww, $matches);
	
	if(count($matches) > 0)
	{
		return $matches[0];
	}
	else
	{
		return FALSE;
	}
}

//---------------------------------------------------------------------------------------------
//Returns true if $needle is a substring of $haystack
//---------------------------------------------------------------------------------------------
function wccp_pro_contains($search_in, $search_for)
{
    return strpos($search_in, $search_for) !== false;
}
//---------------------------------------------------------------------------------------------
//Register Main plugin Actions and Filters
//http://www.mysite.com/mypage.html
//http://www.mysite.com/myfolder/*
//---------------------------------------------------------------------------------------------
$self_url = wccp_pro_get_self_url();

$exclude_this_page = 'False';

$tag = '';

//$url_exclude_list = $wccp_pro_settings['url_exclude_list'];

$url_exclude_list = $c -> wccp_pro_get_setting('url_exclude_list');

// Processes \r\n's first so they aren't converted twice.
$url_exclude_list = str_replace("\\n", "\n", $url_exclude_list);

$self_url = trim($self_url);

$self_url = preg_replace('{/$}', '', $self_url);

$urlParts = parse_url($self_url);

if(isset($urlParts['scheme'])) $urlParts_scheme = $urlParts['scheme'] . '://'; else $urlParts_scheme = '';

if(isset($urlParts['host'])) $urlParts_host = $urlParts['host']; else $urlParts_host = '';

if(isset($urlParts['path'])) $urlParts_path = $urlParts['path']; else $urlParts_path = '';

if(isset($urlParts['query'])) $urlParts_query = '?' . $urlParts['query']; else $urlParts_query = '';

$self_url = $urlParts_scheme . $urlParts_host . $urlParts_path . $urlParts_query;

//echo $self_url;

$url_exclude_list = wccp_pro_multiexplode(array("," ," ", "\n", "|"),$url_exclude_list);

if( !empty($url_exclude_list) )
	{
		for ($i=0; $i <= count($url_exclude_list); $i++)
		{
			if (isset($url_exclude_list[$i]))
			{
				$tag = $url_exclude_list[$i];
				
				$tag = trim($tag);
				
				//$tag = rtrim($tag, "/");
				
				//echo '<br>' . $tag;
			}
			else
			{
				$tag = '';
			}
			if (wccp_pro_contains($tag, '/*')) //Bulk exclusion
			{
				$tag = str_replace("/*", "", $tag);
				
				if (wccp_pro_contains($self_url, $tag))
				{
					$exclude_this_page = 'True';
					
					break;
				}
			}
			else
			{
				if ($self_url == $tag || $self_url. '/' == $tag )
				{
					$exclude_this_page = 'True';
					
					break;
				}
			}
		}
	}

if($exclude_this_page != 'True')
{
	add_action('wp_head','wccp_pro_main_settings'); //Located on 
	
	add_action('wp_head','wccp_pro_right_click_premium_settings'); //Located on 
	
	add_action('wp_head','wccp_pro_css_settings'); //Located on 
	
	add_action('wp_footer','wccp_pro_alert_message'); //Located on 
	
	add_filter('body_class','wccp_pro_class_names'); //Located on play_functions.php
	
	add_action('wp_footer','wccp_pro_images_overlay_settings'); //Located on play_functions.php
	
	add_action('wp_footer','wccp_pro_videos_overlay_settings'); //Located on play_functions.php
	
	add_action('wp_head','wccp_pro_nojs_inject'); //Located on preventer-index.php
	
	add_filter( 'the_content', 'wccp_pro_find_image_urls'); //Located on common-functions.php
}

add_action( 'admin_footer', 'wccp_pro_alert_message' );


//---------------------------------------------------------------------------------------------
//Add plugin settings link to Plugins page
//---------------------------------------------------------------------------------------------
function wccp_pro_plugin_add_settings_link( $links ) {

	$settings_link = '<a href="options-general.php?page=wccp-options-pro">' . __( 'Settings' ) . '</a>';
	
	array_push( $links, $settings_link );
	
	return $links;
}

$plugin = plugin_basename( __FILE__ );

add_filter( "plugin_action_links_$plugin", 'wccp_pro_plugin_add_settings_link' );


//---------------------------------------------------------------------------------------------
//Function to get self url
//---------------------------------------------------------------------------------------------
function wccp_pro_get_self_url()
{ 
    $s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : ""; 
    $protocol = wccp_pro_strleft(strtolower($_SERVER["SERVER_PROTOCOL"]), "/").$s; 
    $port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]); 
    return $protocol."://".$_SERVER['SERVER_NAME'].$port.$_SERVER['REQUEST_URI']; 
}

function wccp_pro_strleft($s1, $s2) { return substr($s1, 0, strpos($s1, $s2)); }

//---------------------------------------------------------------------------------------------
//Multiexplode function
//---------------------------------------------------------------------------------------------
function wccp_pro_multiexplode($delimiters,$string) {
   
    $ready = str_replace($delimiters, $delimiters[0], $string);
    $launch = explode($delimiters[0], $ready);
    return  $launch;
}

//---------------------------------------------------------------------------------------------
//Add nojs action
//---------------------------------------------------------------------------------------------
function wccp_pro_nojs_inject()
{
	global $wccp_pro_settings;
	if ($wccp_pro_settings['no_js_action'] == 'Watermark all')
	{
		if (!isset($_SESSION["no_js"]))
		{
			$pluginsurl = plugins_url( '', __FILE__ );
			
			$nojs_page_url = $pluginsurl . '/no-js.php';
			
			$referrer = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
			
			$nojs_page_url = $nojs_page_url . "?referrer=" .$referrer;
			
			$st = "
				<!-- Redirect to another page (for no-js support) -->
				<noscript><meta http-equiv=\"refresh\" content=\"0;url=$nojs_page_url\"></noscript>
				
				<!-- Show a message -->
				<noscript>You dont have javascript enabled! Please download Google Chrome!</noscript>
			";

			echo $st;
		}
	}
}
if (isset($_SESSION["no_js"]))
{
	add_filter( 'the_content', 'wccp_pro_replace_image_urls');
}

//---------------------------------------------------------------------------------------------
//Replace image urls with nothing
//---------------------------------------------------------------------------------------------
function wccp_pro_replace_image_urls( $content ) {

	global $post;
	
	$wccp_pro_settings = wccp_pro_read_options();
	
	$dw_position = $wccp_pro_settings['dw_position'];
	$dw_text = $wccp_pro_settings['dw_text'];
		$dw_text = str_replace(" ","+",$dw_text);
	$dw_r_text = $wccp_pro_settings['dw_r_text'];
		$dw_r_text = str_replace(" ","+",$dw_r_text);
	$dw_font_color = $wccp_pro_settings['dw_font_color'];
	$dw_r_font_color = $wccp_pro_settings['dw_r_font_color'];
	$dw_font_size_factor = $wccp_pro_settings['dw_font_size_factor'];
	$dw_r_font_size_factor = $wccp_pro_settings['dw_r_font_size_factor'];
	$dw_text_transparency = $wccp_pro_settings['dw_text_transparency'];
	$dw_rotation = $wccp_pro_settings['dw_rotation'];
	$dw_imagefilter = $wccp_pro_settings['dw_imagefilter'];
	$dw_signature = $wccp_pro_settings['dw_signature'];
		$dw_signature = str_replace(" ","+",$dw_signature);
	$dw_logo = $wccp_pro_settings['dw_logo'];
	
	$dw_query = "type=dw&position=$dw_position&text=$dw_text&font_color=$dw_font_color&r_text=$dw_r_text&r_font_color=$dw_r_font_color&font_size_factor=$dw_font_size_factor&r_font_size_factor=$dw_r_font_size_factor&text_transparency=$dw_text_transparency&rotation=$dw_rotation&imagefilter=$dw_imagefilter&signature=$dw_signature&stamp=$dw_logo";
	
	$dw_query = str_replace("#","%23",$dw_query);
	
	$pluginsurl = plugins_url( '', __FILE__ );

	$regexp = '<img[^>]+src=(?:\"|\')\K(.[^">]+?)(?=\"|\')';

	//Watermark images inside the content
	if(preg_match_all("/$regexp/", $content, $matches, PREG_SET_ORDER))
	{
		if( !empty($matches) )
		{
			for ($i=0; $i <= count($matches); $i++)
			{
				if (isset($matches[$i]) && isset($matches[$i][0]))
				{
					$img_src = $matches[$i][0];
				}
				else
				{
					$img_src = '';
				}
				$url_parser = parse_url($img_src); //Array [scheme] => http    [host] => www.example.com    [path] => /foo/bar    [query] => hat=bowler&accessory=cane
				
				$img_file_path = $url_parser['path'];
				
				//$http = $pluginsurl . "/watermark.php?w=watermarksaveas.png&p=c&q=90&src=";
				
				$http = $pluginsurl . '/watermark.php?'. $dw_query . '&src=';

				$encrypted_img_src = $http . $img_file_path;

				$content = str_replace($img_src,$encrypted_img_src,$content);
			}
		}
	}
	$content = str_replace(']]>', ']]&gt;', $content);

return $content;
}

?>