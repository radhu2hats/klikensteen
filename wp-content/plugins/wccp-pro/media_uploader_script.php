<?php
include_once "media_uploader_functions.php";
?>

<script language="javascript">
var WORDPRESS_VER = "<?php echo get_bloginfo("version") ?>";
var RCS_ADMIN_URL = '<?php echo admin_url() ?>';

function rcs_addImage_<?php echo $name; ?>(btn_id){
	
	var imageFormIndex = (new String(btn_id)).split('_').reverse()[0];
	jQuery('#imageFormIndex').val(imageFormIndex);
	jQuery('#imageEditorGoal').val('slide_image');
	jQuery('html').addClass('Image');
	
	
	var frame;
	if (WORDPRESS_VER >= "3.5") {
		
		
		if (frame) {
			
			frame.open();
			return;
		}
		
		frame = wp.media();
		frame.on("select", function(){
			var attachment = frame.state().get("selection").first();
			var fileurl = attachment.attributes.url;
			
			jQuery('#<?php echo $name; ?>').val(fileurl);
			frame.close();
			var img = jQuery('#image_holder_<?php echo $name; ?> img');
			if(img){
				img.remove();
			}
			
			rcs_addMediumImage_<?php echo $name; ?>(fileurl);
		
		});
		frame.open();
	}
	else {
		tb_show("", "media-upload.php?type=image&amp;TB_iframe=true&amp;tab=library");
		return false;
	}
}

//---------------------------------------------------------
function rcs_addMediumImage_<?php echo $name; ?>(attch_url){
	jQuery.ajax({
		type: 'POST',
		url: RCS_ADMIN_URL + 'admin-ajax.php',
		data: {
			action: 'RCS_GET_MEDIUM_IMG_I',
			attch_url: encodeURIComponent(attch_url)
		},
		success: function(data){
			var res = (new String(data)).split('--++##++--');
			jQuery('#image_holder_<?php echo $name; ?>').append('<img  src="' + attch_url + '" id="slide_image_<?php echo $name; ?>" />');
		}
	});
}
//---------------------------------------------------------
function rcs_addLargeImage(attch_url){
	jQuery.ajax({
		type: 'POST',
		url: RCS_ADMIN_URL + 'admin-ajax.php',
		data: {
			action: 'RCS_GET_LARGE_IMG_I',
			attch_url: encodeURIComponent(attch_url)
		},
		success: function(data){
			var res = (new String(data)).split('--++##++--');
			jQuery('#watermark_holder').append('<img src="' + res[1] + '" id="watermark" />');
			jQuery('#watermark_id').val(res[0]);
			jQuery('#deleteWatermark').css('display', 'block');
		}
	});
}
</script>


<style>
.image_holder {
	max-width:180px;
	max-height:80px
}
#image_holder_dw_logo > img {
    border: 1px solid #f2f2f2;
    border-radius: 6px;
    box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.15) inset;
    height: auto;
    margin: auto;
    max-height: 79px;
    max-width: 180px;
    padding: 4px;
    vertical-align: middle;
    width: auto;
}
</style>

<div class="image_container<?php echo $name; ?>">
	<div class="col-md-6 col-xs-12 row">
		<div class="col-xs-12 row" style="padding-bottom: 5px;">
			<?php
			echo "<input type=\"text\" placeholder=\"Choose a png transparent image\" class=\"form-control textbox_custom\" name=\"$name\" id=\"$name\"   value=\"$choosed_option_value\" size=\"25\">";
			?>
		</div>
		<div class="col-xs-12 row" style="padding-bottom: 5px;">
			<button type="button" class="button-secondary" id="add_image_<?php echo $name; ?>" onclick="rcs_addImage_<?php echo $name; ?>(this.id)"><?php _e('Add image', 'rc_slider') ?></button>
		</div>
	</div>
	<?php
	
	$pluginsurl = plugins_url( '', __FILE__ );

	if($choosed_option_value == '') $choosed_option_value = $pluginsurl . '/images/no-logo-avaliable.gif?x=wccp_pro_watermark_pass';
	?>
	<div class="col-md-4 col-xs-12 row" style="padding-bottom: 5px;">
		<div class="image_holder" id="image_holder_<?php echo $name; ?>">
			<img id="slide_image_<?php echo $name; ?>" src="<?php echo $choosed_option_value; ?>?x=wccp_pro_watermark_pass">
		</div>
	</div>
</div>