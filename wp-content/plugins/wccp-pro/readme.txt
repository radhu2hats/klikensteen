=== WP Content Copy Protection & No Right Click (premium) ===
Contributors: wp-buy	
Tags: content, content copy protection, content protection, copy protection, prevent copy, protect blog, image protect, image protection, no right click, plagiarism, secure, theft
Requires at least: 3.5
Tested up to: 4.7.3
Stable tag: 5.0.0.1

This wp plugin protect the posts content from being copied by any other web site author , you dont want your content to spread without your permission!!


== Description ==
This wp plugin protect the posts content from being copied by any other web site author , you dont want your content to spread without your permission!!

**Improve your seo score in Google and Yahoo and other SE's**:
Our plugin  protect your content from being copied by any other web sites so your posts will still uniqe content, this is the best option for seo

**Don't Let Your Stories Go to web thief!**
The plugin will keep your posts and home page protected by multiple techniques (JavaScript + CSS), this techniques does not found in any other wordpress plugin and you will own it for free with this plugin

**Easy to Install**:
Read the installation steps to find that this plugin does not need any coding or theme editing, just use your mouse..

**The Pro Edition Features include:**
1. Protect your content from selection and copy.
2. No one can right click on images from your site if you want
3. Get full Control on Right click or context menu
4. Show alert messages, when user made right click on images, text boxes, links, plain text.. etc
5. Disable the keys  CTRL+A, CTRL+C, CTRL+X,CTRL+S or CTRL+V.
6. Advaced and easy to use control panel.    
7. Admin can exclude Home page Or Single posts from being copy protected
8. Admin can disable copy protection for admin users.
9. Aggressive image protection (its not easy or its impossible for expert users to steal your images !!)
10. Compatible with all major theme frameworks
11. Compatible with all major browsers


== Screenshots ==
1. WP Content Copy Protection premium admin page

== Installation Guide ==
**Installation steps**
1.Download the package.
2.Extract the contents of WP-Content-Copy-Protection.zip to wp-content/plugins/ folder  You should get a folder called WP-Content-Copy-Protection
3.Activate the Plugin in WP-Admin.
4.Goto Settings > **WP-Content-Copy-Protection** to configure options.
5.You will find **4 options** to protect your content,images,homepage and css protection. dont forget to **save** the changes before exit
Thanks!


== Changelog ==
==5.0.0.1==
Fix updater problem with new php7

==4.0.0.6==
Fix exclude urls problem

==4.0.0.5==
Fix alert when using hotkeys with textboxes

==4.0.0.4==
Fix confliction when activate the plugin without deactivating the free version

==4.0.0.2==
Fix watermarking home_path null value
Fix transparent image absulute url and make it relative

==4.0.0.1==
Add disable pritscreen option "works good with some browsers
Add option to enable or disable Ctrl+P option
Add option to enable or disable Ctrl+S option
Allow Message Show Time to be have zero value

=3.0.0.13=
Bug fix

=3.0.0.11=
Bug fix

=3.0.0.10=
Make watermarking image url without shown parameters

=3.0.0.9=
Prevent print screen with firefox

=3.0.0.8=
Fix transparent images watermarking problem

=3.0.0.7=
Fix Division by zero in error inside watermark.php file

=3.0.0.6=
Fix the scrollbar issue with safari

=3.0.0.4=
Fix the relative images path problem
Fix allow_url_fopen which is blocked by some servers

=3.0.0.3=
Admin can disable copy protection for logged in/registered users
disable the possible shortcut keys for copying the Text
You can also choose where this Plugin should work like All Pages (including Home Page and all other Pages & Posts) or Home Page or Custom Pages/Posts using the Settings Page options.
Multiple Text and Image Protection methods

=3.0.0.2=
Advanced Image Protection using Responsive Lightbox
Protect your Text and Images by Disabling the Mouse Right Click and Possible Shortcut Keys for Cut (CTRL+x), Copy (CTRL+c), Paste (CTRL+v), Select All(CTRL+a), View Source (CTRL+u) etc.

=3.0.0.1=
control the protection to be on users only (if admin here dont protect)
Option to Display Alert Message on Mouse Right Click.
Enable Right Click on Hyperlink Option Added
Right click problem fixed on static pages
New flat interface

=2.0.0.4=
<ul>
<li>Compatible with the new 4.2.1 version</li>
<li>Add coloring settins to colorize the alert message</li>
<li>Add Restore defaults Button</li>
</ul>
= 2.0.0.3 =
<ul>
<li>Adding adminbar link and icon redirecting you to the plugin settings page</li>
<li>Adding settings link into the plugins list page</li>
</ul>
= 2.0.0.2 =
<ul>
<li>Adding isset() function to all variables</li>
<li>Improving alert message</li>
<li>Fixing CTRL + U issue</li>
<li>Fixing CSS tricks</li>
</ul>
= 1.5.0.1 =
<ul>
<li>Fixing error (Warning: join(): Invalid arguments passed in /home/retailmakeover/public_html/wp-includes/post-template.php on line 478)</li>
</ul>
= 2.0.0.1 =
<ul>
<li>Admin can disable copy protection for logged in/admin users</li>
<li>disable the possible shortcut keys for copying the Text</li>
<li>You can also choose where this Plugin should work like All Pages (including Home Page and all other Pages & Posts) or Home Page or Custom Pages/Posts using the Settings Page options.</li>
<li>Multiple Text and Image Protection methods</li>
<li>Advanced Image Protection using Responsive Lightbox</li>
<li>Protect your Text and Images by Disabling the Mouse Right Click and Possible Shortcut Keys for Cut (CTRL+x), Copy (CTRL+c), Paste (CTRL+v), Select All(CTRL+a), View Source (CTRL+u) etc.</li>
<li>control the protection to be on users only (if admin here dont protect)</li>
<li>Option to Display Alert Message on Mouse Right Click.</li>
<li>Enable Right Click on Hyperlink Option Added</li>
<li>Right click problem fixed on static pages</li>
<li>New flat interface</li>
</ul>
= 1.0 =
<ul>
<li>initial version</li>
<li>static pages bug fixed</li>
<li>home page problem fixed</li>
<li>Add new Style</li>
</ul>