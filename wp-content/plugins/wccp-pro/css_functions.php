<?php
function wccp_pro_css_script()
{
?>
	<style>
.cover-container {
   border: 1px solid #DDDDDD;
   width: 100%;
   height: 100%;
   position: relative;
}
.glass-cover {
   float: left;
   position: relative;
   left: 0px;
   top: 0px;
   z-index: 1000;
   background-color: #92AD40;
   padding: 5px;
   color: #FFFFFF;
   font-weight: bold;
}
	.unselectable
	{
	-moz-user-select:none;
	-webkit-user-select:none;
	cursor: default;
	}
	html
	{
	-webkit-touch-callout: none;
	-webkit-user-select: none;
	-khtml-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	-webkit-tap-highlight-color: rgba(0,0,0,0);
	}
	img
	{
	-webkit-touch-callout:none;
	-webkit-user-select:none;
	}
	</style>
	<script id="wccp_pro_css_disable_selection" type="text/javascript">
	var e = document.getElementsByTagName('body')[0];
	if(e)
	{
		e.setAttribute('unselectable',on);
	}
	</script>
<?php
}
?>
<?php
function wccp_pro_css_inject()
{
	global $wccp_pro_settings;
	echo str_replace('\"', '"', $wccp_pro_settings['custom_css_code']);
}
?>