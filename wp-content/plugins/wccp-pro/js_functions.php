<?php
$pluginsurl = plugins_url( '', __FILE__ );

$wccp_pro_settings = wccp_pro_read_options();

global $wccp_pro_settings;

$tag = '';

$selection_exclude_classes = '';

if ( isset( $wccp_pro_settings['selection_exclude_classes'] ) ) 
{
	$selection_exclude_classes = $wccp_pro_settings['selection_exclude_classes'];
}

// Processes \r\n's first so they aren't converted twice.
$selection_exclude_classes = str_replace("\\n", "\n", $selection_exclude_classes);

$selection_exclude_classes = str_replace("\n", ",", $selection_exclude_classes);

$selection_exclude_classes = str_replace("\r", ",", $selection_exclude_classes);

$selection_exclude_classes = str_replace("|", ",", $selection_exclude_classes);

$selection_exclude_classes = str_replace(",,", ",", $selection_exclude_classes);

function wccp_pro_disable_Right_Click()
{
global $wccp_pro_settings;
?>
	<script id="wccp_pro_disable_Right_Click" type="text/javascript">
	//<![CDATA[
	document.ondragstart = function() { return false;}
	    function nocontext(e) {
			var excluded_classes = '' + '<?php global $selection_exclude_classes; echo $selection_exclude_classes; ?>';
			
			var class_to_exclude = '';
			
			class_to_exclude = e.target.className;
			
			if(class_to_exclude != '' && excluded_classes.includes(class_to_exclude))
			{
				target.style.cursor = "text";
				return true;
			}
			
	    	var exception_tags = 'NOTAG,';
	        var clickedTag = (e==null) ? event.srcElement.tagName : e.target.tagName;
	        //alert(clickedTag);
	        var checker = '<?php echo $wccp_pro_settings['img'];?>';
	        if ((clickedTag == "IMG" || clickedTag == "PROTECTEDIMGDIV") && checker == 'checked') {
	            if (alertMsg_IMG != "")show_wccp_pro_message(alertMsg_IMG);
	            return false;
	        }else {exception_tags = exception_tags + 'IMG,';}
			
			checker = '<?php echo $wccp_pro_settings['videos'];?>';
			if ((clickedTag == "VIDEO" || clickedTag == "EMBED") && checker == 'checked') {
	            if (alertMsg_VIDEO != "")show_wccp_pro_message(alertMsg_VIDEO);
	            return false;
	        }else {exception_tags = exception_tags + 'VIDEO,EMBED,';}
	        
	        checker = '<?php echo $wccp_pro_settings['a'];?>';
	        if ((clickedTag == "A" || clickedTag == "TIME") && checker == 'checked') {
	            if (alertMsg_A != "")show_wccp_pro_message(alertMsg_A);
	            return false;
	        }else {exception_tags = exception_tags + 'A,';}
	        
	        checker = '<?php echo $wccp_pro_settings['pb'];?>';
	        if ((clickedTag == "P" || clickedTag == "B" || clickedTag == "FONT" ||  clickedTag == "LI" || clickedTag == "UL" || clickedTag == "STRONG" || clickedTag == "OL" || clickedTag == "BLOCKQUOTE" || clickedTag == "TD" || clickedTag == "SPAN" || clickedTag == "EM" || clickedTag == "SMALL" || clickedTag == "I" || clickedTag == "BUTTON") && checker == 'checked') {
	            if (alertMsg_PB != "")show_wccp_pro_message(alertMsg_PB);
	            return false;
	        }else {exception_tags = exception_tags + 'P,B,FONT,LI,UL,STRONG,OL,BLOCKQUOTE,TD,SPAN,EM,SMALL,I,BUTTON,';}
	        
	        checker = '<?php echo $wccp_pro_settings['input'];?>';
	        if ((clickedTag == "INPUT" || clickedTag == "PASSWORD") && checker == 'checked') {
	            if (alertMsg_INPUT != "")show_wccp_pro_message(alertMsg_INPUT);
	            return false;
	        }else {exception_tags = exception_tags + 'INPUT,PASSWORD,';}
	        
	        checker = '<?php echo $wccp_pro_settings['h'];?>';
	        if ((clickedTag == "H1" || clickedTag == "H2" || clickedTag == "H3" || clickedTag == "H4" || clickedTag == "H5" || clickedTag == "H6" || clickedTag == "ASIDE" || clickedTag == "NAV") && checker == 'checked') {
	            if (alertMsg_H != "")show_wccp_pro_message(alertMsg_H);
	            return false;
	        }else {exception_tags = exception_tags + 'H1,H2,H3,H4,H5,H6,';}
	        
	        checker = '<?php echo $wccp_pro_settings['textarea'];?>';
	        if (clickedTag == "TEXTAREA" && checker == 'checked') {
	            if (alertMsg_TEXTAREA != "")show_wccp_pro_message(alertMsg_TEXTAREA);
	            return false;
	        }else {exception_tags = exception_tags + 'TEXTAREA,';}
	        
	        checker = '<?php echo $wccp_pro_settings['emptyspaces'];?>';
	        if ((clickedTag == "DIV" || clickedTag == "BODY" || clickedTag == "HTML" || clickedTag == "ARTICLE" || clickedTag == "SECTION" || clickedTag == "NAV" || clickedTag == "HEADER" || clickedTag == "FOOTER") && checker == 'checked') {
	            if (alertMsg_EmptySpaces != "")show_wccp_pro_message(alertMsg_EmptySpaces);
	            return false;
	        }
	        else
	        {
				//show_wccp_pro_message(exception_tags.indexOf(clickedTag));
	        	if (exception_tags.indexOf(clickedTag)!=-1)
	        	{
		        	return true;
		        }
	        	else
	        	return false;
	        }
	    }
	    var alertMsg_IMG = "<?php echo $wccp_pro_settings['alert_msg_img'];?>";
	    var alertMsg_A = "<?php echo $wccp_pro_settings['alert_msg_a'];?>";
	    var alertMsg_PB = "<?php echo $wccp_pro_settings['alert_msg_pb'];?>";
	    var alertMsg_INPUT = "<?php echo $wccp_pro_settings['alert_msg_input'];?>";
	    var alertMsg_H = "<?php echo $wccp_pro_settings['alert_msg_h'];?>";
	    var alertMsg_TEXTAREA = "<?php echo $wccp_pro_settings['alert_msg_textarea'];?>";
	    var alertMsg_EmptySpaces = "<?php echo $wccp_pro_settings['alert_msg_emptyspaces'];?>";
		var alertMsg_VIDEO = "<?php echo $wccp_pro_settings['alert_msg_videos'];?>";
	    document.oncontextmenu = nocontext;
	//]]>
	</script>
<?php
}?>
<?php
///////////////////////////////////////////////////////////
function wccp_pro_disable_prntscr_key()
{
	global $wccp_pro_settings;
	?>
	<script type="text/javascript">
	
	window.addEventListener("keyup", dealWithPrintScrKey, false);
	
	function dealWithPrintScrKey(e) 
	{
		// gets called when any of the keyboard events are overheard
		var prtsc = e.keyCode||e.charCode;

		if (prtsc == 44)
		{
			document.getElementById("prntscr_disable_field").className = "showme";
			
			var copyDiv = document.querySelector('#prntscr_disable_field');
			
			copyDiv.select();
			
			document.execCommand('Copy');

			var urlField = document.querySelector('#prntscr_disable_field');

			var range = document.createRange();
			// set the Node to select the "range"
			range.selectNode(urlField);
			// add the Range to the set of window selections
			window.getSelection().addRange(range);

			// execute 'copy', can't 'cut' in this case
			document.execCommand('copy');
			
			//window.getSelection().removeAllRanges();
			document.getElementById("prntscr_disable_field").className = "hideme";

			show_wccp_pro_message('<?php echo $wccp_pro_settings['custom_keys_message'];?>');
		}
	}
	</script>
	<style>
	@media print {
	body * { display: none !important;}
		body:after {
		content: "WARNING:  UNAUTHORIZED USE AND/OR DUPLICATION OF THIS MATERIAL WITHOUT EXPRESS AND WRITTEN PERMISSION FROM THIS SITE'S AUTHOR AND/OR OWNER IS STRICTLY PROHIBITED! CONTACT US FOR FURTHER CLARIFICATION.  "; }
	}</style>
<?php
}


///////////////////////////////////////////////////////////
function wccp_pro_disable_selection()
{
global $wccp_pro_settings;
?>
<script id="wccp_pro_disable_selection" type="text/javascript">
//<![CDATA[
var image_save_msg='You cant save images!';
	var no_menu_msg='Context menu disabled!';
	var smessage = "<?php echo $wccp_pro_settings['smessage'];?>";

function disable_hot_keys(e)
{
	var key2;
	
		if(window.event)
			  key2 = window.event.keyCode;     //IE
		else
			key2 = e.which;     //firefox (97)
	if (key2 == 123)//F12 chrome developer key disable
		{
			show_wccp_pro_message('<?php echo $wccp_pro_settings['custom_keys_message'];?>');
			return false;
		}
		
	var elemtype = e.target.tagName;
	
	elemtype = elemtype.toUpperCase();
	
	if (elemtype == "TEXT" || elemtype == "TEXTAREA" || elemtype == "INPUT" || elemtype == "PASSWORD" || elemtype == "SELECT")
	{
		elemtype = 'TEXT';
	}
	
	if (e.ctrlKey)
	{
		var key;
    
		if(window.event)
			  key = window.event.keyCode;     //IE
		else
			key = e.which;     //firefox (97)

		//if (key != 17) alert(key);
		if (elemtype!= 'TEXT' && (key == 97 || key == 65 || key == 67 || key == 99 || key == 88 || key == 120 || key == 26 || key == 85  || key == 86 || key == 43))
		{
			 show_wccp_pro_message('<?php echo $wccp_pro_settings['ctrl_message'];?>');
			 return false;
		}
		
		<?php if($wccp_pro_settings['ctrl_s_protection'] == 'checked')
		{ ?>
		
		var ctrl_s_option = '<?php echo $wccp_pro_settings['ctrl_s_protection']; ?>';
		
		if (key == 83 && ctrl_s_option == 'checked')//Ctrl+s 83
		{
			show_wccp_pro_message('<?php echo $wccp_pro_settings['custom_keys_message'];?>');
			return false;
		}<?php } ?>
		
		<?php if($wccp_pro_settings['ctrl_p_protection'] == 'checked')
		{ ?>
		var ctrl_p_option = '<?php echo $wccp_pro_settings['ctrl_p_protection']; ?>';
			 
		if (key == 80 && ctrl_p_option == 'checked')//Ctrl+p 80
		{
			show_wccp_pro_message('<?php echo $wccp_pro_settings['custom_keys_message'];?>');
			return false;
		}<?php } ?>
		
	else
		return true;
    }
}



"use strict";

document.addEventListener("keydown", function (event) {

  //console.log(event);

  //document.body.innerHTML = "\n    &nbsp;&nbsp;&nbsp;\n    <b>which: " + event.which + "</b>\n    <br>&nbsp;\n    <b>keyCode:</b> " + event.keyCode + "\n    <br>&nbsp;&nbsp;&nbsp;\n    <b>shiftKey:</b> " + event.shiftKey + "\n    </br>&nbsp;&nbsp;&nbsp;&nbsp;\n    <b>altKey:</b> " + event.altKey + "\n    <br>&nbsp;&nbsp;&nbsp;\n    <b>ctrlKey:</b> " + event.ctrlKey + "\n    <br>&nbsp;&nbsp;\n    <b>metaKey:</b> " + event.metaKey + "\n  ";

  if(event.shiftKey){
	  event = event || window.event;

        if (event.stopPropagation)
            event.stopPropagation();

        event.cancelBubble = true;
        return false;
	  //show_wccp_pro_message('fdgs');
  }
  
  });

<?php global $selection_exclude_classes; ?>
  
function disable_copy(e)
{
	var excluded_classes = '' + '<?php echo $selection_exclude_classes; ?>';
	
	var class_to_exclude = '';
	
	class_to_exclude = e.target.className;
	
	if(class_to_exclude != '' && excluded_classes.includes(class_to_exclude))
	{
		target.style.cursor = "text";
		return true;
	}
	
	//disable context menu when shift + right click is pressed
	var shiftPressed = 0;
	var evt = e?e:window.event;
	if (parseInt(navigator.appVersion)>3) {
		if (document.layers && navigator.appName=="Netscape")
			shiftPressed=(evt.modifiers-0>3);
		else
			shiftPressed=evt.shiftKey;
		if (shiftPressed) {
			if (smessage !== "") show_wccp_pro_message(smessage);
			var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
			if (isFirefox) {alert (smessage);}
			return false;
		}
	}
	
	if(e.which === 2 ){
	var clickedTag_a = (e==null) ? event.srcElement.tagName : e.target.tagName;
	   show_wccp_pro_message(smessage);
       return false;
    }
	var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
	var elemtype = e.target.nodeName;
	elemtype = elemtype.toUpperCase();
	var checker_IMG = '<?php echo $wccp_pro_settings['img'];?>';
	if (elemtype == "IMG" && checker_IMG == 'checked' && e.detail == 2) {show_wccp_pro_message(alertMsg_IMG);return false;}
    if (elemtype != "TEXT" && elemtype != "TEXTAREA" && elemtype != "INPUT" && elemtype != "PASSWORD" && elemtype != "SELECT" && elemtype != "EMBED")
	{
		if (smessage !== "" && e.detail == 2)
			show_wccp_pro_message(smessage);
		
		if (isSafari)
			return true;
		else
			return false;
	}	
}
function disable_copy_ie()
{
	var excluded_classes = '' + '<?php echo $selection_exclude_classes; ?>';
	
	var class_to_exclude = '';
	
	class_to_exclude = e.target.className;
	
	if(class_to_exclude != '' && excluded_classes.includes(class_to_exclude))
	{
		target.style.cursor = "text";
		return true;
	}
	
	var elemtype = window.event.srcElement.nodeName;
	elemtype = elemtype.toUpperCase();
	if (elemtype == "IMG") {show_wccp_pro_message(alertMsg_IMG);return false;}
	if (elemtype != "TEXT" && elemtype != "TEXTAREA" && elemtype != "INPUT" && elemtype != "PASSWORD" && elemtype != "SELECT" && elemtype != "OPTION" && elemtype != "EMBED")
	{
		//if (smessage !== "") show_wccp_pro_message(smessage);
		return false;
	}
}	
function reEnable()
{
	return true;
}
document.onkeydown = disable_hot_keys;
document.onselectstart = disable_copy_ie;
if(navigator.userAgent.indexOf('MSIE')==-1)
{
	document.onmousedown = disable_copy;
	document.onclick = reEnable;
}
function disableSelection(target)
{
    //For IE This code will work
    if (typeof target.onselectstart!="undefined")
    target.onselectstart = disable_copy_ie;
    
    //For Firefox This code will work
    else if (typeof target.style.MozUserSelect!="undefined")
    {target.style.MozUserSelect="none";}
    
    //All other  (ie: Opera) This code will work
    else
    target.onmousedown=function(){return false}
    target.style.cursor = "default";
}
//Calling the JS function directly just after body load
window.onload = function(){disableSelection(document.body);};
//]]>
</script>

<?php
}?>
<?php
function wccp_pro_images_overlay()
{
	?>
	<!--Smart protection techniques -->
	<script type="text/javascript">
	//jQuery(window).on('resize',function(){location.reload();});
	jQuery(function() {
		jQuery('IMG').load(function(e) {
			img_load(this);
		});
		
		jQuery('IMG').live('mouseenter touchstart',function(e) {
			img_load(this);
		});
	});

	function img_load(e)
	{
		<?php $pluginsurl = plugins_url( '', __FILE__ ); 
		global $wccp_pro_settings;
		?>
		var logourl = '<?php echo $wccp_pro_settings['dw_logo'];?>';
		var default_logourl = '<?php echo $pluginsurl . '/images/testing-logo.png'; ?>';
		var choosed_logourl = '';
		var useOnAllImages = true;
		// Preload the pixel
		var preload = new Image();
		if ( logourl == ''){
			choosed_logourl = default_logourl;
		}
		else
		{
			choosed_logourl = logourl;
		}
		
		// Only execute if this is not an overlay or skipped
	    var img = jQuery(e);
	    if (img.hasClass('protectionOverlay')) return;
	    if (!useOnAllImages && !img.hasClass('protectMe')) return;
		if (img.width() < 140 || img.hasClass('attachment-small-slider-thumb')) return;
		if(img.attr('src') == 'https://media.today.ng/main/wp-content/uploads/2015/01/today_logo2.svg') return; //must be customized by an option *****
	    // Get the real image's position, add an overlay
	    var pos = img.offset();
	    //var overlay_top1 = jQuery('<img width="' + img.width() + '" height="' + (img.height() + '" ></img>').css({border: '1px sloid #fff', position: 'absolute', zIndex: 9999999, left: pos.left, top: pos.top}).appendTo('body');
		//if($_POST['hotlinking_rule'] == "Watermark" || $_POST['mysite_rule'] == "Watermark")
		var mysite_rule = '<?php echo $wccp_pro_settings['mysite_rule']; ?>';
		var overlay = jQuery('<ProtectedImgDiv style="width:' + img.width() + 'px; height:' + img.height() + 'px" class="protectionOverlaycontainer"><div class="protectionOverlaytextcontainer"><ProtectedImgDiv class="protectionOverlaytext"></ProtectedImgDiv></div></ProtectedImgDiv>').css({position: 'absolute', zIndex: 9999999, left: pos.left, top: pos.top}).appendTo('body');
		if( mysite_rule == 'No Action'){
			var overlaywithlogo = jQuery('<ProtectedImgDiv style="width:' + img.width() + 'px; height:' + img.height() + 'px" class="protectionOverlaycontainer"><div class="protectionOverlaytextcontainer"><ProtectedImgDiv class="protectionOverlaytext"><p><?php //echo $wccp_pro_settings['dw_text'];?></p></ProtectedImgDiv></div></ProtectedImgDiv>').css({position: 'absolute', zIndex: 9999999, left: pos.left, top: pos.top}).appendTo('body');
		}
	}
	</script>
	<style>
	
	.protectionOverlaycontainer{
		background-image: url("<?php echo $pluginsurl.'/images/transparent.gif';?>");
		background-size: cover;
		color: <?php echo $wccp_pro_settings['dw_font_color'];?>;
		font-weight: bold;
		opacity: 0.<?php echo $wccp_pro_settings['dw_text_transparency'];?>;
		text-align: center;
		transform: rotate(0deg);
	}
	.protectionOverlaycontainer > img {
		bottom: 0;
		margin: 5px;
		opacity: 0.<?php echo $wccp_pro_settings['dw_text_transparency'];?>;
		padding: 5px;
		position: absolute;
		pointer-events: none;
		right: 0;
		height: 25%;
	}
	.protectionOverlaytextcontainer{
		height: 100%;
		width: 100%;
		display: table;
		overflow: hidden;
	}
	.protectionOverlaytext{
		display: table-cell;
		vertical-align: middle;
	}
	
	.protectionOverlaytext > p {
	/* Safari */
	-webkit-transform: rotate(-40deg);

	/* Firefox */
	-moz-transform: rotate(-40deg);

	/* IE */
	-ms-transform: rotate(-40deg);

	/* Opera */
	-o-transform: rotate(-40deg);
	
	font-family: tahoma;
	
	font-size: 2.5vw;
	
	font-size: 2em;
	
	font-weight: bold;
	
	opacity: 0.5;
	
	pointer-events: none;
	
	color: <?php echo $wccp_pro_settings['dw_font_color'];?>;
	
	margin: 0 auto;
	
	width: 100% !important;
	}
	
	</style>
	<!--Smart protection techniques -->
	<?php
}
?>
<?php
function wccp_pro_video_overlay()
{
	?>
	<!--just for video protection -->
	<script type="text/javascript">
	jQuery(function() {
		
		jQuery('IFRAME').load(function(e) {
			iframe_load(this);
		});
		
		jQuery('IFRAME').live('mouseenter touchstart',function(e) {
			iframe_load(this);
		});
	});
	function iframe_load(e)
	{
		// Only execute if this is not an overlay or skipped
			var img = jQuery(e);
			
			// Get the real image's position, add an overlay
			
			var pos = img.offset();
			
			var overlay_top = jQuery('<video width="' + img.width() + '" height="' + ((img.height()/2)-30) + '" ></video>').css({border: '1px sloid #fff', position: 'absolute', zIndex: 9999999, left: pos.left, top: pos.top}).appendTo('body');
			
			var overlay_btm = jQuery('<video width="' + img.width() + '" height="' + ((img.height()/2)-65) + '" ></video>').css({border: '1px sloid #fff',position: 'absolute', zIndex: 9999999, left: pos.left, top: pos.top+(img.height()/2)+25}).appendTo('body');
			
			var overlay_left = jQuery('<video width="' + ((img.width()/2)-30) + '" height="' + ((img.height()/2)-25) + '" ></video>').css({border: '1px sloid #fff',position: 'absolute', zIndex: 9999999, left: pos.left, top: pos.top+(img.height()/4)+25}).appendTo('body');
			
			var overlay_right = jQuery('<video width="' + ((img.width()/2)-30) + '" height="' + ((img.height()/2)-25) + '" ></video>').css({border: '1px sloid #000',position: 'absolute', zIndex: 9999999, left: pos.left+(img.width()/2)+30, top: pos.top+(img.height()/4)+25}).appendTo('body');
			
			//if ('ontouchstart' in window) jQuery(document).one('touchend', function(){ setTimeout(function(){ overlay4.remove(); }, 0, overlay4); });
	}
	</script>
	<style>
	video {
		background: transparent none repeat scroll 0 0;
}
.protectionOverlay{
	background: #fff none repeat scroll 0 0;
		border: 2px solid #fff;
		opacity: 0.0;
}
	</style>
	<!--just for iphones end -->
	<?php
}
?>