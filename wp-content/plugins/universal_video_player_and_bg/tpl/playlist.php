<div class="wrap">
	<div id="lbg_logo">
			<h2><?php esc_html_e( 'Playlist for player' , 'universal-background' );?>: <span style="color:#FF0000; font-weight:bold;"><?php echo strip_tags($_SESSION['xname'])?> - ID #<?php echo strip_tags($_SESSION['xid'])?></span></h2>
 	</div>
  <div id="universal_video_player_and_bg_updating_witness"><img src="<?php echo plugins_url('images/ajax-loader.gif', dirname(__FILE__))?>" /> Updating...</div>
  <div id="previewDialog"><iframe id="previewDialogIframe" src="" width="100%" height="600" style="border:0;"></iframe></div>

<div style="text-align:center; padding:0px 0px 20px 0px;"><img src="<?php echo plugins_url('images/icons/add_icon.gif', dirname(__FILE__))?>" alt="add" align="absmiddle" /> <a href="?page=VIDEO_PLAYER_AND_VIDEO_BACKGROUND_Playlist&xmlf=add_playlist_record"><?php esc_html_e( 'Add new' , 'universal-background' );?></a> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; <img src="<?php echo plugins_url('images/icons/magnifier.png', dirname(__FILE__))?>" alt="add" align="absmiddle" /> <a href="javascript: void(0);" onclick="showDialogPreview(<?php echo strip_tags($_SESSION['xid'])?>)"><?php esc_html_e( 'Preview Player' , 'universal-background' );?></a></div>
<div style="text-align:left; padding:10px 0px 10px 14px;">#<?php esc_html_e( 'Initial Order' , 'universal-background' );?></div>


<ul id="universal_video_player_and_bg_sortable">
	<?php foreach ( $result as $row )
	{
		$row=universal_video_player_and_bg_unstrip_array($row); ?>
	<li class="ui-state-default cursor_move" id="<?php echo esc_attr($row['id'])?>">#<?php echo esc_html($row['ord'])?> ---  <img src="<?php echo esc_url($row['data-bottom-thumb'])?>" height="30" align="absmiddle" id="top_image_<?php echo esc_attr($row['id'])?>" /><div class="toogle-btn-closed" id="toogle-btn<?php echo esc_attr($row['ord'])?>" onclick="mytoggle('toggleable<?php echo esc_js($row['ord'])?>','toogle-btn<?php echo esc_js($row['ord'])?>');"></div><div class="options"><a href="javascript: void(0);" onclick="universal_video_player_and_bg_delete_entire_record(<?php echo esc_js($row['id'])?>,<?php echo esc_js($row['ord'])?>);" style="color:#F00;">Delete</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href="?page=VIDEO_PLAYER_AND_VIDEO_BACKGROUND_Playlist&amp;id=<?php echo strip_tags($_SESSION['xid'])?>&amp;name=<?php echo strip_tags($_SESSION['xname'])?>&amp;duplicate_id=<?php echo esc_attr($row['id'])?>"><?php esc_html_e( 'Duplicate' , 'universal-background' );?></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
	<div class="toggleable" id="toggleable<?php echo esc_attr($row['ord'])?>">
    <form method="POST" enctype="multipart/form-data" id="form-playlist-universal_video_player_and_bg-<?php echo esc_attr($row['ord'])?>">
	    <input name="id" type="hidden" value="<?php echo esc_attr($row['id'])?>" />
        <input name="ord" type="hidden" value="<?php echo esc_attr($row['ord'])?>" />
		<table width="100%" cellspacing="0" class="wp-list-table widefat fixed pages" style="background-color:#FFFFFF;">
		  <tr>
		    <td align="left" valign="middle" width="25%"></td>
		    <td align="left" valign="middle" width="77%"></td>
		  </tr>
		  <tr>
		    <td colspan="2" align="center" valign="middle">&nbsp;</td>
		  </tr>
          <tr>
            <td align="right" valign="top" class="row-title"><?php esc_html_e( 'Thumbnail' , 'universal-background' );?></td>
            <td align="left" valign="middle"><input name="data-bottom-thumb" type="text" id="data-bottom-thumb" size="100" value="<?php echo stripslashes($row['data-bottom-thumb']);?>" />
              <input name="upload_img_button_<?php echo esc_attr($row['ord'])?>" type="button" id="upload_img_button_universal_video_player_and_bg_<?php echo esc_attr($row['ord'])?>" value="Change Image" />
              <br />
              <?php esc_html_e( 'Enter an URL or upload an image' , 'universal-background' );?></td>
            </tr>
          <tr>
        <td align="right" valign="top" class="row-title">&nbsp;</td>
        <td align="left" valign="middle"><img src="<?php echo esc_url($row['data-bottom-thumb'])?>" name="data-bottom-thumb_<?php echo esc_attr($row['ord'])?>" id="data-bottom-thumb_<?php echo esc_attr($row['ord'])?>" /></td>
      </tr>
          <tr>
            <td align="right" valign="top" class="row-title"><?php esc_html_e( 'YouTube Video ID' , 'universal-background' );?></td>
            <td align="left" valign="top"><input name="data-youtube" type="text" size="60" id="data-youtube" value="<?php echo stripslashes($row['data-youtube']);?>"/></td>
            </tr>
          <tr>
            <td align="right" valign="top" class="row-title"><?php esc_html_e( 'Vimeo Video idea' , 'universal-background' );?></td>
            <td align="left" valign="top"><input name="data-vimeo" type="text" size="60" id="data-vimeo" value="<?php echo stripslashes($row['data-vimeo']);?>"/></td>
            </tr>
          <tr>
            <td align="right" valign="top" class="row-title"><?php esc_html_e( 'Self Hosted/Third Party Hosted Video .MP4 file' , 'universal-background' );?></td>
            <td align="left" valign="middle"><input name="data-selfhostedMP4" type="text" id="data-selfhostedMP4" size="100" value="<?php echo stripslashes($row['data-selfhostedMP4']);?>" />
              <input name="upload_selfhostedMP4_<?php echo esc_attr($row['ord'])?>" type="button" id="upload_selfhostedMP4_<?php echo esc_attr($row['ord'])?>" value="Upload Video" />
              <br />
              <?php esc_html_e( 'Enter an URL or upload a .mp4 file' , 'universal-background' );?><br /></td>
          </tr>
          <tr>
            <td align="right" valign="top" class="row-title"><?php esc_html_e( 'Self Hosted/Third Party Hosted Video .WEBM file' , 'universal-background' );?></td>
            <td align="left" valign="middle"><input name="data-selfhostedWEBM" type="text" id="data-selfhostedWEBM" size="100" value="<?php echo stripslashes($row['data-selfhostedWEBM']);?>" />
              <input name="upload_selfhostedWEBM_<?php echo esc_attr($row['ord'])?>" type="button" id="upload_selfhostedWEBM_<?php echo esc_attr($row['ord'])?>" value="Upload Video" />
              <br />
              <?php esc_html_e( 'Enter an URL or upload a .webm file' , 'universal-background' );?><br /></td>
          </tr>
          <tr>
            <td align="right" valign="top" class="row-title"><?php esc_html_e( 'Video Title (optional)' , 'universal-background' );?></td>
            <td align="left" valign="top"><input name="data-title" type="text" size="60" id="data-title" value="<?php echo strip_tags($row['data-title']);?>"/></td>
          </tr>
          <tr>
            <td align="right" valign="top" class="row-title"><?php esc_html_e( 'Video Proportion Width' , 'universal-background' );?><br />
              <span style="font-weight:normal; font-style:italic;"><?php esc_html_e( 'Only if is used as full-screen background. The default value, set in "Player Settings" section, is 16' , 'universal-background' );?></span></td>
            <td align="left" valign="top"><input name="data-videoProportionWidth" type="text" size="60" id="data-videoProportionWidth" value="<?php echo strip_tags($row['data-videoProportionWidth']);?>"/></td>
          </tr>
		  <tr>
		    <td colspan="2" align="left" valign="middle">&nbsp;</td>
		    </tr>
		  <tr>
		    <td colspan="2" align="center" valign="middle"><input name="Submit<?php echo esc_attr($row['ord'])?>" id="Submit<?php echo esc_attr($row['ord'])?>" type="submit" class="button-primary" value="Update Playlist Record"></td>
		  </tr>
		</table>


        </form>
            <div id="ajax-message-<?php echo esc_attr($row['ord'])?>" class="ajax-message"></div>
    </div>
    </li>
	<?php } ?>
</ul>


</div>
