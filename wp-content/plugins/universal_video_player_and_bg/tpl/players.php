<div class="wrap">
	<div id="lbg_logo">
			<h2><?php esc_html_e( 'Manage Players' , 'universal-background' );?></h2>
 	</div>
    <div><p><?php esc_html_e( 'From this section you can add multiple players.' , 'universal-background' );?></p>
    </div>

<div id="previewDialog"><iframe id="previewDialogIframe" src="" width="100%" height="600" style="border:0;"></iframe></div>

<div style="text-align:center; padding:0px 0px 20px 0px;"><img src="<?php echo plugins_url('images/icons/add_icon.gif', dirname(__FILE__))?>" alt="add" align="absmiddle" /> <a href="?page=VIDEO_PLAYER_AND_VIDEO_BACKGROUND_Add_New"><?php esc_html_e( 'Add new (Player)' , 'universal-background' );?></a></div>

<table width="100%" class="widefat">

			<thead>
				<tr>
					<th scope="col" width="4%"><?php esc_html_e( 'ID' , 'universal-background' );?></th>
					<th scope="col" width="22%"><?php esc_html_e( 'Name' , 'universal-background' );?></th>
					<th scope="col" width="42%"><?php esc_html_e( 'Shorcode' , 'universal-background' );?></th>
					<th scope="col" width="28%"><?php esc_html_e( 'Actions' , 'universal-background' );?></th>
					<th scope="col" width="4%"><?php esc_html_e( 'Preview' , 'universal-background' );?></th>
				</tr>
			</thead>

<tbody>
<?php foreach ( $result as $row )
	{
		$row=universal_video_player_and_bg_unstrip_array($row); ?>
							<tr class="alternate author-self status-publish" valign="top">
					<td><?php echo esc_html($row['id'])?></td>
					<td><?php echo esc_html($row['name'])?></td>
					<td><strong>Page/Post</strong>: [universal_video_player_and_bg settings_id='<?php echo esc_html($row['id'])?>']<br />
                    <hr style="border-top:1px solid #CCC; border-bottom:0; border-left:0; border-right:0;" />
<strong>For header.php</strong>: <?php echo htmlspecialchars ( "<?php echo do_shortcode(\"[universal_video_player_and_bg settings_id='".$row['id']."']\");?>"); ?></td>
					<td>
						<a href="?page=VIDEO_PLAYER_AND_VIDEO_BACKGROUND_Settings&amp;id=<?php echo esc_attr($row['id'])?>&amp;name=<?php echo esc_attr($row['name'])?>"><?php esc_html_e( 'Player Settings' , 'universal-background' );?></a> &nbsp;&nbsp;|&nbsp;&nbsp;
						<a href="?page=VIDEO_PLAYER_AND_VIDEO_BACKGROUND_Playlist&amp;id=<?php echo esc_attr($row['id'])?>&amp;name=<?php echo esc_attr($row['name'])?>"><?php esc_html_e( 'Playlist' , 'universal-background' );?></a> &nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; <a href="?page=VIDEO_PLAYER_AND_VIDEO_BACKGROUND_Manage_Players&id=<?php echo esc_attr($row['id'])?>" onclick="return confirm('Are you sure?')" style="color:#F00;"><?php esc_html_e( 'Delete' , 'universal-background' );?></a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href="?page=VIDEO_PLAYER_AND_VIDEO_BACKGROUND_Manage_Players&amp;duplicate_id=<?php echo esc_attr($row['id'])?>"><?php esc_html_e( 'Duplicate' , 'universal-background' );?></a></td>
					<td align="center"><a href="javascript: void(0);" onclick="showDialogPreview(<?php echo esc_attr($row['id'])?>)"><img src="<?php echo plugins_url('images/icons/magnifier.png', dirname(__FILE__))?>" alt="preview" border="0" align="absmiddle" /></a></td>
	            </tr>
<?php } ?>
						</tbody>
		</table>


</div>
