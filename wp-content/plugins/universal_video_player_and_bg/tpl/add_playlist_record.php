<script>
jQuery(document).ready(function() {

	// Uploading files

	jQuery('#upload_thumbnail_button').click(function(event) {
		var file_frame;
		event.preventDefault();
		// If the media frame already exists, reopen it.
		if ( file_frame ) {
			file_frame.open();
			return;
		}
		// Create the media frame.
		file_frame = wp.media.frames.file_frame = wp.media({
			title: jQuery( this ).data( 'uploader_title' ),
			button: {
			text: jQuery( this ).data( 'uploader_button_text' ),
			},
			multiple: false // Set to true to allow multiple files to be selected
		});
		// When an image is selected, run a callback.
		file_frame.on( 'select', function() {
			// We set multiple to false so only get one image from the uploader
			attachment = file_frame.state().get('selection').first().toJSON();
			// Do something with attachment.id and/or attachment.url here
				jQuery('#data-bottom-thumb').val(attachment.url);
		});
		// Finally, open the modal
		file_frame.open();
	});




jQuery('#upload_selfhostedMP4').on('click', function( event ){
		var file_frame;
		event.preventDefault();
		// If the media frame already exists, reopen it.
		if ( file_frame ) {
			file_frame.open();
			return;
		}
		// Create the media frame.
		file_frame = wp.media.frames.file_frame = wp.media({
			title: jQuery( this ).data( 'uploader_title' ),
			button: {
			text: jQuery( this ).data( 'uploader_button_text' ),
			},
			multiple: false // Set to true to allow multiple files to be selected
		});
		// When an image is selected, run a callback.
		file_frame.on( 'select', function() {
			// We set multiple to false so only get one image from the uploader
			attachment = file_frame.state().get('selection').first().toJSON();
			// Do something with attachment.id and/or attachment.url here
				jQuery('#data-selfhostedMP4').val(attachment.url);
		});
		// Finally, open the modal
		file_frame.open();
	});

jQuery('#upload_selfhostedWEBM').on('click', function( event ){
		var file_frame;
		event.preventDefault();
		// If the media frame already exists, reopen it.
		if ( file_frame ) {
			file_frame.open();
			return;
		}
		// Create the media frame.
		file_frame = wp.media.frames.file_frame = wp.media({
			title: jQuery( this ).data( 'uploader_title' ),
			button: {
			text: jQuery( this ).data( 'uploader_button_text' ),
			},
			multiple: false // Set to true to allow multiple files to be selected
		});
		// When an image is selected, run a callback.
		file_frame.on( 'select', function() {
			// We set multiple to false so only get one image from the uploader
			attachment = file_frame.state().get('selection').first().toJSON();
			// Do something with attachment.id and/or attachment.url here
			jQuery('#data-selfhostedWEBM').val(attachment.url);
		});
		// Finally, open the modal
		file_frame.open();
	});

});
</script>




<div class="wrap">
	<div id="lbg_logo">
			<h2><?php esc_html_e( 'Playlist for player:' , 'universal-background' );?> <span style="color:#FF0000; font-weight:bold;"><?php echo strip_tags($_SESSION['xname'])?> - ID #<?php echo strip_tags($_SESSION['xid'])?></span> - <?php esc_html_e( 'Add New' , 'universal-background' );?></h2>
 	</div>

    <form method="POST" enctype="multipart/form-data" id="form-add-playlist-record">
	    <input name="playerid" type="hidden" value="<?php echo strip_tags($_SESSION['xid'])?>" />
		<table class="wp-list-table widefat fixed pages" cellspacing="0">
		  <tr>
		    <td align="left" valign="middle" width="25%">&nbsp;</td>
		    <td align="left" valign="middle" width="77%"><a href="?page=VIDEO_PLAYER_AND_VIDEO_BACKGROUND_Playlist" style="padding-left:25%;"><?php esc_html_e( 'Back to Playlist' , 'universal-background' );?></a></td>
		  </tr>
		  <tr>
		    <td colspan="2" align="left" valign="middle">&nbsp;</td>
	      </tr>
		  <tr>
		    <td align="right" valign="middle" class="row-title"><?php esc_html_e( 'Set It First' , 'universal-background' );?></td>
		    <td align="left" valign="top"><input name="setitfirst" type="checkbox" id="setitfirst" value="1" checked="checked" />
		      <label for="setitfirst"></label></td>
	      </tr>
		  <tr>
		    <td align="right" valign="top" class="row-title"><?php esc_html_e( 'Thumbnail' , 'universal-background' );?> </td>
		    <td width="77%" align="left" valign="top"><input name="data-bottom-thumb" type="text" id="data-bottom-thumb" size="60" value="<?php echo (array_key_exists('data-bottom-thumb', $_POST))?strip_tags($_POST['data-bottom-thumb']):''?>" /> <input name="upload_thumbnail_button" type="button" id="upload_thumbnail_button" value="Upload Image" />
	        <br />
	        Enter an URL or upload an image</td>
		  </tr>
          <tr>
            <td align="right" valign="top" class="row-title"><?php esc_html_e( 'YouTube Video ID' , 'universal-background' );?></td>
            <td align="left" valign="top"><input name="data-youtube" type="text" size="60" id="data-youtube" value="<?php echo (array_key_exists('data-youtube', $_POST))?strip_tags($_POST['data-youtube']):''?>"/></td>
          </tr>
          <tr>
            <td align="right" valign="top" class="row-title"><?php esc_html_e( 'Vimeo Video ID' , 'universal-background' );?></td>
            <td align="left" valign="top"><input name="data-vimeo" type="text" size="60" id="data-vimeo" value="<?php echo (array_key_exists('data-vimeo', $_POST))?strip_tags($_POST['data-vimeo']):''?>"/></td>
          </tr>
          <tr>
            <td align="right" valign="top" class="row-title"><?php esc_html_e( 'Self Hosted/Third Party Hosted Video .MP4 file' , 'universal-background' );?></td>
            <td align="left" valign="middle"><input name="data-selfhostedMP4" type="text" id="data-selfhostedMP4" size="100" value="<?php echo (array_key_exists('data-selfhostedMP4', $_POST))?strip_tags($_POST['data-selfhostedMP4']):''?>" />
              <input name="upload_selfhostedMP4" type="button" id="upload_selfhostedMP4" value="Upload Video" />
              <br />
              <?php esc_html_e( 'Enter an URL or upload a .mp4 file' , 'universal-background' );?><br /></td>
          </tr>
          <tr>
            <td align="right" valign="top" class="row-title"><?php esc_html_e( 'Self Hosted/Third Party Hosted Video .WEBM file' , 'universal-background' );?></td>
            <td align="left" valign="middle"><input name="data-selfhostedWEBM" type="text" id="data-selfhostedWEBM" size="100" value="<?php echo (array_key_exists('data-selfhostedWEBM', $_POST))?strip_tags($_POST['data-selfhostedWEBM']):''?>" />
              <input name="upload_selfhostedWEBM" type="button" id="upload_selfhostedWEBM" value="Upload Video" />
              <br />
              <?php esc_html_e( 'Enter an URL or upload a .webm file' , 'universal-background' );?><br /></td>
          </tr>
		  <tr>
		    <td align="right" valign="top" class="row-title"><?php esc_html_e( 'Video Title (optional)' , 'universal-background' );?></td>
		    <td align="left" valign="top"><input name="data-title" type="text" size="60" id="data-title" value="<?php echo (array_key_exists('data-title', $_POST))?strip_tags($_POST['data-title']):''?>"/></td>
	      </tr>
		  <tr>
		    <td align="right" valign="top" class="row-title"><?php esc_html_e( 'Video Proportion Width' , 'universal-background' );?><br />
<span style="font-weight:normal; font-style:italic;"><?php esc_html_e( 'Only if is used as full-screen background. The default value, set in "Player Settings" section, is 16' , 'universal-background' );?></span></td>
		    <td align="left" valign="top"><input name="data-videoProportionWidth" type="text" size="60" id="data-videoProportionWidth" value="<?php echo (array_key_exists('data-videoProportionWidth', $_POST))?strip_tags($_POST['data-videoProportionWidth']):''?>"/>    </td>
		  </tr>
		  <tr>
            <td align="right" valign="top" class="row-title">&nbsp;</td>
		    <td align="left" valign="top">&nbsp;</td>
	      </tr>
		  <tr>
		    <td colspan="2" align="left" valign="middle">&nbsp;</td>
		  </tr>
		  <tr>
		    <td colspan="2" align="center" valign="middle"><input name="Submit" id="Submit" type="submit" class="button-primary" value="Add Record"></td>
		  </tr>
		</table>
  </form>


</div>
