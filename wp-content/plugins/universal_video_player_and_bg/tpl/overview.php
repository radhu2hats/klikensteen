<div class="wrap">
		<div id="lbg_logo">
			<h2><?php esc_html_e( 'Overview' , 'universal-background' );?></h2>
		</div>
		<div class="postbox-container" style="width:100%">
			<div class="postbox">
				<h3 style="padding:7px 10px;"><?php esc_html_e( 'Video Player & Video Background WordPress Plugin' , 'universal-background' );?></h3>
				<div class="inside">
				<p><?php esc_html_e( 'This plugin can be used as Fixed Width/Full Width Video Player and as Full Screen Video Background' , 'universal-background' );?></p>
				<p><?php esc_html_e( 'You have available the following sections:' , 'universal-background' );?></p>
				<ul class="lbg_list-1">
					<li><a href="?page=VIDEO_PLAYER_AND_VIDEO_BACKGROUND_Manage_Players"><?php esc_html_e( 'Manage Players' , 'universal-background' );?></a></li>
					<li><a href="?page=VIDEO_PLAYER_AND_VIDEO_BACKGROUND_Add_New"><?php esc_html_e( 'Add New (Player)' , 'universal-background' );?></a></li>
		          <li><a href="?page=VIDEO_PLAYER_AND_VIDEO_BACKGROUND_Help"><?php esc_html_e( 'Help' , 'universal-background' );?></a></li>
				</ul>
			  </div>
			</div>
		</div>
	</div>
