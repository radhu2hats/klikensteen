; (function ($) {
  'use strict'

  /**
   * JavaScript code for admin dashboard.
   *
   */

  $(function () {
    /* Preloader */
    $("#sp_pcp_view_options .spf-metabox").css("visibility", "hidden");

    var PCP_layout_type = $(
      '#spf-section-sp_pcp_layouts_1 .spf-field-layout_preset .spf-siblings .spf--sibling'
    )
    var PCP_get_layout_value = $(
      '#spf-section-sp_pcp_layouts_1 .spf-field-layout_preset .spf-siblings .spf--sibling.spf--active'
    )
      .find('input')
      .val()

    // Carousel Layout.
    if (PCP_get_layout_value !== 'carousel_layout') {
      $(
        '#sp_pcp_view_options .spf-nav ul li.menu-item_sp_pcp_view_options_3'
      ).hide()
    } else {
      $(
        '#sp_pcp_view_options .spf-nav ul li.menu-item_sp_pcp_view_options_3'
      ).show()
    }

    /**
     * Show/Hide tabs on changing of layout.
     */
    $(PCP_layout_type).on('change', 'input', function (event) {
      //event.stopPropagation()
      var PCP_get_layout_value = $(this).val();

      // Carousel Layout.
      if (PCP_get_layout_value !== 'carousel_layout') {
        $(
          '#sp_pcp_view_options .spf-nav ul li.menu-item_sp_pcp_view_options_3'
        ).hide()
      } else {
        $(
          '#sp_pcp_view_options .spf-nav ul li.menu-item_sp_pcp_view_options_3'
        ).show()
      }
    })

    /* Preloader js */
    $("#sp_pcp_view_options .spf-metabox").css({ "backgroundImage": "none", "visibility": "visible", "minHeight": "auto" });
    $("#sp_pcp_view_options .spf-nav-metabox li").css("opacity", 1);

    /* Copy to clipboard */
    $('button.pcp-copy').click(function (e) {
      e.preventDefault();
      pcp_copyToClipboard($(this).siblings('span'));
      pcp_SelectText($(this).siblings('span'));
      $('.pcp-tooltip').text('Copy');
      $(this).find('.pcp-tooltip').text('copied!').show();
      $(this).siblings('.pcp-shortcode-selectable').focus().select();
    });
    function pcp_copyToClipboard(element) {
      var $temp = $("<input>");
      $("body").append($temp);
      $temp.val($(element).text()).select();
      document.execCommand("copy");
      $temp.remove();
    }
    function pcp_SelectText(element) {
      var r = document.createRange();
      var w = element.get(0);
      r.selectNodeContents(w);
      var sel = window.getSelection();
      sel.removeAllRanges();
      sel.addRange(r);
    }

  })
})(jQuery)
