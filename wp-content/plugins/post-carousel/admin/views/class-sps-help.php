<?php
/**
 * The main class for Meta-box configurations.
 *
 * @package Smart_Post_Show
 * @subpackage Smart_Post_Show/admin/views
 */

if ( ! defined( 'ABSPATH' ) ) {
	die;
} // Cannot access pages directly.

/**
 * Smart Post Show Help Page.
 */
class SPS_Help {

	/**
	 * Smart Post Show single instance of the class
	 *
	 * @var null The instance of the class.
	 * @since 2.0
	 *
	 * @return void
	 */
	protected static $instance = null;

	/**
	 * Main Smart_Post_Show_Help Instance
	 *
	 * @since 2.0
	 * @static
	 *
	 * @return self help instance
	 */
	public static function instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * Add admin sub-menu.
	 *
	 * @return void
	 */
	public function help_page_menu() {
		add_submenu_page(
			'edit.php?post_type=sp_post_carousel',
			__( 'Smart Post Show Help', 'smart-post-show' ),
			__( 'Help', 'smart-post-show' ),
			apply_filters( 'pcp_user_role_permission', 'manage_options' ),
			'pcp_help',
			array(
				$this,
				'help_page_callback',
			)
		);
	}

	/**
	 * The Smart Post Show Help Callback.
	 *
	 * @return void
	 */
	public function help_page_callback() {
		echo '
        <div class="wrap about-wrap sp-pcp-help">
        <h1>' . esc_html__( 'Welcome to Smart Post Show! ', 'smart-post-show' ) . '</h1>
        </div>
        <div class="wrap about-wrap sp-pcp-help">
			<p class="about-text">' . esc_html__( 'Thank you for installing Smart Post Show! You\'re now running the most popular Post Carousel and Post Grid plugin.
This video will help you get started with the plugin.', 'smart-post-show'
		) . '</p>
			<div class="wp-badge"></div>
			<hr>
			<div class="headline-feature feature-video">
			<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLoUb-7uG-5jPJ4tAoZbF_angfWNGeXdye" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
			<hr>
			<div class="feature-section three-col">
				<div class="col">
					<div class="sp-pcp-feature text-center">
						<i class="sp-pcp-font-icon fa-life-ring"></i>
						<h3>' . esc_html__( 'Need any Assistance?', 'smart-post-show' ) . '</h3>
						<p>' . esc_html__( 'Our Expert Support Team is always ready to help you out promptly.', 'smart-post-show' ) . '</p>
						<a href="https://shapedplugin.com/support-forum/" target="_blank" class="button button-primary">' . esc_html__( 'Contact Support', 'smart-post-show' ) . '</a>
					</div>
				</div>
				<div class="col">
					<div class="sp-pcp-feature text-center">
						<i class="sp-pcp-font-icon fa-file-text"></i>
						<h3>' . esc_html__( 'Looking for Documentation?', 'smart-post-show' ) . '</h3>
						<p>' . esc_html__( 'We have detailed documentation on every aspects of Smart Post Show.', 'smart-post-show' ) . '</p>
						<a href="https://shapedplugin.com/docs/docs/post-carousel/introduction/" target="_blank" class="button button-primary">' . esc_html__( 'Documentation', 'smart-post-show' ) . '</a>
					</div>
				</div>
				<div class="col">
					<div class="sp-pcp-feature text-center">
						<i class="sp-pcp-font-icon fa-thumbs-up"></i>
						<h3>' . esc_html__( 'Like This Plugin?', 'smart-post-show' ) . '</h3>
						<p>' . esc_html__( 'If you like Smart Post Show, please leave us a 5 star rating.', 'smart-post-show' ) . '</p>
						<a href="https://wordpress.org/support/plugin/post-carousel/reviews/?filter=5" target="_blank" class="button button-primary">' . esc_html__( 'Rate The Plugin', 'smart-post-show' ) . '</a>
					</div>
				</div>
			</div>
		</div>';
	}

}
