msgid ""
msgstr ""
"Project-Id-Version: maxgalleria-media-library\n"
"POT-Creation-Date: 2019-09-10 07:48-0300\n"
"PO-Revision-Date: 2019-09-10 07:48-0300\n"
"Last-Translator: Alan Pasho <apasho@gmail.com>\n"
"Language-Team: \n"
"Language: en_US\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.9\n"
"X-Poedit-Basepath: .\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e\n"
"X-Poedit-SearchPath-0: ..\n"

#: ../includes/mlf_support.php:14 ../maxgalleria-media-library.php:853
msgid "Support"
msgstr ""

#: ../includes/mlf_support.php:19 ../maxgalleria-media-library.php:990
#: ../maxgalleria-media-library.php:3918 ../maxgalleria-media-library.php:4194
msgid "Makers of"
msgstr ""

#: ../includes/mlf_support.php:19 ../maxgalleria-media-library.php:990
#: ../maxgalleria-media-library.php:3918 ../maxgalleria-media-library.php:4194
msgid "and"
msgstr ""

#: ../includes/mlf_support.php:20 ../maxgalleria-media-library.php:991
#: ../maxgalleria-media-library.php:3919 ../maxgalleria-media-library.php:4195
msgid "Click here to"
msgstr ""

#: ../includes/mlf_support.php:20 ../maxgalleria-media-library.php:991
#: ../maxgalleria-media-library.php:3919 ../maxgalleria-media-library.php:4195
msgid "Fix Common Problems"
msgstr ""

#: ../includes/mlf_support.php:21 ../maxgalleria-media-library.php:992
#: ../maxgalleria-media-library.php:3920 ../maxgalleria-media-library.php:4196
msgid "Need help? Click here for"
msgstr ""

#: ../includes/mlf_support.php:21 ../maxgalleria-media-library.php:992
#: ../maxgalleria-media-library.php:3920 ../maxgalleria-media-library.php:4196
msgid "Awesome Support!"
msgstr ""

#: ../includes/mlf_support.php:22 ../maxgalleria-media-library.php:993
#: ../maxgalleria-media-library.php:3921 ../maxgalleria-media-library.php:4197
msgid "Or Email Us at"
msgstr ""

#: ../includes/mlf_support.php:32
msgid "Troubleshooting Tips"
msgstr ""

#: ../includes/mlf_support.php:33
msgid "Troubleshooting Articles"
msgstr ""

#: ../includes/mlf_support.php:34
msgid "System Information</a>"
msgstr ""

#: ../includes/mlf_support.php:40
msgid "Folder Tree Not Loading"
msgstr ""

#: ../includes/mlf_support.php:42
msgid ""
"Users who report this issue can usually fix it by running the Media Library "
"Folders Reset plugin that comes with Media Library Folders."
msgstr ""

#: ../includes/mlf_support.php:44
msgid ""
"1. First make sure you have installed the latest version of Media Library "
"Folders."
msgstr ""

#: ../includes/mlf_support.php:45
msgid ""
"2. Deactivate Media Library Folders and activate Media Library Folders Reset "
"and run the Reset Database option from the Media Library Folders Reset sub "
"menu in the dashboard."
msgstr ""

#: ../includes/mlf_support.php:46
msgid ""
"3. After that, reactivate Media Library Folders. It will do a fresh scan of "
"your media library database and no changes will be made to the files or "
"folders on your site."
msgstr ""

#: ../includes/mlf_support.php:49
msgid "How to Unhide a Hidden Folder"
msgstr ""

#: ../includes/mlf_support.php:52
msgid ""
"1. Go to the hidden folder via your cPanel or FTP and remove the file ‘mlpp-"
"hidden"
msgstr ""

#: ../includes/mlf_support.php:53
msgid ""
"2. In the Media Library Folders Menu, click the Check for New folders link. "
"This will add the folder back into Media Library Folders."
msgstr ""

#: ../includes/mlf_support.php:54
msgid ""
"3. Visit the unhidden folder in Media Library Folders and click the Sync "
"button to add contents of the folder. Before doing this, check to see that "
"there are no thumbnail images in the current folder since these will be "
"regenerated automatically; these usually have file names such as image-"
"name-150×150.jpg, etc."
msgstr ""

#: ../includes/mlf_support.php:55
msgid "4. Repeat step 3 for each sub folder."
msgstr ""

#: ../includes/mlf_support.php:58 ../includes/mlf_support.php:88
msgid "How to Delete a Folder?"
msgstr ""

#: ../includes/mlf_support.php:60
msgid ""
"To delete a folder, right click (Ctrl-click with Macs) on a folder. A popup "
"menu will appear with the options, ‘Delete this folder?’ and ‘Hide this "
"folder?’. Click the delete option. The folder has to be empty in order to "
"delete it. If you receive a message that the folder is not empty, use the "
"sync function to display files that are still present in the folder."
msgstr ""

#: ../includes/mlf_support.php:62
msgid ""
"Folders and images added to the site by FTP are not showing up in Media "
"Library Folders"
msgstr ""

#: ../includes/mlf_support.php:64
msgid ""
"Media Library Folders does not work like the file manager on you computer. "
"It only display images and folders that have been added to the Media Library "
"database. To display new folders that have not been added through the Media "
"Library Folders you can click the Check for new folders option in the  Media "
"Library Folders submenu in the Wordpress Dashboard. If you allow Wordpress "
"to store images by year and month folders, then you should click the option "
"once each month to add these auto-generated folders."
msgstr ""

#: ../includes/mlf_support.php:66
msgid ""
"To add images that were upload to the site via the cPanel or by FTP, "
"navigate to the folder containing the images in  Media Library Folders and "
"click the Sync button. This will scan the folder looking images not "
"currently found in the Media Library for that folder. The Sync function only "
"scans the current folder. If there are subfolders, you will need to "
"individually sync them."
msgstr ""

#: ../includes/mlf_support.php:68
msgid "Folders Loads Indefinitely"
msgstr ""

#: ../includes/mlf_support.php:70
msgid ""
"This happens when a parent folder is missing from the folder data. To fix "
"this you will need to perform a reset of the Media Library Folders database. "
"To do this, deactivate Media Library Folders and activate Media Library "
"Folders Reset and select the Reset Database option. Once the reset has "
"completed, reactivate Media Library Folders and it will do a fresh scan of "
"the Media Library data."
msgstr ""

#: ../includes/mlf_support.php:72
msgid "Unable to Insert files from Media Library Folders into Posts or Pages"
msgstr ""

#: ../includes/mlf_support.php:74
msgid ""
"For inserting images and files into posts and pages you will have to use the "
"existing Media Library. The ability to insert items from the Media Library "
"Folders user interface is only available in"
msgstr ""

#: ../includes/mlf_support.php:74
msgid ""
"This does not mean you cannot insert files added to Media Library Folders "
"into any Wordpress posts or pages. Media Library Folders adds a folder user "
"interface and file operations to the existing media library and it does not "
"add a second media library. Since all the images are in the same media "
"library there is no obstacle to inserting them anywhere Wordpress allows "
"media files to be inserted. There is just no folder tree available in the "
"media library insert window for locating images in a particular folder. We "
"chose to include the folder tree for inserting images in posts and page in "
"the Pro version along with other features in order to fund the cost of "
"providing free technical support and continued development of the plugin."
msgstr ""

#: ../includes/mlf_support.php:76
msgid "Unable to Update Media Library Folders Reset"
msgstr ""

#: ../includes/mlf_support.php:78
msgid ""
"Media Library Folders Reset is maintenance and diagnostic plugin that is "
"included with Media Library Folders. It automatically updates when Media "
"Library Folders is updated. There is no need to updated it  separately. "
"Users should leave the reset plugin deactivated until it is needed in order "
"to avoid accidentally deleting your site's folder data."
msgstr ""

#: ../includes/mlf_support.php:80
msgid "Images Not Found After Changing the Location of Uploads Folder"
msgstr ""

#: ../includes/mlf_support.php:82
msgid ""
"If you change the location of the uploads folder, your existing files and "
"images will not be moved to the new location. You will need to delete them "
"from media library and upload them again. Also you will need to perform a "
"reset of the Media Library Folders database. To do this, deactivate Media "
"Library Folders and activate Media Library Folders Reset and select the "
"Reset Database option. Once the reset has completed, reactivate Media "
"Library Folders and it will do a fresh scan of the Media Library data."
msgstr ""

#: ../includes/mlf_support.php:84
msgid "Difficulties Uploading or Dragging and Dropping a Large Number of Files"
msgstr ""

#: ../includes/mlf_support.php:86
msgid ""
"Limitations on web server processing time may cause dragging and dropping a "
"large number of files to fail. An error is generated when it takes to longer "
"then 30 seconds to move, copy or upload files. This time limitation can be "
"increased by changing the max_execution_time setting in your site's php.ini "
"file."
msgstr ""

#: ../includes/mlf_support.php:90
msgid ""
"To delete a folder, right click (Ctrl-click with Macs) on a folder. A popup "
"menu will appear with the options, 'Delete this folder?' and 'Hide this "
"folder?'. Click the delete option."
msgstr ""

#: ../includes/mlf_support.php:92
msgid "Fatal error: Maximum execution time exceeded "
msgstr ""

#: ../includes/mlf_support.php:94
msgid ""
"The Maximum execution time error takes place when moving, syncing or "
"uploading too many files at one time. The web site’s server has a setting "
"for how long it can be busy with a task. Depending on your server, size of "
"files and the transmission speed of your internet, you may need to reduce "
"the number of files you upload or move at one time."
msgstr ""

#: ../includes/mlf_support.php:95
msgid ""
"It is possible to change the maximum execution time either with a plugin "
"such as <a href=“http://wordpress.org/plugins/wp-maximum-execution-time-"
"exceeded/” target=“_blank”>WP Maximum Execution Time Exceeded</a> or by "
"editing your site’s .htaccess file and adding this line:"
msgstr ""

#: ../includes/mlf_support.php:96
msgid "php_value max_execution_time 300"
msgstr ""

#: ../includes/mlf_support.php:97
msgid "Which will raise the maximum execution time to five minutes."
msgstr ""

#: ../includes/mlf_support.php:99
msgid "How to Upload Multiple Files"
msgstr ""

#: ../includes/mlf_support.php:100
msgid ""
"Users can upload multiple files by using drag and drop. When the Add Files "
"button is click it revels the file upload area either single or multiple "
"files can be highlight can be dragged from you computer’s file manager and "
"dropped into the file uploads areas."
msgstr ""

#: ../includes/mlf_support.php:117
msgid ""
"You may be asked to provide the information below to help troubleshoot your "
"issue."
msgstr ""

#: ../maxgalleria-media-library.php:14
msgid ""
"You must deactivate Media Library Folders Pro before activating Media "
"Library Folders"
msgstr ""

#: ../maxgalleria-media-library.php:271
msgid "Are you sure you want to delete the selected files?"
msgstr ""

#: ../maxgalleria-media-library.php:272
msgid "No items were selected."
msgstr ""

#: ../maxgalleria-media-library.php:273
msgid "No images were selected."
msgstr ""

#: ../maxgalleria-media-library.php:274
msgid "Folder names cannot contain single or double quotes."
msgstr ""

#: ../maxgalleria-media-library.php:275
msgid "Folder names cannot contain spaces."
msgstr ""

#: ../maxgalleria-media-library.php:276
msgid "Please enter a valid file name with no spaces."
msgstr ""

#: ../maxgalleria-media-library.php:493 ../maxgalleria-media-library.php:1516
#: ../maxgalleria-media-library.php:1570 ../maxgalleria-media-library.php:1599
#: ../maxgalleria-media-library.php:1618 ../maxgalleria-media-library.php:2284
#: ../maxgalleria-media-library.php:2465 ../maxgalleria-media-library.php:2591
#: ../maxgalleria-media-library.php:2675 ../maxgalleria-media-library.php:2846
#: ../maxgalleria-media-library.php:3000 ../maxgalleria-media-library.php:3028
#: ../maxgalleria-media-library.php:3551 ../maxgalleria-media-library.php:3779
#: ../maxgalleria-media-library.php:3842 ../maxgalleria-media-library.php:4267
#: ../maxgalleria-media-library.php:4426 ../maxgalleria-media-library.php:4616
#: ../maxgalleria-media-library.php:4628 ../maxgalleria-media-library.php:4742
#: ../maxgalleria-media-library.php:4874
msgid "missing nonce!"
msgstr ""

#: ../maxgalleria-media-library.php:528
msgid " file's type is invalid."
msgstr ""

#: ../maxgalleria-media-library.php:844 ../maxgalleria-media-library.php:984
msgid "Media Library Folders"
msgstr ""

#: ../maxgalleria-media-library.php:845 ../maxgalleria-media-library.php:847
msgid "Check For New Folders"
msgstr ""

#: ../maxgalleria-media-library.php:846
msgid "Search Library"
msgstr ""

#: ../maxgalleria-media-library.php:850
msgid "Upgrade to Pro"
msgstr ""

#: ../maxgalleria-media-library.php:851 ../maxgalleria-media-library.php:1104
#: ../maxgalleria-media-library.php:3913
msgid "Regenerate Thumbnails"
msgstr ""

#: ../maxgalleria-media-library.php:852 ../maxgalleria-media-library.php:4189
msgid "Image SEO"
msgstr ""

#: ../maxgalleria-media-library.php:854 ../maxgalleria-media-library.php:4228
msgid "Settings"
msgstr ""

#: ../maxgalleria-media-library.php:998
msgid "Click here to learn about the Media Library Folders Pro"
msgstr ""

#: ../maxgalleria-media-library.php:1010
msgid "Current PHP version, "
msgstr ""

#: ../maxgalleria-media-library.php:1010
msgid ", is outdated. Please upgrade to version 5.6."
msgstr ""

#: ../maxgalleria-media-library.php:1029 ../maxgalleria-media-library.php:1473
msgid "Location:"
msgstr ""

#: ../maxgalleria-media-library.php:1043
msgid "Upload new files."
msgstr ""

#: ../maxgalleria-media-library.php:1043
msgid "Add File"
msgstr ""

#: ../maxgalleria-media-library.php:1045
msgid ""
"Create a new folder. Type in a folder name (do not use spaces, single or "
"double quote marks) and click Create Folder."
msgstr ""

#: ../maxgalleria-media-library.php:1045
msgid "Add Folder"
msgstr ""

#: ../maxgalleria-media-library.php:1058
msgid ""
"When moving/copying to a new folder place your pointer, not the image, on "
"the folder where you want the file(s) to go."
msgstr ""

#: ../maxgalleria-media-library.php:1059
msgid ""
"To drag multiple images, check the box under the files you want to move and "
"then drag one of the images to the desired folder."
msgstr ""

#: ../maxgalleria-media-library.php:1060
msgid ""
"To move/copy to a folder nested under the top level folder click the "
"triangle to the left of the folder to show the nested folder that is your "
"target."
msgstr ""

#: ../maxgalleria-media-library.php:1061
msgid ""
"To delete a folder, right click on the folder and a popup menu will appear. "
"Click on the option, \"Delete this folder?\" If the folder is empty, it will "
"be deleted."
msgstr ""

#: ../maxgalleria-media-library.php:1062
msgid ""
"To hide a folder and all its sub folders and files, right click on a folder, "
"On the popup menu that appears, click \"Hide this folder?\" and those "
"folders and files will be removed from the Media Library, but not from the "
"server."
msgstr ""

#: ../maxgalleria-media-library.php:1076
msgid ""
"Move/Copy Toggle. Move or copy selected files to a different folder.<br> "
"When move is selected, images links in posts and pages will be updated.<br> "
"<span class='mlp-warning'>Images IDs used in Jetpack Gallery shortcodes will "
"not be updated.</span>"
msgstr ""

#: ../maxgalleria-media-library.php:1085
msgid ""
"Rename a file; select only one file. Folders cannot be renamed. Type in a "
"new name with no spaces and without the extension and click Rename."
msgstr ""

#: ../maxgalleria-media-library.php:1085 ../maxgalleria-media-library.php:1144
msgid "Rename"
msgstr ""

#: ../maxgalleria-media-library.php:1087
msgid "Delete selected files."
msgstr ""

#: ../maxgalleria-media-library.php:1087
msgid "Delete"
msgstr ""

#: ../maxgalleria-media-library.php:1089
msgid "Select or unselect all files in the folder."
msgstr ""

#: ../maxgalleria-media-library.php:1089
msgid "Select/Unselect All"
msgstr ""

#: ../maxgalleria-media-library.php:1092
msgid "Sort by Name"
msgstr ""

#: ../maxgalleria-media-library.php:1093
msgid "Sort by Date"
msgstr ""

#: ../maxgalleria-media-library.php:1097
msgid "Search"
msgstr ""

#: ../maxgalleria-media-library.php:1100
msgid "Sync the contents of the current folder with the server"
msgstr ""

#: ../maxgalleria-media-library.php:1100
msgid "Sync"
msgstr ""

#: ../maxgalleria-media-library.php:1104
msgid "Regenerates the thumbnails of selected images"
msgstr ""

#: ../maxgalleria-media-library.php:1107
msgid ""
"Add images to an existing MaxGalleria gallery. Folders can not be added to a "
"gallery. Images already in the gallery will not be added. "
msgstr ""

#: ../maxgalleria-media-library.php:1107
msgid "Add to MaxGalleria Gallery"
msgstr ""

#: ../maxgalleria-media-library.php:1128
msgid "Drag & Drop Files Here"
msgstr ""

#: ../maxgalleria-media-library.php:1129
msgid "or select a file or image to upload:"
msgstr ""

#: ../maxgalleria-media-library.php:1132
msgid "Upload Image"
msgstr ""

#: ../maxgalleria-media-library.php:1135
msgid "Image Title Text:"
msgstr ""

#: ../maxgalleria-media-library.php:1136
msgid "Image ALT Text:"
msgstr ""

#: ../maxgalleria-media-library.php:1143
msgid "File Name: "
msgstr ""

#: ../maxgalleria-media-library.php:1179
msgid "Add Images"
msgstr ""

#: ../maxgalleria-media-library.php:1190
msgid "Folder Name: "
msgstr ""

#: ../maxgalleria-media-library.php:1191
msgid "Create Folder"
msgstr ""

#: ../maxgalleria-media-library.php:1842
msgid "Delete this folder?"
msgstr ""

#: ../maxgalleria-media-library.php:1851
msgid "Are you sure you want to delete the selected folder?"
msgstr ""

#: ../maxgalleria-media-library.php:1883
msgid "Hide this folder?"
msgstr ""

#: ../maxgalleria-media-library.php:1889
msgid ""
"Are you sure you want to hide the selected folder and all its sub folders "
"and files?"
msgstr ""

#: ../maxgalleria-media-library.php:2176
msgid "No files were found."
msgstr ""

#: ../maxgalleria-media-library.php:2333
msgid "The folder was created."
msgstr ""

#: ../maxgalleria-media-library.php:2339
msgid "There was a problem creating the folder."
msgstr ""

#: ../maxgalleria-media-library.php:2346
msgid "The folder already exists."
msgstr ""

#: ../maxgalleria-media-library.php:2509
msgid "The folder, "
msgstr ""

#: ../maxgalleria-media-library.php:2509
msgid ", is not empty. Please delete or move files from the folder"
msgstr ""

#: ../maxgalleria-media-library.php:2525 ../maxgalleria-media-library.php:2538
msgid "The folder was deleted."
msgstr ""

#: ../maxgalleria-media-library.php:2527
msgid "The folder could not be deleted."
msgstr ""

#: ../maxgalleria-media-library.php:2549
msgid "The file(s) were deleted"
msgstr ""

#: ../maxgalleria-media-library.php:2551
msgid "The file(s) were not deleted"
msgstr ""

#: ../maxgalleria-media-library.php:2664
msgid "The images were added."
msgstr ""

#: ../maxgalleria-media-library.php:2716 ../maxgalleria-media-library.php:2816
msgid "No files were found matching that name."
msgstr ""

#: ../maxgalleria-media-library.php:2732
msgid "Media Library Folders Search Results"
msgstr ""

#: ../maxgalleria-media-library.php:2734
msgid "Back to Media Library Folders"
msgstr ""

#: ../maxgalleria-media-library.php:2739
msgid ""
"Click on an image to go to its folder or a on folder to view its contents."
msgstr ""

#: ../maxgalleria-media-library.php:2742
msgid "Search results for: "
msgstr ""

#: ../maxgalleria-media-library.php:2868
msgid "Invalid file name."
msgstr ""

#: ../maxgalleria-media-library.php:2873
msgid "The file name cannot contain spaces or tabs."
msgstr ""

#: ../maxgalleria-media-library.php:2989
msgid "Updating attachment links, please wait...The file was renamed"
msgstr ""

#: ../maxgalleria-media-library.php:3012
msgid "Sorting by date."
msgstr ""

#: ../maxgalleria-media-library.php:3016
msgid "Sorting by name."
msgstr ""

#: ../maxgalleria-media-library.php:3070
msgid "Scaning for new folders in "
msgstr ""

#: ../maxgalleria-media-library.php:3097 ../maxgalleria-media-library.php:3126
#: ../maxgalleria-media-library.php:3154 ../maxgalleria-media-library.php:3195
msgid "Adding"
msgstr ""

#: ../maxgalleria-media-library.php:3172
msgid "No new folders were found."
msgstr ""

#: ../maxgalleria-media-library.php:3293
msgid "Rate us Please!"
msgstr ""

#: ../maxgalleria-media-library.php:3294
msgid ""
"Your rating is the simplest way to support Media Library Folders. We really "
"appreciate it!"
msgstr ""

#: ../maxgalleria-media-library.php:3297
msgid "I've already left a review"
msgstr ""

#: ../maxgalleria-media-library.php:3298
msgid "Maybe Later"
msgstr ""

#: ../maxgalleria-media-library.php:3299
msgid "Sure! I'd love to!"
msgstr ""

#: ../maxgalleria-media-library.php:3793
msgid "Media Library Folders Settings"
msgstr ""

#: ../maxgalleria-media-library.php:3801
msgid "Disable floating file tree"
msgstr ""

#: ../maxgalleria-media-library.php:3880
msgid "Error: "
msgstr ""

#: ../maxgalleria-media-library.php:3884
#, php-format
msgid "Unknown error with %s"
msgstr ""

#: ../maxgalleria-media-library.php:3895
#, php-format
msgid "Thumbnails have been regenerated for %d image(s)"
msgstr ""

#: ../maxgalleria-media-library.php:3934
msgid "Cheatin&#8217; uh?"
msgstr ""

#: ../maxgalleria-media-library.php:3945
#, php-format
msgid "Unable to find any images. Are you sure <a href='%s'>some exist</a>?"
msgstr ""

#: ../maxgalleria-media-library.php:3956
msgid ""
"Please wait while the thumbnails are regenerated. This may take a while."
msgstr ""

#: ../maxgalleria-media-library.php:3960
#, php-format
msgid "To go back to the previous page, <a href=\"%s\">click here</a>."
msgstr ""

#: ../maxgalleria-media-library.php:3961
#, php-format
msgid ""
"All done! %1$s image(s) were successfully resized in %2$s seconds and there "
"were %3$s failure(s). To try regenerating the failed images again, <a href="
"\"%4$s\">click here</a>. %5$s"
msgstr ""

#: ../maxgalleria-media-library.php:3962
#, php-format
msgid ""
"All done! %1$s image(s) were successfully resized in %2$s seconds and there "
"were 0 failures. %3$s"
msgstr ""

#: ../maxgalleria-media-library.php:3966 ../maxgalleria-media-library.php:4112
msgid "You must enable Javascript in order to proceed!"
msgstr ""

#: ../maxgalleria-media-library.php:3972
msgid "Abort Resizing Images"
msgstr ""

#: ../maxgalleria-media-library.php:3974
msgid "Debugging Information"
msgstr ""

#: ../maxgalleria-media-library.php:3977
#, php-format
msgid "Total Images: %s"
msgstr ""

#: ../maxgalleria-media-library.php:3978
#, php-format
msgid "Images Resized: %s"
msgstr ""

#: ../maxgalleria-media-library.php:3979
#, php-format
msgid "Resize Failures: %s"
msgstr ""

#: ../maxgalleria-media-library.php:4010
msgid "Stopping..."
msgstr ""

#: ../maxgalleria-media-library.php:4062
#, php-format
msgid ""
"The resize request was abnormally terminated (ID %s). This is likely due to "
"the image exceeding available memory or some other type of fatal error."
msgstr ""

#: ../maxgalleria-media-library.php:4105
msgid ""
"Click the button below to regenerate thumbnails for all images in the Media "
"Library. This is helpful if you have added new thumbnail sizes to your site. "
"Existing thumbnails will not be removed to prevent breaking any links."
msgstr ""

#: ../maxgalleria-media-library.php:4107
msgid ""
"You can regenerate thumbnails for individual images from the Media Library "
"Folders page by checking the box below one or more images and clicking the "
"Regenerate Thumbnails button. The regenerate operation is not reversible but "
"you can always generate the sizes you need by adding additional thumbnail "
"sizes to your theme."
msgstr ""

#: ../maxgalleria-media-library.php:4110
msgid "Regenerate All Thumbnails"
msgstr ""

#: ../maxgalleria-media-library.php:4139
#, php-format
msgid "Failed resize: %s is an invalid image ID."
msgstr ""

#: ../maxgalleria-media-library.php:4142
msgid "Your user account doesn't have permission to resize images"
msgstr ""

#: ../maxgalleria-media-library.php:4147
#, php-format
msgid "The originally uploaded image file cannot be found at %s"
msgstr ""

#: ../maxgalleria-media-library.php:4159
msgid "Unknown failure reason."
msgstr ""

#: ../maxgalleria-media-library.php:4164
#, php-format
msgid "&quot;%1$s&quot; (ID %2$s) was successfully resized in %3$s seconds."
msgstr ""

#: ../maxgalleria-media-library.php:4169
#, php-format
msgid ""
"&quot;%1$s&quot; (ID %2$s) failed to resize. The error message was: %3$s"
msgstr ""

#: ../maxgalleria-media-library.php:4204
msgid ""
"When Image SEO is enabled Media Library Folders automatically adds  ALT and "
"Title attributes with the default settings defined below to all your images "
"as they are uploaded."
msgstr ""

#: ../maxgalleria-media-library.php:4205
msgid ""
"You can easily override the Image SEO default settings when you  are "
"uploading new images. When Image SEO is enabled you will see two fields  "
"under the Upload Box when you add a file - Image Title Text and Image ALT "
"Text.  Whatever you type into these fields overrides the default settings "
"for the  current upload or sync operations."
msgstr ""

#: ../maxgalleria-media-library.php:4206
msgid ""
"To change the settings on an individual image simply click on  the image and "
"change the settings on the far right.  Save and then back click to return to "
"Media  Library Plus or MLPP."
msgstr ""

#: ../maxgalleria-media-library.php:4207
msgid "Image SEO supports two special tags:"
msgstr ""

#: ../maxgalleria-media-library.php:4208
#, php-format
msgid "%filename - replaces image file name ( without extension )"
msgstr ""

#: ../maxgalleria-media-library.php:4209
#, php-format
msgid "%foldername - replaces image folder name"
msgstr ""

#: ../maxgalleria-media-library.php:4233
msgid "Turn on Image SEO:"
msgstr ""

#: ../maxgalleria-media-library.php:4238
msgid "Image ALT attribute:"
msgstr ""

#: ../maxgalleria-media-library.php:4240 ../maxgalleria-media-library.php:4245
msgid "example"
msgstr ""

#: ../maxgalleria-media-library.php:4243
msgid "Image Title attribute:"
msgstr ""

#: ../maxgalleria-media-library.php:4248
msgid "Update Settings"
msgstr ""

#: ../maxgalleria-media-library.php:4291
msgid "The Image SEO setting have been updated "
msgstr ""

#: ../maxgalleria-media-library.php:4460
msgid "The selected folder, subfolders and thier files have been hidden."
msgstr ""

#: ../maxgalleria-media-library.php:4787
msgid "Scanning for new files and folders...please wait."
msgstr ""

#: ../maxgalleria-media-library.php:4792
msgid "Syncing finished."
msgstr ""

#: ../maxgalleria-media-library.php:4813
msgid "Adding "
msgstr ""

#: ../maxgalleria-media-library.php:4939
msgid "Finished copying files. "
msgstr ""

#: ../maxgalleria-media-library.php:4941
msgid "Finished moving files. "
msgstr ""

#: ../maxgalleria-media-library.php:5003
msgid "Unable to copy the file; please check the folder and file permissions."
msgstr ""

#: ../maxgalleria-media-library.php:5115
msgid "Updating attachment links, please wait..."
msgstr ""

#: ../maxgalleria-media-library.php:5120
msgid "Unable to move "
msgstr ""

#: ../maxgalleria-media-library.php:5120
msgid "; please check the folder and file permissions."
msgstr ""

#: ../maxgalleria-media-library.php:5126
msgid "The destination is not a folder: "
msgstr ""

#: ../maxgalleria-media-library.php:5131
msgid "Cannot find destination folder: "
msgstr ""

#: ../maxgalleria-media-library.php:5136
msgid "Coping or moving a folder is not allowed."
msgstr ""

#: ../maxgalleria-media-library.php:5141
msgid "Cannot find the file: "
msgstr ""

#: ../maxgalleria-media-library.php:5148
msgid " was copied to "
msgstr ""

#: ../maxgalleria-media-library.php:5150
msgid " was not copied."
msgstr ""

#: ../maxgalleria-media-library.php:5154
msgid " was moved to "
msgstr ""

#: ../maxgalleria-media-library.php:5156
msgid " was not moved."
msgstr ""
