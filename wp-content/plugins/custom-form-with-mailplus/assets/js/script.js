jQuery(function($) {
    $("#cfwm-loader").hide();
    var modal = $("#downloadModalBox");
    var btn = $("#myBtn");
    var span = $(".close");

    $('body').on('click', '.download-brochure', function(e) {
        e.preventDefault();
        $("#downloadModalBox").css('display', "block");
    });

    window.onclick = function(event) {
        if (event.target == modal) {
            jQuery("#downloadModalBox").css('display', "none");
        }
    };

    $('body').on('click', '#downloadModalBox .close', function(e) {
        $("#downloadModalBox").css('display', "none");
    });

    $('body').on('click', '#final_step', function(e) {
        e.preventDefault();
        openLoader();
        var $form = $('#keuzehulp');
        var formData = $form.serializeArray();
        var data = {
            'action': 'submit_mailplus_form_ajax',
            'data': getFormData(formData),
            'security': cfwmAjax.security
        };

        $.post(cfwmAjax.ajaxurl, data, function(response) {
            response = JSON.parse(response);
            var postdata = response.data;
            var url =  "/keuzehulp-resultaat/";
            var url = new URL(response.redirecturl);

            if(postdata.training_huidige ){
                url.searchParams.append('huidige', postdata.training_huidige);
            }
            if(postdata.training_leerdoel ){
                url.searchParams.append('leerdoel', postdata.training_leerdoel);
            }
            if(postdata.training_werkervaring ){
                url.searchParams.append('werkervaring', postdata.training_werkervaring);
            }
            if(postdata.training_werkveld ){
                url.searchParams.append('werkveld', postdata.training_werkveld);
            }
            if(postdata.Voornaam ){
                url.searchParams.append('voornaam', postdata.Voornaam);
            }
            if(postdata.Tussenv ){
                url.searchParams.append('tussenv', postdata.Tussenv);
            }
            if(postdata.Achternaam ){
                url.searchParams.append('achternaam', postdata.Achternaam);
            }
            if(postdata.email ){
                url.searchParams.append('email', postdata.email);
            }
            //if(response.status){
            window.location.href = url.toString();
            closeLoader();
            //}

        });
    });

    function openLoader() {
        var div = document.createElement("div");
        div.className += "ajx-overlay";
        div.style.background = cfwmAjax.ajaxoverlay;
        document.body.appendChild(div);
    }

    function closeLoader() {
        $('.ajx-overlay').remove();
    }
    //utility function
    function getFormData(data) {
        var unindexed_array = data;
        console.log(unindexed_array);
        var indexed_array = {};

        $.map(unindexed_array, function(n, i) {
            var isCheckbox = n['name'].endsWith("[]", n['name'].length)
            if (isCheckbox) {
                n['value'] = $("input[name='" + n['name'] + "']:checked").map(function() {
                    return this.value;
                }).get().join(', ');
                n['name'] = n['name'].replace('[]', '');
            }

            indexed_array[n['name']] = n['value'];
        });
        console.log(indexed_array);
        return indexed_array;
    }
});