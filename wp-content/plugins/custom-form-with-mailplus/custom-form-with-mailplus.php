<?php
   /*
   Plugin Name: Custom Mailplus form
   Plugin URI: #
   description: A custom form help to link  with Mailplus
   Text Domain: cfwm
   Domain Path: /languages/
   Version: 1.2
   Author: #
   Author URI: #
   License: GPL2
   */
   include_once('includes/form_submit.php');
   include_once('includes/shortcodes.php');
   include_once('includes/admin-settings.php');  
   include_once('includes/mailplusforms.php');  
   