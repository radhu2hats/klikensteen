<?php
set_include_path(get_include_path() . PATH_SEPARATOR . dirname(__FILE__));

require_once 'Zend/Rest/Client/Result.php';
require 'rest/Client.php';
require 'Zend/Oauth/Consumer.php';

class mailplus_forms_api
{
	 public function get_client()
	{
	    $options = get_option( 'stp_api_settings' );
        
	    $config = array(
	        'requestScheme' => "header",
	        'version' => '1.0',
	        'signatureMethod' => "HMAC-SHA1",
	        'consumerKey' => $options['cfwm_api_key'],
	        'consumerSecret' => $options['cfwm_api_secret']
	    );
	    $token = new Zend_Oauth_Token_Access();
	    $httpClient = $token->getHttpClient($config);

	    $client = new Zend_Rest_Client("https://restapi.mailplus.nl/integrationservice");
	    $client->setHttpClient($httpClient);
	    return $client;
	}

	 public function get_forms()
    {
        $client = $this->get_client();

        $result = $client->get("/integrationservice/form/list");

        $forms = null;
        foreach ($result as $form) {
            $forms[] = $form;
        }

        usort($forms, "cmp_form");
        return $forms;
    }

    public function get_form($formId, $posturl, $encId = null)
    {
     
        $outputFormat = 'XHTML1STRICT';
        
        $client = $this->get_client();
        $client->postUrl($posturl);
        $client->outputFormat($outputFormat);
        $client->outputMode($outputMode);

        if ($encId) {
            $client->encId($encId);
        }

        return $client->get("/integrationservice/form/" . $formId);
    }

    public function post_form($postvars)
    {
        
        $outputFormat = 'HTML4STRICT';
        $formId = get_option( 'stp_api_settings' )['cfwm_api_from_id'];
        $client = $this->get_client();
        $response = $client->restPost("/integrationservice-1.1.0/form/result/" . $formId,$postvars
            ,
            "application/json"
        );
        //print_r($response);
        return new Zend_Rest_Client_Result($response->getBody());
    }

}

