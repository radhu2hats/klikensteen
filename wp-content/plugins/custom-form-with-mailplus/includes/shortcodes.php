<?php
add_shortcode('custom-form-with-mailplus', 'customFormWithMailPlus');
function customFormWithMailPlus()
{

   $training_werkveld = get_terms('training_werkveld', array('fields' => 'all', 'parent' => 0, 'hide_empty' => true, 'number' => 8));
   $leerdoel = get_terms('leerdoel', array('fields' => 'all', 'parent' => 0, 'hide_empty' => false, 'number' => 8));
   $training_functie = get_terms('training_functie', array('fields' => 'all', 'parent' => 0, 'hide_empty' => true, 'number' => 8));
   $werkervaring_niveau = get_terms('werkervaring_niveau', array('fields' => 'all', 'parent' => 0, 'hide_empty' => false, 'number' => 8));
   $voornaam = $tussenv = $achternaam = $email = '';
   if (isset($_GET['voornaam'])) {
      $voornaam = $_GET['voornaam'];
   }
   if (isset($_GET['tussenv'])) {
      $tussenv = $_GET['tussenv'];
   }
   if (isset($_GET['achternaam'])) {
      $achternaam = $_GET['achternaam'];
   }
   if (isset($_GET['email'])) {
      $email = $_GET['email'];
   }
   ob_start();

?>
   <section class="section keuzehulp blue">
      <!-- <div class="container white_bg_block">
      
    </div> -->
      <div class="vc_row wpb_row vc_row-fluid container">
         <div class="wpb_column vc_column_container vc_col-sm-8">
            <form id="keuzehulp" method="post" action="<?php echo plugins_url('form_submit.php', __FILE__); ?>">
               <div class="keuzehulp-slider" id="keuzehulpSlider">
                  <div class="keuzehulp-slide intro pink-cta">
                     <div class="inner-content">
                        <div class="icon-wrap"><i class="fas fa-paper-plane"></i></div>
                        <div class="lbl">Stimulansz</div>
                        <h1>Keuzehulp</h1>
                        <p>Wat een mooie stap dat u zich wilt oriënteren op uw ontwikkeling. We beginnen met drie korte
                           vragen, zodat we de keuzehulp kunnen afstemmen op uw situatie. Laten we snel van start gaan!</p>
                        <div class="btns-wrapper">
                           <div class="selecter-wrapper">
                              <div class="form-wrapper">
                                 <div class="row">
                                    <div class="form-field input-medium">
                                       <label class="gfield_label" for="input_4_21">Voornaam *</label>
                                       <div class="ginput_container ginput_container_text">
                                          <input name="Voornaam" id="input_4_21" type="text" value="<?php echo $voornaam; ?>" class="medium help-form-input" tabindex="1" aria-invalid="false">
                                       </div>
                                    </div>
                                    <div class="form-field input-small">
                                       <label class="gfield_label" for="input_4_22">Tussenv</label>
                                       <div class="ginput_container ginput_container_text">
                                          <input name="Tussenv" id="input_4_22" type="text" value="<?php echo $tussenv; ?>" class="medium help-form-input" tabindex="1" ria-invalid="false">
                                       </div>
                                    </div>
                                    <div class="form-field input-medium">
                                       <label class="gfield_label" for="input_4_23">Achternaam *</label>
                                       <div class="ginput_container ginput_container_text">
                                          <input name="Achternaam" id="input_4_23" type="text" value="<?php echo $achternaam; ?>" class="medium help-form-input" tabindex="1" aria-invalid="false">
                                       </div>
                                    </div>

                                 </div>
                                 <div class="row">
                                    <div class="form-field input-large">
                                       <label class="gfield_label" for="input_4_24">E-mailadres *</label>
                                       <div class="ginput_container ginput_container_text">
                                          <input name="E-mailadres" id="input_4_24" type="text" value="<?php echo $email; ?>" class="large help-form-input" tabindex="1" aria-invalid="false">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="form-field">
                                       <input name="Ik ontvang graag de keuzehulp" type="checkbox" value="Ja" class="form-field-checkbox" id="choice_4_11_1" tabindex="10" checked>
                                       <label for="choice_4_11_1" id="label_4_11_1">Ik ontvang graag de keuzehulp en
                                          wekelijks een passend trainingsaanbod</label>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="form-field">
                                       <input name="input_11.1" type="checkbox" value=""  class="form-field-checkbox" id="choice_4_11_2" tabindex="10">
                                       <label for="choice_4_11_2" id="choice_4_11_2">Ik heb het privacy statement gelezen en ga hier mee akkoord </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <button href="#" class="start btn-navigate btn-white" id="keuzehulp_btn" onClick="gotoSlide(event, 1)">Start</button>
                        </div>
                     </div>
                  </div>
                  <div class="keuzehulp-slide option-slide werkveld">
                     <div class="inner-content">
                        <div class="title-head">
                           <ul class="stepper">
                              <li class="active"><a href="#werkveld">Werkveld</a></li>
                              <li><a href="#leerdoel">Leerdoel</a></li>
                              <li><a href="#huidige-functie">Huidige functie</a></li>
                              <li><a href="#werkervaring">Werkervaring</a></li>
                           </ul>
                           <h2>In welk werkveld bent u geïnteresseerd?</h2>
                        </div>
                        <div class="selecter-wrapper">
                           <ul class="options-list">
                              <?php
                              foreach ($training_werkveld as $cat) :
                              ?>
                                 <li><span class="<?php echo $cat->slug; ?>" style="background-color:<?php the_field('werkveld_dot_color', $cat); ?>;"></span> <input type="checkbox" value="<?php echo $cat->slug; ?>" id="<?php echo $cat->slug; ?>" name="training_werkveld[]" /> <label for="<?php echo $cat->slug; ?>"><?php echo $cat->name ?></label></li>
                              <?php
                              endforeach;
                              ?>
                           </ul>
                        </div>
                        <div class="btns-wrapper">
                           <button type="button" onClick="gotoSlide(event, 0)" class="keuzehulp-prev btn-navigate btn-back"><i class="fas fa-arrow-left"></i></button>
                           <button type="button" onClick="gotoSlide(event, 2)" class="keuzehulp-next btn-navigate btn-orange">Volgende <i class="fas fa-arrow-right"></i></button>
                        </div>
                     </div>
                  </div>
                  <div class="keuzehulp-slide option-slide leerdoel">
                     <div class="inner-content">
                        <div class="title-head">
                           <ul class="stepper">
                              <li class="completed"><a href="#werkveld">Werkveld</a></li>
                              <li class="active"><a href="#leerdoel">Leerdoel</a></li>
                              <li><a href="#huidige-functie">Huidige functie</a></li>
                              <li><a href="#werkervaring">Werkervaring</a></li>
                           </ul>
                           <h2>Wat zijn uw leerdoelen?</h2>
                        </div>
                        <div class="selecter-wrapper">
                           <ul class="options-list">
                              <?php
                              foreach ($leerdoel as $cat) :
                              ?>
                                 <li><span style="background-color: #1AA09C;"></span> <input type="checkbox" value="<?php echo $cat->slug; ?>" id="<?php echo $cat->slug; ?>" name="training_leerdoel[]" /> <label for="<?php echo $cat->slug; ?>"><?php echo $cat->name ?></label></li>
                              <?php
                              endforeach;
                              ?>
                           </ul>
                        </div>
                        <div class="btns-wrapper">
                           <button type="button" onClick="gotoSlide(event, 1)" class="keuzehulp-prev btn-navigate btn-back"><i class="fas fa-arrow-left"></i></button>
                           <button type="button" onClick="gotoSlide(event, 3)" class="keuzehulp-next btn-navigate btn-orange">Volgende <i class="fas fa-arrow-right"></i></button>
                        </div>
                     </div>
                  </div>
                  <div class="keuzehulp-slide option-slide functie">
                     <div class="inner-content">
                        <div class="title-head">
                           <ul class="stepper">
                              <li class="completed"><a href="#werkveld">Werkveld</a></li>
                              <li class="completed"><a href="#leerdoel">Leerdoel</a></li>
                              <li class="active"><a href="#huidige-functie">Huidige functie</a></li>
                              <li class="completed"><a href="#werkervaring">Werkervaring</a></li>
                           </ul>
                           <h2>Wat is uw functie?</h2>
                        </div>
                        <div class="selecter-wrapper">
                           <ul class="options-list">
                              <?php
                              foreach ($training_functie as $cat) :
                              ?>
                                 <li><span style="background-color: #1AA09C;"></span> <input type="checkbox" value="<?php echo $cat->slug; ?>" id="<?php echo $cat->slug; ?>" name="training_huidige[]" /> <label for="<?php echo $cat->slug; ?>"><?php echo $cat->name ?></label></li>
                              <?php
                              endforeach;
                              ?>
                           </ul>
                        </div>
                        <div class="btns-wrapper">
                           <button type="button" onClick="gotoSlide(event, 2)" class="keuzehulp-prev btn-navigate btn-back"><i class="fas fa-arrow-left"></i></button>
                           <button type="button" onClick="gotoSlide(event, 4)" class="keuzehulp-next btn-navigate btn-orange">Volgende <i class="fas fa-arrow-right"></i></button>
                        </div>
                     </div>
                  </div>
                  <div class="keuzehulp-slide option-slide werkervaring">
                     <div class="inner-content">
                        <div class="title-head">
                           <ul class="stepper">
                              <li class="completed"><a href="#werkveld">Werkveld</a></li>
                              <li class="completed"><a href="#leerdoel">Leerdoel</a></li>
                              <li class="completed"><a href="#huidige-functie">Huidige functie</a></li>
                              <li class="active"><a href="#werkervaring">Werkervaring</a></li>
                           </ul>
                           <h2>Hoeveel jaar werkervaring</h2>
                        </div>
                        <div class="selecter-wrapper">
                           <ul class="options-list">
                              <?php
                              foreach ($werkervaring_niveau as $cat) :
                              ?>
                                 <li><input type="checkbox" value="<?php echo $cat->slug; ?>" id="<?php echo $cat->slug; ?>" name="training_werkervaring[]" /> <label for="<?php echo $cat->slug; ?>"><?php echo $cat->name ?></label></li>
                              <?php
                              endforeach;
                              ?>
                           </ul>
                        </div>
                        <div class="btns-wrapper">
                           <button type="button" onClick="gotoSlide(event, 3)" class="keuzehulp-prev btn-navigate btn-back"><i class="fas fa-arrow-left"></i></button>
                           <button type="submit" class="keuzehulp-next btn-navigate btn-orange" id="final_step">Bekijk de resultaten <i class="fas fa-arrow-right"></i></button>
                        </div>
                     </div>
                  </div>
                  <div class="keuzehulp-slide final">
                     <div class="inner-content">
                        <div class="icon-wrap"><i class="fas fa-paper-plane"></i></div>
                        <div class="lbl">Stimulansz Keuzehulp</div>
                        <h2>U vindt de Keuzehulp nu in uw mailbox</h2>
                        <p>We hopen dat er een training bij zit die aansluit bij uw wensen. Heeft u nog vragen? Onze
                           opleidingsadviseurs staan voor u klaar met hulp of advies.</p>
                        <a href="#">030 – 29 828 00</a> <br />
                        <a href="#">academie@stimulansz.nl</a>
                        <div class="btns-wrapper">
                           <a href="#" class="end btn-navigate btn-orange" onClick="gotoSlide(event, 0)">Naar homepage</a>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </section>
<?php
   return ob_get_clean();
}
