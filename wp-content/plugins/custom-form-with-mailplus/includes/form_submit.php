<?php
include_once('api/functions.php');

function add_cfwmplugin_scripts() {
   if(!is_admin()) {
       $path =  plugin_dir_url(__FILE__) . '../assets/';
       $script_data_array = array(
         'ajaxurl' => admin_url('admin-ajax.php'),
         'security' => wp_create_nonce('mailplus_submit'),
         'ajaxoverlay' => 'url('.plugin_dir_url(__FILE__) . '../assets/images/loading.svg'.') no-repeat center center'
      );
       wp_enqueue_style( 'plugin-css', $path . '/css/styles.css', array(), '1.1', 'all');
       
       wp_register_script('cfwm-script', $path . '/js/script.js', array ( 'jquery' ), 1.1, true);
       wp_localize_script('cfwm-script', 'cfwmAjax', $script_data_array);
       wp_enqueue_script('cfwm-script');
   }
 }

add_action( 'init', 'add_cfwmplugin_scripts', 9999 );

add_action('wp_ajax_submit_mailplus_form_ajax', 'submit_mailplus_form_ajax_callback');
add_action('wp_ajax_nopriv_submit_mailplus_form_ajax', 'submit_mailplus_form_ajax_callback');

function submit_mailplus_form_ajax_callback()
{
    check_ajax_referer('mailplus_submit', 'security');
    $posturl  = site_url();
    $mailplus = new mailplus_forms_api();
    
    $data = $_POST['data'];
    $postvars = '{
      "postUrl": "'.$posturl.'",
      "formParams": {
          "formEncId": [
              "aMkUMsywRL75kPBzhpCR"
          ],
          "field1812": [
            "'.$data["Voornaam"].'"
          ],
          "field1813": [
              "'.$data["Tussenv"].'"
          ],
          "field1814": [
            "'.$data["Achternaam"].'"
          ],
          "field1811": [
            "'.$data["E-mailadres"].'"
          ],
          "field1861": [
            "'.$data["Ik ontvang graag de keuzehulp"].'"
          ],
          "field1862": [
            "'.$data["Ik ga akkoord met het privacy statement"].'"
          ],
          "field1821": [
            "'.$data["training_werkveld"].'"
          ],
          "field1822": [
            "'.$data["training_leerdoel"].'"
          ],
          "field1823": [
            "'.$data["training_huidige"].'"
          ],
          "field1824": [
            "'.$data["training_werkervaring"].'"
          ]
      }
    }';
    // $postvars = '{
    //   "postUrl":"'.$posturl.'",
    //   "prefilledValues": {
    //       "Voornaam": [
    //           "'.$data["Voornaam"].'"
    //       ],
    //       "Tussenvoegsel": [
    //           "'.$data["Tussenv"].'"
    //       ],
    //       "Achternaam": [
    //         "'.$data["Achternaam"].'"
    //       ],
    //       "E-mailadres": [
    //         "'.$data["Achternaam"].'"
    //       ],
    //       "Ik ontvang graag de keuzehulp":[
    //         "'.$data["Ik ontvang graag de keuzehulp"].'"
    //       ],
    //       "Ik ga akkoord met het privacy statement":[
    //         "'.$data["Ik ga akkoord met het privacy statement"].'"
    //       ],
    //       "Werkveld":[
    //         "'.$data["training_werkveld"].'"
    //       ],
    //       "Leerdoel":[
    //         "'.$data["training_leerdoel"].'"
    //       ],
    //       "Huidige functie":[
    //         "'.$data["training_huidige"].'"
    //       ],
    //       "Werkervaring":[
    //         "'.$data["training_werkervaring"].'"
    //       ]
    //   }
    // }';
    $return = $mailplus->post_form( $postvars);
    $redirecturl = get_permalink( get_page_by_path( 'keuzehulp-resultaat' ) );
    $data['email'] = $data['E-mailadres'];  
    echo json_encode(array('status' => '1', 'message' => 'success', 'data' => $data, 'redirecturl' => $redirecturl ));

    wp_die();
}