<?php
add_action( 'admin_menu', 'cfwm_add_settings_page' );
add_action( 'admin_init', 'cfwm_hubspot_plugin_settings_page' );

function cfwm_add_settings_page(  ) {
    add_options_page( __('Mailplus Form Settings','cfwh'), __('Mailplus Form Settings','cfwh'), 'manage_options', 'cfwm-settings', 'cfwm_api_options_page' );
}

function cfwm_hubspot_plugin_settings_page(  ) {
    register_setting( 'stpPlugin', 'stp_api_settings' );
    add_settings_section(
        'stp_api_stpPlugin_section',
        '',
        '',
        'stpPlugin'
    );

    add_settings_field(
        'cfwh_api_key',
        __( 'Mailplus Key', 'cfwm' ),
        'cfwm_api_key_render',
        'stpPlugin',
        'stp_api_stpPlugin_section'
    );
    add_settings_field(
        'cfwm_api_secret',
        __( 'Mailplus Secret', 'cfwm' ),
        'cfwm_api_secret_render',
        'stpPlugin',
        'stp_api_stpPlugin_section'
    );
    add_settings_field(
        'cfwm_api_from_id',
        __( 'Mailplus Formid', 'cfwm' ),
        'cfwm_api_from_id_render',
        'stpPlugin',
        'stp_api_stpPlugin_section'
    );
}

function cfwm_api_key_render(  ) {
    $options = get_option( 'stp_api_settings' );
    ?>
    <input type='text' name='stp_api_settings[cfwm_api_key]' value='<?php echo $options['cfwm_api_key']; ?>'>
    <?php
}

function cfwm_api_secret_render(  ) {
    $options = get_option( 'stp_api_settings' );
    ?>
    <input type='text' name='stp_api_settings[cfwm_api_secret]' value='<?php echo $options['cfwm_api_secret']; ?>'>

<?php
}

function cfwm_api_from_id_render(  ) {
    $options = get_option( 'stp_api_settings' );
    ?>
    <input type='text' name='stp_api_settings[cfwm_api_from_id]' value='<?php echo $options['cfwm_api_from_id']; ?>'>

<?php
}



function cfwm_api_options_page(  ) {
    ?>
    <form action='options.php' method='post'>

    <h2><?php _e('Mailplus Form Settings','cfwm'); ?></h2>

    <div style="max-width: 600px;">
        <?php
            settings_fields( 'stpPlugin' );
            do_settings_sections( 'stpPlugin' );
        ?>
        <div style="max-width: 100px; margin-left: 200px;">
            <?php
                submit_button();
            ?>
        </div>
    </div>
    </form>
    <?php
}