<?php

namespace wpsolr\pro\extensions\theme_listify;

use WP_Query;
use wpsolr\core\classes\engines\solarium\WPSOLR_SearchSolariumClient;
use wpsolr\core\classes\engines\WPSOLR_AbstractIndexClient;
use wpsolr\core\classes\engines\WPSOLR_AbstractSearchClient;
use wpsolr\core\classes\extensions\localization\OptionLocalization;
use wpsolr\core\classes\extensions\WpSolrExtensions;
use wpsolr\core\classes\services\WPSOLR_Service_Container;
use wpsolr\core\classes\ui\WPSOLR_Data_Sort;
use wpsolr\core\classes\ui\WPSOLR_Query;
use wpsolr\core\classes\utilities\WPSOLR_Option;
use wpsolr\core\classes\WPSOLR_Events;
use wpsolr\core\classes\WpSolrSchema;
use wpsolr\pro\extensions\geolocation\WPSOLR_Option_GeoLocation;

/**
 * Class WPSOLR_Theme_Listify
 *
 * Manage Listify theme
 */
class WPSOLR_Theme_Listify extends WpSolrExtensions {
	const WPSOLR_GEOLOCATION = 'wpsolr_listify_geolocation'; // Concatenat lat and long fields
	const GEOLOCATION_LAT = 'geolocation_lat';
	const GEOLOCATION_LONG = 'geolocation_long';
	const GEOLOCATED = 'geolocated';
	const GEOLOCATION_CITY = 'geolocation_city';
	const GEOLOCATION_COUNTRY_LONG = 'geolocation_country_long';
	const GEOLOCATION_COUNTRY_SHORT = 'geolocation_country_short';
	const GEOLOCATION_FORMATTED_ADDRESS = 'geolocation_formatted_address';
	const GEOLOCATION_STATE_LONG = 'geolocation_state_long';
	const GEOLOCATION_STATE_SHORT = 'geolocation_state_short';
	const GEOLOCATION_STREET = 'geolocation_street';
	const GEOLOCATION_STREET_NUMBER = 'geolocation_street_number';
	const GEOLOCATION_POSTCODE = 'geolocation_postcode';
	const RATING = 'rating';
	const POST_TYPE_LISTIFY_JOB_LISTING = 'job_listing';

	/** @var bool $listify_feature_listings_in_location_search */
	protected $listify_feature_listings_in_location_search = false;

	/** @var bool $before_get_job_listings */
	protected $before_get_job_listings = false;

	/** @var bool $is_ajax */
	protected $is_ajax_processing = false;

	/** @var  WPSOLR_Theme_Listify_Filter_By_Distance $filter_by_distance */
	protected $filter_by_distance;

	/** @var bool bool $is_caching */
	protected $is_caching;

	/** @var  array $search_listings_args */
	protected $search_listings_args;

	/**
	 * @inheritDoc
	 */
	public function __construct() {

		$this->init_default_events();

		if ( is_admin() ) {

			// Activate geolocation type on indexing fields
			add_filter( WPSOLR_Events::WPSOLR_FILTER_SOLR_FIELD_TYPES, [
				WPSOLR_Option_GeoLocation::class,
				'wpsolr_filter_solr_field_types',
			], 10, 1 );

			add_filter( WPSOLR_Events::WPSOLR_FILTER_FIELDS, [
				$this,
				'wpsolr_filter_add_fields',
			], 10, 4 );

			add_filter( WPSOLR_Events::WPSOLR_FILTER_SOLARIUM_DOCUMENT_FOR_UPDATE, [
				$this,
				'wpsolr_filter_solarium_document_for_update',
			], 10, 5 );

		}

		if ( ! is_admin() && WPSOLR_Service_Container::getOption()->get_theme_listify_is_replace_search() ) {

			$this->is_caching = WPSOLR_Service_Container::getOption()->get_theme_listify_is_caching();

			add_filter( 'listify_filters_sort_by', [ $this, 'listify_filters_sort_by' ], 9, 1 );

			// Intercept filter before geolocation SQL in method Listify_WP_Job_Manager_Map::geolocation_search()
			add_filter( 'listify_feature_listings_in_location_search', [
				$this,
				'listify_feature_listings_in_location_search'
			], 9 );

			// Intercept filter before SQL in function get_job_listings()
			add_action( 'before_get_job_listings', [ $this, 'before_get_job_listings' ], 9, 2 );

			// Search parameters
			add_filter( 'job_manager_get_listings_args', [ $this, 'job_manager_get_listings_args' ], 9, 1 );

			add_filter( 'get_job_listings_cache_results', [ $this, 'get_job_listings_cache_results' ], 9, 1 );

			// Intercept get_products() in YITH_WCAS::ajax_search_products()
			add_filter( 'posts_pre_query', [ $this, 'query' ], 10, 2 );


			add_action( WPSOLR_Events::WPSOLR_ACTION_SOLARIUM_QUERY, [
				$this,
				'wpsolr_action_query',
			], 10, 1 );

		}

	}


	/**
	 * Replace Listify sort options with WPSOLR's
	 *
	 * Array
	 * (
	 *  [date-desc] => Le plus r&eacute;cent en premier
	 *  [date-asc] => Le plus ancien en premier
	 *  [random] => Random
	 *  [rating-desc] => Highest Rating
	 *  [rating-asc] => Lowest Rating
	 * )
	 *
	 *
	 * @param array $options
	 *
	 * @return mixed
	 */
	public function listify_filters_sort_by( $options ) {

		if ( ! WPSOLR_Service_Container::getOption()->get_theme_listify_is_replace_sort_options() ) {
			// Use standard WooCommerce sort items.
			return $options;
		}

		$results = [];

		// Retrieve WPSOLR sort fields, with their translations.
		$sorts = WPSOLR_Data_Sort::get_data(
			WPSOLR_Service_Container::getOption()->get_sortby_items_as_array(),
			WPSOLR_Service_Container::getOption()->get_sortby_items_labels(),
			WPSOLR_Service_Container::get_query()->get_wpsolr_sort(),
			OptionLocalization::get_options()
		);

		if ( ! empty( $sorts ) && ! empty( $sorts['items'] ) ) {
			foreach ( $sorts['items'] as $sort_item ) {
				$results[ $sort_item['id'] ] = $sort_item['name'];
			}
		}

		return $results;
	}

	/**
	 * Create geolocation field content from Listify lat and long fields
	 *
	 * @param array $document_for_update
	 * @param $solr_indexing_options
	 * @param $post
	 * @param $attachment_body
	 * @param WPSOLR_AbstractIndexClient $search_engine_client
	 *
	 * @return array Document updated with fields
	 */
	function wpsolr_filter_solarium_document_for_update( array $document_for_update, $solr_indexing_options, $post, $attachment_body, WPSOLR_AbstractIndexClient $search_engine_client ) {

		if ( ! empty( $document_for_update[ self::GEOLOCATION_LAT . WpSolrSchema::_SOLR_DYNAMIC_TYPE_S ] )
		     && ! empty( $document_for_update[ self::GEOLOCATION_LONG . WpSolrSchema::_SOLR_DYNAMIC_TYPE_S ] )
		) {
			$document_for_update[ self::WPSOLR_GEOLOCATION . WPSOLR_Option_GeoLocation::_SOLR_DYNAMIC_TYPE_LATITUDE_LONGITUDE ] =
				sprintf( '%s,%s',
					$document_for_update[ self::GEOLOCATION_LAT . WpSolrSchema::_SOLR_DYNAMIC_TYPE_S ][0],
					$document_for_update[ self::GEOLOCATION_LONG . WpSolrSchema::_SOLR_DYNAMIC_TYPE_S ][0]
				);

		}

		return $document_for_update;
	}

	/**
	 * Add field post capabilities
	 *
	 * @param array $fields
	 * @param WPSOLR_Query $wpsolr_query
	 * @param WPSOLR_AbstractSearchClient $search_engine_client
	 *
	 * @return array
	 */
	public
	function wpsolr_filter_add_fields(
		$fields, WPSOLR_Query $wpsolr_query, WPSOLR_AbstractSearchClient $search_engine_client
	) {
		$fields[] = self::WPSOLR_GEOLOCATION . WpSolrSchema::_SOLR_DYNAMIC_TYPE_STRING;

		return $fields;
	}

	/**
	 * @inheritdoc
	 */
	/*
	protected function get_default_sorts() {
		return [
			sprintf( '%s%s_%s', self::RATING, WpSolrSchema::_SOLR_DYNAMIC_TYPE_STRING, WPSOLR_SearchSolariumClient::SORT_ASC ),
			sprintf( '%s%s_%s', self::RATING, WpSolrSchema::_SOLR_DYNAMIC_TYPE_STRING, WPSOLR_SearchSolariumClient::SORT_DESC ),
		];
	}*/

	/**
	 * @inheritdoc
	 */
	protected function get_default_taxonomies() {
		return [ 'job_listing_region', 'job_listing_category', 'job_listing_type', 'job_listing_tag' ];
	}

	/**
	 * @inheritdoc
	 */
	protected function get_default_post_types() {
		return [ self::POST_TYPE_LISTIFY_JOB_LISTING ];
	}

	/**
	 * @inheritdoc
	 */
	protected function get_default_custom_fields() {

		return [
			self::WPSOLR_GEOLOCATION            => [
				WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_SOLR_TYPE               => WPSOLR_Option_GeoLocation::_SOLR_DYNAMIC_TYPE_LATITUDE_LONGITUDE,
				WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_CONVERSION_ERROR_ACTION => WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_CONVERSION_ERROR_ACTION_THROW_ERROR
			],
			self::GEOLOCATION_LAT               => [
				WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_SOLR_TYPE               => WpSolrSchema::_SOLR_DYNAMIC_TYPE_S,
				WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_CONVERSION_ERROR_ACTION => WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_CONVERSION_ERROR_ACTION_IGNORE_FIELD
			],
			self::GEOLOCATION_LONG              => [
				WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_SOLR_TYPE               => WpSolrSchema::_SOLR_DYNAMIC_TYPE_S,
				WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_CONVERSION_ERROR_ACTION => WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_CONVERSION_ERROR_ACTION_IGNORE_FIELD
			],
			self::GEOLOCATED                    => [
				WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_SOLR_TYPE               => WpSolrSchema::_SOLR_DYNAMIC_TYPE_S,
				WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_CONVERSION_ERROR_ACTION => WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_CONVERSION_ERROR_ACTION_IGNORE_FIELD
			],
			self::GEOLOCATION_CITY              => [
				WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_SOLR_TYPE               => WpSolrSchema::_SOLR_DYNAMIC_TYPE_S,
				WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_CONVERSION_ERROR_ACTION => WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_CONVERSION_ERROR_ACTION_IGNORE_FIELD
			],
			self::GEOLOCATION_COUNTRY_LONG      => [
				WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_SOLR_TYPE               => WpSolrSchema::_SOLR_DYNAMIC_TYPE_S,
				WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_CONVERSION_ERROR_ACTION => WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_CONVERSION_ERROR_ACTION_IGNORE_FIELD
			],
			self::GEOLOCATION_COUNTRY_SHORT     => [
				WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_SOLR_TYPE               => WpSolrSchema::_SOLR_DYNAMIC_TYPE_S,
				WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_CONVERSION_ERROR_ACTION => WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_CONVERSION_ERROR_ACTION_IGNORE_FIELD
			],
			self::GEOLOCATION_FORMATTED_ADDRESS => [
				WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_SOLR_TYPE               => WpSolrSchema::_SOLR_DYNAMIC_TYPE_S,
				WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_CONVERSION_ERROR_ACTION => WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_CONVERSION_ERROR_ACTION_IGNORE_FIELD
			],
			self::GEOLOCATION_STATE_LONG        => [
				WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_SOLR_TYPE               => WpSolrSchema::_SOLR_DYNAMIC_TYPE_S,
				WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_CONVERSION_ERROR_ACTION => WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_CONVERSION_ERROR_ACTION_IGNORE_FIELD
			],
			self::GEOLOCATION_STATE_SHORT       => [
				WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_SOLR_TYPE               => WpSolrSchema::_SOLR_DYNAMIC_TYPE_S,
				WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_CONVERSION_ERROR_ACTION => WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_CONVERSION_ERROR_ACTION_IGNORE_FIELD
			],
			self::GEOLOCATION_STREET            => [
				WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_SOLR_TYPE               => WpSolrSchema::_SOLR_DYNAMIC_TYPE_S,
				WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_CONVERSION_ERROR_ACTION => WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_CONVERSION_ERROR_ACTION_IGNORE_FIELD
			],
			self::GEOLOCATION_STREET_NUMBER     => [
				WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_SOLR_TYPE               => WpSolrSchema::_SOLR_DYNAMIC_TYPE_S,
				WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_CONVERSION_ERROR_ACTION => WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_CONVERSION_ERROR_ACTION_IGNORE_FIELD
			],
			self::GEOLOCATION_POSTCODE          => [
				WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_SOLR_TYPE               => WpSolrSchema::_SOLR_DYNAMIC_TYPE_S,
				WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_CONVERSION_ERROR_ACTION => WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_CONVERSION_ERROR_ACTION_IGNORE_FIELD
			],
			self::RATING                        => [
				WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_SOLR_TYPE               => WpSolrSchema::_SOLR_DYNAMIC_TYPE_FLOAT,
				WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_CONVERSION_ERROR_ACTION => WPSOLR_Option::OPTION_INDEX_CUSTOM_FIELD_PROPERTY_CONVERSION_ERROR_ACTION_IGNORE_FIELD
			],
		];

	}


	/**
	 * @param array $query_args
	 * @param array $args
	 */
	public function listify_feature_listings_in_location_search( $is_true ) {

		$this->listify_feature_listings_in_location_search = true;

		$this->filter_by_distance = WPSOLR_Theme_Listify_Filter_By_Distance::wpsolr_replace_wpdb( $this );

		return $is_true;
	}

	/**
	 * @param array $query_args
	 * @param array $args
	 */
	public function before_get_job_listings( $query_args, $args ) {

		$this->before_get_job_listings = true;
	}

	/**
	 * @param $is_cache
	 *
	 * @return bool
	 */
	public function get_job_listings_cache_results( $is_cache ) {

		// Use Listify cache system, or not
		return $this->is_caching;
	}

	/**
	 * Store search parameters
	 *
	 * @param array args
	 *
	 * @return bool
	 */
	public function job_manager_get_listings_args( $args ) {

		// Store
		$this->search_listings_args = $args;

		// Do nothing
		return $args;
	}

	/**
	 * Stop WordPress performing a DB query for its main loop.
	 *
	 * As of WordPress 4.6, it is possible to bypass the main WP_Query entirely.
	 * This saves us one unnecessary database query! :)
	 *
	 * @since 2.7.0
	 *
	 * @param  null $retval Current return value for filter.
	 * @param  WP_Query $query Current WordPress query object.
	 *
	 * @return null|array
	 */
	function query( $retval, $query ) {

		if ( $this->is_ajax_processing ) {
			// Recurse call, stop now.
			return $retval;
		}

		if ( ! $this->before_get_job_listings ) {
			// This is not a Listify filter.
			return $retval;
		}

		// To prevent recursive infinite calls
		$this->is_ajax_processing = true;
		//remove_filter( 'posts_pre_query', [ $this, 'query' ] );


		$wpsolr_query = new WPSOLR_Query(); // Potential recurse here
		$wpsolr_query->wpsolr_set_wp_query( $query );
		$wpsolr_query->query['post_type'] = ! isset( $query->query['post_type'] ) ? self::POST_TYPE_LISTIFY_JOB_LISTING : $query->query['post_type'];
		$wpsolr_query->query['s']         = ! isset( $this->search_listings_args['search_keywords'] ) ? '' : $this->search_listings_args['search_keywords'];
		$wpsolr_query->wpsolr_set_nb_results_by_page(
			! isset( $this->search_listings_args['posts_per_page'] ) ?
				WPSOLR_Service_Container::getOption()->get_search_max_nb_results_by_page()
				: $this->search_listings_args['posts_per_page']
		);
		$wpsolr_query->query_vars['paged'] = ( ! isset( $this->search_listings_args['offset'] ) ) ? '0' : 1 + (int)( $this->search_listings_args['offset'] / intval( $this->search_listings_args['posts_per_page'] ) );
		$this->add_sort( $wpsolr_query );
		$products = $wpsolr_query->get_posts();

		// To prevent recursive infinite calls
		$this->is_ajax_processing = false;

		// Return $results, which prevents standard $wp_query to execute it's SQL.
		$post_ids = array_column( $products, 'ID' );

		$query->post_count    = $wpsolr_query->post_count;
		$query->found_posts   = $wpsolr_query->found_posts;
		$query->max_num_pages = $wpsolr_query->max_num_pages;

		return $post_ids;
	}


	/**
	 * @param WPSOLR_Query $wpsolr_query
	 */
	protected function add_sort( WPSOLR_Query $wpsolr_query ) {

		/**
		 * Add sort
		 */

		$listify_search_sort = '';
		if ( isset( $_REQUEST['form_data'] ) ) {
			wp_parse_str( wp_unslash( $_REQUEST['form_data'] ), $params );

			if ( isset( $params['search_sort'] ) ) {
				$listify_search_sort = $params['search_sort'];
			}
		}

		if ( ! WPSOLR_Service_Container::getOption()->get_theme_listify_is_replace_sort_options() ) {

			$wpsolr_sort = '';
			if ( ! empty( $listify_search_sort ) ) {
				// Convert Listify sort to wpsolr sort

				switch ( $listify_search_sort ) {
					case 'date-asc':
						$wpsolr_sort = WPSOLR_SearchSolariumClient::SORT_CODE_BY_DATE_ASC;
						break;

					case 'date-desc':
						$wpsolr_sort = WPSOLR_SearchSolariumClient::SORT_CODE_BY_DATE_DESC;
						break;

					case 'random':
						$wpsolr_sort = WPSOLR_SearchSolariumClient::SORT_CODE_BY_DATE_ASC;
						break;

					case 'rating-desc':
						$wpsolr_sort = sprintf( '%s%s_%s', self::RATING, WpSolrSchema::_SOLR_DYNAMIC_TYPE_STRING, WPSOLR_SearchSolariumClient::SORT_DESC );
						break;

					case 'rating-asc':
						$wpsolr_sort = sprintf( '%s%s_%s', self::RATING, WpSolrSchema::_SOLR_DYNAMIC_TYPE_STRING, WPSOLR_SearchSolariumClient::SORT_ASC );
						break;

					default:
						// Relevancy first
						break;

				}
			}

		} else {

			// Plain wpsolr sort
			$wpsolr_sort = $listify_search_sort;
		}

		if ( ! empty( $wpsolr_sort ) ) {
			$wpsolr_query->set_wpsolr_sort( $wpsolr_sort );
		}

	}

	/**
	 *
	 * Add a filter on product post type.
	 *
	 * @param array $parameters
	 *
	 */
	public function wpsolr_action_query( $parameters ) {
		global $wpdb;

		/* @var WPSOLR_Query $wpsolr_query */
		$wpsolr_query = $parameters[ WPSOLR_Events::WPSOLR_ACTION_SOLARIUM_QUERY__PARAM_WPSOLR_QUERY ];
		/* @var WPSOLR_AbstractSearchClient $search_engine_client */
		$search_engine_client = $parameters[ WPSOLR_Events::WPSOLR_ACTION_SOLARIUM_QUERY__PARAM_SOLARIUM_CLIENT ];

		$wp_query = $wpsolr_query->wpsolr_get_wp_query();

		// post_type url parameter
		if ( ! empty( $wp_query->query['post_type'] ) ) {

			$search_engine_client->search_engine_client_add_filter_term( sprintf( 'WPSOLR_Theme_Listify type:%s', $wpsolr_query->query['post_type'] ), WpSolrSchema::_FIELD_NAME_TYPE, false, $wpsolr_query->query['post_type'] );
		}

		// taxonomy parameter
		if ( isset( $wp_query->query['tax_query'] ) && ! empty( $wp_query->query['tax_query'] ) ) {

			foreach ( $wp_query->query['tax_query'] as $tax_query ) {
				$field_name = $tax_query['taxonomy'] . WpSolrSchema::_SOLR_DYNAMIC_TYPE_STRING;

				$terms = $tax_query['terms'];
				if ( 'term_id' === $tax_query['field'] ) {
					$terms = get_terms( [
						'taxonomy' => $tax_query['taxonomy'],
						'include'  => $tax_query['terms'],
						'fields'   => 'names'
					] );
				}

				$search_engine_client->search_engine_client_add_filter_in_terms(
					sprintf( 'WPSOLR_Theme_Listify taxonomy %s', $tax_query['taxonomy'] ),
					$field_name,
					$terms
				);
			}
		}

		// Add geo distance filter
		// Test search_location to fix a Listify bug (does not clear lat and long when the location is cleared)
		if ( ! empty( $this->search_listings_args['search_location'] ) && is_object( $this->filter_by_distance ) ) {
			$search_engine_client->search_engine_client_add_filter_geolocation_distance(
				self::WPSOLR_GEOLOCATION . WPSOLR_Option_GeoLocation::_SOLR_DYNAMIC_TYPE_LATITUDE_LONGITUDE,
				$this->filter_by_distance->wpsolr_get_latitude(),
				$this->filter_by_distance->wpsolr_get_longitude(),
				$this->filter_by_distance->wpsolr_get_radius()
			);
		}

	}

}