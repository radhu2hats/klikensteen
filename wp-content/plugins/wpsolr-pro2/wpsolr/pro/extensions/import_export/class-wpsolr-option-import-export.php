<?php

namespace wpsolr\pro\extensions\import_export;

use wpsolr\core\classes\extensions\WpSolrExtensions;

/**
 * Class WPSOLR_Option_Import_Export
 * @package wpsolr\pro\extensions\import_export
 *
 * Import / Export WPSOLR PRO settings to / from a file.
 */
class WPSOLR_Option_Import_Export extends WpSolrExtensions {


	/**
	 * Constructor
	 * Subscribe to actions/filters
	 **/
	function __construct() {
	}

}
